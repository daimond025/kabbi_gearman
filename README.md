# Gearman workers

## Работа с воркерами
1. Перезапуск воркеров в среде разработки 
 - systemctl --user restart gearman_workers.target
    
## Тестирование
1. Создание билда тестов в среде разработки
    - php7.1 vendor/bin/codecept build
2. Запуск тестов в среде разработки    
    - php7.1 vendor/bin/codecept run 


