<?php

namespace api\controllers;

use gearman\JobFactory;
use gearman\jobs\BaseJob;
use healthCheck\CheckResult;
use healthCheck\checks\BaseCheck;
use healthCheck\checks\CustomCheck;
use healthCheck\checks\HttpServiceCheck;
use healthCheck\checks\MongoDBCheck;
use healthCheck\checks\MySqlCheck;
use healthCheck\checks\RedisCheck;
use healthCheck\exceptions\HealthCheckException;
use healthCheck\HealthCheckService;
use Ramsey\Uuid\Uuid;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\helpers\Html;
use yii\web\Application;
use yii\web\Controller;

/**
 * Class ApiController
 * @package api\controllers
 */
class ApiController extends Controller
{
    const SERVICE_OPERATIONAL = 'SERVICE OPERATIONAL';
    const SERVICE_NOT_OPERATIONAL = 'SERVICE NOT OPERATIONAL';

    const STATUS_OK = 'ok';
    const STATUS_ERROR = 'error';

    const STATUS_LABELS = [
        self::STATUS_OK    => '[ OK    ]',
        self::STATUS_ERROR => '[ ERROR ]',
    ];

    const MIME_TYPE_TEXT_PLAIN = 'text/plain';

    /**
     * @var Application
     */
    private $application;

    /**
     * @inheritdoc
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->application = \Yii::$app;
    }

    /**
     * @return string
     */
    private function getVersion()
    {
        $name    = $this->application->id;
        $version = $this->application->params['version'];

        return "{$name} v{$version}";
    }

    /**
     * @return string
     */
    private function getPHPVersion()
    {
        $php = PHP_VERSION;

        return "work on php v{$php}";
    }

    /**
     * @return bool
     */
    private function isTextFormatRequested()
    {
        return array_key_exists(self::MIME_TYPE_TEXT_PLAIN, $this->application->request->acceptableContentTypes);
    }

    /**
     * @return BaseCheck[]
     * @throws InvalidConfigException
     */
    private function getHealthChecks()
    {
        $application   = $this->application;
        $gearmanClient = $this->application->get('gearman');

        $checks = [
            new MySqlCheck('database "db"', $application->get('db')),
            new MySqlCheck('database "db_log"', $application->get('db_log')),
            new MongoDBCheck('database "mongodb"', $application->get('mongodb')),
            new RedisCheck('database "redis (cache)"', $application->get('redis_cache_check_status')),
            new HttpServiceCheck('api "paygate"', $application->params['paymentGateApi.url'] . 'version'),
            new HttpServiceCheck('api "service"', $application->params['pushApi.url'] . 'version'),
            new HttpServiceCheck('api "cashdesk"', $application->params['cashDeskApi.url'] . 'version'),
        ];

        foreach ([
                     JobFactory::TASK_BILLING,
                     JobFactory::TASK_EMAIL,
                     JobFactory::TASK_SMS,
                     JobFactory::TRACK_ORDER_STATISTIC,
                     JobFactory::TRACK_ORDER_TRACK,
                     JobFactory::TRACK_PARKING_ADD,
                 ] as $task) {
            $name     = 'worker "' . $task . '"';
            $checks[] = new CustomCheck($name, function () use ($task, $gearmanClient) {
                if (!$gearmanClient->doHigh($task, ['action' => BaseJob::ACTION_PING]) === BaseJob::RESULT_PING) {
                    throw new HealthCheckException('Received incorrect response from Gearman worker');
                }
            });
        }

        return $checks;
    }

    public function actionVersion()
    {
        $result = implode(PHP_EOL, [$this->getVersion(), $this->getPHPVersion()]);

        $requestId = $this->application->request->headers->get('Request-Id', Uuid::uuid4()->toString());
        applicationLog("[{$this->application->id}] requestId={$requestId} Getting version: \"{$result}\"");

        return $this->isTextFormatRequested()
            ? $result : Html::tag('pre', Html::encode($result),
                ['style' => 'word - wrap: break-word; white - space: pre - wrap']);
    }

    public function actionStatus()
    {
        $status = self::STATUS_OK;
        $lines  = [$this->getVersion(), $this->getPHPVersion(), ''];

        $checkResults = (new HealthCheckService($this->getHealthChecks()))->getCheckResults();
        foreach ($checkResults as $checkResult) {
            /* @var $checkResult CheckResult */
            if ($checkResult->isSuccessful()) {
                $lines[] = self::STATUS_LABELS[self::STATUS_OK] . " {$checkResult->getName()}";
            } else {
                $lines[] = self::STATUS_LABELS[self::STATUS_ERROR] . " {$checkResult->getName()} ({$checkResult->getErrorMessage()})";
                $status  = self::STATUS_ERROR;
            }
        }

        $lines[]       = '';
        $summaryStatus = $status === self::STATUS_OK ? self::SERVICE_OPERATIONAL : self::SERVICE_NOT_OPERATIONAL;
        $lines[]       = $summaryStatus;
        $result        = implode(PHP_EOL, $lines);

        $requestId = $this->application->request->headers->get('Request-Id', Uuid::uuid4()->toString());
        applicationLog("[{$this->application->id}] requestId={$requestId} Getting status: \"{$summaryStatus}\"");

        return $this->isTextFormatRequested()
            ? $result : Html::tag('pre', Html::encode($result),
                ['style' => 'word - wrap: break-word; white - space: pre - wrap']);
    }



    public function actionSend()
    {

        $MessageBird = new \MessageBird\Client('nMVTZi8iDOQBeNdUaYzADTbDC');
        $Message             = new \MessageBird\Objects\Message();
        $Message->originator = 'kabbi.com';
        $Message->recipients = array('447405748367');   // 393399957060   447441906544
        $Message->body       = 'KABBI test';
        $Message->datacoding = 'unicode';


        try {
            $MessageResult = $MessageBird->messages->create($Message);
            $date_recipients = (array)$MessageResult->recipients;


            var_dump($date_recipients);
            exit();

            if($date_recipients['totalDeliveryFailedCount'] === 0){
                return true;
            }
            return false;
        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            // That means that your accessKey is unknown
            echo 'wrong login';
            return false;
        } catch (\MessageBird\Exceptions\BalanceException $e) {
            // That means that you are out of credits, so do something about it.
            echo 'no balance';
            return false;
        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

}