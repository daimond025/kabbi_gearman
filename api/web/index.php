<?php

require __DIR__ . '/../../vendor/autoload.php';

$env = new Dotenv\Dotenv(dirname(dirname(__DIR__)));
$env->load();

//defined('YII_ENV') or define('YII_ENV', getenv('YII_ENV'));
//defined('YII_DEBUG') or define('YII_DEBUG', (bool)getenv('YII_DEBUG'));

require __DIR__ . '/../../console/include/functions.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../console/config/bootstrap.php';

$application = new yii\web\Application(require __DIR__ . '/../config/web.php');
$application->run();
