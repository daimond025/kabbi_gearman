<?php

namespace healthCheck;

use healthCheck\checks\BaseCheck;

/**
 * Class HealthCheckService
 * @package healthCheck
 */
class HealthCheckService
{
    /**
     * @var BaseCheck[]
     */
    private $checks;

    /**
     * HealthCheckService constructor.
     *
     * @param BaseCheck[] $checks
     */
    public function __construct(array $checks = [])
    {
        $this->checks = $checks;
    }

    /**
     * @return CheckResult[]
     */
    public function getCheckResults()
    {
        $result = [];
        foreach ($this->checks as $check) {
            try {
                $check->run();
                $result[] = new CheckResult($check->getName(), true);
            } catch (\Exception $ex) {
                $result[] = new CheckResult($check->getName(), false, $ex->getMessage());
            }
        }

        return $result;
    }
}