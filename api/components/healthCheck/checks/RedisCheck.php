<?php

namespace healthCheck\checks;

use Ramsey\Uuid\Uuid;
use healthCheck\exceptions\HealthCheckException;
use yii\redis\Connection;

/**
 * Class RedisCheck
 * @package healthCheck\checks
 */
class RedisCheck extends BaseCheck
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * RedisCheck constructor.
     *
     * @param string     $name
     * @param Connection $connection
     */
    public function __construct($name, Connection $connection)
    {
        $this->connection = $connection;

        parent::__construct($name);
    }

    public function run()
    {
        try {
            $key = Uuid::uuid4()->toString();

            $value = 'value';
            $this->connection->executeCommand('set', [$key, $value]);
            $actualValue = (string)$this->connection->executeCommand('get', [$key]);
            if ($value !== $actualValue) {
                throw new HealthCheckException('Incorrect actual value after insert');
            }

            $value = 'new value';
            $this->connection->executeCommand('set', [$key, $value]);

            $actualValue = (string)$this->connection->executeCommand('get', [$key]);
            if ($value !== $actualValue) {
                throw new HealthCheckException('Incorrect actual value after update');
            }

            $this->connection->executeCommand('del', [$key]);
            $actualValue = $this->connection->executeCommand('get', [$key]);
            if (!empty($actualValue)) {
                throw new HealthCheckException('Incorrect actual value after delete');
            }
        } catch (\Exception $ex) {
            throw new HealthCheckException(
                'Check Redis exception: error=' . $ex->getMessage() . ', code=' . $ex->getCode(), 0, $ex);
        }
    }
}