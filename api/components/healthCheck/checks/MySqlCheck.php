<?php

namespace healthCheck\checks;

use console\components\db\ReconnectTrait;
use Ramsey\Uuid\Uuid;
use healthCheck\exceptions\HealthCheckException;
use yii\db\Connection;

/**
 * Class MySqlCheck
 * @package healthCheck\checks
 */
class MySqlCheck extends BaseCheck
{
    use ReconnectTrait;
    /**
     * @var Connection
     */
    private $connection;

    /**
     * MySqlCheck constructor.
     *
     * @param string $name
     * @param Connection $connection
     */
    public function __construct($name, Connection $connection)
    {
        $this->connection = $connection;

        parent::__construct($name);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        self::reconnectIfNeeded($this->connection);
        $transaction = $this->connection->beginTransaction();
        try {
            $key = Uuid::uuid4()->toString();

            $value = 'value';
            $this->connection->createCommand(
                'INSERT INTO `tbl_check` (`key`, `value`) VALUES (:key, :value)',
                ['key' => $key, 'value' => $value])->execute();
            $actualValue = (string)$this->connection->createCommand('SELECT `value` FROM `tbl_check` WHERE `key` = :key',
                ['key' => $key])->queryScalar();
            if ($value !== $actualValue) {
                throw new HealthCheckException('Incorrect actual value after insert');
            }

            $value = 'new value';
            $this->connection->createCommand(
                'UPDATE `tbl_check` SET `value` = :value WHERE `key` = :key',
                ['key' => $key, 'value' => $value])->execute();
            $actualValue = (string)$this->connection->createCommand('SELECT `value` FROM `tbl_check` WHERE `key` = :key',
                ['key' => $key])->queryScalar();
            if ($value !== $actualValue) {
                throw new HealthCheckException('Incorrect actual value after update');
            }

            $this->connection->createCommand(
                'DELETE FROM `tbl_check` WHERE `key` = :key',
                ['key' => $key])->execute();
            $actualValue = (string)$this->connection->createCommand('SELECT `value` FROM `tbl_check` WHERE `key` = :key',
                ['key' => $key])->queryScalar();
            if (!empty($actualValue)) {
                throw new HealthCheckException('Incorrect actual value after delete');
            }

            $transaction->rollBack();
        } catch (\Exception $ex) {
            $transaction->rollBack();
            throw new HealthCheckException(
                'Check MySql exception: error=' . $ex->getMessage() . ', code=' . $ex->getCode(), 0, $ex);
        }
    }
}
