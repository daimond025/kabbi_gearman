<?php

namespace api\components\gearman;

use yii\base\Object;

/**
 * Class Client
 * @package app\components\gearman
 */
class Client extends Object
{
    public $host;
    public $port;

    /**
     * @var \GearmanClient
     */
    private $client;

    public function init()
    {
        $this->client = new \GearmanClient();
        $this->client->addServer($this->host, $this->port);

        parent::init();
    }

    /**
     * @param string $task
     * @param array  $params
     *
     * @return string
     */
    public function doHigh($task, array $params = [])
    {
        return $this->client->doHigh($task, json_encode($params));
    }

}
