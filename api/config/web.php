<?php

use yii\helpers\ArrayHelper;

return ArrayHelper::merge(require __DIR__ . '/../../console/config/base.php', [
    'id'                  => 'gearman_workers',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'          => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                'GET version' => 'api/version',
                'GET status'  => 'api/status',
            ],
        ],
        'gearman'    => [
            'class' => 'api\components\gearman\Client',
            'host'  => getenv('GEARMAN_MAIN_HOST'),
            'port'  => getenv('GEARMAN_MAIN_PORT'),
        ],
    ],
]);