<?php

namespace console\controllers;

use GuzzleHttp\Client;
use notification\cashdesk\ApiClient as CashdeskClient;
use notification\cashdesk\CashdeskService;
use notification\cashdesk\repositories\ReceiptRepository;
use notification\CashdeskConsumer;
use notification\CompleteTransactionProducer;
use notification\MQConnection;
use notification\NotificationLogger;
use notification\workerBalance\ApiClient;
use notification\workerBalance\WorkerBalanceService;
use notification\WorkerBalanceConsumer;
use yii\console\Controller;

/**
 * Class NotificationController
 * @package console\controllers
 */
class NotificationController extends Controller
{
    public function actionWorkerBalance()
    {
        $logger = $this->getLogger('worker-balance-consumer');

        $apiClient = new ApiClient($this->getHttpClient());
        $service = new WorkerBalanceService($apiClient, $logger);
        $consumer = new WorkerBalanceConsumer($this->getConnection(), $service, $logger);
        $consumer->start();
    }

    public function actionCashdesk()
    {
        $logger = $this->getLogger('cashdesk-consumer');

        $apiClient = new CashdeskClient($this->getHttpClient(), getenv('API_CASHDESK_URL'));
        $repository = new ReceiptRepository(\Yii::$app->getDb());
        $service = new CashdeskService($repository, $apiClient, $logger);
        $consumer = new CashdeskConsumer($this->getConnection(), $service, $logger);
        $consumer->start();
    }

    /**
     * @param int $transactionId
     */
    public function actionPublishCompleteTransactionNotification($transactionId)
    {
        $producer = new CompleteTransactionProducer($this->getConnection(),
            $this->getLogger('complete-transaction-producer'));
        $producer->notify($transactionId);
    }

    /**
     * @return MQConnection
     */
    private function getConnection()
    {
        return new MQConnection(
            \Yii::$app->params['rabbitMQ.host'],
            \Yii::$app->params['rabbitMQ.port'],
            \Yii::$app->params['rabbitMQ.user'],
            \Yii::$app->params['rabbitMQ.password'],
            \Yii::$app->params['rabbitMQ.virtualHost']);
    }

    /**
     * @param $class
     *
     * @return NotificationLogger
     */
    private function getLogger($class)
    {
        return new NotificationLogger($class);
    }

    /**
     * @return Client
     */
    private function getHttpClient()
    {
        return new Client(['timeout' => \Yii::$app->params['my.networkTimeout']]);
    }
}