<?php
/** 
 * Controller for start gearman worker
 * Example: php /var/www/yii2/yii worker/work Email &
 */
namespace console\controllers;

class WorkerController extends \yii\console\Controller
{
    public function actionWork($task)
    {
        \Yii::$app->gearman->working($task);
    }

}
