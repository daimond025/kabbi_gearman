<?php

namespace console\controllers;

use console\modules\sms\models\comtass\ComtassProvider;
use console\modules\sms\models\smsLine\SmsLine;
use console\modules\sms\models\smsLine\SmsLineProvider;
use console\modules\sms\models\TenantHasSms;
use console\modules\sms\components\SmsManager;
use yii\helpers\Console;

class TestController extends \yii\console\Controller
{

    /**
     * send sms
     * @param type $phone
     * @param type $text
     * @param type $server_id
     */
    public function actionSendSms($phone, $text = 'Test msg', $domain, $server_id = null)
    {
        $smsManager = new SmsManager();

        $tenant_id = (new \yii\db\Query)->from('tbl_tenant')->select('tenant_id')->where(['domain' => $domain])->scalar();

        $data['server'] = TenantHasSms::find()
                ->asArray()
                ->where(['tenant_id' => $tenant_id])
                ->andWhere(["active" => 1])
                ->andFilterWhere(['server_id' => $server_id])
                ->all();
        $data['tenant_id'] = $tenant_id;
        $data['phone'] = $phone;
        $data['text'] = $text;
        $result = $smsManager->sendSms($data);
        Console::output('Result is: ' . $result);
    }


    /**
     * get sms servers by domain name
     * @param string $domain
     *
     */
    public function actionGetServers($domain)
    {
        $servers = TenantHasSms::find()
                ->select(['tbl_sms_server.name', 'tbl_sms_server' . '.server_id'])
                ->innerJoin('tbl_tenant', 'tbl_tenant.tenant_id = ' . TenantHasSms::tableName() . '.tenant_id')
                ->where(['tbl_tenant.domain' => $domain])
                ->innerJoin('tbl_sms_server', 'tbl_sms_server.server_id = ' . TenantHasSms::tableName() . '.server_id')
                ->andWhere([TenantHasSms::tableName() . ".active" => 1])
                ->orderBy('tbl_sms_server.name')
//                ->createCommand(app()->db)->getRawSql();
                ->asArray()
                ->all();
        Console::output('Server list:');
        foreach ($servers as $server) {
            Console::output($server['name'] . ' ' . $server['server_id']);
        }
    }

    public function actionIndex()
    {
//        $provider = new ComtassProvider();
//        $resp = $provider->sendSms('teletaxi', 'taxi@tele', 'test 2', '249123034408', 'TeleTaxi');
//        $this->stdout((int)$resp);
        $this->stdout(PHP_EOL);

    }



}
