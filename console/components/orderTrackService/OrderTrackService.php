<?php

namespace orderTrackService;

use orderTrackService\exceptions\SavingOrderTrackException;
use orderTrackService\ar\mongodb\OrderTrack;

/**
 * Class OrderTrackService
 * @package app\models\order
 */
class OrderTrackService
{
    const COORDINATE_ACCURACY = 6;

    /**
     * Getting order track
     *
     * @param int $orderId
     *
     * @return array
     */
    public function getTrack($orderId)
    {
        $record = OrderTrack::find()
            ->where(['order_id' => $orderId])
            ->asArray()
            ->one();

        return empty($record['tracking']) ? [] : $record['tracking'];
    }

    /**
     * Setting order track
     *
     * @param int   $orderId
     * @param array $track
     *
     * @return bool
     * @throws SavingOrderTrackException
     */
    public function setTrack($orderId, $track)
    {
        $record = OrderTrack::find()
            ->where(['order_id' => $orderId])
            ->one();

        if (empty($record)) {
            $record = new OrderTrack(['order_id' => $orderId]);
        }

        $track = array_map(function ($item) {
            if (isset($item['lat'])) {
                $item['lat'] = (string)round($item['lat'], self::COORDINATE_ACCURACY);
            }

            if (isset($item['lat'])) {
                $item['lon'] = (string)round($item['lon'], self::COORDINATE_ACCURACY);
            }

            return $item;
        }, $track);

        $record->tracking = $track;
        $record->time     = time();

        if (!$record->save()) {
            throw new SavingOrderTrackException(implode('; ', $record->getFirstErrors()));
        }
    }
}