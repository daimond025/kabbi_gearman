<?php

namespace orderTrackService\ar\mongodb;

use yii\mongodb\ActiveRecord;

/**
 * Class OrderTrack
 * @package app\models\order\ar\mongodb
 *
 * @property int    _id
 * @property int    order_id
 * @property string tracking
 * @property int    time
 */
class OrderTrack extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'orderTrack';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['_id', 'order_id', 'tracking', 'time'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id'      => '_ID',
            'order_id' => 'Order ID',
            'tracking' => 'Tracking',
            'time'     => 'Time',
        ];
    }
}