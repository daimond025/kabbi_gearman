<?php

namespace orderTrackService\exceptions;

use yii\base\Exception;

/**
 * Class SavingOrderTrackException
 * @package orderTrackService\exceptions
 */
class SavingOrderTrackException extends Exception
{

}