<?php

namespace bonusSystem\models\ar;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%client_bonus}}".
 *
 * @property integer                $bonus_id
 * @property integer                $city_id
 * @property integer                $class_id
 * @property integer                $tenant_id
 * @property string                 $name
 * @property integer                $blocked
 * @property integer                $position_id
 * @property integer                $position_class_id
 * @property integer                $bonus_system_id
 * @property integer                $created_at
 * @property integer                $updated_at
 * @property string                 $actual_date
 * @property string                 $bonus_type
 * @property string                 $bonus
 * @property string                 $min_cost
 * @property string                 $payment_method
 * @property string                 $max_payment_type
 * @property string                 $max_payment
 * @property string                 $bonus_app_type
 * @property string                 $bonus_app
 *
 * @property BonusFailLog[]         $bonusFailLogs
 * @property BonusSystem            $bonusSystem
 * @property City                   $city
 * @property CarClass               $class
 * @property Position               $positionClass
 * @property Position               $position
 * @property Tenant                 $tenant
 * @property ClientBonusGootax      $clientBonusGootax
 * @property ClientBonusHasTariff[] $clientBonusHasTariffs
 * @property OrderDetailCost[]      $orderDetailCosts
 * @property OrderDetailCost[]      $orderDetailCosts0
 */
class ClientBonus extends ActiveRecord
{
    const BONUS_TYPE_PERCENT = 'PERCENT';
    const BONUS_TYPE_BONUS = 'BONUS';

    const PAYMENT_METHOD_FULL = 'FULL';
    const PAYMENT_METHOD_PARTIAL = 'PARTIAL';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_bonus}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'tenant_id', 'name', 'bonus'], 'required'],
            [
                [
                    'city_id',
                    'class_id',
                    'tenant_id',
                    'blocked',
                    'position_id',
                    'position_class_id',
                    'bonus_system_id',
                    'created_at',
                    'updated_at',
                ],
                'integer',
            ],
            [['actual_date', 'bonus_type', 'payment_method', 'max_payment_type', 'bonus_app_type'], 'string'],
            [['bonus', 'min_cost', 'max_payment', 'bonus_app'], 'number'],
            [['name'], 'string', 'max' => 255],
            [
                ['tenant_id', 'city_id', 'name'],
                'unique',
                'targetAttribute' => ['tenant_id', 'city_id', 'name'],
                'message'         => 'The combination of City ID, Tenant ID and Name has already been taken.',
            ],
            [
                ['bonus_system_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => BonusSystem::className(),
                'targetAttribute' => ['bonus_system_id' => 'id'],
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => City::className(),
                'targetAttribute' => ['city_id' => 'city_id'],
            ],
            [
                ['class_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CarClass::className(),
                'targetAttribute' => ['class_id' => 'class_id'],
            ],
            [
                ['position_class_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Position::className(),
                'targetAttribute' => ['position_class_id' => 'position_id'],
            ],
            [
                ['position_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Position::className(),
                'targetAttribute' => ['position_id' => 'position_id'],
            ],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_id'          => 'Bonus ID',
            'city_id'           => 'City ID',
            'class_id'          => 'Class ID',
            'tenant_id'         => 'Tenant ID',
            'name'              => 'Name',
            'blocked'           => 'Blocked',
            'position_id'       => 'Position ID',
            'position_class_id' => 'Position Class ID',
            'bonus_system_id'   => 'Bonus System ID',
            'created_at'        => 'Created At',
            'updated_at'        => 'Updated At',
            'actual_date'       => 'Actual Date',
            'bonus_type'        => 'Bonus Type',
            'bonus'             => 'Bonus',
            'min_cost'          => 'Min Cost',
            'payment_method'    => 'Payment Method',
            'max_payment_type'  => 'Max Payment Type',
            'max_payment'       => 'Max Payment',
            'bonus_app_type'    => 'Bonus App Type',
            'bonus_app'         => 'Bonus App',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusFailLogs()
    {
        return $this->hasMany(BonusFailLog::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusSystem()
    {
        return $this->hasOne(BonusSystem::className(), ['id' => 'bonus_system_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionClass()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBonusGootax()
    {
        return $this->hasOne(ClientBonusGootax::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBonusHasTariffs()
    {
        return $this->hasMany(ClientBonusHasTariff::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetailCosts()
    {
        return $this->hasMany(OrderDetailCost::className(), ['writeoff_bonus_id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetailCosts0()
    {
        return $this->hasMany(OrderDetailCost::className(), ['refill_bonus_id' => 'bonus_id']);
    }
}
