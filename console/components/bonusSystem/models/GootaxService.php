<?php

namespace bonusSystem\models;

use bonusSystem\BonusSystem;
use bonusSystem\exceptions\GetBalanceException;
use bonusSystem\exceptions\GetPaymentStrategyException;
use bonusSystem\exceptions\PaymentStrategyNotFoundException;
use bonusSystem\models\ar\Account;
use bonusSystem\models\ar\Client;
use bonusSystem\models\ar\ClientBonus;

/**
 * Class Gootax
 * @package bonusSystem
 */
class GootaxService
{
    /**
     * @param int $clientId
     * @param int $currencyId
     *
     * @return float
     * @throws GetBalanceException
     */
    public function getBalance($clientId, $currencyId)
    {
        try {
            $tenantId = Client::find()
                ->select('tenant_id')
                ->where(['client_id' => $clientId])
                ->scalar();

            $account = Account::find()
                ->where([
                    'acc_kind_id' => Account::ACCOUNT_KIND_ID_CLIENT_BONUS,
                    'acc_type_id' => Account::ACCOUNT_TYPE_ID_PASSIVE,
                    'tenant_id'   => $tenantId,
                    'owner_id'    => $clientId,
                    'currency_id' => $currencyId,
                ])
                ->one();

            return empty($account) ? 0 : $account->balance;
        } catch (\Exception $ex) {
            throw new GetBalanceException('An error occurred while getting user balance', 0, $ex);
        }
    }

    /**
     * Getting payment strategy
     *
     * @param int $tariffId
     *
     * @return PaymentStrategy
     * @throws GetPaymentStrategyException
     * @throws PaymentStrategyNotFoundException
     */
    public function getPaymentStrategy($tariffId)
    {
        try {
            $model = ClientBonus::find()
                ->select(['t.bonus_id', 'cg.payment_method', 'cg.max_payment_type', 'cg.max_payment'])
                ->alias('t')
                ->joinWith('clientBonusHasTariffs as ct')
                ->joinWith('clientBonusGootax as cg')
                ->where([
                    't.bonus_system_id' => BonusSystem::BONUS_SYSTEM_ID_GOOTAX,
                    'ct.tariff_id'      => $tariffId,
                    't.blocked'         => 0,
                ])
                ->orderBy(['t.bonus_id' => SORT_ASC])
                ->asArray()
                ->one();

            if (empty($model)) {
                throw new PaymentStrategyNotFoundException('Payment strategy no found by this client tariff');
            }

            return new PaymentStrategy(
                (int)$model['bonus_id'],
                (string)$model['payment_method'],
                (string)$model['max_payment_type'],
                (float)$model['max_payment']
            );
        } catch (PaymentStrategyNotFoundException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new GetPaymentStrategyException('An error occurred while getting payment strategy', 0, $ex);
        }
    }

}