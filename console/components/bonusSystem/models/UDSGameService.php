<?php

namespace bonusSystem\models;

use bonusSystem\BonusSystem;
use bonusSystem\exceptions\ClientIsNotRegisteredException;
use bonusSystem\exceptions\CreatePurchaseOperationException;
use bonusSystem\exceptions\GetBalanceException;
use bonusSystem\exceptions\GetCustomerException;
use bonusSystem\exceptions\GetPaymentStrategyException;
use bonusSystem\exceptions\InvalidClientTariffException;
use bonusSystem\exceptions\OrderIsNotRegisteredException;
use bonusSystem\exceptions\PaymentStrategyNotFoundException;
use bonusSystem\exceptions\RegisterClientException;
use bonusSystem\exceptions\RegisterOrderException;
use bonusSystem\models\ar\ClientBonus;
use bonusSystem\models\ar\Order;
use bonusSystem\models\ar\UDSGameClient;
use bonusSystem\models\ar\UDSGameOrder;
use bonusSystem\models\UDSGame\Purchase;
use bonusSystem\models\UDSGame\UDSGameApi;
use Ramsey\Uuid\Uuid;
use yii\base\Exception;

/**
 * Class UDSGame
 * @package bonusSystem
 */
class UDSGameService
{
    /**
     * @var UDSGameApi
     */
    private $api;

    /**
     * UDSGame constructor.
     *
     * @param UDSGameApi $api
     */
    public function __construct(UDSGameApi $api)
    {
        $this->api = $api;
    }

    /**
     * Is registered client
     *
     * @param int $clientId
     *
     * @return bool
     */
    public function isRegisteredClient($clientId)
    {
        $model = UDSGameClient::find()
            ->where(['client_id' => $clientId])
            ->one();

        return isset($model->profile_id);
    }

    /**
     * Is registered order
     *
     * @param int $orderId
     *
     * @return bool
     */
    public function isRegisteredOrder($orderId)
    {
        $model = UDSGameOrder::find()
            ->where(['order_id' => $orderId])
            ->one();

        return isset($model->order_id);
    }

    /**
     * Is registered order for write-off
     *
     * @param int $orderId
     *
     * @return bool
     */
    public function isRegisteredOrderForWriteOff($orderId)
    {
        $model = UDSGameOrder::find()
            ->where(['order_id' => $orderId])
            ->one();

        return !empty($model->promo_code);
    }

    /**
     * Order registration
     *
     * @param int    $orderId
     * @param string $promoCode
     *
     * @throws RegisterOrderException
     * @throws ClientIsNotRegisteredException
     * @throws GetCustomerException
     * @throws InvalidClientTariffException
     */
    public function registerOrder($orderId, $promoCode = null)
    {
        try {
            $order = Order::findOne($orderId);

            $this->getPaymentStrategy($order->tariff_id);

            $client = UDSGameClient::find()
                ->where(['client_id' => $order->client_id])
                ->one();
            if(!$client) {
                throw new ClientIsNotRegisteredException('Client is not registered');
            }

            $customerId = $client->profile_id;
            if ($promoCode !== null) {
                $customer   = $this->api->getCustomerByCode($promoCode);
                $customerId = $customer->getId();
            }

            $model = UDSGameOrder::find()
                ->where(['order_id' => $orderId])
                ->one();

            if (empty($model)) {
                $model = new UDSGameOrder();
            }

            $model->order_id    = $orderId;
            $model->customer_id = (string)$customerId;
            $model->promo_code  = (string)$promoCode;
            $model->request_id  = Uuid::uuid4()->toString();

            if (!$model->save()) {
                throw new Exception(implode(' ', $model->getFirstErrors()));
            }
        } catch (PaymentStrategyNotFoundException $ex) {
            throw new InvalidClientTariffException('Invalid client tariff', 0, $ex);
        } catch (GetCustomerException $ex) {
            throw $ex;
        } catch (ClientIsNotRegisteredException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new RegisterOrderException('An error occurred while register order', 0, $ex);
        }
    }

    /**
     * Register order payment
     *
     * @param int   $orderId
     * @param float $sum
     * @param float $bonus
     *
     * @throws CreatePurchaseOperationException
     * @throws OrderIsNotRegisteredException
     */
    public function registerOrderPayment($orderId, $sum, $bonus)
    {
        $model = UDSGameOrder::find()
            ->where(['order_id' => $orderId])
            ->one();

        if (empty($model)) {
            throw new OrderIsNotRegisteredException('Order is not registered');
        }

        if ($model->operation_id !== null) {
            return;
        }

        $customerId = $model->customer_id;
        $promoCode  = $model->promo_code;
        $requestId  = $model->request_id;

        $total  = round($sum, 2);
        $scores = round($bonus, 2);
        $cash   = $total - $scores;

        $purchase  = new Purchase($customerId, $promoCode, $orderId, $total, $cash, $scores);
        $operation = $this->api->createPurchaseOperation($purchase, $requestId);

        $model->operation_id = (string)$operation->getId();
        $model->save();
    }

    /**
     * Client registration
     *
     * @param int    $clientId
     * @param string $promoCode
     *
     * @throws RegisterClientException
     * @throws \bonusSystem\exceptions\GetCustomerException
     */
    public function registerClient($clientId, $promoCode)
    {
        try {
            $customer = $this->api->getCustomerByCode($promoCode);

            $model = UDSGameClient::find()
                ->where(['client_id' => $clientId])
                ->one();

            if (empty($model)) {
                $model = new UDSGameClient();
            }

            $model->client_id  = $clientId;
            $model->profile_id = $customer->getId();

            if (!$model->save()) {
                throw new Exception(implode(' ', $model->getFirstErrors()));
            }
        } catch (GetCustomerException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new RegisterClientException('An error occurred while register client', 0, $ex);
        }
    }

    /**
     * Getting balance
     *
     * @param int $clientId
     * @param int $currencyId
     *
     * @return float
     * @throws ClientIsNotRegisteredException
     * @throws GetBalanceException
     */
    public function getBalance($clientId, $currencyId)
    {
        try {
            $model = UDSGameClient::find()
                ->where(['client_id' => $clientId])
                ->one();

            if (empty($model)) {
                throw new ClientIsNotRegisteredException('Client is not registered');
            }

            $customer = $this->api->getCustomerById($model->profile_id);

            return $customer->getScores();
        } catch (ClientIsNotRegisteredException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new GetBalanceException('An error occurred while getting balance', 0, $ex);
        }
    }

    /**
     * Getting payment strategy
     *
     * @param int $tariffId
     *
     * @return PaymentStrategy
     * @throws PaymentStrategyNotFoundException
     * @throws GetPaymentStrategyException
     */
    public function getPaymentStrategy($tariffId)
    {
        try {
            $model = ClientBonus::find()
                ->select(['t.bonus_id'])
                ->alias('t')
                ->joinWith('clientBonusHasTariffs as ct')
                ->where([
                    't.bonus_system_id' => BonusSystem::BONUS_SYSTEM_ID_UDS_GAME,
                    'ct.tariff_id'      => $tariffId,
                    't.blocked'         => 0,
                ])
                ->orderBy(['t.bonus_id' => SORT_ASC])
                ->asArray()
                ->one();
            if (empty($model)) {
                throw new PaymentStrategyNotFoundException('Payment strategy no found by this client tariff');
            }

            return new PaymentStrategy(
                (int)$model['bonus_id'],
                ClientBonus::PAYMENT_METHOD_FULL,
                ClientBonus::BONUS_TYPE_BONUS,
                0.0
            );
        } catch (PaymentStrategyNotFoundException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new GetPaymentStrategyException('An error occurred while getting payment strategy', 0, $ex);
        }
    }

}