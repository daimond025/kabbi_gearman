<?php

namespace bonusSystem\models\UDSGame;

use bonusSystem\exceptions\CreatePurchaseOperationException;
use bonusSystem\exceptions\GetCompanyException;
use bonusSystem\exceptions\GetCustomerException;
use GuzzleHttp\Client;
use Ramsey\Uuid\Uuid;

/**
 * Class UDSGameApi
 * @package bonusSystem\models\UDSGame
 */
class UDSGameApi
{
    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * UDSGameApi constructor.
     *
     * @param string      $baseUrl
     * @param string      $apiKey
     * @param float|null  $connectionTimeout
     * @param float|null  $timeout
     * @param Client|null $httpClient
     */
    public function __construct(
        $baseUrl,
        $apiKey,
        $connectionTimeout = null,
        $timeout = null,
        Client $httpClient = null
    ) {
        $this->baseUrl           = $baseUrl;
        $this->apiKey            = $apiKey;
        $this->connectionTimeout = $connectionTimeout;
        $this->timeout           = $timeout;

        $this->httpClient = $httpClient === null
            ? new Client(['base_uri' => $baseUrl]) : $httpClient;
    }


    /**
     * Generate request headers
     *
     * @param string|null $requestId
     *
     * @return array
     */
    private function generateHeaders($requestId = null)
    {
        return [
            'X-Api-Key'           => $this->apiKey,
            'X-Timestamp'         => date(\DateTime::ISO8601),
            'X-Origin-Request-Id' => $requestId === null
                ? Uuid::uuid4()->toString() : $requestId,
        ];
    }

    /**
     * Getting company
     *
     * @return Company
     * @throws GetCompanyException
     */
    public function getCompany()
    {
        try {
            $response = $this->httpClient->request('GET', 'company', [
                'headers'            => $this->generateHeaders(),
                'connection_timeout' => $this->connectionTimeout,
                'timeout'            => $this->timeout,
            ]);

            $body = json_decode($response->getBody(), true);

            return new Company(
                (int)(isset($body['id']) ? $body['id'] : null),
                (string)(isset($body['name']) ? $body['name'] : null),
                (string)(isset($body['promoCode']) ? $body['promoCode'] : null),
                (string)(isset($body['baseDiscountPolicy']) ? $body['baseDiscountPolicy'] : null),
                new MarketingSettings(
                    (float)(isset($body['marketingSettings']['discountBase']) ? $body['marketingSettings']['discountBase'] : null),
                    (float)(isset($body['marketingSettings']['discountLevel1']) ? $body['marketingSettings']['discountLevel1'] : null),
                    (float)(isset($body['marketingSettings']['discountLevel2']) ? $body['marketingSettings']['discountLevel2'] : null),
                    (float)(isset($body['marketingSettings']['discountLevel3']) ? $body['marketingSettings']['discountLevel3'] : null),
                    (float)(isset($body['marketingSettings']['maxScoresDiscount']) ? $body['marketingSettings']['maxScoresDiscount'] : null)
                )
            );
        } catch (\Exception $ex) {
            throw new GetCompanyException('An error occurred while getting company', 0, $ex);
        }
    }

    /**
     * Getting customer
     *
     * @param int|null    $customerId
     * @param string|null $promoCode
     *
     * @return Customer
     * @throws GetCustomerException
     */
    private function getCustomer($customerId, $promoCode = null)
    {
        try {
            $response = $this->httpClient->request('GET', 'customer', [
                'query'              => [
                    'code'       => $promoCode,
                    'customerId' => $customerId,
                ],
                'headers'            => $this->generateHeaders(),
                'connection_timeout' => $this->connectionTimeout,
                'timeout'            => $this->timeout,
            ]);

            $body = json_decode($response->getBody(), true);

            return new Customer(
                (int)(isset($body['id']) ? $body['id'] : null),
                (string)(isset($body['name']) ? $body['name'] : null),
                (string)(isset($body['surname']) ? $body['surname'] : null),
                (string)(isset($body['phone']) ? $body['phone'] : null),
                (float)(isset($body['scores']) ? $body['scores'] : null)
            );
        } catch (\Exception $ex) {
            throw new GetCustomerException('An error occurred while getting customer', 0, $ex);
        }
    }

    /**
     * Getting customer by id
     *
     * @param int $customerId
     *
     * @return Customer
     * @throws GetCustomerException
     */
    public function getCustomerById($customerId)
    {
        return $this->getCustomer($customerId);
    }

    /**
     * Getting customer by promo code
     *
     * @param string $promoCode
     *
     * @return Customer
     * @throws GetCustomerException
     */
    public function getCustomerByCode($promoCode)
    {
        return $this->getCustomer(null, $promoCode);
    }

    /**
     * Create purchase operation
     *
     * @param Purchase $purchase
     * @param string   $requestId
     *
     * @return Operation
     * @throws CreatePurchaseOperationException
     */
    public function createPurchaseOperation($purchase, $requestId)
    {
        try {
            $response = $this->httpClient->request('POST', 'purchase', [
                'json'               => [
                    'customerId'    => $purchase->getCustomerId(),
                    'code'          => $purchase->getPromoCode(),
                    'total'         => $purchase->getTotal(),
                    'scores'        => $purchase->getScores(),
                    'cash'          => $purchase->getCash(),
                    'invoiceNumber' => $purchase->getOrderId(),
                ],
                'headers'            => $this->generateHeaders($requestId),
                'connection_timeout' => $this->connectionTimeout,
                'timeout'            => $this->timeout,
            ]);

            $body = json_decode($response->getBody(), true);

            return new Operation(
                (int)(isset($body['operation']['id']) ? $body['operation']['id'] : null),
                (float)(isset($body['operation']['scoresDelta']) ? $body['operation']['scoresDelta'] : null),
                (float)(isset($body['operation']['scoresNew']) ? $body['operation']['scoresNew'] : null),
                (float)(isset($body['operation']['cash']) ? $body['operation']['cash'] : null),
                (float)(isset($body['operation']['total']) ? $body['operation']['total'] : null)
            );
        } catch (\Exception $ex) {
            throw new CreatePurchaseOperationException(
                'An error occurred while creating purchase operation', 0, $ex);
        }
    }

}