<?php

namespace bonusSystem\models\UDSGame;

/**
 * Class Purchase
 * @package bonusSystem\models\UDSGame
 */
class Purchase
{
    /**
     * @var string
     */
    private $customerId;

    /**
     * @var string
     */
    private $promoCode;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var float
     */
    private $total;

    /**
     * @var float
     */
    private $cash;

    /**
     * @var float
     */
    private $scores;

    /**
     * Purchase constructor.
     *
     * @param string $customerId
     * @param string $promoCode
     * @param int    $orderId
     * @param float  $total
     * @param float  $cash
     * @param float  $scores
     */
    public function __construct($customerId, $promoCode, $orderId, $total, $cash, $scores)
    {
        $this->customerId = $customerId;
        $this->promoCode  = $promoCode;
        $this->orderId    = $orderId;
        $this->total      = $total;
        $this->cash       = $cash;
        $this->scores     = $scores;
    }

    /**
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function getPromoCode()
    {
        return $this->promoCode;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return float
     */
    public function getCash()
    {
        return $this->cash;
    }

    /**
     * @return float
     */
    public function getScores()
    {
        return $this->scores;
    }

}