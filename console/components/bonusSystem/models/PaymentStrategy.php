<?php

namespace bonusSystem\models;

/**
 * Class PaymentStrategy
 * @package bonusSystem\models
 */
class PaymentStrategy
{
    /**
     * @var int
     */
    private $bonusId;

    /**
     * @var string "FULL"|"PARTIAL"
     */
    private $paymentMethod;

    /**
     * @var string "BONUS"|"PERCENT"
     */
    private $maxPaymentType;

    /**
     * @var float
     */
    private $maxPayment;

    /**
     * PaymentStrategy constructor.
     *
     * @param int    $bonusId
     * @param string $paymentMethod
     * @param string $maxPaymentType
     * @param float  $maxPayment
     */
    public function __construct($bonusId, $paymentMethod, $maxPaymentType, $maxPayment)
    {
        $this->bonusId        = $bonusId;
        $this->paymentMethod  = $paymentMethod;
        $this->maxPaymentType = $maxPaymentType;
        $this->maxPayment     = $maxPayment;
    }

    /**
     * @return int
     */
    public function getBonusId()
    {
        return $this->bonusId;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @return string
     */
    public function getMaxPaymentType()
    {
        return $this->maxPaymentType;
    }

    /**
     * @return float
     */
    public function getMaxPayment()
    {
        return $this->maxPayment;
    }

}