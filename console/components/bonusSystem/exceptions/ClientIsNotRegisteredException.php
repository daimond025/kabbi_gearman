<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class ClientIsNotRegisteredException
 * @package bonusSystem\exceptions
 */
class ClientIsNotRegisteredException extends Exception
{

}