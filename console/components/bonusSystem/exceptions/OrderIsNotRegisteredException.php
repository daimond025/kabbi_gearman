<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class OrderIsNotRegisteredException
 * @package bonusSystem\exceptions
 */
class OrderIsNotRegisteredException extends Exception
{

}