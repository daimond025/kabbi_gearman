<?php

namespace promoCode\models;

use promoCode\exceptions\PromoBonusException;
use promoCode\models\ar\PromoBonusOperation;

/**
 * Class PromoBonusRepository
 * @package promoCode\models
 */
class PromoBonusRepository
{
    public function setPromoBonusCompleted(PromoBonus $promoBonus): void
    {
        $model = PromoBonusOperation::findOne($promoBonus->getId());

        $model->completed = 1;

        if (!$model->save()) {
            throw new PromoBonusException('Set promo bonus completed error: error="' . implode(';',
                    $model->getFirstErrors()) . '"');
        }
    }

    public function getPromoBonuses(int $orderId): array
    {
        $data = $this->getPromoBonusData($orderId);

        $models = [];
        foreach ($data ?? [] as $item) {
            /* @var $item PromoBonusOperation */
            $models[] = new PromoBonus(
                (int)$item->bonus_operation_id,
                $item->bonus_subject,
                (int)($item->bonus_subject === PromoBonus::SUBJECT_TYPE_CLIENT ? $item->client_id : $item->worker_id),
                (float)$item->promo_bonus);
        }

        return $models;
    }

    private function getPromoBonusData(int $orderId): array
    {
        return PromoBonusOperation::find()
            ->where(['order_id' => $orderId])
            ->all();
    }
}