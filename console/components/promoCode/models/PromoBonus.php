<?php

namespace promoCode\models;

/**
 * Class PromoBonus
 * @package promoCode\models
 */
class PromoBonus
{
    const SUBJECT_TYPE_CLIENT = 'client';
    const SUBJECT_TYPE_WORKER = 'worker';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $subjectType;

    /**
     * @var int
     */
    private $subjectId;

    /**
     * @var float
     */
    private $bonus;

    public function __construct(int $id, string $subjectType, int $subjectId, float $bonus)
    {
        $this->id = $id;
        $this->subjectType = $subjectType;
        $this->subjectId = $subjectId;
        $this->bonus = $bonus;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubjectType(): string
    {
        return $this->subjectType;
    }

    /**
     * @return int
     */
    public function getSubjectId(): int
    {
        return $this->subjectId;
    }

    /**
     * @return float
     */
    public function getBonus(): float
    {
        return $this->bonus;
    }

}