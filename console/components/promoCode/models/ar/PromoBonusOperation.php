<?php

namespace promoCode\models\ar;

/**
 * This is the model class for table "tbl_promo_bonus_operation".
 *
 * @property integer $bonus_operation_id
 * @property integer $order_id
 * @property integer $client_id
 * @property integer $worker_id
 * @property string  $bonus_subject
 * @property integer $completed
 * @property integer $promo_bonus
 * @property integer $created_at
 * @property integer $updated_at
 */
class PromoBonusOperation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_promo_bonus_operation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'bonus_subject'], 'required'],
            [['order_id', 'client_id', 'worker_id', 'completed', 'created_at', 'updated_at'], 'integer'],
            [['promo_bonus'], 'number'],
            [['bonus_subject'], 'string'],
        ];
    }

}
