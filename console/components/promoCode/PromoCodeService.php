<?php

namespace promoCode;

use promoCode\models\PromoBonus;
use promoCode\models\PromoBonusRepository;

/**
 * Class PromoCodeService
 * @package referralSystem
 */
class PromoCodeService
{
    /**
     * @var PromoBonusRepository
     */
    private $repository;

    public function __construct(PromoBonusRepository $repository)
    {
        $this->repository = $repository;
    }

    public static function getInstance(): PromoCodeService
    {
        $repository = new PromoBonusRepository();

        return new self($repository);
    }

    public function getPromoBonuses(int $orderId): array
    {
        return $this->repository->getPromoBonuses($orderId);
    }

    public function setPromoBonusCompleted(PromoBonus $promoBonus): void
    {
        $this->repository->setPromoBonusCompleted($promoBonus);
    }
}