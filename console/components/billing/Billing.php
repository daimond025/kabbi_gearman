<?php

namespace console\components\billing;

use console\components\ErrorHandler;
use Ramsey\Uuid\Uuid;
use console\components\billing\transactions\Transaction;

class Billing extends \yii\base\Object
{
    /**
     * Операция проведения транзакции.
     *
     * ~~~
     * Массив входных данных.
     *
     * Типы транзакции:
     * 1 - Пополнение;
     * 2 - Снятие;
     * 3 - Оплата за заказ.
     *
     * Типы оплаты:
     * 1 - CASH;
     * 2 - CARD;
     * 3 - PERSONAL_ACCOUNT;
     * 4 - CORP_BALANCE;
     * 5 - TERMINAL.
     * 6 - WITHDRAWAL
     *
     * Пример массива для транзакции типа 1 - Пополнение
     * $transactionData = [
     *      'type_id' => 1, //Тип транзакции
     *      'sender_acc_id' => 22, //ID счета отправителя
     *      'sum' => 100, //Cумма транзакции
     *      'payment_method' => 1, //Тип оплаты
     *      'comment' => 'Пополнение баланса' //Комментарий
     * ];
     *
     * Пример массива для транзакции типа 2 - Снятие
     * 'payment_method' для транзакции списания:
     * * 'WITHDRAWAL' - списание за что-либо
     * * 'WITHDRAWAL_FOR_TARIFF' - списание за тариф
     * * 'WITHDRAWAL_FOR_CASH_OUT' - обналичивание средств
     *
     * $transactionData = [
     *      'type_id' => 2, //Тип транзакции
     *      'sender_acc_id' => 22, //ID счета списания
     *      'sum' => 100, //Cумма транзакции
     *      'comment' => 'Выход на линию' //Комментарий
     *      'payment_method' => 'WITHDRAWAL' //Тип списания
     * ];
     *
     * Пример массива для транзакции типа 3 - Пополнение
     * $transactionData = ['order_id' => 18415];
     * ~~~
     *
     * @param array $transactionData Данные для транзакции.
     *
     * @return integer Код ответа
     * ~~~
     * 100 - SUCCESS_RESPONSE;
     * 120 - NOT_SUPPORTED_RESPONSE;
     * 140 - SYSTEM_ERROR_RESPONSE;
     * 160 - NO_MONEY_RESPONSE;
     * 180 - NOT_FOUND_RESPONSE;
     * 200 - HELD_PAYMENT_RESPONSE;
     * ~~~
     */

    public function createTransaction($transactionData)
    {
        app()->on(ErrorHandler::BEFORE_LOG_EXCEPTION_EVENT, function ($event) {
            app()->logger->export();
        });
        app()->logger->params = $this->getLoggerParams($transactionData);

        $result = Transaction::createTransaction($transactionData);

        app()->logger->export();

        return $result;
    }

    /**
     * Getting logger params
     *
     * @param array $transactionData
     *
     * @return array
     */
    private function getLoggerParams($transactionData)
    {
        $params = [];

        $params['uuid'] = empty($transactionData['request_id'])
            ? Uuid::uuid4() : $transactionData['request_id'];
        if (isset($transactionData['order_id'])) {
            $params['order_id'] = $transactionData['order_id'];
        }

        return $params;
    }
}
