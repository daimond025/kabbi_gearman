<?php

namespace console\components\billing;

use console\components\db\ReconnectTrait;
use Yii;

/**
 * This is the model class for table "{{%operation}}".
 *
 * @property integer       $operation_id
 * @property integer       $type_id
 * @property integer       $account_id
 * @property integer       $transaction_id
 * @property float         $sum
 * @property float         $saldo
 * @property string        $comment
 * @property string        $date
 * @property string        $owner_name
 *
 * @property Account       $account
 * @property Transaction   $transaction
 * @property OperationType $type
 */
class Operation extends \yii\db\ActiveRecord
{
    use ReconnectTrait;
    /**
     * Приход
     */
    const INCOME_TYPE = 1;
    /**
     * Расход
     */
    const EXPENSES_TYPE = 2;
    /**
     * Обналичивание
     */
    const CASH_OUT_TYPE = 3;

    /**
     * @var float Баланс счета операции
     */
    public $account_balance;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%operation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'account_id', 'transaction_id'], 'required'],
            [
                'sum',
                'number',
                'min' => -999999999.99,
                'max' => 999999999.99,
            ],
            [['owner_name', 'comment'], 'string', 'max' => 255],
            [['type_id', 'account_id', 'transaction_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'operation_id'   => 'Operation ID',
            'type_id'        => 'Type ID',
            'account_id'     => 'Account ID',
            'transaction_id' => 'Transaction ID',
            'sum'            => 'Sum',
            'saldo'          => 'Saldo',
            'date'           => 'Date',
            'comment'        => 'Comment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['transaction_id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OperationType::className(), ['type_id' => 'type_id']);
    }

    public function beforeSave($insert)
    {
        try {
            if ($this->type_id == self::INCOME_TYPE) {
                $balance = $this->account_balance + $this->sum;
            } elseif ($this->type_id == self::EXPENSES_TYPE || $this->type_id == self::CASH_OUT_TYPE) {
                $balance = $this->account_balance - $this->sum;
            }
            $this->saldo = $balance;
            self::reconnectIfNeeded(app()->db);
            $dbCommand   = app()->db->createCommand()->update(Account::tableName(), ['balance' => $balance],
                ['account_id' => $this->account_id]);
            $dbCommand->execute();
        } catch (\yii\db\Exception $exc) {
            Yii::error('Ошибка обновления баланса счета №' . $this->account_id . '. Тип операции: ' . $this->type_id . '. Ошибка ' . $exc->getMessage());

            return false;
        }

        return parent::beforeSave($insert);
    }
}
