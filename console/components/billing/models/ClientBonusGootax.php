<?php

namespace console\components\billing\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%client_bonus_gootax}}".
 *
 * @property integer     $id
 * @property integer     $bonus_id
 * @property string      $actual_date
 * @property string      $bonus_type
 * @property string      $bonus
 * @property string      $min_cost
 * @property string      $payment_method
 * @property string      $max_payment_type
 * @property string      $max_payment
 * @property string      $bonus_app_type
 * @property string      $bonus_app
 * @property integer     $created_at
 * @property integer     $updated_at
 *
 * @property ClientBonus $clientBonus
 */
class ClientBonusGootax extends ActiveRecord
{
    const BONUS_TYPE_PERCENT = 'PERCENT';
    const BONUS_TYPE_BONUS = 'BONUS';

    const PAYMENT_METHOD_FULL = 'FULL';
    const PAYMENT_METHOD_PARTIAL = 'PARTIAL';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_bonus_gootax}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus'], 'required'],
            [['bonus_id', 'created_at', 'updated_at'], 'integer'],
            [['actual_date', 'bonus_type', 'payment_method', 'max_payment_type', 'bonus_app_type'], 'string'],
            [['bonus', 'min_cost', 'max_payment', 'bonus_app'], 'number'],
            [['bonus_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'bonus_id'         => Yii::t('client-bonus', 'Bonus ID'),
            'actual_date'      => Yii::t('client-bonus', 'Actual Date'),
            'ActiveDate'       => Yii::t('client-bonus', 'Actual Date'),
            'bonus_type'       => Yii::t('client-bonus', 'Bonus Type'),
            'bonus'            => Yii::t('client-bonus', 'Points for one trip'),
            'min_cost'         => Yii::t('client-bonus', 'With the cost of the trip'),
            'payment_method'   => Yii::t('client-bonus', 'Possibility to pay by bonus'),
            'max_payment_type' => Yii::t('client-bonus', 'to'),
            'max_payment'      => Yii::t('client-bonus', 'Max Payment'),
            'bonus_app_type'   => Yii::t('client-bonus', 'Bonus App Type'),
            'bonus_app'        => Yii::t('client-bonus', 'The first trip through the app'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBonus()
    {
        return $this->hasOne(ClientBonus::className(), ['bonus_id' => 'bonus_id']);
    }

}
