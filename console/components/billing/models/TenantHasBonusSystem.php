<?php

namespace console\components\billing\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tenant_has_bonus_system}}".
 *
 * @property integer     $id
 * @property integer     $tenant_id
 * @property integer     $bonus_system_id
 * @property string      $api_key
 *
 * @property BonusSystem $bonusSystem
 * @property Tenant      $tenant
 */
class TenantHasBonusSystem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_bonus_system}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'bonus_system_id', 'api_key'], 'required'],
            [['tenant_id', 'bonus_system_id'], 'integer'],
            [['api_key'], 'string', 'max' => 255],
            [['tenant_id'], 'unique'],
            [
                ['bonus_system_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => BonusSystem::className(),
                'targetAttribute' => ['bonus_system_id' => 'id'],
            ],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'tenant_id'       => 'Tenant ID',
            'bonus_system_id' => 'Bonus System ID',
            'bonus_system'    => 'Bonus system',
            'api_key'         => 'Api Key',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusSystem()
    {
        return $this->hasOne(BonusSystem::className(), ['id' => 'bonus_system_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}
