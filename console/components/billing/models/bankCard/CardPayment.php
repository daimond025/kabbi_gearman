<?php

namespace console\components\billing\models\bankCard;

use Yii;
use console\components\billing\models\Order;

/**
 * This is the model class for table "{{%card_payment}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string  $pan
 * @property string  $to_pan
 * @property float   $payment_sum
 * @property string  $payment_order_id
 * @property string  $payment_status
 * @property string  $created_at
 * @property string  $updated_at
 *
 * @property Order   $order
 */
class CardPayment extends \yii\db\ActiveRecord
{

    const PAYMENT_STATUS_PAID = 'PAID';
    const PAYMENT_STATUS_REFUNDED = 'REFUNDED';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%card_payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'pan'], 'required'],
            [['order_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['payment_sum'], 'number'],
            [['pan'], 'string', 'max' => 25],
            [['payment_order_id'], 'string', 'max' => 36],
            ['payment_status', 'string'],
            [['to_pan'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'order_id'         => 'Order ID',
            'pan'              => 'Pan',
            'to_pan'           => 'To Pan',
            'payment_sum'      => 'Payment Sum',
            'payment_order_id' => 'Payment Order ID',
            'payment_status'   => 'Payment Status',
            'created_at'       => 'Created At',
            'updated_at'       => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
