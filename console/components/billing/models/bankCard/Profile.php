<?php

namespace console\components\billing\models\bankCard;

/**
 * Profile of payment gate
 */
class Profile extends \yii\base\Object
{
    const BASE_URL = 'v0/profiles';

    private $url;

    /**
     * @var string Uuid4
     */
    public $requestId;

    public function init()
    {
        $this->url = rtrim(app()->params['paymentGateApi.url'], '/');

        parent::init();
    }

    /**
     * Getting headers for request
     * @return array
     */
    private function getRequestHeaders()
    {
        $headers = [];
        if (!empty($this->requestId)) {
            $headers['Request-Id'] = $this->requestId;
        }

        return $headers;
    }

    /**
     * Get profile of payment gate
     *
     * @param integer $tenantId
     *
     * @return array|null
     * @throw \console\components\curl\exceptions\RestException
     */
    public function get($tenantId)
    {
        return app()->paygateCurl->get(implode("/", [
            $this->url,
            static::BASE_URL,
            $tenantId,
        ]), $this->getRequestHeaders());
    }
}
