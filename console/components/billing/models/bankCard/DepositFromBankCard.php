<?php

namespace console\components\billing\models\bankCard;

use console\components\billing\transactions\Transaction;

/**
 * This is the model class for table "{{%deposit_from_bank_card}}".
 *
 * @property integer     $id
 * @property string      $request_id
 * @property string      $pan
 * @property integer     $transaction_id
 * @property string      $payment_order_id
 *
 * @property Transaction $transaction
 */
class DepositFromBankCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%deposit_from_bank_card}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id', 'pan', 'transaction_id', 'payment_order_id'], 'required'],
            [['transaction_id'], 'integer'],
            [['request_id', 'pan', 'payment_order_id'], 'string', 'max' => 255],
            [['request_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'request_id'       => 'Request ID',
            'pan'              => 'Pan',
            'transaction_id'   => 'Transaction ID',
            'payment_order_id' => 'Payment Order ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['transaction_id' => 'transaction_id']);
    }
}
