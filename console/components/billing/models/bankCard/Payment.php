<?php

namespace console\components\billing\models\bankCard;

use Yii;
use console\components\curl\exceptions\RestException;

/**
 * Model of payment
 */
class Payment extends \yii\base\Object
{

    const BASE_URL = 'v0/payments';
    const ACTION_PAY = 'PAY';
    const ACTION_REFUND = 'REFUND';

    private $url;

    /**
     * @var string Uuid4
     */
    public $requestId;

    public function init()
    {
        $this->url = rtrim(app()->params['paymentGateApi.url'], '/');

        parent::init();
    }

    /**
     * Getting headers for request
     * @return array
     */
    private function getRequestHeaders()
    {
        $headers = [];
        if (!empty($this->requestId)) {
            $headers['Request-Id'] = $this->requestId;
        }

        return $headers;
    }

    /**
     * Pay for order
     *
     * @param integer $tenantId
     * @param string  $profile
     * @param integer $orderId
     * @param integer $clientId
     * @param integer $workerId
     * @param string  $pan
     * @param string  $toPan
     * @param integer $amount
     * @param integer $orderNumber
     * @param integer $currency
     * @param array   $params
     * @param string  $description
     *
     * @return array
     * @throws \yii\base\Exception
     * @throws RestException
     * @throws \Exception
     */
    public function pay(
        $tenantId,
        $profile,
        $orderId,
        $clientId,
        $workerId,
        $pan,
        $toPan,
        $amount,
        $orderNumber,
        $currency,
        $params = null,
        $description = null
    ) {
        $client = !empty($clientId) ? "client_{$clientId}" : "worker_{$workerId}";
        $url    = implode('/', [$this->url, static::BASE_URL, $profile, $client]);

        $postParams = [
            'pan'         => $pan,
            'amount'      => $amount,
            'orderNumber' => $orderNumber,
            'currency'    => $currency,
            'params'      => $params,
            'description' => $description,
        ];

        if ($toPan !== null) {
            $postParams['destination'] = $toPan;
        }

        try {
            $response = app()->paygateCurl->post($url, $postParams, $this->getRequestHeaders());

            CardPaymentLog::log($tenantId, $profile, $orderId, $clientId, $workerId,
                CardPaymentLog::TYPE_PAY, $postParams, $response, CardPaymentLog::RESULT_OK);

            return $response;
        } catch (RestException $ex) {
            $response  = [
                'message'  => $ex->getMessage(),
                'code'     => $ex->getCode(),
                'response' => $ex->getResponse(),
            ];
            $exception = $ex;
        } catch (\Exception $ex) {
            $response  = [
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode(),
            ];
            $exception = $ex;
        }

        CardPaymentLog::log($tenantId, $profile, $orderId, $clientId, $workerId,
            CardPaymentLog::TYPE_PAY, $postParams, $response, CardPaymentLog::RESULT_ERROR);

        $message = implode("\n",
            ['Error occurred payment', "Url: {$url}", print_r($postParams, true), print_r($response, true)]);
        Yii::error($message, 'bank-card');

        throw $exception;
    }

    /**
     * Refund payment
     *
     * @param integer $tenantId
     * @param string  $profile
     * @param integer $orderId
     * @param integer $clientId
     * @param integer $workerId
     * @param string  $paymentOrderId
     *
     * @return mixed
     * @throws RestException
     * @throws \Exception
     */
    public function refund($tenantId, $profile, $orderId, $clientId, $workerId, $paymentOrderId)
    {
        $client = empty($workerId) ? "client_{$clientId}" : "worker_{$workerId}";
        $url    = implode('/', [
            $this->url,
            static::BASE_URL,
            $profile,
            $client,
            $paymentOrderId,
        ]);

        try {
            $response = app()->paygateCurl->post($url, [
                'empty' => 'empty',
            ], $this->getRequestHeaders());
            $result   = CardPaymentLog::RESULT_OK;
        } catch (RestException $ex) {
            $response  = [
                'message'  => $ex->getMessage(),
                'code'     => $ex->getCode(),
                'response' => $ex->getResponse(),
            ];
            $exception = $ex;
            $result    = CardPaymentLog::RESULT_ERROR;
        } catch (\Exception $ex) {
            $response  = [
                'message' => $ex->getMessage(),
                'code'    => $ex->getCode(),
            ];
            $exception = $ex;
            $result    = CardPaymentLog::RESULT_ERROR;
        }

        CardPaymentLog::log($tenantId, $profile, $orderId, $clientId, $workerId,
            CardPaymentLog::TYPE_REFUND, compact('paymentOrderId'), $response, $result);

        if ($result == CardPaymentLog::RESULT_OK) {
            return $response;
        }

        $message = implode("\n", [
            'Error occurred refund payment',
            "Url: {$url}",
            compact('paymentOrderId'),
            print_r($response, true),
        ]);
        Yii::error($message, 'bank-card');

        throw $exception;
    }
}
