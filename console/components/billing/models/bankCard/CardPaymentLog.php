<?php

namespace console\components\billing\models\bankCard;

use console\components\db\ReconnectTrait;
use Yii;
use console\components\billing\models\Order;

/**
 * This is the model class for table "{{%card_payment_log}}".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property string  $profile
 * @property integer $client_id
 * @property integer $worker_id
 * @property integer $order_id
 * @property string  $type
 * @property string  $params
 * @property string  $response
 * @property integer $result
 * @property string  $created_at
 * @property string  $updated_at
 *
 * @property Order   $order
 */
class CardPaymentLog extends \yii\db\ActiveRecord
{
    use ReconnectTrait;

    const TYPE_PAY = 'PAY';
    const TYPE_REFUND = 'REFUND';
    const TYPE_ADD_CARD = 'ADD CARD';
    const TYPE_CHECK_CARD = 'CHECK CARD';
    const TYPE_REMOVE_CARD = 'REMOVE CARD';

    const RESULT_OK = 1;
    const RESULT_ERROR = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%card_payment_log}}';
    }

    /**
     * @return mixed|\yii\db\Connection
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public static function getDb()
    {
        self::reconnectIfNeeded(Yii::$app->db_log);

        return Yii::$app->db_log;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'type', 'result'], 'required'],
            [['tenant_id', 'client_id', 'worker_id', 'order_id', 'result'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['profile', 'type', 'params', 'response'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'tenant_id'  => 'Tenant ID',
            'profile'    => 'Profile',
            'client_id'  => 'Client ID',
            'worker_id'  => 'Worker ID',
            'order_id'   => 'Order ID',
            'type'       => 'Type',
            'params'     => 'Params',
            'response'   => 'Response',
            'result'     => 'Result',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * Added result of the payment gate operation to log
     *
     * @param integer $tenantId
     * @param string  $profile
     * @param integer $orderId
     * @param integer $clientId
     * @param integer $workerId
     * @param string  $type
     * @param array   $params
     * @param array   $response
     * @param integer $result
     *
     * @throws \yii\base\Exception
     */
    public static function log(
        $tenantId,
        $profile,
        $orderId,
        $clientId,
        $workerId,
        $type,
        $params,
        $response,
        $result
    ) {
        self::reconnectIfNeeded(app()->db_log);
        $transaction = app()->db_log->beginTransaction();
        try {
            $model = new static([
                'tenant_id' => $tenantId,
                'profile'   => $profile,
                'order_id'  => $orderId,
                'client_id' => $clientId,
                'worker_id' => $workerId,
                'type'      => $type,
                'params'    => json_encode($params,
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                'response'  => json_encode($response,
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                'result'    => $result,
            ]);

            if ($model->save()) {
                $transaction->commit();

                app()->logger->log("Card payment log saved");

                return;
            } else {
                $transaction->rollBack();
                $message = implode("\n", $model->getFirstErrors());
                app()->logger->log("Card payment log is not saved: " . implode("\n", $model->getFirstErrors()));
            }
        } catch (\Exception $ex) {
            $transaction->rollBack();
            $message = $ex->getMessage();
            app()->logger->log("Card payment log is not saved: " . $ex);
        }

        Yii::error(implode("\n", [
            "Error occurred saving payment information to log.",
            print_r($model->toArray(), true),
            $message,
        ]), "bank-card");
    }
}
