<?php

namespace console\components\billing\models;

use Yii;
use console\components\billing\models\Currency;
use console\components\billing\models\bankCard\CardPayment;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer         $order_id
 * @property integer         $tenant_id
 * @property integer         $car_id
 * @property integer         $city_id
 * @property integer         $tariff_id
 * @property integer         $user_create
 * @property integer         $status_id
 * @property integer         $user_modifed
 * @property integer         $company_id
 * @property integer         $parking_id
 * @property string          $address
 * @property string          $comment
 * @property string          $predv_price
 * @property string          $device
 * @property integer         $order_number
 * @property string          $payment
 * @property integer         $show_phone
 * @property integer         $create_time
 * @property integer         $status_time
 * @property integer         $time_to_client
 * @property string          $client_device_token
 * @property integer         $order_time
 * @property string          $predv_distance
 * @property integer         $predv_time
 * @property integer         $call_warning_id
 * @property string          $phone
 * @property integer         $client_id
 * @property integer         $currency_id
 * @property integer         $worker_id
 *
 * @property OrderDetailCost $orderDetailCost
 * @property Currency        $currency
 * @property Worker          $worker
 * @property Client          $client
 * @property CardPayment     $cardPayment
 */
class Order extends \yii\db\ActiveRecord
{

    const DEVICE_IOS = 'IOS';
    const DEVICE_ANDROID = 'ANDROID';
    const DEVICE_WINPHONE = 'WINFON';
    const STATUS_ID_COMPLETE_PAID = 37;
    const STATUS_ID_COMPLETE_NOT_PAID = 38;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['tenant_id', 'tariff_id', 'status_id', 'address', 'order_number', 'status_time', 'client_id'],
                'required',
            ],
            [
                [
                    'tenant_id',
                    'car_id',
                    'city_id',
                    'tariff_id',
                    'user_create',
                    'status_id',
                    'user_modifed',
                    'company_id',
                    'parking_id',
                    'order_number',
                    'show_phone',
                    'create_time',
                    'status_time',
                    'time_to_client',
                    'order_time',
                    'predv_time',
                    'call_warning_id',
                    'client_id',
                    'worker_id',
                ],
                'integer',
            ],
            [['address', 'comment', 'device', 'payment', 'client_device_token'], 'string'],
            [['predv_price', 'predv_distance'], 'number'],
            [['phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'            => 'Order ID',
            'tenant_id'           => 'Tenant ID',
            'car_id'              => 'Car ID',
            'city_id'             => 'City ID',
            'tariff_id'           => 'Tariff ID',
            'user_create'         => 'User Create',
            'status_id'           => 'Status ID',
            'user_modifed'        => 'User Modifed',
            'company_id'          => 'Company ID',
            'parking_id'          => 'Parking ID',
            'address'             => 'Address',
            'comment'             => 'Comment',
            'predv_price'         => 'Predv Price',
            'device'              => 'Device',
            'order_number'        => 'Order Number',
            'payment'             => 'Payment',
            'show_phone'          => 'Show Phone',
            'create_time'         => 'Create Time',
            'status_time'         => 'Status Time',
            'time_to_client'      => 'Time To Client',
            'client_device_token' => 'Client Device Token',
            'order_time'          => 'Order Time',
            'predv_distance'      => 'Predv Distance',
            'predv_time'          => 'Predv Time',
            'call_warning_id'     => 'Call Warning ID',
            'phone'               => 'Phone',
            'client_id'           => 'Client ID',
            'currency_id'         => 'Currency ID',
            'worker_id'           => 'Worker ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetailCost()
    {
        return $this->hasOne(OrderDetailCost::className(), ['order_id' => 'order_id'])
            ->orderBy(['detail_id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardPayment()
    {
        return $this->hasOne(CardPayment::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

}