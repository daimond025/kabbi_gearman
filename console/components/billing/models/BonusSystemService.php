<?php

namespace console\components\billing\models;

use yii\base\Object;
use yii\db\Query;

/**
 * Class BonusSystemService
 * @package console\components\billing\models
 */
class BonusSystemService extends Object
{
    const BONUS_SYSTEM_ID_GOOTAX = 1;
    const BONUS_SYSTEM_ID_UDS_GAME = 2;

    /**
     * Getting bonus system id
     *
     * @param int $tenantId
     *
     * @return int
     */
    public function getBonusSystemId($tenantId)
    {
        $bonusSystemId = (new Query())
            ->select('bonus_system_id')
            ->from('tbl_tenant_has_bonus_system')
            ->where([
                'tenant_id' => $tenantId,
            ])
            ->scalar();

        return empty($bonusSystemId) ? self::BONUS_SYSTEM_ID_GOOTAX : (int)$bonusSystemId;
    }

    /**
     * Can execute bonus transaction
     *
     * @param int $tenantId
     *
     * @return bool
     */
    public function canExecuteBonusTransaction($tenantId)
    {
        $bonusSystemId = $this->getBonusSystemId($tenantId);

        return $bonusSystemId === self::BONUS_SYSTEM_ID_GOOTAX;
    }
}