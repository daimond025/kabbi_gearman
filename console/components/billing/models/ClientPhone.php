<?php

namespace console\components\billing\models;

use Yii;

/**
 * This is the model class for table "{{%client_phone}}".
 *
 * @property integer $phone_id
 * @property integer $client_id
 * @property string $value
 *
 * @property Client $client
 */
class ClientPhone extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_phone}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'value'], 'required'],
            [['client_id'], 'integer'],
            [['value'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone_id'  => 'Phone ID',
            'client_id' => 'Client ID',
            'value'     => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }
}
