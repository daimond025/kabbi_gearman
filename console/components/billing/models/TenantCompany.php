<?php

namespace console\components\billing\models;

use yii\db\ActiveQuery;

/**
 * This is the model class for table "tbl_tenant_company".
 *
 * @property integer $tenant_company_id
 * @property integer $tenant_id
 * @property string $name
 * @property string $phone
 * @property integer $sort
 * @property integer $block
 * @property string $logo
 * @property integer $use_logo_company
 * @property string $stripe_account
 *
 * @property Worker[] $workers
 */
class TenantCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'sort', 'block', 'use_logo_company'], 'integer'],
            [['name', 'logo', 'stripe_account'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tenant_company_id' => 'Tenant Company ID',
            'tenant_id'         => 'Tenant ID',
            'name'              => 'Name',
            'phone'             => 'Phone',
            'sort'              => 'Sort',
            'block'             => 'Block',
            'logo'              => 'Logo',
            'use_logo_company'  => 'Use Logo Company',
            'stripe_account'    => 'Stripe Account',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

}
