<?php

namespace console\components\billing\models;

use console\components\billing\Account;
use Yii;
use yii\base\Component;
use yii\db\ActiveQuery;

class Bonus extends Component
{
    const VALID_DEVICE_TOKEN_TIME = 60 * 60 * 24 * 14;

    /**
     * Return calculated bonus by orderId
     *
     * @param int $orderId
     *
     * @param int $orderStatusId
     *
     * @return float
     */
    public function calculateOrderBonus($orderId, $orderStatusId)
    {
        $order = Order::findOne($orderId);

        if (empty($order) || !in_array($order->payment, ['CASH', 'CARD', 'PERSONAL_ACCOUNT'])) {
            return 0;
        } else {
            $summaryCost = empty($order->orderDetailCost) ?
                0 : $order->orderDetailCost->summary_cost;

            $clientBonus = $this->getClientBonus($order->tenant_id, $order->city_id,
                $order->tariff_id, $summaryCost, $order->order_time);

            if ($clientBonus) {
                return $this->calculateBonus($orderId, $order->client_id,
                    $orderStatusId, $order->device, $order->client_device_token,
                    $order->order_time, $summaryCost, $clientBonus);
            } else {
                return 0;
            }
        }
    }

    /**
     * Return calculated bonus
     *
     * @param int         $orderId
     * @param int         $clientId
     * @param integer     $orderStatusId
     * @param string      $device
     * @param string      $deviceToken
     * @param int         $orderTime
     * @param float       $summaryCost
     * @param ClientBonus $clientBonus
     *
     * @return float
     */
    public function calculateBonus(
        $orderId,
        $clientId,
        $orderStatusId,
        $device,
        $deviceToken,
        $orderTime,
        $summaryCost,
        $clientBonus
    ) {
        if (false || $orderStatusId == Order::STATUS_ID_COMPLETE_NOT_PAID) {
            return 0;
        }

        return round($this->getBonus($clientBonus, $summaryCost)
            + $this->getFirstOrderFromAppBonus($orderId, $clientId,
                $device, $deviceToken, $orderTime, $summaryCost, $clientBonus), 2);
    }

    /**
     * Getting client bonus
     *
     * @param int   $tenantId
     * @param int   $cityId
     * @param int   $tariffId
     * @param float $summaryCost
     * @param int   $orderTime
     *
     * @return ClientBonus|false
     */
    public function getClientBonus(
        $tenantId,
        $cityId,
        $tariffId,
        $summaryCost,
        $orderTime
    ) {
        $clientBonuses = $this->filterClientBonusesByActualDate(
            $this->getClientBonuses($tenantId, $cityId, $tariffId, $summaryCost),
            $orderTime);

        return empty($clientBonuses) ? false : $clientBonuses[0];
    }

    /**
     * Get client bonuses by params
     *
     * @param integer $tenantId
     * @param integer $cityId
     * @param integer $tariffId
     * @param float   $summaryCost
     *
     * @return ClientBonus[]
     */
    protected function getClientBonuses($tenantId, $cityId, $tariffId, $summaryCost)
    {
        $bonusSystemId = Account::getBonusSystemId($tenantId);

        return ClientBonus::find()
            ->alias('c')
            ->innerJoinWith([
                'gootaxBonus as g'           => function ($query) use ($summaryCost) {
                    /** @var $query ActiveQuery */
                    $query->where(['<=', 'g.min_cost', (float)$summaryCost]);
                },
                'clientBonusHasTariffs as t' => function ($query) use ($tariffId) {
                    /** @var $query ActiveQuery */
                    $query->where(['t.tariff_id' => $tariffId]);
                },
            ])
            ->where([
                'c.bonus_system_id' => $bonusSystemId,
                'c.tenant_id'       => $tenantId,
                'c.city_id'         => $cityId,
                'c.blocked'         => 0,
            ])
            ->orderBy(['c.bonus_id' => SORT_DESC])
            ->all();
    }


    /**
     * Check actual date
     *
     * @param string $actualDate
     * @param int    $orderTime
     *
     * @return boolean
     */
    public function checkActualDate($actualDate, $orderTime)
    {
        if ((string)$actualDate === '') {
            return true;
        }

        $bonusDates = explode(';', $actualDate);
        foreach ($bonusDates as $bonusDate) {
            $parsedDate = explode('|', $bonusDate);

            if (in_array($parsedDate[0], [
                date('l', $orderTime),
                date('d.m', $orderTime),
                date('d.m.Y', $orderTime),
            ])) {
                if (empty($parsedDate[1])) {
                    return true;
                }

                $parsedTime    = explode('-', $parsedDate[1]);
                $formattedTime = date('H:i', $orderTime);
                if ($formattedTime >= $parsedTime[0] && $formattedTime <= $parsedTime[1]) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Filter client bonuses by actual date
     *
     * @param array $clientBonuses
     * @param int   $orderTime
     *
     * @return array
     */
    protected function filterClientBonusesByActualDate($clientBonuses, $orderTime)
    {
        $bonuses = [];
        if (is_array($clientBonuses)) {
            foreach ($clientBonuses as $bonus) {
                if ($this->checkActualDate($bonus->gootaxBonus->actual_date, $orderTime)) {
                    $bonuses[] = $bonus;
                }
            }
        }

        return $bonuses;
    }

    /**
     * Add bonus fail log
     *
     * @param int    $clientId
     * @param string $device
     * @param string $deviceToken
     * @param int    $orderTime
     * @param int    $orderId
     */
    private function addToBonusFailLog(
        $orderId,
        $clientId,
        $device,
        $deviceToken,
        $orderTime,
        $bonusId,
        $bonus
    ) {
        $bonusFailLog               = new BonusFailLog;
        $bonusFailLog->order_id     = $orderId;
        $bonusFailLog->client_id    = $clientId;
        $bonusFailLog->device       = $device;
        $bonusFailLog->device_token = $deviceToken;
        $bonusFailLog->order_time   = $orderTime;
        $bonusFailLog->bonus_id     = $bonusId;
        $bonusFailLog->bonus        = $bonus;

        if (!$bonusFailLog->save()) {
            throw new \yii\base\Exception("Error create record in table bonus_fail_log.\n"
                . 'Model errors: ' . implode("\n", $bonusFailLog->firstErrors) . "\n\n"
                . "Function params (addToBonusFailLog):\n"
                . "orderId: $orderId\n"
                . "clientId: $clientId\n"
                . "device: $device\n"
                . "deviceToken: $deviceToken\n"
                . "orderTime: $orderTime\n"
                . "bonusId: $bonusId\n"
                . "bonus: $bonus\n");
        }
    }

    private function createClientOrderFromApp(
        $orderId,
        $clientId,
        $device,
        $deviceToken,
        $orderTime
    ) {
        $clientOrderFromApp               = new ClientOrderFromApp;
        $clientOrderFromApp->order_id     = $orderId;
        $clientOrderFromApp->client_id    = $clientId;
        $clientOrderFromApp->device       = $device;
        $clientOrderFromApp->device_token = $deviceToken;
        $clientOrderFromApp->order_time   = $orderTime;

        if (!$clientOrderFromApp->save()) {
            throw new \yii\base\Exception("Error create record in table client_order_from_app.\n"
                . 'Model errors: ' . implode("\n", $clientOrderFromApp->firstErrors) . "\n\n"
                . "Function params (createCLientOrderFromApp):\n"
                . "orderId: $orderId\n"
                . "clientId: $clientId\n"
                . "device: $device\n"
                . "deviceToken: $deviceToken\n"
                . "orderTime: $orderTime\n");
        }
    }

    /**
     * Return first order from app bonus value
     *
     * @param int         $orderId
     * @param int         $clientId
     * @param string      $device
     * @param string      $deviceToken
     * @param int         $orderTime
     * @param float       $orderCost
     * @param ClientBonus $clientBonus
     *
     * @return float
     */
    public function getFirstOrderFromAppBonus(
        $orderId,
        $clientId,
        $device,
        $deviceToken,
        $orderTime,
        $orderCost,
        $clientBonus
    ) {
        if (empty($deviceToken)) {
            $message = "Device token is empty (bonus for first order from app): orderId={$orderId}, clientId={$clientId}";
            app()->logger->log($message);

            return 0;
        }

        if (!in_array($device, [
            Order::DEVICE_IOS,
            Order::DEVICE_ANDROID,
            Order::DEVICE_WINPHONE,
        ])
        ) {
            return 0;
        }

        if (!empty(ClientOrderFromApp::find()->where(['client_id' => $clientId])->count())) {
            return 0;
        }

        $bonus = $this->getBonusApp($clientBonus, $orderCost);
        if (!empty(ClientOrderFromApp::find()
            ->where(['device_token' => $deviceToken])
            ->andWhere(['>', 'order_time', $orderTime - self::VALID_DEVICE_TOKEN_TIME])
            ->count())
        ) {
            $this->addToBonusFailLog(
                $orderId, $clientId, $device, $deviceToken, $orderTime,
                $clientBonus->bonus_id, $bonus);

            return 0;
        } else {
            $this->createClientOrderFromApp(
                $orderId, $clientId, $device, $deviceToken, $orderTime);

            return $bonus;
        }
    }

    /**
     * Return bonus value
     *
     * @param ClientBonus $clientBonus
     * @param float       $orderCost
     *
     * @return float
     */
    public function getBonus($clientBonus, $orderCost)
    {
        switch ($clientBonus->gootaxBonus->bonus_type) {
            case ClientBonusGootax::BONUS_TYPE_PERCENT:
                return $orderCost * $clientBonus->gootaxBonus->bonus / 100;
            case ClientBonusGootax::BONUS_TYPE_BONUS:
                return $clientBonus->gootaxBonus->bonus;
            default:
                return 0;
        }
    }

    /**
     * Return bonus value for first order from app
     *
     * @param ClientBonus $clientBonus
     * @param float       $orderCost
     *
     * @return float
     */
    public function getBonusApp($clientBonus, $orderCost)
    {
        switch ($clientBonus->gootaxBonus->bonus_app_type) {
            case ClientBonusGootax::BONUS_TYPE_PERCENT:
                return $orderCost * $clientBonus->gootaxBonus->bonus_app / 100;
            case ClientBonusGootax::BONUS_TYPE_BONUS:
                return $clientBonus->gootaxBonus->bonus_app;
            default:
                return 0;
        }
    }

}