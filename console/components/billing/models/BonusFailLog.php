<?php

namespace console\components\billing\models;

use console\components\db\ReconnectTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%bonus_fail_log}}".
 *
 * @property integer     $id
 * @property string      $order_id
 * @property string      $client_id
 * @property string      $device
 * @property string      $device_token
 * @property integer     $order_time
 * @property integer     $bonus_id
 * @property string      $bonus
 *
 * @property ClientBonus $clientBonus
 * @property Client      $client
 * @property Order       $order
 */
class BonusFailLog extends ActiveRecord
{
    use ReconnectTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bonus_fail_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'client_id', 'device', 'device_token', 'order_time', 'bonus_id', 'bonus'], 'required'],
            [['order_id', 'client_id', 'order_time', 'bonus_id'], 'integer'],
            [['bonus'], 'number'],
            [['device', 'device_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'order_id'     => 'Order ID',
            'client_id'    => 'Client ID',
            'device'       => 'Device',
            'device_token' => 'Device Token',
            'order_time'   => 'Order Time',
            'bonus_id'     => 'Bonus ID',
            'bonus'        => 'Bonus',
        ];
    }

    /**
     * @return mixed|\yii\db\Connection
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public static function getDb()
    {
        self::reconnectIfNeeded(Yii::$app->db_log);

        return Yii::$app->db_log;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientBonus()
    {
        return $this->hasOne(ClientBonus::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
