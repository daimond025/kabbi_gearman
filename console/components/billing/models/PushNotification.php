<?php

namespace console\components\billing\models;

/**
 * Push notification
 */
class PushNotification
{

    /**
     * Device types
     */
    const DEVICE_ANDROID = 'ANDROID';
    const DEVICE_IOS = 'IOS';

    /**
     * Application types
     */
    const TYPE_APP_CLIENT = 'client';
    const TYPE_APP_WORKER = 'worker';

    /**
     * Default language
     */
    const DEFAULT_LANG = 'ru';

    /**
     * @var integer
     */
    public $tenantId;

    /**
     * @var string
     */
    public $tenantLogin;

    /**
     * @var string
     */
    public $typeApp;

    /**
     * @var string
     */
    public $device;

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $lang;

    /**
     * @var string
     */
    public $currency;

    /**
     * @var float
     */
    public $balance;

    /**
     * @var float
     */
    public $bonusBalance;

    /* @var integer */
    public $appId;

    public function __construct(
        $tenantId,
        $tenantLogin,
        $typeApp,
        $device,
        $token,
        $lang,
        $currency,
        $balance,
        $bonusBalance = null,
        $appId = null
    ) {
        $this->tenantId     = $tenantId;
        $this->tenantLogin  = $tenantLogin;
        $this->typeApp      = $typeApp;
        $this->device       = $device;
        $this->token        = $token;
        $this->lang         = empty($lang) ? self::DEFAULT_LANG : $lang;
        $this->currency     = $currency;
        $this->balance      = $balance;
        $this->bonusBalance = $bonusBalance;
        $this->appId        = $appId;
    }

    /**
     * Return url-encoded string with notification params
     * @return string
     */
    public function generateParams()
    {
        $params = [];

        $params['tenant_id']    = $this->tenantId;
        $params['tenant_login'] = $this->tenantLogin;
        $params['device']       = $this->device;
        $params['token']        = $this->token;
        $params['lang']         = $this->lang;
        $params['currency']     = $this->currency;
        $params['balance']      = $this->balance;

        if ($this->bonusBalance !== null) {
            $params['bonus_balance'] = $this->bonusBalance;
        }

        if ($this->appId !== null) {
            $params['app_id'] = $this->appId;
        }

        return http_build_query($params, '', '&', PHP_QUERY_RFC3986);
    }
}
