<?php

namespace console\components\billing\models;

use Yii;

/**
 * This is the model class for table "{{%order_detail_cost}}".
 *
 * @property integer $detail_id
 * @property integer $order_id
 * @property string  $accrual_city
 * @property string  $accrual_out
 * @property string  $summary_distance
 * @property string  $summary_cost
 * @property string  $summary_time
 * @property string  $city_time
 * @property string  $city_distance
 * @property string  $city_cost
 * @property string  $out_city_time
 * @property string  $out_city_distance
 * @property string  $out_city_cost
 * @property string  $city_time_wait
 * @property string  $out_time_wait
 * @property string  $before_time_wait
 * @property string  $additional_cost
 * @property string  $is_fix
 * @property string  $city_next_km_price
 * @property string  $out_next_km_price
 * @property string  $supply_price
 * @property string  $city_wait_time
 * @property string  $out_wait_time
 * @property string  $city_wait_driving
 * @property string  $out_wait_driving
 * @property string  $city_wait_price
 * @property string  $out_wait_price
 * @property integer $time
 * @property string  $distance_for_plant
 * @property string  $planting_price
 * @property string  $planting_include
 * @property string  $start_point_location
 * @property string  $city_wait_cost
 * @property string  $out_wait_cost
 * @property string  $distance_for_plant_cost
 * @property string  $before_time_wait_cost
 * @property string  $commission
 * @property string  $surcharge
 * @property string  $tax
 * @property string  $bonus
 * @property int     $writeoff_bonus_id
 * @property string  $refill_bonus
 * @property int     $refill_bonus_id
 */
class OrderDetailCost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_detail_cost}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'time'], 'integer'],
            [['accrual_city', 'accrual_out', 'start_point_location'], 'string'],
            [
                [
                    'summary_distance',
                    'summary_cost',
                    'summary_time',
                    'city_time',
                    'city_distance',
                    'city_cost',
                    'out_city_time',
                    'out_city_distance',
                    'out_city_cost',
                    'city_time_wait',
                    'out_time_wait',
                    'before_time_wait',
                    'additional_cost',
                    'is_fix',
                    'city_next_km_price',
                    'out_next_km_price',
                    'supply_price',
                    'city_wait_time',
                    'out_wait_time',
                    'city_wait_driving',
                    'out_wait_driving',
                    'city_wait_price',
                    'out_wait_price',
                    'distance_for_plant',
                    'planting_price',
                    'planting_include',
                    'commission',
                    'surcharge',
                    'tax',
                ],
                'string',
                'max' => 45,
            ],
            [
                ['city_wait_cost', 'out_wait_cost', 'distance_for_plant_cost', 'before_time_wait_cost'],
                'string',
                'max' => 255,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'detail_id'               => 'Detail ID',
            'order_id'                => 'Order ID',
            'accrual_city'            => 'Accrual City',
            'accrual_out'             => 'Accrual Out',
            'summary_distance'        => 'Summary Distance',
            'summary_cost'            => 'Summary Cost',
            'summary_time'            => 'Summary Time',
            'city_time'               => 'City Time',
            'city_distance'           => 'City Distance',
            'city_cost'               => 'City Cost',
            'out_city_time'           => 'Out City Time',
            'out_city_distance'       => 'Out City Distance',
            'out_city_cost'           => 'Out City Cost',
            'city_time_wait'          => 'City Time Wait',
            'out_time_wait'           => 'Out Time Wait',
            'before_time_wait'        => 'Before Time Wait',
            'additional_cost'         => 'Additional Cost',
            'is_fix'                  => 'Is Fix',
            'city_next_km_price'      => 'City Next Km Price',
            'out_next_km_price'       => 'Out Next Km Price',
            'supply_price'            => 'Supply Price',
            'city_wait_time'          => 'City Wait Time',
            'out_wait_time'           => 'Out Wait Time',
            'city_wait_driving'       => 'City Wait Driving',
            'out_wait_driving'        => 'Out Wait Driving',
            'city_wait_price'         => 'City Wait Price',
            'out_wait_price'          => 'Out Wait Price',
            'time'                    => 'Time',
            'distance_for_plant'      => 'Distance For Plant',
            'planting_price'          => 'Planting Price',
            'planting_include'        => 'Planting Include',
            'start_point_location'    => 'Start Point Location',
            'city_wait_cost'          => 'City Wait Cost',
            'out_wait_cost'           => 'Out Wait Cost',
            'distance_for_plant_cost' => 'Distance For Plant Cost',
            'before_time_wait_cost'   => 'Before Time Wait Cost',
            'commission'              => 'Commission',
            'surcharge'               => 'Surcharge',
            'tax'                     => 'Tax',
        ];
    }
}
