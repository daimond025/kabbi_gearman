<?php

namespace console\components\billing\models;

use Yii;


/**
 * This is the model class for table "{{%client_order_from_app}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $client_id
 * @property string $device
 * @property string $device_token
 * @property integer $order_time
 *
 * @property Client $client
 * @property Order $order
 */
class ClientOrderFromApp extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_order_from_app}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'required'],
            [['device', 'device_token'], 'string', 'max' => 255],
            [['order_id', 'client_id', 'order_time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'order_id'     => 'Order ID',
            'client_id'    => 'Client ID',
            'device'       => 'Device',
            'device_token' => 'Device token',
            'order_time'   => 'Order time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

}
