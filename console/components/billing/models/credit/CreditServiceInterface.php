<?php

namespace credit;

/**
 * Interface CreditInterface
 * @package credit
 */
interface CreditServiceInterface
{
    /**
     * @return float
     */
    public function getLimit();
}