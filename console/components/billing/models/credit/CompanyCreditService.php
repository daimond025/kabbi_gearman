<?php

namespace credit;

use yii\db\Query;

/**
 * Class CompanyCreditService
 * @package credit
 */
class CompanyCreditService implements CreditServiceInterface
{
    /**
     * @var int
     */
    private $companyId;

    /**
     * @param int $companyId
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * @return float
     */
    private function getCreditValue()
    {
        $value = (new Query())
            ->from('{{%client_company}}')
            ->select('credits')
            ->where(['company_id' => $this->companyId])
            ->scalar();

        return empty($value) ? 0.0 : (float)$value;
    }

    /**
     * @return float
     */
    public function getLimit()
    {
        return $this->getCreditValue();
    }

    /**
     * @param int $companyId
     *
     * @return CompanyCreditService
     */
    public static function getCompany($companyId)
    {
        return new self($companyId);
    }

}
