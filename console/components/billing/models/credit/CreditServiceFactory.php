<?php

namespace credit;

/**
 * Class CreditServiceFactory
 * @package credit
 */
class CreditServiceFactory
{
    const TYPE_CLIENT = 'client';
    const TYPE_COMPANY = 'company';

    /**
     * @param string $type
     * @param int    $id
     *
     * @return CompanyCreditService
     * @throws UnknownCreditServiceTypeException
     */
    public function getCreditService($type, $id)
    {
        switch ($type) {
            case self::TYPE_COMPANY:
                $service = new CompanyCreditService();
                $service->setCompanyId($id);

                return $service;
            default:
                throw new UnknownCreditServiceTypeException();
        }
    }
}