<?php

namespace console\components\billing\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tenant_setting}}".
 *
 * @property string $setting_id
 * @property string $tenant_id
 * @property string $name
 * @property string $value
 * @property string $type
 * @property integer $city_id
 * @property integer $position_id
 */
class TenantSetting extends ActiveRecord
{
    const SETTING_USE_STRIPE_CONNECT = 'USE_STRIPE_CONNECT';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name'], 'required'],
            [['tenant_id', 'city_id', 'position_id'], 'integer'],
            [['name', 'value', 'type'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id'  => 'Setting ID',
            'tenant_id'   => 'Tenant ID',
            'name'        => 'Name',
            'value'       => 'Value',
            'type'        => 'Type',
            'city_id'     => 'City ID',
            'position_id' => 'Position ID',
        ];
    }

    /**
     * Getting setting value.
     *
     * @param int $tenantId
     * @param string $setting
     * @param int $cityId
     * @param int $positionId
     *
     * @return string
     */
    public static function getSettingValue($tenantId, $setting, $cityId = null, $positionId = null)
    {
        return self::find()
            ->where([
                'tenant_id' => $tenantId,
                'name'      => $setting,
            ])
            ->andFilterWhere([
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])
            ->select('value')
            ->scalar();
    }
}
