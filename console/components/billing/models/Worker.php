<?php

namespace console\components\billing\models;

use console\components\billing\Account;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * This is the model class for table "tbl_worker".
 *
 * @property integer $worker_id
 * @property integer $tenant_id
 * @property integer $callsign
 * @property string $password
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $photo
 * @property string $device
 * @property string $device_token
 * @property string $lang
 * @property string $description
 * @property string $device_info
 * @property string $email
 * @property string $partnership
 * @property string $birthday
 * @property integer $block
 * @property integer $create_time
 * @property string $yandex_account_number
 * @property integer $tenant_company_id
 *
 * @property Account[] $accounts
 * @property TenantCompany $tenantCompany
 */
class Worker extends ActiveRecord
{
    const SQL_LAST_CITY_ID = <<<SQL
SELECT c.city_id
FROM tbl_worker_has_city c
  JOIN tbl_worker w ON c.worker_id = w.worker_id
  JOIN tbl_tenant_has_city t
    ON c.city_id = t.city_id AND w.tenant_id = t.tenant_id
WHERE w.worker_id = :workerId AND t.block = 0
ORDER BY c.last_active DESC, c.city_id
LIMIT 1
SQL;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'callsign', 'phone'], 'required'],
            [['tenant_id', 'callsign', 'block', 'create_time', 'tenant_company_id'], 'integer'],
            [['device', 'device_token', 'partnership', 'yandex_account_number'], 'string'],
            [['birthday'], 'safe'],
            [['password', 'photo', 'description', 'device_info'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name', 'email'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 15],
            [['lang'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'worker_id'    => 'Worker ID',
            'tenant_id'    => 'Tenant ID',
            'callsign'     => 'Callsign',
            'password'     => 'Password',
            'last_name'    => 'Last Name',
            'name'         => 'Name',
            'second_name'  => 'Second Name',
            'phone'        => 'Phone',
            'photo'        => 'Photo',
            'device'       => 'Device',
            'device_token' => 'Device Token',
            'lang'         => 'Lang',
            'description'  => 'Description',
            'device_info'  => 'Device Info',
            'email'        => 'Email',
            'partnership'  => 'Partnership',
            'birthday'     => 'Birthday',
            'block'        => 'Block',
            'create_time'  => 'Create Time',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), ['owner_id' => 'worker_id'])
            ->andOnCondition([Account::tableName() . '.acc_kind_id' => Account::WORKER_KIND]);
    }

    /**
     * @return int|null
     *
     * @throws Exception
     */
    public function getLastCityId(): ?int
    {
        $cityId = \Yii::$app->getDb()
            ->createCommand(self::SQL_LAST_CITY_ID, ['workerId' => $this->worker_id])
            ->queryScalar();

        return empty($cityId) ? null : (int)$cityId;
    }

    /**
     * @return ActiveQuery
     */
    public function getTenantCompany()
    {
        return $this->hasOne(TenantCompany::className(), ['tenant_company_id' => 'tenant_company_id']);
    }
}
