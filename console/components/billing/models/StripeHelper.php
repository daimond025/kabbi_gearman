<?php

namespace console\components\billing\models;

class StripeHelper
{
    /**
     * @param int $tenantId
     * @param int $cityId
     * @param Worker $worker
     * @return null|string
     */
    public static function getStripeAccount($tenantId, $cityId, $worker)
    {
        $useConnect = TenantSetting::getSettingValue($tenantId, TenantSetting::SETTING_USE_STRIPE_CONNECT, $cityId);
        if ($useConnect !== '1') {
            return null;
        }

        if ($worker->tenantCompany instanceof TenantCompany) {
            return empty($worker->tenantCompany->stripe_account) ? null : (string)$worker->tenantCompany->stripe_account;
        }

        return null;
    }
}
