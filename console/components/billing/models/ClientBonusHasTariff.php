<?php

namespace console\components\billing\models;

use Yii;

/**
 * This is the model class for table "{{%client_bonus_has_tariff}}".
 *
 * @property integer $id
 * @property integer $bonus_id
 * @property integer $tariff_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ClientBonus $bonus
 * @property TaxiTariff $tariff
 */
class ClientBonusHasTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_bonus_has_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_id', 'tariff_id', 'created_at', 'updated_at'], 'required'],
            [['bonus_id', 'tariff_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bonus_id' => 'Bonus ID',
            'tariff_id' => 'Tariff ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus()
    {
        return $this->hasOne(ClientBonus::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @inheritdoc
     * @return ClientBonusHasTariffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientBonusHasTariffQuery(get_called_class());
    }
}

class ClientBonusHasTariffQuery extends \yii\db\ActiveQuery
{
    public function byTariffId($tariffId)
    {
        return $this->andWhere([ClientBonusHasTariff::tableName() . '.tariff_id' => $tariffId]);
    }

}
