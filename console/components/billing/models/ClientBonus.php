<?php

namespace console\components\billing\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%client_bonus}}".
 *
 * @property integer                $bonus_id
 * @property integer                $bonus_system_id
 * @property integer                $city_id
 * @property integer                $class_id
 * @property integer                $tenant_id
 * @property string                 $name
 * @property integer                $blocked
 * @property integer                $created_at
 * @property integer                $updated_at
 *
 * @property City                   $city
 * @property CarClass               $class
 * @property Tenant                 $tenant
 * @property BonusSystem            $bonusSystem
 * @property ClientBonusGootax      $gootaxBonus
 * @property ClientBonusHasTariff[] $clientBonusHasTariffs
 */
class ClientBonus extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_bonus}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_system_id', 'city_id', 'class_id', 'tenant_id', 'name', 'created_at', 'updated_at'], 'required',],
            [['bonus_system_id', 'city_id', 'class_id', 'tenant_id', 'blocked', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_id'        => 'Bonus ID',
            'bonus_system_id' => 'Bonus System ID',
            'city_id'         => 'City ID',
            'class_id'        => 'Class ID',
            'tenant_id'       => 'Tenant ID',
            'name'            => 'Name',
            'blocked'         => 'Blocked',
            'created_at'      => 'Created At',
            'updated_at'      => 'Updated At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getBonusSystem()
    {
        return $this->hasOne(BonusSystem::className(), ['id' => 'bonus_system_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGootaxBonus()
    {
        return $this->hasOne(ClientBonusGootax::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getClientBonusHasTariffs()
    {
        return $this->hasMany(ClientBonusHasTariff::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * @inheritdoc
     * @return ClientBonusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClientBonusQuery(get_called_class());
    }

}

/**
 * Class ClientBonusQuery
 * @package console\components\billing\models
 */
class ClientBonusQuery extends ActiveQuery
{

    /**
     * @param $tenantId
     *
     * @return $this
     */
    public function byTenantId($tenantId)
    {
        return $this->andWhere(['tenant_id' => $tenantId]);
    }

    /**
     * @param $cityId
     *
     * @return $this
     */
    public function byCityId($cityId)
    {
        return $this->andWhere(['city_id' => $cityId]);
    }

    /**
     * @param $orderCost
     *
     * @return $this
     */
    public function byOrderCost($orderCost)
    {
        return $this->andWhere(['<=', 'min_cost', (float)$orderCost]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['blocked' => 0]);
    }

}