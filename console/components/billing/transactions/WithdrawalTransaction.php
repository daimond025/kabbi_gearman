<?php

namespace console\components\billing\transactions;

use Yii;
use yii\base\Exception;
use console\components\billing\Operation;
use console\components\billing\Account;

/**
 * Class WithdrawalTransaction
 * @package console\components\billing\transactions
 */
class WithdrawalTransaction extends Transaction
{
    /**
     * @var boolean
     */
    public $isTenantPayment = false;

    public function init()
    {
        parent::init();

        app()->logger->log('Transaction type: Withdrawal');

        if ($this->isTenantPayment) {
            $this->setupGootaxAccounts();

            $this->sender_acc_id      = $this->tenantAccount->account_id;
            $this->sender_acc_kind_id = $this->tenantAccount->acc_kind_id;
            $this->sender_acc_balance = $this->tenantAccount->balance;

            $this->receiver_acc_id      = $this->gootaxAccount->account_id;
            $this->receiver_acc_balance = $this->gootaxAccount->balance;
        } else {
            $this->receiver_acc_id      = $this->system_acc_id;
            $this->receiver_acc_balance = $this->system_balance;
        }
    }

    /**
     * Withdrawal from balance
     * @throws \yii\base\Exception
     */
    private function withdrawalFromBalance()
    {
        $type_id = (string)$this->payment_method === self::WITHDRAWAL_CASH_OUT_PAYMENT
            ? Operation::CASH_OUT_TYPE : Operation::EXPENSES_TYPE;

        $ownerData = $this->getOwnerData($this->sender_acc_id,
            $this->sender_acc_kind_id);

        //Снимаем деньги со счета пользователя
        $cashOutUser = new Operation([
            'type_id'         => $type_id,
            'account_id'      => $this->sender_acc_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $this->sum,
            'account_balance' => $this->sender_acc_balance,
            'comment'         => $this->comment,
            'owner_name'      => $ownerData['name'],
        ]);

        if (!$cashOutUser->save()) {
            Yii::error($cashOutUser->getErrors());
            throw new Exception('Ошибка проведения операции списания средств со счета пользователя №' . $this->sender_acc_id);
        }

        app()->logger->log('Operation (Write-off from balance): ' . json_encode([
                'type_id'     => $type_id,
                'account_id'  => $this->sender_acc_id,
                'acc_kind_id' => $this->sender_acc_kind_id,
                'sum'         => $this->sum,
            ]));

        //Снимаем деньги из системы
        $cashOutSystem = new Operation([
            'type_id'         => $type_id,
            'account_id'      => $this->receiver_acc_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $this->sum,
            'account_balance' => $this->receiver_acc_balance,
            'comment'         => 'Withdrawal from account №' . $this->sender_acc_id,
        ]);

        if (!$cashOutSystem->save()) {
            Yii::error($cashOutSystem->getErrors());
            throw new Exception('Ошибка проведения операции списания средств с системного счета №' . $this->receiver_acc_id);
        }

        app()->logger->log('Operation (Write-off from balance [SYSTEM]): ' . json_encode([
                'type_id'    => $type_id,
                'account_id' => $this->receiver_acc_id,
                'sum'        => $this->sum,
            ]));

        $balance = $this->getTruncatedSum(
            $this->sender_acc_balance - $this->sum, $this->currency_id);

        $bonusBalance = null;
        if ((int)$this->sender_acc_kind_id === Account::CLIENT_KIND) {
            $accountKindId = Account::getClientBonusKind($this->tenant_id);

            $clientBonusAccount = $this->getAccount(
                $this->sender_owner_id,
                $this->tenant_id,
                $accountKindId,
                $this->currency_id);

            $bonusBalance = empty($clientBonusAccount)
                ? 0 : +$clientBonusAccount->balance;
            $bonusBalance = $this->getTruncatedSum($bonusBalance, $this->currency_id);
        }

        // update push notification
        $this->updatePushNotification(
            $this->tenant_id,
            $this->getTenantLogin(),
            $this->getApplicationType($this->sender_acc_kind_id),
            $ownerData['device'],
            $ownerData['token'],
            $ownerData['lang'],
            $this->getCurrency(),
            $balance,
            $bonusBalance,
            empty($ownerData['appId']) ? null : $ownerData['appId']);
    }

    /**
     * Withdrawal from tenant balance
     */
    private function withdrawalFromTenantBalance()
    {
        $this->createOperation(Operation::EXPENSES_TYPE,
            $this->tenantAccount->account_id,
            $this->transaction_id,
            $this->sum,
            $this->tenantAccount->balance,
            $this->comment
        );
        app()->logger->log("Operation (Writeoff from tenant balance]): " . json_encode([
                'type_id'     => Operation::EXPENSES_TYPE,
                'account_id'  => $this->tenantAccount->account_id,
                'acc_kind_id' => $this->tenantAccount->acc_kind_id,
                'sum'         => $this->sum,
            ]));
        $this->tenantAccount->refresh();

        $this->createOperation(Operation::EXPENSES_TYPE,
            $this->gootaxAccount->account_id,
            $this->transaction_id,
            $this->sum,
            $this->gootaxAccount->balance,
            'Withdrawal from account №' . $this->sender_acc_id
        );
        app()->logger->log("Operation (Writeoff from tenant balance [GOOTAX]): " . json_encode([
                'type_id'    => Operation::EXPENSES_TYPE,
                'account_id' => $this->gootaxAccount->account_id,
                'sum'        => $this->sum,
            ]));
    }

    /**
     * Проводим транзакцию
     * @throws Exception
     */
    protected function doTransactOperation()
    {
        if ($this->isTenantPayment) {
            $this->withdrawalFromTenantBalance();
        } else {
            $this->withdrawalFromBalance();
        }
    }

}
