<?php

namespace console\components\billing\transactions;

use console\components\billing\exceptions\BlockedCompanyException;
use console\components\billing\exceptions\ForbiddenBonusTransactionException;
use console\components\db\ReconnectTrait;
use credit\CreditServiceFactory;
use notification\CompleteTransactionProducer;
use notification\MQConnection;
use notification\NotificationLogger;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use console\components\billing\exceptions\NotFoundException;
use console\components\billing\exceptions\NoMoneyException;
use console\components\billing\exceptions\NoBonusForPaymentException;
use console\components\billing\exceptions\HeldPaymentException;
use console\components\billing\exceptions\PaymentAlreadyExistsException;
use console\components\billing\exceptions\WriteoffBonusInCorpPaymentException;
use console\components\billing\exceptions\InvalidBonusValueException;
use console\components\billing\exceptions\InvalidSumException;
use console\components\billing\exceptions\NoBalanceChangesException;
use console\components\billing\exceptions\InvalidCardPaymentParamsException;
use console\components\billing\exceptions\CardPaymentFailedException;
use console\components\billing\Account;
use console\components\billing\Operation;
use console\components\billing\models\Currency;
use console\components\billing\models\PushNotification;
use console\components\billing\PushNotificationManager;

/**
 * This is the model class for table "{{%transaction}}".
 *
 * @property integer         $transaction_id
 * @property integer         $type_id
 * @property integer         $sender_acc_id
 * @property integer         $receiver_acc_id
 * @property double          $sum
 * @property string          $payment_method
 * @property string          $date
 * @property string          $comment
 * @property integer         $user_created
 * @property integer         $order_id
 * @property integer         $city_id
 * @property string          $terminal_transact_date
 * @property string          $terminal_transact_number
 *
 * @property Operation[]     $operations
 * @property Account         $receiverAcc
 * @property Account         $senderAcc
 * @property TransactionType $type
 */
abstract class Transaction extends \yii\db\ActiveRecord
{
    use ReconnectTrait;

    const GOOTAX_ACCOUNT_ID = 1;

    /**
     * Пополнение
     */
    const DEPOSIT_TYPE = 1;

    /**
     * Снятие
     */
    const WITHDRAWAL_TYPE = 2;

    /**
     * Оплата за заказ
     */
    const ORDER_PAYMENT_TYPE = 3;

    /**
     * Оплата за Гутакс
     */
    const GOOTAX_PAYMENT_TYPE = 4;

    //Методы оплаты
    const BONUS_PAYMENT = 'BONUS';
    const CASH_PAYMENT = 'CASH';
    const CARD_PAYMENT = 'CARD';
    const PERSONAL_ACCOUNT_PAYMENT = 'PERSONAL_ACCOUNT';
    const CORP_BALANCE_PAYMENT = 'CORP_BALANCE';
    const WITHDRAWAL_PAYMENT = 'WITHDRAWAL';
    const WITHDRAWAL_TARIFF_PAYMENT = 'WITHDRAWAL_FOR_TARIFF';
    const WITHDRAWAL_CASH_OUT_PAYMENT = 'WITHDRAWAL_FOR_CASH_OUT';
    const TERMINAL_ELECSNET_PAYMENT = 'TERMINAL_ELECSNET';
    const TERMINAL_QIWI_PAYMENT = 'TERMINAL_QIWI';
    const TERMINAL_MILLION_PAYMENT = 'TERMINAL_MILLION';
    const TERMINAL_PAYNET_PAYMENT = 'TERMINAL_PAYNET';

    //Коды ошибок
    const SUCCESS_RESPONSE = 100;
    const NOT_SUPPORTED_RESPONSE = 120;
    const SYSTEM_ERROR_RESPONSE = 140;
    const NO_MONEY_RESPONSE = 160;
    const NO_BONUS_FOR_PAYMENT_RESPONSE = 170;
    const WRITEOFF_BONUS_IN_CORP_PAYMENT_RESPONSE = 175;
    const NOT_FOUND_RESPONSE = 180;
    const HELD_PAYMENT_RESPONSE = 200;
    const PAYMENT_ALREADY_EXISTS_RESPONSE = 220;
    const INVALID_BONUS_VALUE_RESPONSE = 230;
    const INVALID_SUM_RESPONSE = 240;
    const INVALID_CARD_PAYMENT_PARAMS_RESPONSE = 250;
    const CARD_PAYMENT_FAILED_RESPONSE = 260;
    const FORBIDDEN_BONUS_TRANSACTION_RESPONSE = 270;
    const BLOCKED_COMPANY_RESPONSE = 280;

    public $tenant_id;
    private $_tenant_login;
    public $currency_id;
    private $_currency;

    /**
     * @var string
     */
    public $request_id;

    /**
     * Cоответствует полю acc_balance класса console\components\billing\Account
     * @var float Баланс счета отправителя
     */
    public $sender_acc_kind_id;

    /**
     * Id собственника счета отправителя
     * Cоответствует полю owner_id класса console\components\billing\Account
     * @var integer
     */
    public $sender_owner_id;

    /**
     * @var float Баланс счета получателя
     */
    protected $sender_acc_balance;

    /**
     * Вид счета отправителя (Клиент, исполнитель)
     * Cоответствует полю acc_kind_id класса console\components\billing\Account
     * @var integer
     */
    protected $receiver_acc_balance;

    /**
     * @var integer ID системного счета
     */
    protected $system_acc_id;

    /**
     * @var float Баланс системы
     */
    protected $system_balance;

    /**
     * Push notifications
     * @var array
     */
    public $notifications = [];

    /**
     * Список данных владельца счета
     * @var array
     */
    protected $account_owner_data = [];

    public $order_status_id;

    /**
     * @var Account
     */
    protected $gootaxAccount;

    /**
     * @var Account
     */
    protected $tenantAccount;

    /**
     * Push notification manager
     * @var PushNotificationManager
     */
    public $pushNotificationManager;

    /**
     * Getting truncated sum
     *
     * @param float $sum
     * @param int   $currencyId
     *
     * @return float
     */
    protected function getTruncatedSum($sum, $currencyId)
    {
        $currency  = Currency::getCachedCurrency($currencyId);
        $minorUnit = isset($currency['minor_unit']) ? $currency['minor_unit'] : 0;

        return floor(round($sum * pow(10, $minorUnit), 1)) / pow(10, $minorUnit);
    }

    public function init()
    {
        parent::init();

        if (isset($this->sender_acc_kind_id)) {
            if (!$this->isOwnerExists($this->sender_owner_id,
                $this->sender_acc_kind_id)
            ) {
                throw new NotFoundException("Sender doesn't exists");
            }
        }

        if (empty($this->currency_id)) {
            throw new NotFoundException("Not specified transaction currency");
        }

        if ($this->sum < 0) {
            throw new InvalidSumException("Invalid sum of payment");
        }

        //        if (empty($this->sum)) {
        //            throw new NoBalanceChangesException("Transaction sum is empty and balance doesn't changed");
        //        }

        $this->sum = $this->getTruncatedSum($this->sum, $this->currency_id);

        if (isset($this->sender_acc_kind_id)) {
            $senderAccount            = $this->findAccountAndCreateIfNotExists(
                $this->sender_owner_id, $this->tenant_id, $this->sender_acc_kind_id,
                Account::PASSIVE_TYPE, $this->currency_id);
            $this->sender_acc_id      = $senderAccount->account_id;
            $this->city_id            = $senderAccount->getEntityCityId();
            $this->sender_acc_balance = $senderAccount->balance;

            //Поиск счета системы
            $systemAccount        = $this->findAccountAndCreateIfNotExists(
                0, $this->tenant_id, Account::SYSTEM_KIND, Account::ACTIVE_TYPE,
                $this->currency_id);
            $this->system_acc_id  = $systemAccount->account_id;
            $this->system_balance = $systemAccount->balance;
        }
    }

    /**
     * Does owner exists
     *
     * @param integer $ownerId
     * @param integer $kindId
     *
     * @return boolean
     */
    protected function isOwnerExists($ownerId, $kindId)
    {
        $entityData = Account::getEntityTableData($kindId);

        return (new \yii\db\Query())
            ->from($entityData['table'])
            ->where([$entityData['pk'] => $ownerId])
            ->exists();
    }

    /**
     * Setup gootax accounts
     */
    protected function setupGootaxAccounts()
    {
        $this->gootaxAccount = $this->getAccount(0, null, Account::GOOTAX_KIND, $this->currency_id);
        $this->tenantAccount = $this->findAccountAndCreateIfNotExists(
            0, $this->tenant_id, Account::TENANT_KIND, Account::PASSIVE_TYPE, $this->currency_id);
    }

    /**
     * Getting tenant login by tenant id
     *
     * @param integer $tenantId
     *
     * @return string
     */
    protected function getTenantLogin()
    {
        if (empty($this->_tenant_login)) {
            $this->_tenant_login = (new \yii\db\Query)
                ->from('{{%tenant}}')
                ->select('domain')
                ->where(['tenant_id' => $this->tenant_id])
                ->scalar();
        }

        return $this->_tenant_login;
    }

    /**
     * Getting currency code
     * @return string
     */
    protected function getCurrency()
    {
        if (empty($this->_currency)) {
            $this->_currency = Currency::find()
                ->where(['currency_id' => $this->currency_id])
                ->select('code')
                ->scalar();
        }

        return $this->_currency;
    }

    /**
     * Getting application type by acc_kind_id
     *
     * @param integer $accKindId
     *
     * @return string
     */
    public function getApplicationType($accKindId)
    {
        $map = [
            Account::CLIENT_KIND                => PushNotification::TYPE_APP_CLIENT,
            Account::CLIENT_BONUS_KIND          => PushNotification::TYPE_APP_CLIENT,
            Account::CLIENT_BONUS_UDS_GAME_KIND => PushNotification::TYPE_APP_CLIENT,
            Account::COMPANY_KIND               => null,
            Account::WORKER_KIND                => PushNotification::TYPE_APP_WORKER,
        ];

        return $map[$accKindId];
    }

    /**
     * Update push notification in notification list and create if not exists
     *
     * @param integer $tenantId
     * @param string  $tenantLogin
     * @param string  $typeApp
     * @param string  $device
     * @param string  $token
     * @param string  $lang
     * @param string  $currency
     * @param float   $balance
     * @param float   $bonusBalance
     * @param integer $appId
     *
     * @return boolean
     */
    protected function updatePushNotification(
        $tenantId,
        $tenantLogin,
        $typeApp,
        $device,
        $token,
        $lang,
        $currency,
        $balance = null,
        $bonusBalance = null,
        $appId = null
    ) {
        $key = "{$tenantLogin}_{$token}_{$typeApp}";

        if (!isset($device)) {
            return false;
        }

        if (empty($this->notifications[$key])) {
            $this->notifications[$key] = new PushNotification(
                $tenantId, $tenantLogin, $typeApp, $device, $token, $lang,
                $currency, $balance, $bonusBalance, $appId);
        } else {
            $this->notifications[$key]->balance      =
                $balance !== null ? $balance : $this->notifications[$key]->balance;
            $this->notifications[$key]->bonusBalance =
                $bonusBalance !== null ? $bonusBalance : $this->notifications[$key]->bonusBalance;
            $this->notifications[$key]->appId        =
                $appId !== null ? $appId : $this->notifications[$key]->appId;
        }

        return true;
    }

    /**
     * Getting account owner data
     *
     * @param integer $accountId
     * @param integer $accKindId
     *
     * @return array
     */
    protected function getOwnerData($accountId, $accKindId)
    {
        $key = "{$accountId}_{$accKindId}";
        if (!isset($this->account_owner_data[$key])) {
            $this->account_owner_data[$key] = Account::getOwnerData($accountId, $accKindId);
        }

        return $this->account_owner_data[$key];
    }

    /**
     * Получаем сумму баланса Гутакса.
     * @return float
     */
    protected function getGootaxBalance()
    {
        return Account::find()
            ->where(['account_id' => self::GOOTAX_ACCOUNT_ID])
            ->select('balance')
            ->scalar();
    }

    /**
     * Get account by params
     *
     * @param integer $ownerId
     * @param integer $tenantId
     * @param integer $kindId
     * @param integer $currencyId
     *
     * @return Account
     */
    protected function getAccount($ownerId, $tenantId, $kindId, $currencyId)
    {
        $query = Account::find()
            ->andFilterWhere(['tenant_id' => $tenantId])
            ->andWhere(['acc_kind_id' => $kindId])
            ->andWhere(['currency_id' => $currencyId]);

        if (!empty($ownerId)) {
            $query->andWhere(['owner_id' => $ownerId]);
        }

        return $query->one();
    }

    /**
     * Create account
     *
     * @param integer $ownerId
     * @param integer $tenantId
     * @param integer $kindId
     * @param integer $typeId
     * @param integer $currencyId
     *
     * @return Account
     * @throws Exception
     */
    protected function createAccount(
        $ownerId,
        $tenantId,
        $kindId,
        $typeId,
        $currencyId
    ) {
        $account = new Account([
            'owner_id'    => $ownerId,
            'tenant_id'   => $tenantId,
            'acc_kind_id' => $kindId,
            'acc_type_id' => $typeId,
            'currency_id' => $currencyId,
            'balance'     => 0,
        ]);

        if (!$account->save()) {
            \Yii::error($account->getErrors());
            throw new Exception('An error occurred during account creation');
        } else {
            return $account;
        }
    }

    /**
     * Find account and create if account is not exists
     *
     * @param integer $ownerId
     * @param integer $tenantId
     * @param integer $kindId
     * @param integer $typeId
     * @param integer $currencyId
     *
     * @return Account
     * @throws Exception
     */
    protected function findAccountAndCreateIfNotExists(
        $ownerId,
        $tenantId,
        $kindId,
        $typeId,
        $currencyId
    ) {
        $account = $this->getAccount($ownerId, $tenantId, $kindId, $currencyId);

        if (empty($account)) {
            $account = $this->createAccount(
                $ownerId, $tenantId, $kindId, $typeId, $currencyId);
        }

        return $account;
    }

    /**
     * Return operation type by transaction type
     *
     * @param integer $transactionType
     *
     * @return integer
     */
    protected function getOperationType($transactionType)
    {
        switch ($transactionType) {
            case self::WITHDRAWAL_TYPE:
                return Operation::EXPENSES_TYPE;
            case self::DEPOSIT_TYPE:
            case self::ORDER_PAYMENT_TYPE:
                return Operation::INCOME_TYPE;
        }
    }

    /**
     * Create operation
     *
     * @param int    $operationType
     * @param int    $accountId
     * @param int    $transactionId
     * @param float  $sum
     * @param float  $accountBalance
     * @param string $comment
     * @param string $ownerName
     *
     * @throws \Exception
     */
    protected function createOperation(
        $operationType,
        $accountId,
        $transactionId,
        $sum,
        $accountBalance,
        $comment = '',
        $ownerName = ''
    ) {
        $operation = new Operation([
            'type_id'         => $operationType,
            'account_id'      => $accountId,
            'transaction_id'  => $transactionId,
            'sum'             => $sum,
            'account_balance' => $accountBalance,
            'comment'         => $comment,
            'owner_name'      => $ownerName,
        ]);

        if (!$operation->save()) {
            \Yii::error($operation->getErrors());
            throw new \Exception(
                'An error occurred during the operation on the account №' . $accountId);
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->doTransactOperation();
    }

    /**
     * Callback function will be executed when doTransactionOperation will be failed
     */
    public function afterRollback()
    {
    }

    /**
     * Проводим транзакцию
     */
    abstract protected function doTransactOperation();

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'sender_acc_id', 'receiver_acc_id', 'user_created', 'city_id'], 'integer'],
            [['receiver_acc_id', 'sum', 'payment_method'], 'required'],
            [['sum'], 'number'],
            ['sum', 'default', 'value' => 0],
            [['date'], 'safe'],
            [
                ['comment', 'payment_method', 'terminal_transact_number', 'terminal_transact_date'],
                'string',
                'max' => 255,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'transaction_id'  => 'Transaction ID',
            'type_id'         => 'Type ID',
            'sender_acc_id'   => 'Sender Acc ID',
            'receiver_acc_id' => 'Receiver Acc ID',
            'sum'             => 'Sum',
            'payment_method'  => 'Payment Method',
            'date'            => 'Date',
            'comment'         => 'Comment',
            'user_created'    => 'Author',
            'city_id'         => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(),
            ['transaction_id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiverAcc()
    {
        return $this->hasOne(Account::className(),
            ['account_id' => 'receiver_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSenderAcc()
    {
        return $this->hasOne(Account::className(),
            ['account_id' => 'sender_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransactionType::className(),
            ['type_id' => 'type_id']);
    }

    /**
     * Проведение транзакции, соответствующего типа.
     *
     * @param array $data Массив с данными для транзакции
     *
     * @return integer Код ответа
     * @throws NotSupportedException
     * @throws Exception
     */
    public static function createTransaction($data)
    {
        $category = 'billing';

        self::reconnectIfNeeded(app()->db);
        //Открываем бд транзакцию
        $dbTransaction = app()->db->beginTransaction();
        $response      = null;

        $paramStr = "Income parameters:\n" . print_r($data, true) . "\n";

        app()->logger->log("Create transaction");
        app()->logger->log("Parameters: " . json_encode($data));

        try {
            if (!empty($data['order_id'])) {
                $factory     = new CreditServiceFactory();
                $transaction = new OrderTransaction($factory, $data);
            } elseif ($data['payment_method'] == self::BONUS_PAYMENT) {
                $category    = 'bonus';
                $transaction = new BonusTransaction($data);
                $transaction->initData();
            } elseif ($data['type_id'] == self::WITHDRAWAL_TYPE) {
                $transaction = new WithdrawalTransaction($data);
            } elseif ($data['type_id'] == self::DEPOSIT_TYPE) {
                $transaction = new DepositTransaction($data);
            } elseif ($data['type_id'] == self::GOOTAX_PAYMENT_TYPE) {
                $transaction = new GootaxPaymentTransaction($data);
            } else {
                throw new NotSupportedException('Данный тип транзакции ' . $data['type_id'] . ' не поддерживается');
            }

            if (!$transaction->save()) {
                $errors    = $transaction->getErrors();
                $error_mes = '';

                foreach ($errors as $error) {
                    if (!empty($error_mes)) {
                        $error_mes .= ' ';
                    }
                    $error_mes .= implode('. ', $error);
                }
                throw new Exception('Ошибка валидации транзакции: ' . $error_mes);
            }

            $dbTransaction->commit();

            if (!isset($transaction->pushNotificationManager)) {
                $transaction->pushNotificationManager = new PushNotificationManager;
            }
            $transaction->pushNotificationManager->sendNotifications($transaction->notifications);

            $transaction->publishNotification();

            app()->logger->log('Transaction completed');

            return self::SUCCESS_RESPONSE;

        } catch (NoBalanceChangesException $exc) {
            $exception = $exc;
            $response  = self::SUCCESS_RESPONSE;
        } catch (NoMoneyException $exc) {
            $exception = $exc;
            $response  = self::NO_MONEY_RESPONSE;
        } catch (InvalidCardPaymentParamsException $exc) {
            Yii::error($paramStr . $exc->getMessage(), $category);
            $exception = $exc;
            $response  = self::INVALID_CARD_PAYMENT_PARAMS_RESPONSE;
        } catch (NoBonusForPaymentException $exc) {
            Yii::error($paramStr . $exc->getMessage(), $category);
            $exception = $exc;
            $response  = self::NO_BONUS_FOR_PAYMENT_RESPONSE;
        } catch (WriteoffBonusInCorpPaymentException $exc) {
            Yii::error($paramStr . $exc->getMessage(), $category);
            $exception = $exc;
            $response  = self::WRITEOFF_BONUS_IN_CORP_PAYMENT_RESPONSE;
        } catch (BlockedCompanyException $exc) {
            $exception = $exc;
            $response  = self::BLOCKED_COMPANY_RESPONSE;
        } catch (NotFoundException $exc) {
            Yii::error($paramStr . $exc->getMessage(), $category);
            $exception = $exc;
            $response  = self::NOT_FOUND_RESPONSE;
        } catch (NotSupportedException $exc) {
            $response  = self::NOT_SUPPORTED_RESPONSE;
            $exception = $exc;
        } catch (ForbiddenBonusTransactionException $exc) {
            Yii::warning($paramStr . $exc->getMessage(), $category);
            $exception = $exc;
            $response  = self::FORBIDDEN_BONUS_TRANSACTION_RESPONSE;
        } catch (HeldPaymentException $exc) {
            Yii::warning($paramStr . $exc->getMessage(), $category);
            $exception = $exc;
            $response  = self::HELD_PAYMENT_RESPONSE;
        } catch (PaymentAlreadyExistsException $exc) {
            Yii::warning($paramStr . $exc->getMessage(), $category);
            $exception = $exc;
            $response  = self::PAYMENT_ALREADY_EXISTS_RESPONSE;
        } catch (InvalidBonusValueException $exc) {
            Yii::error($paramStr . $exc->getMessage(), $category);
            $exception = $exc;
            $response  = self::INVALID_BONUS_VALUE_RESPONSE;
        } catch (InvalidSumException $exc) {
            Yii::error($paramStr . $exc->getMessage(), $category);
            $exception = $exc;
            $response  = self::INVALID_SUM_RESPONSE;
        } catch (CardPaymentFailedException $exc) {
            Yii::error($exc);
            $exception = $exc;
            $response  = self::CARD_PAYMENT_FAILED_RESPONSE;
        } catch (\Exception $exc) {
            Yii::error($paramStr . $exc->getMessage(), $category);
            Yii::error($exc->getTraceAsString(), $category);
            $exception = $exc;
            $response  = self::SYSTEM_ERROR_RESPONSE;
        }

        app()->logger->log("Error: " . $exception->getMessage());
        $dbTransaction->rollBack();

        if ($response != self::SUCCESS_RESPONSE && isset($transaction)) {
            try {
                $transaction->afterRollback();
            } catch (\Exception $exc) {
                Yii::error($exc, $category);
            }
        }

        app()->logger->log('Transaction failed');

        return $response;
    }

    private function publishNotification()
    {
        try {
            $producer = new CompleteTransactionProducer(new MQConnection(
                \Yii::$app->params['rabbitMQ.host'],
                \Yii::$app->params['rabbitMQ.port'],
                \Yii::$app->params['rabbitMQ.user'],
                \Yii::$app->params['rabbitMQ.password'],
                \Yii::$app->params['rabbitMQ.virtualHost']), new NotificationLogger('billing'));

            $producer->notify($this->transaction_id);
        } catch (\Exception $ex) {
            app()->logger->log("Publish complete transaction notification error: transactionId={$this->transaction_id}, error={$ex->getMessage()}");
        }
    }
}
