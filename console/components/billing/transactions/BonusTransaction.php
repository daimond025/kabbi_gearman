<?php

namespace console\components\billing\transactions;

use console\components\billing\exceptions\ForbiddenBonusTransactionException;
use console\components\billing\exceptions\NotFoundException;

use console\components\billing\Account;
use console\components\billing\models\BonusSystemService;
use console\components\billing\models\PushNotification;

class BonusTransaction extends Transaction
{
    /**
     * Client account
     * @var Account
     */
    protected $clientAccount;

    /**
     * Client bonus account
     * @var Account
     */
    protected $clientBonusAccount;

    /**
     * System bonus account
     * @var Account
     */
    protected $systemBonusAccount;

    /**
     * @var BonusSystemService
     */
    private $bonusSystemService;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'type_id',
                    'sender_acc_id',
                    'receiver_acc_id',
                    'sum',
                    'payment_method',
                    'city_id',
                ],
                'required',
            ],
            [
                'type_id',
                'in',
                'range' => [
                    self::DEPOSIT_TYPE,
                    self::WITHDRAWAL_TYPE,
                    self::ORDER_PAYMENT_TYPE,
                ],
            ],
            [['type_id', 'sender_acc_id', 'receiver_acc_id', 'city_id'], 'integer'],
            [
                ['comment', 'payment_method', 'terminal_transact_number', 'terminal_transact_date'],
                'string',
                'max' => 255,
            ],
            [['sum'], 'number'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        if ($this->bonusSystemService === null) {
            $this->bonusSystemService = \Yii::createObject(BonusSystemService::className());
        }
    }

    public function initData()
    {
        app()->logger->log('Transaction type: Bonus');

        if (empty($this->currency_id)) {
            throw new NotFoundException('Not specified transaction currency');
        }

        //        if (!$this->bonusSystemService->canExecuteBonusTransaction($this->tenant_id)) {
        //            throw new ForbiddenBonusTransactionException('Bonus transaction forbidden');
        //        }

        $this->sum = $this->getTruncatedSum($this->sum, $this->currency_id);

        $this->clientAccount = $this->findAccountAndCreateIfNotExists(
            $this->sender_owner_id,
            $this->tenant_id,
            Account::CLIENT_KIND,
            Account::PASSIVE_TYPE,
            $this->currency_id);

        $clientAccountKindId      = Account::getClientBonusKind($this->tenant_id);
        $this->clientBonusAccount = $this->findAccountAndCreateIfNotExists(
            $this->sender_owner_id,
            $this->tenant_id,
            $clientAccountKindId,
            Account::PASSIVE_TYPE,
            $this->currency_id);

        $systemAccountKindId      = Account::getSystemBonusKind($this->tenant_id);
        $this->systemBonusAccount = $this->findAccountAndCreateIfNotExists(
            0,
            $this->tenant_id,
            $systemAccountKindId,
            Account::ACTIVE_TYPE,
            $this->currency_id);

        $this->sender_acc_id   = $this->clientBonusAccount->account_id;
        $this->receiver_acc_id = $this->systemBonusAccount->account_id;
        $this->city_id         = $this->clientBonusAccount->getEntityCityId();
        $this->payment_method  = self::BONUS_PAYMENT;
    }

    /**
     * Execute transaction operation
     * @throws \Exception
     */
    protected function doTransactOperation()
    {
        $operationType = $this->getOperationType($this->type_id);

        $ownerData = $this->getOwnerData($this->clientBonusAccount->account_id,
            $this->clientBonusAccount->acc_kind_id);

        $this->createOperation(
            $operationType,
            $this->clientBonusAccount->account_id,
            $this->transaction_id,
            $this->sum,
            $this->clientBonusAccount->balance,
            $this->comment,
            $ownerData['name']
        );
        app()->logger->log("Operation (Refill bonus balance): " . json_encode([
                'type_id'     => $operationType,
                'account_id'  => $this->clientBonusAccount->account_id,
                'acc_kind_id' => $this->clientBonusAccount->acc_kind_id,
                'sum'         => $this->sum,
            ]));

        $this->createOperation(
            $operationType,
            $this->systemBonusAccount->account_id,
            $this->transaction_id,
            $this->sum,
            $this->systemBonusAccount->balance,
            $this->comment
        );

        app()->logger->log("Operation (Refill bonus balance [SYSTEM]): " . json_encode([
                'type_id'    => $operationType,
                'account_id' => $this->systemBonusAccount->account_id,
                'sum'        => $this->sum,
            ]));

        $this->clientBonusAccount->refresh();
        $this->sender_acc_balance = $this->clientBonusAccount->balance;

        $balance      = $this->getTruncatedSum($this->clientAccount->balance, $this->currency_id);
        $bonusBalance = $this->getTruncatedSum($this->clientBonusAccount->balance, $this->currency_id);

        $this->updatePushNotification(
            $this->tenant_id,
            $this->getTenantLogin(),
            PushNotification::TYPE_APP_CLIENT,
            $ownerData['device'],
            $ownerData['token'],
            $ownerData['lang'],
            $this->getCurrency(),
            $balance,
            $bonusBalance,
            empty($ownerData['appId']) ? null : $ownerData['appId']);
    }

}
