<?php

namespace console\components\billing\transactions;

use bonusSystem\BonusSystem;
use console\components\billing\exceptions\BlockedCompanyException;
use console\components\billing\models\Company;
use console\components\db\ReconnectTrait;
use console\components\billing\models\StripeHelper;
use credit\CreditServiceFactory;
use paymentGate\exceptions\ProfileNotFoundException;
use paymentGate\ProfileService;
use promoCode\models\PromoBonus;
use promoCode\PromoCodeService;
use referralSystem\exceptions\BonusForOrderNotFoundException;
use referralSystem\models\OrderRepository;
use referralSystem\models\ReferralSystemRepository;
use referralSystem\ReferralSystemService;
use Yii;
use yii\di\Container;
use yii\base\Exception;
use console\components\billing\exceptions\PaymentAlreadyExistsException;
use console\components\billing\exceptions\NotFoundException;
use console\components\billing\exceptions\NoMoneyException;
use console\components\billing\exceptions\NoBonusForPaymentException;
use console\components\billing\exceptions\WriteoffBonusInCorpPaymentException;
use console\components\billing\exceptions\InvalidBonusValueException;
use console\components\billing\exceptions\InvalidSumException;
use console\components\billing\exceptions\NoBalanceChangesException;
use console\components\billing\exceptions\InvalidCardPaymentParamsException;
use console\components\billing\exceptions\CardPaymentFailedException;
use console\components\billing\Operation;
use console\components\billing\Account;
use console\components\billing\models\Bonus;
use console\components\billing\models\Order;
use console\components\billing\models\bankCard\Payment;
use console\components\billing\models\bankCard\Profile;
use console\components\billing\models\bankCard\CardPayment;

class OrderTransaction extends Transaction
{
    use ReconnectTrait;
    /**
     * @var Order
     */
    private $order;

    /**
     * @var Account
     */
    private $systemAccount;
    /**
     * @var Account
     */
    private $systemBonusAccount;
    /**
     * @var Account
     */
    private $clientAccount;
    /**
     * @var Account
     */
    private $clientBonusAccount;
    /**
     * @var Account
     */
    private $referrerAccount;
    /**
     * @var Account
     */
    private $referrerBonusAccount;
    /**
     * @var Account
     */
    private $workerAccount;

    private $_worker_push_lang;

    /* @var boolean */
    private $needPushToClient = false;
    /* @var boolean */
    private $needPushToWorker = false;
    /* @var boolean */
    private $needPushToReferrer = false;
    /* @var boolean */
    private $emptySummaryCost = false;

    /**
     * @var float
     */
    protected $payment_sum;

    /**
     * @var string
     */
    protected $payment_order_id;

    /**
     * @var Container
     */
    public $container;

    /**
     * @var BonusSystem
     */
    private $bonusSystem;

    /* @var float */
    private $summaryCost;
    /* @var float */
    private $bonus;
    /* @var float */
    private $commission;
    /* @var float */
    private $surcharge;
    /* @var float */
    private $tax;

    /* @var CreditServiceFactory */
    private $creditServiceFactory;

    /**
     * @var string
     */
    private $paymentGateProfile;

    public function __construct(CreditServiceFactory $creditFactory, array $config = [])
    {
        $this->creditServiceFactory = $creditFactory;

        parent::__construct($config);
    }

    public function init()
    {
        app()->logger->log('Transaction type: Order');

        $this->order = $this->getOrder();
        if (empty($this->order)) {
            throw new NotFoundException('Не найден заказ с №' . $this->order_id);
        }

        if (Order::STATUS_ID_COMPLETE_PAID === (int)$this->order->status_id) {
            throw new PaymentAlreadyExistsException("Оплата заказа №{$this->order_id} уже произведена");
        }

        if (empty($this->order->currency_id)) {
            throw new NotFoundException('Not specified transaction currency');
        }

        //Получение метода оплаты
        $this->payment_method = $this->getPaymentMethod($this->order->payment);
        if (empty($this->payment_method)) {
            throw new NotFoundException('Отсутствует тип платежа');
        }

        app()->logger->log("[{$this->payment_method}]");

        //Сумма транзакции
        $this->sum = $this->getTruncatedSum(
            (float)$this->order->orderDetailCost->summary_cost,
            $this->order->currency_id);

        if ($this->sum < 0) {
            throw new InvalidSumException('Invalid sum of payment');
        }

        if (!$this->isValidBonusValue()) {
            throw new InvalidBonusValueException();
        }

        $this->workerAccount = $this->findAccountAndCreateIfNotExists(
            $this->order->worker_id,
            $this->order->tenant_id,
            Account::WORKER_KIND,
            Account::PASSIVE_TYPE,
            $this->order->currency_id);

        $this->clientAccount = $this->findAccountAndCreateIfNotExists(
            $this->order->client_id,
            $this->order->tenant_id,
            Account::CLIENT_KIND,
            Account::PASSIVE_TYPE,
            $this->order->currency_id);

        $this->systemAccount = $this->findAccountAndCreateIfNotExists(
            0,
            $this->order->tenant_id,
            Account::SYSTEM_KIND,
            Account::ACTIVE_TYPE,
            $this->order->currency_id);

        $clientAccountKindId = Account::getClientBonusKind($this->order->tenant_id);
        $this->clientBonusAccount = $this->findAccountAndCreateIfNotExists(
            $this->order->client_id,
            $this->order->tenant_id,
            $clientAccountKindId,
            Account::PASSIVE_TYPE,
            $this->order->currency_id);

        $systemAccountKindId = Account::getSystemBonusKind($this->order->tenant_id);
        $this->systemBonusAccount = $this->findAccountAndCreateIfNotExists(
            0,
            $this->order->tenant_id,
            $systemAccountKindId,
            Account::ACTIVE_TYPE,
            $this->order->currency_id);

        $this->receiver_acc_id = $this->workerAccount->account_id;
        $this->receiver_acc_balance = $this->workerAccount->balance;

        $this->system_acc_id = $this->systemAccount->account_id;
        $this->system_balance = $this->systemAccount->balance;

        $this->type_id = self::ORDER_PAYMENT_TYPE;
        $this->city_id = $this->order->city_id;

        $this->tenant_id = $this->order->tenant_id;
        $this->currency_id = $this->order->currency_id;

        $this->bonusSystem = \Yii::createObject(BonusSystem::class, [$this->order->tenant_id]);
        $this->setDependencies();

        $this->summaryCost = empty($this->order->orderDetailCost->summary_cost)
            ? 0 : $this->getTruncatedSum((float)$this->order->orderDetailCost->summary_cost, $this->currency_id);
        $this->bonus = empty($this->order->orderDetailCost->bonus)
            ? 0 : $this->getTruncatedSum((float)$this->order->orderDetailCost->bonus, $this->currency_id);
        $this->commission = empty($this->order->orderDetailCost->commission)
            ? 0 : $this->getTruncatedSum((float)$this->order->orderDetailCost->commission, $this->currency_id);
        $this->surcharge = empty($this->order->orderDetailCost->surcharge)
            ? 0 : $this->getTruncatedSum((float)$this->order->orderDetailCost->surcharge, $this->currency_id);
        $this->tax = empty($this->order->orderDetailCost->tax)
            ? 0 : $this->getTruncatedSum((float)$this->order->orderDetailCost->tax, $this->currency_id);
    }

    /**
     * Setup dependencies
     */
    protected function setDependencies()
    {
        $this->container = new Container();

        $this->container->setSingleton(
            'payment', Payment::className(), [['requestId' => $this->request_id]]);

        $this->container->setSingleton(
            'profile', Profile::className(), [['requestId' => $this->request_id]]);
    }

    public function beforeSave($insert)
    {
        if (in_array($this->payment_method, [
                self::PERSONAL_ACCOUNT_PAYMENT,
                self::CORP_BALANCE_PAYMENT,
            ], false)
            || (in_array($this->payment_method, [
                    self::CASH_PAYMENT,
                    self::CARD_PAYMENT,
                ], false) && $this->isBonusPayment())
        ) {
            $creditLimit = 0;

            //Собственник счета
            if ($this->payment_method == self::CORP_BALANCE_PAYMENT) {
                $ownerAccountId = $this->order->company_id;
                $this->sender_acc_kind_id = Account::COMPANY_KIND;

                $service = $this->creditServiceFactory->getCreditService(
                    CreditServiceFactory::TYPE_COMPANY, $ownerAccountId);
                $creditLimit = $service->getLimit();
            } else {
                $ownerAccountId = $this->order->client_id;
                $this->sender_acc_kind_id = Account::CLIENT_KIND;
            }

            //Поиск счета отправителя
            $senderAccount = $this->findAccountAndCreateIfNotExists(
                $ownerAccountId,
                $this->order->tenant_id,
                $this->sender_acc_kind_id,
                Account::PASSIVE_TYPE,
                $this->order->currency_id);

            $this->sender_acc_id = $senderAccount->account_id;
            $this->sender_acc_balance = $senderAccount->balance;

            $this->comment = 'Payment for order №' . $this->order->order_number;

            $minorUnit = empty($this->order->currency->minor_unit) ? 0 : $this->order->currency->minor_unit;
            $balanceWithCreditLimit = $this->truncateSum($this->sender_acc_balance + $creditLimit, $minorUnit);

            $delta = 10 ** -$minorUnit;
            if ($this->summaryCost - $this->bonus > $delta && $balanceWithCreditLimit <= 0
                && in_array($this->payment_method, [self::PERSONAL_ACCOUNT_PAYMENT, self::CORP_BALANCE_PAYMENT], false)
            ) {
                throw new NoMoneyException('Недостаточно средств для совершения платежа');
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * Get order
     * @return Order
     */
    private function getOrder()
    {
        return Order::find()
            ->with([
                'orderDetailCost',
                'cardPayment',
                'currency',
                'client',
                'client.clientPhones',
            ])
            ->where(['order_id' => $this->order_id])
            ->one();
    }

    private function paymentMap()
    {
        return [
            'CASH'             => self::CASH_PAYMENT,
            'CARD'             => self::CARD_PAYMENT,
            'PERSONAL_ACCOUNT' => self::PERSONAL_ACCOUNT_PAYMENT,
            'CORP_BALANCE'     => self::CORP_BALANCE_PAYMENT,
        ];
    }

    private function getPaymentMethod($order_payment)
    {
        $payment_map = $this->paymentMap();

        return getValue($payment_map[$order_payment]);
    }

    /**
     * Return formatted error message
     *
     * @param array $errors
     *
     * @return string
     */
    private function getErrorMessage($errors)
    {
        return array_reduce($errors, function ($result, $item) {
            return $result . ' ' . implode("\n", $item);
        });
    }

    /**
     * Is bonus payment
     * @return boolean
     */
    private function isBonusPayment()
    {
        return !empty($this->order->orderDetailCost->bonus);
    }

    /**
     * Is valid bonus value (order_detail_cost column `bonus`)
     * @return boolean
     */
    private function isValidBonusValue()
    {
        $sum = (float)$this->order->orderDetailCost->summary_cost;
        $bonus = (float)$this->order->orderDetailCost->bonus;

        return empty($bonus) || $sum >= $bonus;
    }

    /**
     * @inheritdoc
     * @throws \console\components\billing\exceptions\CardPaymentFailedException
     * @throws \yii\base\InvalidConfigException
     */
    public function afterRollback()
    {
        if (isset($this->payment_order_id)) {
            /* @var $payment Payment */
            $payment = $this->container->get('payment');
            try {
                $payment->refund(
                    $this->order->tenant_id,
                    $this->paymentGateProfile,
                    $this->order->order_id,
                    $this->order->client_id,
                    null,
                    $this->order->cardPayment->payment_order_id);

                app()->logger->log('Operation (Refund payment [CARD]): ' . $this->order->cardPayment->payment_order_id);
            } catch (\Exception $ex) {
                throw new CardPaymentFailedException('An error occurred while returning the money for payment',
                    CardPaymentFailedException::ERROR_REFUND, $ex);
            }
        }
    }

    /**
     * Raise exception if writeoff bonus in corp payment
     * @throws WriteoffBonusInCorpPaymentException
     */
    private function checkWriteoffBonusInCorpPayment()
    {
        if (!empty($this->order->orderDetailCost->bonus)
            && $this->payment_method === self::CORP_BALANCE_PAYMENT
        ) {
            throw new WriteoffBonusInCorpPaymentException('Write-off bonus in corp payment');
        }
    }

    /**
     * @throws BlockedCompanyException
     */
    private function checkCompanyOfCorpPayment()
    {
        if ($this->payment_method === self::CORP_BALANCE_PAYMENT) {
            $company = Company::findOne($this->order->company_id);
            if ($company !== null && (int)$company->block === 1) {
                throw new BlockedCompanyException('Company is blocked');
            }
        }
    }

    /**
     * Need refill bonus?
     * @return boolean
     */
    private function isNeedRefillBonus()
    {
        return in_array($this->payment_method, [
            self::CASH_PAYMENT,
            self::CARD_PAYMENT,
            self::PERSONAL_ACCOUNT_PAYMENT,
        ], false);
    }


    /**
     * Операции проведения оплаты заказа исполнителю.
     *
     * @param float $sum
     *
     * @throws Exception
     */
    private function payWorkerForOrder($sum)
    {
        $ownerData = $this->getOwnerData($this->sender_acc_id,
            $this->sender_acc_kind_id);

        //Списываем стоимость заказа с клиента
        $operation_1 = new Operation([
            'type_id'         => Operation::EXPENSES_TYPE,
            'account_id'      => $this->sender_acc_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $sum,
            'account_balance' => $this->sender_acc_balance,
            'comment'         => 'Debit for the order №' . $this->order->order_number,
            'owner_name'      => $ownerData['name'],
        ]);

        if (!$operation_1->save()) {
            Yii::error($operation_1->getErrors());
            throw new Exception('Ошибка проведения операции списания средств со счета клиента №' . $this->sender_acc_id . ' за заказ с ID = ' . $this->order->order_id);
        }

        app()->logger->log('Operation (Pay worker for order [CLIENT]): ' . json_encode([
                'type_id'    => Operation::EXPENSES_TYPE,
                'account_id' => $this->sender_acc_id,
                'acc_kid_id' => $this->sender_acc_kind_id,
                'sum'        => $sum,
            ]));

        if ($this->payment_method == self::PERSONAL_ACCOUNT_PAYMENT) {
            $this->needPushToClient = true;
        }

        $ownerData = $this->getOwnerData($this->receiver_acc_id, Account::WORKER_KIND);

        //Добавляем стоимость заказа исполнителю
        $operation_2 = new Operation([
            'type_id'         => Operation::INCOME_TYPE,
            'account_id'      => $this->receiver_acc_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $sum,
            'account_balance' => $this->receiver_acc_balance,
            'comment'         => 'Incoming for the order №' . $this->order->order_number,
            'owner_name'      => $ownerData['name'],
        ]);

        if (!$operation_2->save()) {
            Yii::error($operation_2->getErrors());
            throw new Exception('Ошибка проведения операции добавления средств исполнителю с ID = ' . $this->receiver_acc_id . ' за заказ с ID = ' . $this->order->order_id . ' на cчет №' . $this->receiver_acc_id);
        }

        app()->logger->log('Operation (Pay worker for order [WORKER]): ' . json_encode([
                'type_id'     => Operation::INCOME_TYPE,
                'account_id'  => $this->receiver_acc_id,
                'acc_kind_id' => Account::WORKER_KIND,
                'sum'         => $sum,
            ]));

        $this->needPushToWorker = false;

        $this->receiver_acc_balance += $sum;
    }

    /**
     * Writeoff tax
     *
     * @param float $tax
     *
     * @throws Exception
     * @throws \Exception
     */
    private function writeOffTax($tax)
    {
        if (empty($tax)) {
            return;
        }

        $ownerData = $this->getOwnerData($this->receiver_acc_id, Account::WORKER_KIND);

        //Списываем % за заказ со счета исполнителя
        $operation_1 = new Operation([
            'type_id'         => Operation::EXPENSES_TYPE,
            'account_id'      => $this->receiver_acc_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $tax,
            'account_balance' => $this->receiver_acc_balance,
            'comment'         => 'Debit tax for the order №' . $this->order->order_number,
            'owner_name'      => $ownerData['name'],
        ]);

        if (!$operation_1->save()) {
            Yii::error($operation_1->getErrors());
            throw new \Exception('Ошибка проведения операции списания налога со счета исполнителя №' . $this->receiver_acc_id . ' за заказ c ID = ' . $this->order->order_id);
        }
        app()->logger->log('Operation (Write-off tax): ' . json_encode([
                'type_id'     => Operation::EXPENSES_TYPE,
                'account_id'  => $this->receiver_acc_id,
                'acc_kind_id' => Account::WORKER_KIND,
                'tax'         => $tax,
            ]));

        $this->needPushToWorker = true;

        //Списываем % за заказ со счета система
        $operation_2 = new Operation([
            'type_id'         => Operation::EXPENSES_TYPE,
            'account_id'      => $this->system_acc_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $tax,
            'account_balance' => $this->system_balance,
            'comment'         => 'Debit tax for the order №' . $this->order->order_number . ' from account №' . $this->receiver_acc_id,
        ]);

        if (!$operation_2->save()) {
            Yii::error($operation_2->getErrors());
            throw new Exception('Ошибка проведения операции списания налога с системного счета №' . $this->system_acc_id);
        }
        app()->logger->log('Operation (Write-off tax [SYSTEM]): ' . json_encode([
                'type_id'    => Operation::EXPENSES_TYPE,
                'account_id' => $this->system_acc_id,
                'tax'        => $tax,
            ]));

        $this->receiver_acc_balance -= $tax;
        $this->system_balance -= $tax;
    }

    /**
     * Операции списания комиссии с исполнителя за заказ.
     *
     * @param float $commission
     *
     * @throws Exception
     * @throws \Exception
     */
    private function writeOffWorkerCommission($commission)
    {
        $ownerData = $this->getOwnerData($this->receiver_acc_id, Account::WORKER_KIND);

        //Списываем % за заказ со счета исполнителя
        $operation_1 = new Operation([
            'type_id'         => Operation::EXPENSES_TYPE,
            'account_id'      => $this->receiver_acc_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $commission,
            'account_balance' => $this->receiver_acc_balance,
            'comment'         => 'Debit commission for the order №' . $this->order->order_number,
            'owner_name'      => $ownerData['name'],
        ]);

        if (!$operation_1->save()) {
            Yii::error($operation_1->getErrors());
            throw new \Exception('Ошибка проведения операции списания коммисии со счета исполнителя №' . $this->receiver_acc_id . ' за заказ c ID = ' . $this->order->order_id);
        }
        app()->logger->log('Operation (Write-off worker commission): ' . json_encode([
                'type_id'     => Operation::EXPENSES_TYPE,
                'account_id'  => $this->receiver_acc_id,
                'acc_kind_id' => Account::WORKER_KIND,
                'commission'  => $commission,
            ]));

        $this->needPushToWorker = true;

        //Списываем % за заказ со счета система
        $operation_2 = new Operation([
            'type_id'         => Operation::EXPENSES_TYPE,
            'account_id'      => $this->system_acc_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $commission,
            'account_balance' => $this->system_balance,
            'comment'         => 'Debit commission for the order №' . $this->order->order_number . ' from account №' . $this->receiver_acc_id,
        ]);

        if (!$operation_2->save()) {
            Yii::error($operation_2->getErrors());
            throw new Exception('Ошибка проведения операции списания коммисии с системного счета №' . $this->system_acc_id);
        }
        app()->logger->log('Operation (Write-off worker commission [SYSTEM]): ' . json_encode([
                'type_id'    => Operation::EXPENSES_TYPE,
                'account_id' => $this->system_acc_id,
                'commission' => $commission,
            ]));

        $this->receiver_acc_balance -= $commission;
        $this->system_balance -= $commission;
    }

    /**
     * Операции начисления доплат исполнителю
     *
     * @param float $surcharge
     *
     * @throws Exception
     * @throws \Exception
     */
    private function depositSurcharge($surcharge)
    {
        if (empty($surcharge)) {
            return;
        } elseif ($surcharge < 0) {
            app()->logger->log("Incorrect sum of surcharge: {$surcharge}");

            return;
        }

        $ownerData = $this->getOwnerData($this->receiver_acc_id, Account::WORKER_KIND);

        $this->createOperation(
            Operation::INCOME_TYPE,
            $this->receiver_acc_id,
            $this->transaction_id,
            $surcharge,
            $this->receiver_acc_balance,
            'Deposit surcharge for the order №' . $this->order->order_number,
            $ownerData['name']
        );
        app()->logger->log('Operation (Deposit surcharge): ' . json_encode([
                'type_id'     => Operation::INCOME_TYPE,
                'account_id'  => $this->receiver_acc_id,
                'acc_kind_id' => Account::WORKER_KIND,
                'surcharge'   => $surcharge,
            ]));

        $this->createOperation(
            Operation::INCOME_TYPE,
            $this->system_acc_id,
            $this->transaction_id,
            $surcharge,
            $this->system_balance,
            'Deposit surcharge for the order №' . $this->order->order_number . ' from account №' . $this->receiver_acc_id
        );
        app()->logger->log('Operation (Deposit surcharge [SYSTEM]): ' . json_encode([
                'type_id'    => Operation::INCOME_TYPE,
                'account_id' => $this->system_acc_id,
                'surcharge'  => $surcharge,
            ]));

        $this->needPushToWorker = true;

        $this->receiver_acc_balance += $surcharge;
        $this->system_balance += $surcharge;
    }

    /**
     * Register order payment in UDS Game
     * @throws \yii\db\Exception
     */
    private function registerOrderPayment()
    {
        if (!$this->bonusSystem->isRegisteredOrder($this->order_id)) {
            return;
        }

        app()->logger->log('UDS Game transaction started');

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if (!empty($this->bonus)) {
                $comment = '(UDS Game) Refill bonuses for the order №' . $this->order->order_number;

                $ownerData = $this->getOwnerData($this->clientBonusAccount->account_id,
                    $this->clientBonusAccount->acc_kind_id);

                $this->createOperation(
                    Operation::INCOME_TYPE,
                    $this->clientBonusAccount->account_id,
                    $this->transaction_id,
                    $this->bonus,
                    $this->clientBonusAccount->balance,
                    $comment,
                    $ownerData['name']
                );
                app()->logger->log('Operation (Refill bonus for order from UDS Game): ' . json_encode([
                        'type_id'     => Operation::EXPENSES_TYPE,
                        'account_id'  => $this->clientBonusAccount->account_id,
                        'acc_kind_id' => $this->clientBonusAccount->acc_kind_id,
                        'sum'         => $this->bonus,
                    ]));

                $this->createOperation(
                    Operation::INCOME_TYPE,
                    $this->systemBonusAccount->account_id,
                    $this->transaction_id,
                    $this->bonus,
                    $this->systemBonusAccount->balance,
                    $comment
                );
                app()->logger->log('Operation (Refill bonus for order from UDS Game[SYSTEM]): ' . json_encode([
                        'type_id'    => Operation::EXPENSES_TYPE,
                        'account_id' => $this->systemBonusAccount->account_id,
                        'sum'        => $this->bonus,
                    ]));

                $this->clientBonusAccount->refresh();
                $this->systemBonusAccount->refresh();

                $this->needPushToClient = true;
            }

            $this->bonusSystem->registerOrderPayment($this->order_id, $this->summaryCost, $this->bonus);
            app()->logger->log('Register order payment (UDS Game): ' . json_encode([
                    'order_id' => $this->order_id,
                    'sum'      => $this->summaryCost,
                    'bonus'    => $this->bonus,
                ]));

            $transaction->commit();

            app()->logger->log('UDS Game transaction completed');
        } catch (\Exception $ex) {
            $previousException = $ex->getPrevious();
            $transaction->rollBack();

            \Yii::error($ex);
            app()->logger->log('Error: '
                . $ex->getMessage()
                . ($previousException !== null ? ' (' . $previousException->getMessage() . ')' : ''));
            app()->logger->log('UDS Game transaction failed');
        }
    }

    /**
     * Writeoff bonus from client account
     * @throws NoBonusForPaymentException
     * @throws \Exception
     */
    private function writeoffBonusFromClientAccount()
    {
        $comment = 'Write-off bonuses for the order №' . $this->order->order_number;

        $sum = $this->bonus;

        // убрал проверку т.к. была ошибка при расчете суммы бонусов к оплате,
        // данная сумма могла превышать суммы бонусов на балансе
        if ($sum > $this->clientBonusAccount->balance) {
            $message = 'Not enough bonus for payment: ' . json_encode([
                    'order_id' => $this->order->order_id,
                    'sum'      => $sum,
                    'balance'  => $this->clientBonusAccount->balance,
                ]);
            app()->logger->log($message);
            \Yii::error($message);
            // throw new NoBonusForPaymentException("Недостаточно бонусов на счете клиента (sum = {$this->sum}, balance = {$this->clientBonusAccount->balance})");
        }

        $ownerData = $this->getOwnerData($this->clientBonusAccount->account_id,
            $this->clientBonusAccount->acc_kind_id);

        $this->createOperation(
            Operation::EXPENSES_TYPE,
            $this->clientBonusAccount->account_id,
            $this->transaction_id,
            $sum,
            $this->clientBonusAccount->balance,
            $comment,
            $ownerData['name']
        );
        app()->logger->log('Operation (Write-off bonus for order): ' . json_encode([
                'type_id'     => Operation::EXPENSES_TYPE,
                'account_id'  => $this->clientBonusAccount->account_id,
                'acc_kind_id' => $this->clientBonusAccount->acc_kind_id,
                'sum'         => $sum,
            ]));

        $this->createOperation(
            Operation::EXPENSES_TYPE,
            $this->systemBonusAccount->account_id,
            $this->transaction_id,
            $sum,
            $this->systemBonusAccount->balance,
            $comment
        );
        app()->logger->log('Operation (Writeoff bonus for order [SYSTEM]): ' . json_encode([
                'type_id'    => Operation::EXPENSES_TYPE,
                'account_id' => $this->systemBonusAccount->account_id,
                'sum'        => $sum,
            ]));

        $this->clientBonusAccount->refresh();
        $this->systemBonusAccount->refresh();

        $this->needPushToClient = true;
    }

    /**
     * Deposit for the order which was paid by card
     *
     * @param float $sum
     *
     * @throws \Exception
     */
    private function depositMoneyToWorkerAccount($sum)
    {
        $comment = 'Deposit for the order that was paid by card №'
            . $this->order->order_number;

        $ownerData = $this->getOwnerData($this->receiver_acc_id,
            Account::WORKER_KIND);

        $this->createOperation(
            Operation::INCOME_TYPE,
            $this->receiver_acc_id,
            $this->transaction_id,
            $sum,
            $this->receiver_acc_balance,
            $comment,
            $ownerData['name']
        );
        app()->logger->log('Operation (Deposit money to worker account): ' . json_encode([
                'type_id'     => Operation::INCOME_TYPE,
                'account_id'  => $this->receiver_acc_id,
                'acc_kind_id' => Account::WORKER_KIND,
                'sum'         => $sum,
            ]));

        $this->createOperation(
            Operation::INCOME_TYPE,
            $this->system_acc_id,
            $this->transaction_id,
            $sum,
            $this->system_balance,
            $comment
        );
        app()->logger->log('Operation (Deposit money to worker account [SYSTEM]): ' . json_encode([
                'type_id'    => Operation::INCOME_TYPE,
                'account_id' => $this->system_acc_id,
                'sum'        => $sum,
            ]));

        $this->receiver_acc_balance += $sum;
        $this->system_balance += $sum;
    }

    /**
     * Deposit for the order which was partial paid by bonus
     *
     * @throws \Exception
     */
    private function depositMoneyToClientAccount()
    {
        $comment = 'Transfer of funds from the bonus account';

        $sum = $this->bonus;

        $ownerData = $this->getOwnerData($this->sender_acc_id,
            $this->sender_acc_kind_id);

        $this->createOperation(
            Operation::INCOME_TYPE,
            $this->sender_acc_id,
            $this->transaction_id,
            $sum,
            $this->sender_acc_balance,
            $comment,
            $ownerData['name']
        );
        app()->logger->log('Operation (Deposit money from bonus account): ' . json_encode([
                'type_id'     => Operation::INCOME_TYPE,
                'account_id'  => $this->sender_acc_id,
                'acc_kind_id' => $this->sender_acc_kind_id,
                'sum'         => $sum,
            ]));

        $this->createOperation(
            Operation::INCOME_TYPE,
            $this->system_acc_id,
            $this->transaction_id,
            $sum,
            $this->system_balance,
            $comment
        );
        app()->logger->log('Operation (Deposit money from bonus account [SYSTEM]): ' . json_encode([
                'type_id'    => Operation::INCOME_TYPE,
                'account_id' => $this->system_acc_id,
                'sum'        => $sum,
            ]));

        $this->sender_acc_balance += $sum;
        $this->system_balance += $sum;
    }

    /**
     * Create bonus operations
     *
     * @param float $bonusValue
     *
     * @return boolean
     * @throws \Exception
     */
    protected function createBonusOperations($bonusValue)
    {
        app()->logger->log('Create bonus transaction');
        self::reconnectIfNeeded(app()->db);
        $dbTransaction = app()->db->beginTransaction();
        try {
            $comment = 'Bonuses for the order №' . $this->order->order_number;

            $ownerData = $this->getOwnerData($this->clientBonusAccount->account_id,
                $this->clientBonusAccount->acc_kind_id);

            $this->createOperation(
                Operation::INCOME_TYPE,
                $this->clientBonusAccount->account_id,
                $this->transaction_id,
                $bonusValue,
                $this->clientBonusAccount->balance,
                $comment,
                $ownerData['name']
            );
            app()->logger->log('Operation (Refill bonus for order): ' . json_encode([
                    'type_id'     => Operation::INCOME_TYPE,
                    'account_id'  => $this->clientBonusAccount->account_id,
                    'acc_kind_id' => $this->clientBonusAccount->acc_kind_id,
                    'sum'         => $bonusValue,
                ]));

            $this->createOperation(
                Operation::INCOME_TYPE,
                $this->systemBonusAccount->account_id,
                $this->transaction_id,
                $bonusValue,
                $this->systemBonusAccount->balance,
                $comment
            );
            app()->logger->log('Operation (Refill bonus for order [SYSTEM]): ' . json_encode([
                    'type_id'    => Operation::INCOME_TYPE,
                    'account_id' => $this->systemBonusAccount->account_id,
                    'sum'        => $bonusValue,
                ]));

            $this->clientBonusAccount->refresh();
            $this->systemBonusAccount->refresh();

            $this->needPushToClient = true;

            $dbTransaction->commit();
            app()->logger->log('Bonus transaction completed');

            return true;
        } catch (Exception $ex) {
            Yii::error($ex, 'bonus');
            $dbTransaction->rollBack();

            app()->logger->log('Error: ' . $ex->getMessage());
            app()->logger->log('Bonus transaction failed');

            return false;
        }
    }

    /**
     * Refill bonus to client account
     *
     * @throws \Exception
     */
    private function refillBonus()
    {
        $bonus = new Bonus;

        $summaryCost = $this->summaryCost - $this->bonus;

        $clientBonus = $bonus->getClientBonus(
            $this->order->tenant_id,
            $this->order->city_id,
            $this->order->tariff_id,
            $summaryCost,
            $this->order->order_time);

        if (!empty($clientBonus)) {
            $bonusValue = $bonus->calculateBonus(
                $this->order->order_id,
                $this->order->client_id,
                $this->order_status_id,
                $this->order->device,
                $this->order->client_device_token,
                $this->order->order_time,
                $summaryCost,
                $clientBonus);

            $bonusValue = $this->getTruncatedSum($bonusValue, $this->currency_id);

            if (!empty($this->order->orderDetailCost)) {
                $this->order->orderDetailCost->refill_bonus_id = $clientBonus->bonus_id;
                $this->order->orderDetailCost->refill_bonus = $bonusValue;
                $this->order->orderDetailCost->save();
            }

            if (!empty($bonusValue)) {
                return $this->createBonusOperations($bonusValue);
            }
        }

        return false;
    }

    private function refillPromoBonus()
    {
        $service = PromoCodeService::getInstance();
        $bonuses = $service->getPromoBonuses($this->order_id);

        if (empty($bonuses)) {
            return false;
        }

        app()->logger->log('Create promo code bonus transaction');
        self::reconnectIfNeeded(app()->db);
        $dbTransaction = app()->db->beginTransaction();

        try {
            foreach ($bonuses as $bonus) {
                /* @var $bonus PromoBonus */
                $bonusValue = $this->getTruncatedSum($bonus->getBonus(), $this->currency_id);

                if ($bonusValue > 0) {
                    $comment = 'Promo code bonus for the order №' . $this->order->order_number;

                    $accountKindId = $bonus->getSubjectType() === PromoBonus::SUBJECT_TYPE_CLIENT
                        ? Account::CLIENT_BONUS_KIND : Account::WORKER_KIND;
                    $account = $this->findAccountAndCreateIfNotExists($bonus->getSubjectId(), $this->tenant_id,
                        $accountKindId, Account::PASSIVE_TYPE, $this->currency_id);
                    $subject = $this->getOwnerData($account->account_id, $account->acc_kind_id);

                    $this->createOperation(
                        Operation::INCOME_TYPE,
                        $account->account_id,
                        $this->transaction_id,
                        $bonusValue,
                        $account->balance,
                        $comment,
                        $subject['name']
                    );
                    app()->logger->log('Operation (Refill promo code bonus for order): ' . json_encode([
                            'type_id'     => Operation::INCOME_TYPE,
                            'account_id'  => $account->account_id,
                            'acc_kind_id' => $account->acc_kind_id,
                            'sum'         => $bonusValue,
                        ]));

                    if ($this->clientBonusAccount->account_id === $account->account_id) {
                        $this->needPushToClient = true;
                        $this->clientBonusAccount->refresh();
                    } elseif ($bonus->getSubjectType() === PromoBonus::SUBJECT_TYPE_WORKER) {
                        $account->refresh();
                        $balance = $this->getTruncatedSum($account->balance, $this->currency_id);

                        $this->updatePushNotification(
                            $this->tenant_id,
                            $this->getTenantLogin(),
                            $this->getApplicationType($account->acc_kind_id),
                            $subject['device'],
                            $subject['token'],
                            $subject['lang'],
                            $this->getCurrency(),
                            $balance);
                    }

                    $this->createOperation(
                        Operation::INCOME_TYPE,
                        $this->systemBonusAccount->account_id,
                        $this->transaction_id,
                        $bonusValue,
                        $this->systemBonusAccount->balance,
                        $comment
                    );
                    app()->logger->log('Operation (Refill promo code bonus for order) [SYSTEM]): ' . json_encode([
                            'type_id'    => Operation::INCOME_TYPE,
                            'account_id' => $this->systemBonusAccount->account_id,
                            'sum'        => $bonusValue,
                        ]));
                    $this->systemBonusAccount->refresh();
                }

                $service->setPromoBonusCompleted($bonus);
            }

            $dbTransaction->commit();
            app()->logger->log('Promo code bonus transaction was completed');

            return true;
        } catch (\Exception $ex) {
            $dbTransaction->rollBack();
            Yii::error("Promo code bonus transaction was failed: orderId={$this->order_id}, error=\"{$ex->getMessage()}\"",
                'bonus');

            app()->logger->log("Promo code bonus transaction was failed: error={$ex->getMessage()}");

            return false;
        }
    }

    /**
     * Refill referral bonus
     *
     * @throws \Exception
     */
    private function refillReferralBonus()
    {
        $orderRepository = new OrderRepository();
        $referralSystemRepository = new ReferralSystemRepository(\Yii::$app->getDb());
        $service = new ReferralSystemService($orderRepository, $referralSystemRepository);

        try {
            $bonus = $service->getCalculatedBonus($this->order_id);
        } catch (BonusForOrderNotFoundException $ignore) {
            return false;
        }

        app()->logger->log('Create referral bonus transaction');
        self::reconnectIfNeeded(app()->db);
        $dbTransaction = app()->db->beginTransaction();
        try {
            $comment = 'Referral bonus for the order №' . $this->order->order_number;

            $bonusValue = $this->getTruncatedSum(
                $bonus->getReferrerBonus()->getBonusValue()->getAmount(),
                $bonus->getReferrerBonus()->getBonusValue()->getCurrencyId());
            if ($bonusValue > 0) {
                $this->referrerAccount = $this->findAccountAndCreateIfNotExists(
                    $bonus->getReferrerBonus()->getClientId(), $this->tenant_id, Account::CLIENT_KIND,
                    Account::PASSIVE_TYPE, $this->currency_id);
                $this->referrerBonusAccount = $this->findAccountAndCreateIfNotExists(
                    $bonus->getReferrerBonus()->getClientId(), $this->tenant_id, Account::CLIENT_BONUS_KIND,
                    Account::PASSIVE_TYPE, $this->currency_id);

                $referrer = $this->getOwnerData(
                    $this->referrerBonusAccount->account_id, $this->referrerBonusAccount->acc_kind_id);

                $this->createOperation(
                    Operation::INCOME_TYPE,
                    $this->referrerBonusAccount->account_id,
                    $this->transaction_id,
                    $bonusValue,
                    $this->referrerBonusAccount->balance,
                    $comment,
                    $referrer['name']
                );
                app()->logger->log('Operation (Refill referral bonus for order (referrer)): ' . json_encode([
                        'type_id'     => Operation::INCOME_TYPE,
                        'account_id'  => $this->referrerBonusAccount->account_id,
                        'acc_kind_id' => $this->referrerBonusAccount->acc_kind_id,
                        'sum'         => $bonusValue,
                    ]));

                $this->createOperation(
                    Operation::INCOME_TYPE,
                    $this->systemBonusAccount->account_id,
                    $this->transaction_id,
                    $bonusValue,
                    $this->systemBonusAccount->balance,
                    $comment
                );
                app()->logger->log('Operation (Refill referral bonus for order (referrer) [SYSTEM]): ' . json_encode([
                        'type_id'    => Operation::INCOME_TYPE,
                        'account_id' => $this->systemBonusAccount->account_id,
                        'sum'        => $bonusValue,
                    ]));

                $this->needPushToReferrer = true;
                $this->referrerBonusAccount->refresh();
                $this->systemBonusAccount->refresh();
            }

            $bonusValue = $this->getTruncatedSum(
                $bonus->getReferralBonus()->getBonusValue()->getAmount(),
                $bonus->getReferralBonus()->getBonusValue()->getCurrencyId());
            if ($bonusValue > 0) {
                $referral = $this->getOwnerData($this->clientBonusAccount->account_id,
                    $this->clientBonusAccount->acc_kind_id);

                $this->createOperation(
                    Operation::INCOME_TYPE,
                    $this->clientBonusAccount->account_id,
                    $this->transaction_id,
                    $bonusValue,
                    $this->clientBonusAccount->balance,
                    $comment,
                    $referral['name']
                );
                app()->logger->log('Operation (Refill referral bonus for order (referral)): ' . json_encode([
                        'type_id'     => Operation::INCOME_TYPE,
                        'account_id'  => $this->clientBonusAccount->account_id,
                        'acc_kind_id' => $this->clientBonusAccount->acc_kind_id,
                        'sum'         => $bonusValue,
                    ]));

                $this->createOperation(
                    Operation::INCOME_TYPE,
                    $this->systemBonusAccount->account_id,
                    $this->transaction_id,
                    $bonusValue,
                    $this->systemBonusAccount->balance,
                    $comment
                );
                app()->logger->log('Operation (Refill referral bonus for order (referral) [SYSTEM]): ' . json_encode([
                        'type_id'    => Operation::INCOME_TYPE,
                        'account_id' => $this->systemBonusAccount->account_id,
                        'sum'        => $bonusValue,
                    ]));

                $this->needPushToClient = true;
                $this->clientBonusAccount->refresh();
                $this->systemBonusAccount->refresh();
            }

            try {
                $service->setBonusCompleted($this->order_id);
            } catch (BonusForOrderNotFoundException $ignore) {
                return false;
            }

            $dbTransaction->commit();
            app()->logger->log('Referral bonus transaction was completed');

            return true;
        } catch (\Exception $ex) {
            $dbTransaction->rollBack();
            Yii::error($ex, 'bonus');

            app()->logger->log('Error: ' . $ex->getMessage());
            app()->logger->log('Referral bonus transaction was failed');

            return false;
        }
    }

    /**
     * Saving card payment information
     *
     * @param string $paymentOrderId
     * @param float  $paymentSum
     * @param string $paymentStatus
     *
     * @throws CardPaymentFailedException
     */
    private function saveCardPayment($paymentOrderId, $paymentSum, $paymentStatus)
    {
        $this->order->cardPayment->payment_order_id = $paymentOrderId;
        $this->order->cardPayment->payment_sum = $paymentSum;
        $this->order->cardPayment->payment_status = $paymentStatus;

        if (!$this->order->cardPayment->save()) {
            $error = implode('; ', $this->order->cardPayment->getFirstErrors());
            throw new CardPaymentFailedException("Error saving payment information: error=$error");
        }
    }

    /**
     * Truncate sum to currency minor unit
     *
     * @param float $sum
     * @param int   $currencyMinorUnit
     *
     * @return float
     */
    private function truncateSum($sum, $currencyMinorUnit = 0)
    {
        return floor($sum * (10 ** $currencyMinorUnit));
    }

    /**
     * Pay for order from card
     *
     * @param float  $sum
     * @param string $yandexAccountNumber
     *
     * @throws CardPaymentFailedException
     */
    private function payForOrderFromCard($sum, $yandexAccountNumber = null)
    {
        $this->order->cardPayment->to_pan = $yandexAccountNumber;

        /* @var $payment Payment */
        try {
            $payment = $this->container->get('payment');

            $amount = $this->truncateSum(
                $sum, empty($this->order->currency->minor_unit) ? 0 : $this->order->currency->minor_unit);

            $orderNumber = $this->order->order_number;
            $tenantId = $this->order->tenant_id;
            $clientId = $this->order->client_id;
            $workerId = $this->order->worker_id;

            $description = "Payment for order #{$orderNumber} (tenant #{$tenantId} client #{$clientId})";

            $params = [
                'phone' => empty($this->order->client->clientPhones[0]->value) ? null : $this->order->client->clientPhones[0]->value,
                'email' => empty($this->order->client->email) ? null : $this->order->client->email,
            ];
            $stripeAccount = StripeHelper::getStripeAccount($tenantId, $this->order->city_id, $this->order->worker);
            if ($stripeAccount !== null) {
                $params['stripeAccount'] = $stripeAccount;
            }

            $response = $payment->pay(
                $tenantId,
                $this->paymentGateProfile,
                $this->order->order_id,
                $clientId,
                $workerId,
                $this->order->cardPayment->pan,
                $this->order->cardPayment->to_pan,
                $amount,
                $orderNumber,
                $this->order->currency->numeric_code,
                $params,
                $description
            );

            app()->logger->log('Operation (Pay for order from card): ' . json_encode([
                    'description' => $description,
                    'pan'         => $this->order->cardPayment->pan,
                    'to_pan'      => $this->order->cardPayment->to_pan,
                    'amount'      => $amount,
                    'currency'    => $this->order->currency->numeric_code,
                ]));
        } catch (\Exception $ex) {
            throw new CardPaymentFailedException("Card payment error: {$ex->getMessage()}",
                CardPaymentFailedException::ERROR_PAY, $ex);
        }

        $this->payment_order_id = $response['orderId'];

        $this->saveCardPayment($this->payment_order_id, $sum, CardPayment::PAYMENT_STATUS_PAID);
    }

    /**
     * Getting profile of payment gate
     *
     * @param string $paygateProfile
     *
     * @return array|null
     * @throws CardPaymentFailedException
     */
    protected function getProfile($paygateProfile)
    {
        try {
            return $this->container->get('profile')->get($paygateProfile);
        } catch (\Exception $ex) {
            throw new CardPaymentFailedException('An error occurred while receiving profile',
                CardPaymentFailedException::ERROR_GET_PROFILE, $ex);
        }
    }

    /**
     * @param int $tenantId
     * @param int|null $cityId
     * @return string
     *
     * @throws ProfileNotFoundException
     * @throws \yii\db\Exception
     */
    private function getPaymentGateProfile(int $tenantId, ?int $cityId): string
    {
        $profileService = new ProfileService(\Yii::$app->getDb());

        return $profileService->getProfile($tenantId, $cityId);
    }

    /**
     * Транзакция с методом оплаты - CASH
     * @throws Exception
     * @throws \Exception
     */
    private function cashPayment()
    {
        if ($this->bonusSystem->isUDSGameBonusSystem()) {
            $this->registerOrderPayment();
        }

        if ($this->isBonusPayment()) {
            $this->writeoffBonusFromClientAccount();
            $this->depositMoneyToClientAccount();
            $this->payWorkerForOrder($this->bonus);
        }

        $this->writeOffWorkerCommission($this->commission);
        $this->writeOffTax($this->tax);
        $this->depositSurcharge($this->surcharge);
    }

    /**
     * Транзакция с методом оплаты - CARD
     * @throws InvalidCardPaymentParamsException
     * @throws CardPaymentFailedException
     * @throws \Exception
     * @throws \console\components\billing\exceptions\NoBonusForPaymentException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    private function cardPayment()
    {
        if (empty($this->order->cardPayment->pan)) {
            throw new InvalidCardPaymentParamsException('Pan is empty');
        }

        if ($this->bonusSystem->isUDSGameBonusSystem()) {
            $this->registerOrderPayment();
        }

        if ($this->isBonusPayment()) {
            $this->writeoffBonusFromClientAccount();
            $this->depositMoneyToClientAccount();
            $this->payWorkerForOrder($this->bonus);
        }

        $commission = $this->commission;

        $sum = $this->summaryCost - $this->bonus;
        if ($sum > 0) {
            $this->paymentGateProfile = $this->getPaymentGateProfile($this->tenant_id, $this->order->city_id);
            $profile = $this->getProfile($this->paymentGateProfile);

            $percent = empty($profile['commission']) ? 0 : (float)$profile['commission'];
            $bankCommission = $sum * ((float)$percent <= 0 ? 0 : (float)$percent) / 100;
            if ($commission <= $bankCommission) {
                $commission = $bankCommission;
            }

            $yandexAccountNumber = null;
            if (isset($profile['secureDeal']) && (int)$profile['secureDeal'] === 1) {
                $yandexAccountNumber = isset($this->order->worker->yandex_account_number)
                    ? $this->order->worker->yandex_account_number : null;

                if ($yandexAccountNumber === null) {
                    throw new InvalidCardPaymentParamsException("Worker doesn't have a yandex account number: tenantId={$this->tenant_id}, workerId={$this->order->worker_id}");
                }
            }

            if ($yandexAccountNumber === null) {
                $this->depositMoneyToWorkerAccount($sum);
            }

            $this->payForOrderFromCard($sum, $yandexAccountNumber);
        }

        $commission = $this->getTruncatedSum($commission, $this->currency_id);
        $this->writeOffWorkerCommission($commission);
        $this->writeOffTax($this->tax);
        $this->depositSurcharge($this->surcharge);
    }

    /**
     * Транзакция с методом оплаты - PERSONAL_ACCOUNT и CORP_BALANCE
     * @throws Exception
     * @throws \Exception
     */
    private function accountPayment()
    {
        $this->checkWriteoffBonusInCorpPayment();
        $this->checkCompanyOfCorpPayment();

        if ($this->bonusSystem->isUDSGameBonusSystem()) {
            $this->registerOrderPayment();
        }

        if ($this->isBonusPayment()) {
            $this->writeoffBonusFromClientAccount();
            $this->depositMoneyToClientAccount();
        }

        //Операции оплаты заказа исполнителю
        $this->payWorkerForOrder($this->summaryCost);

        //Списываем комиссию с исполнителю
        $this->writeOffWorkerCommission($this->commission);
        $this->writeOffTax($this->tax);
        $this->depositSurcharge($this->surcharge);
    }


    protected function doTransactOperation()
    {
        if (!$this->emptySummaryCost) {
            switch ($this->payment_method) {
                case self::CASH_PAYMENT:
                    $this->cashPayment();
                    break;
                case self::CARD_PAYMENT:
                    $this->cardPayment();
                    break;
                case self::PERSONAL_ACCOUNT_PAYMENT:
                case self::CORP_BALANCE_PAYMENT:
                    $this->accountPayment();
                    break;
            }
        }

        $isBonusRefilled = false;
        if ($this->isNeedRefillBonus()) {
            $isBonusRefilled = $this->refillBonus();
        }

        $isReferralBonusRefilled = $this->refillReferralBonus();

        $isPromoBonusesRefilled = $this->refillPromoBonus();

        if ($this->emptySummaryCost && !$isBonusRefilled && !$isReferralBonusRefilled && !$isPromoBonusesRefilled) {
            throw new NoBalanceChangesException("Account balance doesn't changed");
        }

        if ($this->needPushToClient) {
            $ownerData = $this->getOwnerData($this->clientAccount->account_id,
                $this->clientAccount->acc_kind_id);

            $this->clientAccount->refresh();
            $this->clientBonusAccount->refresh();

            $bonusBalance = $this->clientBonusAccount->balance;
            if ($this->bonusSystem->isUDSGameBonusSystem()) {
                try {
                    $bonusBalance = $this->bonusSystem->getBalance(
                        $this->order->client_id, $this->currency_id);
                } catch (\Exception $ex) {
                    $bonusBalance = null;
                }
            }

            $balance = $this->getTruncatedSum($this->clientAccount->balance, $this->currency_id);
            $bonusBalance = $this->getTruncatedSum($bonusBalance, $this->currency_id);

            $this->updatePushNotification(
                $this->tenant_id,
                $this->getTenantLogin(),
                $this->getApplicationType($this->clientAccount->acc_kind_id),
                $ownerData['device'],
                $ownerData['token'],
                $ownerData['lang'],
                $this->getCurrency(),
                $balance,
                $bonusBalance,
                empty($ownerData['appId']) ? null : $ownerData['appId']);
        }

        if ($this->needPushToReferrer && $this->referrerAccount !== null) {
            $ownerData = $this->getOwnerData($this->referrerAccount->account_id,
                $this->referrerAccount->acc_kind_id);

            $this->referrerAccount->refresh();
            $this->referrerBonusAccount->refresh();

            $this->updatePushNotification(
                $this->tenant_id,
                $this->getTenantLogin(),
                $this->getApplicationType($this->referrerAccount->acc_kind_id),
                $ownerData['device'],
                $ownerData['token'],
                $ownerData['lang'],
                $this->getCurrency(),
                $this->getTruncatedSum($this->referrerAccount->balance, $this->currency_id),
                $this->getTruncatedSum($this->referrerBonusAccount->balance, $this->currency_id),
                empty($ownerData['appId']) ? null : $ownerData['appId']);
        }

        if ($this->needPushToWorker) {
            $ownerData = $this->getOwnerData($this->workerAccount->account_id,
                $this->workerAccount->acc_kind_id);

            $this->workerAccount->refresh();

            $balance = $this->getTruncatedSum($this->workerAccount->balance, $this->currency_id);

            $this->updatePushNotification(
                $this->tenant_id,
                $this->getTenantLogin(),
                $this->getApplicationType($this->workerAccount->acc_kind_id),
                $ownerData['device'],
                $ownerData['token'],
                $ownerData['lang'],
                $this->getCurrency(),
                $balance);
        }
    }
}