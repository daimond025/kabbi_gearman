<?php

namespace console\components\billing\transactions;

use console\components\billing\Account;
use console\components\billing\Operation;
use yii\base\NotSupportedException;

/**
 * Class GootaxPaymentTransaction
 * @package console\components\billing\transactions
 */
class GootaxPaymentTransaction extends Transaction
{
    const ACTION_REGISTER = 'register payment';
    const ACTION_REFUND = 'refund payment';

    /**
     * @var string
     */
    public $action;

    /**
     * @var integer
     */
    public $payment_id;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->payment_method = Transaction::PERSONAL_ACCOUNT_PAYMENT;

        $this->setupGootaxAccounts();

        switch ($this->action) {
            case static::ACTION_REGISTER:
                $this->sender_acc_id   = $this->tenantAccount->account_id;
                $this->receiver_acc_id = $this->gootaxAccount->account_id;
                break;
            case static::ACTION_REFUND:
                $this->sender_acc_id   = $this->gootaxAccount->account_id;
                $this->receiver_acc_id = $this->tenantAccount->account_id;
                break;
            default:
                throw new NotSupportedException('Action doesn\'t supported (' . $this->action . ')');
        }
    }


    /**
     * Register payment
     * @throws \Exception
     */
    private function registerPayment()
    {
        $this->createOperation(Operation::EXPENSES_TYPE,
            $this->tenantAccount->account_id,
            $this->transaction_id,
            $this->sum,
            $this->tenantAccount->balance,
            $this->comment
        );
        app()->logger->log("Operation (Withdrawal from tenant balance]): " . json_encode([
                'type_id'     => Operation::EXPENSES_TYPE,
                'account_id'  => $this->tenantAccount->account_id,
                'acc_kind_id' => $this->tenantAccount->acc_kind_id,
                'sum'         => $this->sum,
            ]));

        $this->createOperation(Operation::EXPENSES_TYPE,
            $this->gootaxAccount->account_id,
            $this->transaction_id,
            $this->sum,
            $this->getGootaxBalance(),
            'Withdrawal from tenant account №' . $this->gootaxAccount->account_id
        );
        app()->logger->log("Operation (Withdrawal from tenant balance [GOOTAX]): " . json_encode([
                'type_id'    => Operation::EXPENSES_TYPE,
                'account_id' => $this->gootaxAccount->account_id,
                'sum'        => $this->sum,
            ]));
    }


    /**
     * Refund payment
     * @throws \Exception
     */
    private function refundPayment()
    {
        $this->createOperation(Operation::INCOME_TYPE,
            $this->tenantAccount->account_id,
            $this->transaction_id,
            $this->sum,
            $this->tenantAccount->balance,
            $this->comment
        );
        app()->logger->log("Operation (Refill tenant balance]): " . json_encode([
                'type_id'     => Operation::INCOME_TYPE,
                'account_id'  => $this->tenantAccount->account_id,
                'acc_kind_id' => $this->tenantAccount->acc_kind_id,
                'sum'         => $this->sum,
            ]));

        $this->createOperation(Operation::INCOME_TYPE,
            $this->gootaxAccount->account_id,
            $this->transaction_id,
            $this->sum,
            $this->gootaxAccount->balance,
            'Refill tenant account №' . $this->tenantAccount->account_id
        );
        app()->logger->log("Operation (Refill tenant balance [GOOTAX]): " . json_encode([
                'type_id'    => Operation::INCOME_TYPE,
                'account_id' => $this->gootaxAccount->account_id,
                'sum'        => $this->sum,
            ]));
    }


    /**
     * @inheritdoc
     */
    protected function doTransactOperation()
    {
        switch ($this->action) {
            case static::ACTION_REGISTER:
                $this->registerPayment('Withdrawal from tenant balance №' . $this->tenantAccount->account_id);
                break;
            case static::ACTION_REFUND:
                $this->refundPayment('Refill tenant balance №' . $this->tenantAccount->account_id);
                break;
        }
    }

}