<?php

namespace console\components\billing\transactions;

use console\components\billing\exceptions\CardPaymentFailedException;
use console\components\billing\models\bankCard\DepositFromBankCard;
use console\components\billing\models\bankCard\Payment;
use console\components\billing\models\bankCard\Profile;
use console\components\billing\models\Currency;
use console\components\billing\models\StripeHelper;
use console\components\billing\models\Worker;
use paymentGate\exceptions\ProfileNotFoundException;
use paymentGate\ProfileService;
use Yii;
use yii\db\Query;

use yii\base\Exception;
use yii\base\NotSupportedException;
use console\components\billing\exceptions\HeldPaymentException;

use console\components\billing\Operation;
use console\components\billing\Account;
use yii\di\Container;
use yii\helpers\ArrayHelper;

class DepositTransaction extends Transaction
{
    /**
     * @var boolean
     */
    public $isTenantPayment = false;

    /**
     * @var string
     */
    public $pan;

    /**
     * @var string
     */
    protected $payment_order_id;

    /**
     * @var Container
     */
    public $container;

    /**
     * @var array
     */
    private $ownerData = [];

    /**
     * @var string
     */
    private $paymentGateProfile;

    /**
     * Setup dependencies
     */
    protected function setDependencies()
    {
        $this->container = new Container();
        $this->container->setSingleton('payment', Payment::className(), [['requestId' => $this->request_id]]);
        $this->container->setSingleton('profile', Profile::className(), [['requestId' => $this->request_id]]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['request_id', 'pan'], 'string'],
        ]);
    }

    /**
     * @inheritdoc
     * @throws \console\components\billing\exceptions\HeldPaymentException
     */
    public function init()
    {
        parent::init();

        $this->setDependencies();

        app()->logger->log('Transaction type: Deposit');

        if ($this->isTenantPayment) {
            $this->setupGootaxAccounts();

            $this->sender_acc_id      = $this->tenantAccount->account_id;
            $this->sender_acc_kind_id = $this->tenantAccount->acc_kind_id;
            $this->sender_acc_balance = $this->tenantAccount->balance;

            $this->receiver_acc_id      = $this->gootaxAccount->account_id;
            $this->receiver_acc_balance = $this->gootaxAccount->balance;
        } else {
            $this->receiver_acc_id      = $this->system_acc_id;
            $this->receiver_acc_balance = $this->system_balance;

            if ($this->isNeedCheckHeldPayment()) {
                $this->checkPaymentAlreadyHeld();
            }
        }
    }

    /**
     * Is terminal payment?
     * @return boolean
     */
    protected function isNeedCheckHeldPayment()
    {
        return in_array($this->payment_method, [
            self::TERMINAL_ELECSNET_PAYMENT,
            self::TERMINAL_QIWI_PAYMENT,
            self::TERMINAL_MILLION_PAYMENT,
            self::TERMINAL_PAYNET_PAYMENT,
            self::CARD_PAYMENT,
        ], false);
    }

    /**
     * Is cash payment
     * @return boolean
     */
    protected function isCashPayment()
    {
        return $this->payment_method == self::CASH_PAYMENT;
    }

    /**
     * Is card payment
     * @return boolean
     */
    protected function isCardPayment()
    {
        return $this->payment_method == self::CARD_PAYMENT;
    }

    /**
     * Check payment is already held
     * @throws \console\components\billing\exceptions\HeldPaymentException
     */
    private function checkPaymentAlreadyHeld()
    {
        switch ($this->payment_method) {
            case self::TERMINAL_ELECSNET_PAYMENT:
                $this->checkElecsnetPaymentIsHeld();
                break;
            case self::TERMINAL_QIWI_PAYMENT:
                $this->checkQiwiPaymentIsHeld();
                break;
            case self::TERMINAL_MILLION_PAYMENT:
                $this->checkMillionPaymentIsHeld();
                break;
            case self::CARD_PAYMENT:
                $this->checkDepositFromCardIsHeld();
                break;
            case self::TERMINAL_PAYNET_PAYMENT:
                $this->checkPayNetPaymentIsHeld();
        }
    }


    /**
     * Check Elecsnet payment is already held
     * @throws HeldPaymentException
     */
    protected function checkElecsnetPaymentIsHeld()
    {
        $res = (new Query())
            ->from(parent::tableName())
            ->where([
                'type_id'                  => $this->type_id,
                'sender_acc_id'            => $this->sender_acc_id,
                'terminal_transact_number' => $this->terminal_transact_number,
                'payment_method'           => $this->payment_method,
            ])
            ->exists();

        if ($res) {
            throw new HeldPaymentException('Платеж с auth_code=' . $this->terminal_transact_number . ' уже был проведен.');
        }
    }

    /**
     * Check Qiwi payment is already held
     * @throws HeldPaymentException
     */
    protected function checkQiwiPaymentIsHeld()
    {
    }

    /**
     * Check Elecsnet payment is already held
     * @throws HeldPaymentException
     */
    protected function checkMillionPaymentIsHeld()
    {
        $res = (new Query())
            ->from(parent::tableName())
            ->where([
                'type_id'                  => $this->type_id,
                'sender_acc_id'            => $this->sender_acc_id,
                'terminal_transact_number' => $this->terminal_transact_number,
                'payment_method'           => $this->payment_method,
            ])
            ->exists();

        if ($res) {
            throw new HeldPaymentException(
                'Payment is already held (transaction: ' . $this->terminal_transact_number
                . ', transaction date: ' . $this->terminal_transact_date . ')');
        }
    }

    /**
     * Check PayNet payment is already held
     * @throws HeldPaymentException
     */
    protected function checkPayNetPaymentIsHeld()
    {
        $res = (new Query())
            ->from(parent::tableName())
            ->where([
                'type_id'                  => $this->type_id,
                'sender_acc_id'            => $this->sender_acc_id,
                'terminal_transact_number' => $this->terminal_transact_number,
                'payment_method'           => $this->payment_method,
            ])
            ->exists();

        if ($res) {
            throw new HeldPaymentException(
                'Payment is already held (transaction: ' . $this->terminal_transact_number
                . ', transaction date: ' . $this->terminal_transact_date . ')');
        }
    }

    /**
     * Checking deposit from card is already held
     * @throws HeldPaymentException
     */
    protected function checkDepositFromCardIsHeld()
    {
        $isPaymentExists = DepositFromBankCard::find()
            ->where(['request_id' => $this->request_id])
            ->exists();

        if ($isPaymentExists) {
            throw new HeldPaymentException('Request #' . $this->request_id . ' was processed.');
        }
    }

    /**
     * Проводим транзакцию
     * @throws NotSupportedException
     */
    protected function doTransactOperation()
    {
        if ($this->isTenantPayment) {
            $this->refillTenantBalance();
        } else {
            switch ($this->payment_method) {
                case self::TERMINAL_ELECSNET_PAYMENT:
                case self::TERMINAL_QIWI_PAYMENT:
                    $this->refillTenantBalance();
                    $this->refillBalance();
                    break;
                case self::TERMINAL_MILLION_PAYMENT:
                case self::TERMINAL_PAYNET_PAYMENT:
                case self::CASH_PAYMENT:
                    $this->refillBalance();
                    break;
                case self::CARD_PAYMENT:
                    $this->refillBalance();
                    $this->doCardPayment();
                    break;
                default:
                    throw new NotSupportedException('Payment method is not support.');
            }
        }
    }

    /**
     * Refill balance
     * @throws Exception
     * @throws \Exception
     */
    private function refillBalance()
    {
        $this->createOperation(Operation::INCOME_TYPE,
            $this->receiver_acc_id,
            $this->transaction_id,
            $this->sum,
            $this->receiver_acc_balance,
            'Refill balance №' . $this->sender_acc_id
        );
        $this->receiver_acc_balance += $this->sum;
        app()->logger->log('Operation (Refill balance [SYSTEM]): ' . json_encode([
                'type_id'    => Operation::INCOME_TYPE,
                'account_id' => $this->receiver_acc_id,
                'sum'        => $this->sum,
            ]));

        $this->ownerData = $this->getOwnerData($this->sender_acc_id, $this->sender_acc_kind_id);

        $this->createOperation(Operation::INCOME_TYPE,
            $this->sender_acc_id,
            $this->transaction_id,
            $this->sum,
            $this->sender_acc_balance,
            $this->comment,
            $this->ownerData['name']
        );
        $this->sender_acc_balance += $this->sum;
        app()->logger->log('Operation (Refill balance): ' . json_encode([
                'type_id'     => Operation::INCOME_TYPE,
                'account_id'  => $this->sender_acc_id,
                'acc_kind_id' => $this->sender_acc_kind_id,
                'sum'         => $this->sum,
            ]));

        $balance      = $this->getTruncatedSum($this->sender_acc_balance, $this->currency_id);
        $bonusBalance = null;

        if ((int)$this->sender_acc_kind_id === Account::CLIENT_KIND) {
            $accountKindId = Account::getClientBonusKind($this->tenant_id);

            $clientBonusAccount = $this->getAccount(
                $this->sender_owner_id,
                $this->tenant_id,
                $accountKindId,
                $this->currency_id);

            $bonusBalance = empty($clientBonusAccount) ? 0 : +$clientBonusAccount->balance;
            $bonusBalance = $this->getTruncatedSum($bonusBalance, $this->currency_id);
        }

        // update push notification
        $this->updatePushNotification(
            $this->tenant_id,
            $this->getTenantLogin(),
            $this->getApplicationType($this->sender_acc_kind_id),
            $this->ownerData['device'],
            $this->ownerData['token'],
            $this->ownerData['lang'],
            $this->getCurrency(),
            $balance,
            $bonusBalance,
            empty($this->ownerData['appId']) ? null : $this->ownerData['appId']);
    }

    /**
     * @param float $commission
     *
     * @throws \Exception
     */
    private function writeoffCommissionForCardPayment($commission)
    {
        $this->createOperation(Operation::EXPENSES_TYPE,
            $this->receiver_acc_id,
            $this->transaction_id,
            $commission,
            $this->receiver_acc_balance,
            'Write-off commission for card payment №' . $this->sender_acc_id
        );
        $this->receiver_acc_balance -= $commission;
        app()->logger->log('Operation (Refill balance [SYSTEM]): ' . json_encode([
                'type_id'    => Operation::EXPENSES_TYPE,
                'account_id' => $this->receiver_acc_id,
                'sum'        => $this->sum,
            ]));

        $this->ownerData = $this->getOwnerData($this->sender_acc_id, $this->sender_acc_kind_id);

        $this->createOperation(Operation::EXPENSES_TYPE,
            $this->sender_acc_id,
            $this->transaction_id,
            $commission,
            $this->sender_acc_balance,
            'Write-off commission for card payment',
            $this->ownerData['name']
        );
        $this->sender_acc_balance -= $commission;
        app()->logger->log('Operation (Refill balance): ' . json_encode([
                'type_id'     => Operation::EXPENSES_TYPE,
                'account_id'  => $this->sender_acc_id,
                'acc_kind_id' => $this->sender_acc_kind_id,
                'sum'         => $this->sum,
            ]));

        $balance      = $this->getTruncatedSum($this->sender_acc_balance, $this->currency_id);
        $bonusBalance = null;

        $this->updatePushNotification(
            $this->tenant_id,
            $this->getTenantLogin(),
            $this->getApplicationType($this->sender_acc_kind_id),
            $this->ownerData['device'],
            $this->ownerData['token'],
            $this->ownerData['lang'],
            $this->getCurrency(),
            $balance,
            $bonusBalance,
            empty($this->ownerData['appId']) ? null : $this->ownerData['appId']);
    }

    /**
     * Generate unique order number by transaction id
     *
     * @param int    $transactionId
     * @param string $prefix
     * @param int    $length
     *
     * @return string
     */
    private function generateOrderNumber($transactionId, $prefix = '1', $length = 15)
    {
        return $prefix . str_pad($transactionId, $length, '0', STR_PAD_LEFT);
    }

    /**
     * Card payment
     * @throws \console\components\billing\exceptions\CardPaymentFailedException
     */
    private function doCardPayment()
    {
        try {
            $tenantId = $this->tenant_id;
            $workerId = $this->sender_owner_id;
            $worker = $this->getWorker($workerId);
            $workerLastCityId = $worker->getLastCityId();

            $this->paymentGateProfile = $this->getPaymentGateProfile($tenantId, $workerLastCityId);

            /* @var $profile Profile */
            $profile = $this->container->get('profile')->get($this->paymentGateProfile);

            $percent = empty($profile['commission']) ? 0 : (float)$profile['commission'];
            $commission = $this->getTruncatedSum($this->sum * ((float)$percent <= 0 ? 0 : (float)$percent) / 100,
                $this->currency_id);

            if ($commission > 0) {
                $this->writeoffCommissionForCardPayment($commission > $this->sum ? $this->sum : $commission);
            }

            $currency = Currency::findOne($this->currency_id);
            $amount = floor($this->sum * pow(10, empty($currency->minor_unit) ? 0 : $currency->minor_unit));

            $orderNumber = $this->generateOrderNumber($this->transaction_id);

            $description = "Refill account (tenant #{$tenantId} worker #{$workerId} order/transaction #\"{$orderNumber}\")";

            $params = [
                'phone' => empty($worker->phone) ? null : $worker->phone,
                'email' => empty($worker->email) ? null : $worker->email,
            ];
            $stripeAccount = StripeHelper::getStripeAccount($tenantId, $workerLastCityId, $worker);
            if ($stripeAccount !== null) {
                $params['stripeAccount'] = $stripeAccount;
            }

            /* @var $payment Payment */
            $payment = $this->container->get('payment');
            $response = $payment->pay(
                $tenantId,
                $this->paymentGateProfile,
                null,
                null,
                $workerId,
                $this->pan,
                null,
                $amount,
                $orderNumber,
                $currency->numeric_code,
                $params,
                $description);

            app()->logger->log('Operation (Refill account by bank card): ' . json_encode([
                    'description' => $description,
                    'pan'         => $this->pan,
                    'amount'      => $amount,
                    'currency'    => $currency->numeric_code,
                ]));
        } catch (\Exception $ex) {
            throw new CardPaymentFailedException("Card payment error: {$ex->getMessage()}",
                CardPaymentFailedException::ERROR_PAY, $ex);
        }

        $this->payment_order_id = $response['orderId'];

        $this->savePaymentInformation($this->payment_order_id);
    }

    /**
     * @param int $tenantId
     * @param int|null $cityId
     * @return string
     *
     * @throws ProfileNotFoundException
     * @throws \yii\db\Exception
     */
    private function getPaymentGateProfile(int $tenantId, ?int $cityId): string
    {
        $profileService = new ProfileService(\Yii::$app->getDb());

        return $profileService->getProfile($tenantId, $cityId);
    }

    private function getWorker($workerId)
    {
        return Worker::find()->where(['worker_id' => $workerId])->one();
    }

    /**
     * Saving payment information
     *
     * @param int $paymentOrderId
     *
     * @throws CardPaymentFailedException
     */
    private function savePaymentInformation($paymentOrderId)
    {
        $record                   = new DepositFromBankCard();
        $record->request_id       = $this->request_id;
        $record->pan              = $this->pan;
        $record->transaction_id   = $this->transaction_id;
        $record->payment_order_id = $paymentOrderId;

        if (!$record->save()) {
            throw new CardPaymentFailedException('Error saving payment information');
        }
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \console\components\billing\exceptions\CardPaymentFailedException
     */
    public function afterRollback()
    {
        if (isset($this->payment_order_id)) {
            /* @var $payment Payment */
            $payment = $this->container->get('payment');
            try {
                $payment->refund(
                    $this->tenant_id,
                    $this->paymentGateProfile,
                    null,
                    null,
                    $this->sender_owner_id,
                    $this->payment_order_id);

                app()->logger->log('Operation (Refund payment [CARD]): ' . $this->payment_order_id);
            } catch (\Exception $ex) {
                throw new CardPaymentFailedException("An error occurred while returning the money for payment: error={$ex->getMessage()}",
                    CardPaymentFailedException::ERROR_REFUND, $ex);
            }
        }
    }

    /**
     * Refill tenant balance
     * @throws NotSupportedException
     * @throws Exception
     */
    private function refillTenantBalance()
    {
        if ($this->currency_id != Account::RUS_RUBLE_CURRENCY) {
            throw new NotSupportedException('Through the terminal can only be paid in russian rubles');
        }

        if (!isset($this->gootaxAccount)) {
            $this->setupGootaxAccounts();
        }

        //Добавляем сумму операции Гутаксу
        $operation_1 = new Operation([
            'type_id'         => Operation::INCOME_TYPE,
            'account_id'      => $this->gootaxAccount->account_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $this->sum,
            'account_balance' => $this->gootaxAccount->balance,
            'comment'         => 'Refill balance №' . $this->sender_acc_id,
        ]);

        if (!$operation_1->save()) {
            Yii::error($operation_1->getErrors());
            throw new Exception('Ошибка проведения операции добавления средств на счет Гутакса №' . $this->receiver_acc_id);
        }

        app()->logger->log("Operation (Refill tenant balance [SYSTEM]): " . json_encode([
                'type_id'    => Operation::INCOME_TYPE,
                'account_id' => self::GOOTAX_ACCOUNT_ID,
                'sum'        => $this->sum,
            ]));

        //Добавляем сумму операции тенанту
        $operation_2 = new Operation([
            'type_id'         => Operation::INCOME_TYPE,
            'account_id'      => $this->tenantAccount->account_id,
            'transaction_id'  => $this->transaction_id,
            'sum'             => $this->sum,
            'account_balance' => $this->tenantAccount->balance,
            'comment'         => 'Refill balance №' . $this->sender_acc_id,
        ]);

        if (!$operation_2->save()) {
            Yii::error($operation_2->getErrors());
            throw new Exception('Ошибка проведения операции добавления средств на счет тенанта №' . $this->receiver_acc_id);
        }

        app()->logger->log("Operation (Refill tenant balance): " . json_encode([
                'type_id'     => Operation::INCOME_TYPE,
                'account_id'  => $this->tenantAccount->account_id,
                'acc_kind_id' => $this->tenantAccount->acc_kind_id,
                'sum'         => $this->sum,
            ]));
    }

}
