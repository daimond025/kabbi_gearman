<?php

namespace console\components\billing;

use console\components\billing\models\Worker;
use Yii;
use yii\base\Exception;
use yii\db\Query;

use console\components\billing\models\Client;
use console\components\billing\models\Company;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer       $account_id
 * @property integer       $acc_kind_id
 * @property integer       $acc_type_id
 * @property integer       $owner_id
 * @property integer       $currency_id
 * @property integer       $tenant_id
 * @property double        $balance
 *
 * @property AccountKind   $accKind
 * @property AccountType   $accType
 * @property Currency      $currency
 * @property Tenant        $tenant
 * @property Operation[]   $operations
 * @property Transaction[] $transactions
 */
class Account extends \yii\db\ActiveRecord
{

    const ACTIVE_TYPE = 1;
    const PASSIVE_TYPE = 2;

    const TENANT_KIND = 1;
    const WORKER_KIND = 2;
    const CLIENT_KIND = 3;
    const COMPANY_KIND = 4;
    const SYSTEM_KIND = 5;
    const GOOTAX_KIND = 6;
    const CLIENT_BONUS_KIND = 7;
    const SYSTEM_BONUS_KIND = 8;
    const CLIENT_BONUS_UDS_GAME_KIND = 9;
    const SYSTEM_BONUS_UDS_GAME_KIND = 10;

    const DEFAULT_CURRENCY = 1;
    const RUS_RUBLE_CURRENCY = 1;

    const BONUS_SYSTEM_ID_GOOTAX = 1;
    const BONUS_SYSTEM_ID_UDS_GAME = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_kind_id', 'acc_type_id', 'owner_id', 'currency_id', 'tenant_id'], 'required'],
            [['acc_kind_id', 'acc_type_id', 'owner_id', 'currency_id', 'tenant_id'], 'integer'],
            [['balance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id'  => 'Account ID',
            'acc_kind_id' => 'Acc Kind ID',
            'acc_type_id' => 'Acc Type ID',
            'owner_id'    => 'Owner ID',
            'currency_id' => 'Currency ID',
            'tenant_id'   => 'Tenant ID',
            'balance'     => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccKind()
    {
        return $this->hasOne(AccountKind::className(), ['kind_id' => 'acc_kind_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccType()
    {
        return $this->hasOne(AccountType::className(), ['type_id' => 'acc_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['sender_acc_id' => 'account_id']);
    }

    /**
     * Getting bonus system id
     *
     * @param int $tenantId
     *
     * @return int
     */
    public static function getBonusSystemId($tenantId)
    {
        $bonusSystemId = (new Query())
            ->select('bonus_system_id')
            ->from('tbl_tenant_has_bonus_system')
            ->where([
                'tenant_id' => $tenantId,
            ])
            ->scalar();

        return empty($bonusSystemId) ? self::BONUS_SYSTEM_ID_GOOTAX : (int)$bonusSystemId;
    }

    /**
     * Getting client bonus kind
     *
     * @param $tenantId
     *
     * @return int
     * @throws Exception
     */
    public static function getClientBonusKind($tenantId)
    {
        $bonusSystemId = self::getBonusSystemId($tenantId);

        switch ($bonusSystemId) {
            case self::BONUS_SYSTEM_ID_GOOTAX:
                return Account::CLIENT_BONUS_KIND;
            case self::BONUS_SYSTEM_ID_UDS_GAME:
                return Account::CLIENT_BONUS_UDS_GAME_KIND;
            default:
                throw new Exception('Unknown bonus system');
        }
    }

    /**
     * Getting client bonus kind
     *
     * @param $tenantId
     *
     * @return int
     * @throws Exception
     */
    public static function getSystemBonusKind($tenantId)
    {
        $bonusSystemId = self::getBonusSystemId($tenantId);

        switch ($bonusSystemId) {
            case self::BONUS_SYSTEM_ID_GOOTAX:
                return Account::SYSTEM_BONUS_KIND;
            case self::BONUS_SYSTEM_ID_UDS_GAME:
                return Account::SYSTEM_BONUS_UDS_GAME_KIND;
            default:
                throw new Exception('Unknown bonus system');
        }
    }

    /**
     * Получаем city_id сущности счета
     * @return boolean|integer
     */
    public function getEntityCityId()
    {
        $entityTableMap  = self::getEntityTableMap();
        $entityTableData = getValue($entityTableMap[$this->acc_kind_id]);

        if (empty($entityTableData)) {
            return false;
        }

        if ($this->acc_kind_id == self::WORKER_KIND) {
            return (new Query())
                ->select('city_id')
                ->from('tbl_worker_has_city')
                ->where(['worker_id' => $this->owner_id])
                ->orderBy([
                    'last_active' => SORT_DESC,
                    'city_id'     => SORT_ASC,
                ])
                ->limit(1)
                ->scalar();
        } else {
            return (new Query())
                ->select('city_id')
                ->from($entityTableData['table'])
                ->where([$entityTableData['pk'] => $this->owner_id])
                ->scalar();
        }
    }

    /**
     * Ассоциативный массив вида счета и таблицы сущности
     * @return array
     */
    public static function getEntityTableMap()
    {
        $clientKind = [
            'table'  => '{{%client}}',
            'pk'     => 'client_id',
            'select' => [
                'client_id',
                'last_name',
                'name',
                'second_name',
                'device',
                'device_token',
                'lang',
            ],
        ];

        return [
            self::WORKER_KIND                => [
                'table'  => '{{%worker}}',
                'pk'     => 'worker_id',
                'select' => [
                    'worker_id',
                    'last_name',
                    'name',
                    'second_name',
                    'callsign',
                    'device',
                    'device_token',
                    'lang',
                ],
            ],
            self::TENANT_KIND                => ['table' => '{{%tenant}}', 'pk' => 'tenant_id'],
            self::CLIENT_KIND                => $clientKind,
            self::CLIENT_BONUS_KIND          => $clientKind,
            self::CLIENT_BONUS_UDS_GAME_KIND => $clientKind,
            self::COMPANY_KIND               => [
                'table'  => '{{%client_company}}',
                'pk'     => 'company_id',
                'select' => [
                    'company_id',
                    'full_name',
                    'name',
                ],
            ],
        ];
    }

    public static function getEntityTableData($account_kind)
    {
        $entityTableMap = self::getEntityTableMap();

        return getValue($entityTableMap[$account_kind]);
    }

    /**
     * Return account owner
     *
     * @param integer $accountId
     * @param integer $accKindId
     *
     * @return array
     */
    public static function getOwnerData($accountId, $accKindId)
    {
        $entity = [
            'name'   => null,
            'device' => null,
            'token'  => null,
            'lang'   => null,
        ];

        switch ($accKindId) {
            case Account::CLIENT_KIND:
            case Account::CLIENT_BONUS_KIND:
            case Account::CLIENT_BONUS_UDS_GAME_KIND:
                $client = Client::find()
                    ->with('clientPhones')
                    ->innerJoinWith([
                        'accounts' => function ($query) use ($accountId) {
                            $query->andWhere([Account::tableName() . '.account_id' => $accountId]);
                        },
                    ])
                    ->one();

                $entity['name'] = implode(' ', array_filter([
                    $client->last_name,
                    $client->name,
                    $client->second_name,
                ]));
                if (empty($entity['name']) && count($client->clientPhones)) {
                    $entity['name'] = $client->clientPhones[0]->value;
                }
                $entity['device'] = $client->device;
                $entity['token']  = $client->device_token;
                $entity['lang']   = $client->lang;
                $entity['appId']  = $client->app_id;
                break;
            case Account::COMPANY_KIND:
                $company = Company::find()
                    ->innerJoinWith([
                        'accounts' => function ($query) use ($accountId) {
                            $query->andWhere([Account::tableName() . '.account_id' => $accountId]);
                        },
                    ])
                    ->one();

                $entity['name'] = empty($company->full_name) ? $company->name
                    : $company->full_name;
                break;
            case Account::WORKER_KIND:
                $worker = Worker::find()
                    ->innerJoinWith([
                        'accounts' => function ($query) use ($accountId) {
                            $query->andWhere([Account::tableName() . '.account_id' => $accountId]);
                        },
                    ])
                    ->one();

                $entity['name']   = implode(' ', [
                    $worker->last_name,
                    $worker->name,
                    $worker->second_name,
                    '(' . $worker->callsign . ')',
                ]);
                $entity['device'] = $worker->device;
                $entity['token']  = $worker->device_token;
                $entity['lang']   = $worker->lang;
                break;
        }

        return $entity;
    }

}
