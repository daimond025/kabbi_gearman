<?php

namespace console\components\billing;

use console\components\curl\CurlResponse;
use Yii;
use yii\base\Component;
use console\components\billing\models\PushNotification;
use yii\base\Exception;

/**
 * Push notification manager
 */
class PushNotificationManager extends Component
{
    /**
     * Getting url
     *
     * @param string $typeApp
     *
     * @return string
     *
     * @throws Exception
     */
    public function getUrl($typeApp)
    {
        switch ($typeApp) {
            case PushNotification::TYPE_APP_CLIENT:
                return 'notification/client_send_push_transaction';
            case PushNotification::TYPE_APP_WORKER:
                return 'notification/worker_send_push_transaction';
            default:
                throw new Exception('Invalid type application');
        }
    }

    /**
     * Send push notification to the push service
     *
     * @param PushNotification $notification
     *
     * @return bool|CurlResponse
     * @throws Exception
     */
    protected function sendNotification($notification)
    {
        $curl = app()->curl;

        $curl->headers['Cache-Control']          = 'no-cache';
        $curl->options['CURLOPT_RETURNTRANSFER'] = 1;
        $curl->options['CURLOPT_TIMEOUT']        = getenv('CURL_TIMEOUT');

        $response = $curl->post(app()->params['pushApi.url']
            . $this->getUrl($notification->typeApp), $notification->generateParams());

        if ($response === false) {
            throw new Exception($curl->error());
        }

        return $response;
    }

    /**
     * Send push notifications
     *
     * @param PushNotification[] $notifications
     */
    public function sendNotifications($notifications)
    {
        if (is_array($notifications)) {
            app()->logger->log('Send notifications');
            foreach ($notifications as $notification) {
                try {
                    $this->sendNotification($notification);
                    app()->logger->log('Notification sent: ' . json_encode($notification));
                } catch (\Exception $exc) {
                    Yii::warning(implode("\n", [
                        'An error occurred while sending a push notification.',
                        serialize($notification),
                        $exc,
                    ]));
                    app()->logger->log('Notification failed (' . $exc->getMessage() . '):' . json_encode($notification));
                }
            }
        }
    }
}
