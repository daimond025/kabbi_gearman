<?php

namespace console\components\billing\exceptions;

use yii\base\Exception;

class NoBalanceChangesException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'There were no changes in the balance of accounts';
    }
}