<?php

namespace console\components\billing\exceptions;

use yii\base\Exception;

class NoBonusForPaymentException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'No bonus';
    }
}
