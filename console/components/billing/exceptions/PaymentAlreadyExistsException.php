<?php

namespace console\components\billing\exceptions;

use yii\base\Exception;

class PaymentAlreadyExistsException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Payment already exists';
    }
}
