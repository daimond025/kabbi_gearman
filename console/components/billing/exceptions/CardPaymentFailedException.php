<?php

namespace console\components\billing\exceptions;

use yii\base\Exception;

class CardPaymentFailedException extends Exception
{
    const ERROR_PAY = 1;
    const ERROR_REFUND = 2;
    const ERROR_GET_PROFILE = 3;

    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Card payment failed';
    }
}
