<?php

namespace console\components\billing\exceptions;

use yii\base\Exception;

class WriteoffBonusInCorpPaymentException extends Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Writeoff bonus in corp payment';
    }
}
