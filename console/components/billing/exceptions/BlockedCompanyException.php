<?php

namespace console\components\billing\exceptions;

use yii\base\Exception;

/**
 * Class BlockedCompanyException
 * @package console\components\billing\exceptions
 */
class BlockedCompanyException extends Exception
{
}
