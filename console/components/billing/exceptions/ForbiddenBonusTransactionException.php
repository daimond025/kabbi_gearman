<?php

namespace console\components\billing\exceptions;

use yii\base\Exception;

/**
 * Class ForbiddenBonusTransactionException
 * @package console\components\billing\exceptions
 */
class ForbiddenBonusTransactionException extends Exception
{

}
