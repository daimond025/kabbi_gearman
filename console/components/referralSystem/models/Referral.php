<?php

namespace referralSystem\models;

/**
 * Class Referral
 * @package referralSystem\models
 */
class Referral
{
    /**
     * @var int
     */
    private $clientId;

    /**
     * @var int
     */
    private $orderCount;

    /**
     * Referral constructor.
     *
     * @param int $clientId
     * @param int $orderCount
     */
    public function __construct($clientId, $orderCount)
    {
        $this->clientId   = $clientId;
        $this->orderCount = $orderCount;
    }

    /**
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return int
     */
    public function getOrderCount()
    {
        return $this->orderCount;
    }
}