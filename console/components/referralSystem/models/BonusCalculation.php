<?php

namespace referralSystem\models;

use referralSystem\exceptions\UnknownBonusValueTypeException;

/**
 * Class BonusCalculation
 * @package referralSystem\models
 */
class BonusCalculation
{
    const TYPE_PERCENT = 'PERCENT';
    const TYPE_BONUS = 'BONUS';
    /**
     * @var float
     */
    private $value;
    /**
     * @var int
     */
    private $type;


    /**
     * BonusCalculation constructor.
     *
     * @param float $value
     * @param int   $type
     */
    public function __construct($value, $type)
    {
        $this->value = $value;
        $this->type  = $type;
    }

    /**
     * @param float $amount
     *
     * @return float
     * @throws UnknownBonusValueTypeException
     */
    public function calculate($amount)
    {
        switch ($this->type) {
            case self::TYPE_BONUS:
                return $this->value;
            case self::TYPE_PERCENT:
                return $amount * $this->value / 100;
            default:
                throw new UnknownBonusValueTypeException("Unknown type: type={$this->type}");
        }
    }
}