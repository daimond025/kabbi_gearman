<?php

namespace referralSystem\models;

/**
 * Class BonusValue
 * @package referralSystem\models
 */
class BonusValue
{
    /**
     * @var float
     */
    private $amount;
    /**
     * @var int
     */
    private $currencyId;

    /**
     * BonusValue constructor.
     *
     * @param float $amount
     * @param int   $currencyId
     */
    public function __construct($amount, $currencyId)
    {
        $this->amount     = $amount;
        $this->currencyId = $currencyId;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }
}