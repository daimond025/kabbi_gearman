<?php

namespace referralSystem\models;

use referralSystem\exceptions\OrderWithDetailCostNotFoundException;
use yii\db\Query;

/**
 * Class OrderRepository
 * @package referralSystem\models
 */
class OrderRepository
{
    /**
     * @param $orderId
     *
     * @return array
     * @throws OrderWithDetailCostNotFoundException
     */
    private function getOrderDataById($orderId)
    {
        $order = (new Query())
            ->select(['o.tenant_id', 'o.city_id', 'o.client_id', 'c.summary_cost', 'c.bonus', 'o.currency_id'])
            ->from('{{%order}} o')
            ->innerJoin('{{%order_detail_cost}} c', 'o.order_id = c.order_id')
            ->where(['o.order_id' => $orderId])
            ->one();

        if (empty($order)) {
            throw new OrderWithDetailCostNotFoundException("Order (with detail cost) not found: orderId={$orderId}");
        }

        return $order;
    }

    /**
     * @param int $orderId
     *
     * @return Order
     * @throws OrderWithDetailCostNotFoundException
     */
    public function getOrder($orderId)
    {
        $orderData = $this->getOrderDataById($orderId);

        return new Order($orderId, (int)$orderData['tenant_id'], (int)$orderData['city_id'],
            (int)$orderData['client_id'],
            (float)$orderData['summary_cost'] - (float)$orderData['bonus'], (int)$orderData['currency_id']);
    }
}