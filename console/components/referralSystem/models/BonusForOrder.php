<?php

namespace referralSystem\models;

/**
 * Class BonusForOrder
 * @package referralSystem\models
 */
class BonusForOrder
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var Order
     */
    private $order;
    /**
     * @var Bonus
     */
    private $referrerBonus;
    /**
     * @var Bonus
     */
    private $referralBonus;
    /**
     * @var bool
     */
    private $isCompleted;

    /**
     * BonusForOrder constructor.
     *
     * @param int   $id
     * @param Order $order
     * @param Bonus $referrerBonus
     * @param Bonus $referralBonus
     * @param bool  $isCompleted
     */
    public function __construct($id, Order $order, Bonus $referrerBonus, Bonus $referralBonus, $isCompleted = false)
    {
        $this->id            = $id;
        $this->order         = $order;
        $this->referrerBonus = $referrerBonus;
        $this->referralBonus = $referralBonus;
        $this->isCompleted   = $isCompleted;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return Bonus
     */
    public function getReferrerBonus()
    {
        return $this->referrerBonus;
    }

    /**
     * @return Bonus
     */
    public function getReferralBonus()
    {
        return $this->referralBonus;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->isCompleted;
    }

    /**
     * @param bool $isCompleted
     */
    public function setIsCompleted($isCompleted)
    {
        $this->isCompleted = $isCompleted;
    }

}