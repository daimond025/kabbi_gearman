<?php

namespace referralSystem;

use referralSystem\exceptions\BonusForOrderIsAlreadyCompletedException;
use referralSystem\exceptions\BonusForOrderNotFoundException;
use referralSystem\exceptions\OrderWithDetailCostNotFoundException;
use referralSystem\exceptions\ReferralSystemException;
use referralSystem\exceptions\ReferralSystemNotFoundException;
use referralSystem\models\Bonus;
use referralSystem\models\BonusForOrder;
use referralSystem\models\BonusValue;
use referralSystem\models\Order;
use referralSystem\models\OrderRepository;
use referralSystem\models\ReferralSystemRepository;
use yii\db\Exception;

/**
 * Class ReferralSystemService
 * @package referralSystem
 */
class ReferralSystemService
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var ReferralSystemRepository
     */
    private $referralSystemRepository;

    /**
     * ReferralSystemService constructor.
     *
     * @param OrderRepository          $orderRepository
     * @param ReferralSystemRepository $referralSystemRepository
     */
    public function __construct(OrderRepository $orderRepository, ReferralSystemRepository $referralSystemRepository)
    {
        $this->orderRepository          = $orderRepository;
        $this->referralSystemRepository = $referralSystemRepository;
    }

    /**
     * @param Order $order
     *
     * @throws BonusForOrderIsAlreadyCompletedException
     * @throws Exception
     */
    private function deleteCalculatedBonus(Order $order)
    {
        try {
            $this->referralSystemRepository->deleteBonusForOrder($order);
        } catch (BonusForOrderNotFoundException $ignore) {
        }
    }

    /**
     * @param int $orderId
     *
     * @throws OrderWithDetailCostNotFoundException
     * @throws ReferralSystemNotFoundException
     * @throws ReferralSystemException
     */
    public function calculateBonus($orderId)
    {
        try {
            $order          = $this->orderRepository->getOrder($orderId);
            $referralSystem = $this->referralSystemRepository->getReferralSystem($order->getTenantId(),
                $order->getCityId(), $order->getClientId());

            $this->deleteCalculatedBonus($order);

            $minOrderPrice = $referralSystem->getMinOrderPrice();
            $maxOrderCount = $referralSystem->getMaxOrderCount();

            if ($order->getAmount() >= $minOrderPrice
                && ($maxOrderCount === null || $referralSystem->getReferral()->getOrderCount() < $maxOrderCount)
            ) {
                $referrerBonus = new Bonus($referralSystem->getReferrer()->getClientId(),
                    new BonusValue($referralSystem->getReferrerCalculation()->calculate($order->getAmount()),
                        $order->getCurrencyId()));
                $referralBonus = new Bonus($referralSystem->getReferral()->getClientId(),
                    new BonusValue($referralSystem->getReferralCalculation()->calculate($order->getAmount()),
                        $order->getCurrencyId()));

                $bonusForOrder = new BonusForOrder(null, $order, $referrerBonus, $referralBonus);

                $this->referralSystemRepository->saveBonusForOrder($bonusForOrder);
            }
        } catch (OrderWithDetailCostNotFoundException $ex) {
            throw $ex;
        } catch (ReferralSystemNotFoundException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new ReferralSystemException("Calculate bonus error ({$ex->getMessage()})", 0, $ex);
        }
    }

    /**
     * @param int $orderId
     *
     * @return BonusForOrder
     * @throws BonusForOrderNotFoundException
     * @throws ReferralSystemException
     */
    public function getCalculatedBonus($orderId)
    {
        try {
            $order = $this->orderRepository->getOrder($orderId);

            return $this->referralSystemRepository->getBonusForOrder($order);
        } catch (BonusForOrderNotFoundException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new ReferralSystemException("Get bonus error ({$ex->getMessage()})", 0, $ex);
        }
    }

    /**
     * @param int $orderId
     *
     * @throws ReferralSystemException
     * @throws BonusForOrderNotFoundException
     */
    public function setBonusCompleted($orderId)
    {
        try {
            $order = $this->orderRepository->getOrder($orderId);
            $bonus = $this->referralSystemRepository->getBonusForOrder($order);
            $bonus->setIsCompleted(true);

            $this->referralSystemRepository->saveBonusForOrder($bonus);
        } catch (BonusForOrderNotFoundException $ex) {
            throw $ex;
        } catch (\Exception $ex) {
            throw new ReferralSystemException("Set bonus completed error ({$ex->getMessage()})", 0, $ex);
        }
    }
}