<?php

namespace referralSystem\exceptions;

/**
 * Class ReferralSystemNotFoundException
 * @package referralSystem\models
 */
class ReferralSystemNotFoundException extends \DomainException
{

}