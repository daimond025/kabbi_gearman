<?php

namespace console\components\db;

use Yii;
use yii\db\Connection;

trait ReconnectTrait
{
    /**
     * @param Connection $connection
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    protected static function reconnectIfNeeded(Connection $connection)
    {
        /** @var DbReconnectManager $reconnectManager */
        $reconnectManager = Yii::createObject(DbReconnectManager::class);

        $reconnectManager->reconnectIfNeeded($connection);
    }
}
