<?php

namespace console\components\db;

use Yii\db\Connection;

class DbReconnectManager
{
    /**
     * @param Connection $connection
     * @throws \yii\db\Exception
     */
    public function reconnectIfNeeded(Connection $connection)
    {
        try {
            $connection->createCommand('SELECT 1')->execute();
        } catch (\Exception $e) {
            $connection->close();
            $connection->open();
            app()->logger->log('MySql connection was reconnected. Error: ' . $e->getMessage());
        }
    }
}
