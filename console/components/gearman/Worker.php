<?php

namespace gearman;

use yii\base\Object;
use yii\redis\Cache;
use yii\swiftmailer\Mailer;

/**
 * Class Worker
 * @package gearman
 */
class Worker extends Object
{
    /** @var \GearmanWorker */
    private $worker;

    /** @var string */
    public $host;

    /** @var  int */
    public $port;

    /** @var JobFactory */
    private $jobFactory;

    /**
     * Worker constructor.
     *
     * @param JobFactory $jobFactory
     * @param array      $config
     */
    public function __construct(JobFactory $jobFactory, array $config = [])
    {
        parent::__construct($config);

        $this->jobFactory = $jobFactory;
        $this->jobFactory->setApplication(\Yii::$app);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        // ignore errors
        error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE);

        $this->worker = new \GearmanWorker();
        $this->worker->addServer($this->host, $this->port);

        parent::init();
    }

    public function closeConnections()
    {
        try {
            $mailerLog = \Yii::$app->get('mailer_log', false);
            if ($mailerLog instanceof Mailer) {
                $mailerLog->getTransport()->stop();
            }

            $mongoDB = \Yii::$app->get('mongodb', false);
            if ($mongoDB instanceof \yii\mongodb\Connection) {
                $mongoDB->close();
            }

            $cache = \Yii::$app->get('cache', false);
            if ($cache instanceof Cache) {
                $cache->redis->close();
            }
        } catch (\Exception $ignore) {
        }
    }

    /**
     * @param string $task
     *
     * @throws \Exception
     */
    public function working($task)
    {
        try {
            $job = $this->jobFactory->createJob($task);
        } catch (\Exception $ex) {
            applicationLog("[main] Create job error: task={$task}");
            throw $ex;
        }

        $this->worker->addFunction($task, [$job, 'run']);

        applicationLog("[main] New task was started: task={$task}");
        while ($this->worker->work()) {
            $this->closeConnections();

            $returnCode = $this->worker->returnCode();
            if ($returnCode !== GEARMAN_SUCCESS) {
                applicationLog("[main] Task was failed: task={$task}, code={$returnCode}");
                break;
            }
        }
    }
}