<?php

namespace gearman;

use console\components\billing\Billing;
use console\modules\sms\components\SmsManager;
use console\modules\sms\components\SmsProviderFactory;
use gearman\exceptions\UnknownTaskException;
use gearman\jobs\BaseJob;
use gearman\jobs\BillingJob;
use gearman\jobs\EmailJob;
use gearman\jobs\OrderStatisticJob;
use gearman\jobs\OrderTrackJob;
use gearman\jobs\ParkingAddJob;
use gearman\jobs\SmsJob;
use orderTrackService\OrderTrackService;
use yii\base\Application;

/**
 * Class JobFactory
 * @package gearman
 */
class JobFactory
{
    const TASK_BILLING = 'Billing';
    const TASK_EMAIL = 'Email';
    const TASK_SMS = 'Sms';
    const TASK_INFO = 'Info';
    const TRACK_ORDER_TRACK = 'OrderTrack';
    const TRACK_PARKING_ADD = 'Parking_Add';
    const TRACK_ORDER_STATISTIC = 'OrderStatistic';

    /**
     * @var Application
     */
    private $application;

    /**
     * @param Application $application
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }

    /**
     * @param string $task
     *
     * @return BaseJob
     * @throws UnknownTaskException
     */
    public function createJob($task)
    {
        switch ($task) {
            case self::TASK_BILLING:
                return new BillingJob(new Billing());

            case self::TASK_EMAIL:
                return new EmailJob();

            case self::TRACK_ORDER_STATISTIC:
                return new OrderStatisticJob();

            case self::TRACK_ORDER_TRACK:
                return new OrderTrackJob(new OrderTrackService());

            case self::TRACK_PARKING_ADD:
                return new ParkingAddJob($this->application->db);

            case self::TASK_SMS:
                return new SmsJob(new SmsManager(new SmsProviderFactory()));

            default:
                throw new UnknownTaskException("Unknown task: {$task}");
        }
    }

}