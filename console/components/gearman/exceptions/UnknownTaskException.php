<?php

namespace gearman\exceptions;

/**
 * Class UnknownTaskException
 * @package gearman\exceptions
 */
class UnknownTaskException extends \DomainException
{

}