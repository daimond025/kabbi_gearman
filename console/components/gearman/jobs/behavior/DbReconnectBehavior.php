<?php

namespace gearman\jobs\behavior;

use console\components\db\DbReconnectManager;
use gearman\jobs\BaseJob;
use yii\base\Behavior;
use yii\db\Connection;

class DbReconnectBehavior extends Behavior
{
    /**
     * @var Connection
     */
    public $connection;

    /**
     * @var DbReconnectManager
     */
    private $reconnectManager;

    public function __construct(DbReconnectManager $reconnectManager, array $config = [])
    {
        parent::__construct($config);
        $this->reconnectManager = $reconnectManager;
    }

    public function events()
    {
        return [
            BaseJob::EVENT_BEFORE_RUN => 'reconnect',
        ];
    }

    /**
     * @throws \yii\db\Exception
     */
    public function reconnect()
    {
        $this->reconnectManager->reconnectIfNeeded($this->connection);
    }
}
