<?php

namespace gearman\jobs;

use gearman\jobs\behavior\DbReconnectBehavior;
use Ramsey\Uuid\Uuid;
use orderTrackService\OrderTrackService;

/**
 * Class OrderTrackJob
 * @package gearman\jobs
 */
class OrderTrackJob extends BaseJob
{
    /**
     * @var OrderTrackService
     */
    private $orderTrackService;

    /**
     * OrderTrackJob constructor.
     *
     * @param OrderTrackService $orderTrackService
     * @param array             $config
     */
    public function __construct(OrderTrackService $orderTrackService, array $config = [])
    {
        parent::__construct($config);

        $this->orderTrackService = $orderTrackService;
    }

    public function behaviors()
    {
        return [
            ['class' => DbReconnectBehavior::class, 'connection' => \Yii::$app->db]
        ];
    }

    /**
     * @param array $data
     *
     * @return mixed|void
     */
    public function execute(array $data)
    {
        $requestId = array_key_exists('requestId', $data) ? $data['requestId'] : Uuid::uuid4()->toString();
        applicationLog("[orderTrack] requestId={$requestId},orderId={$data['orderId']} Saving order track to mongoDB");

        try {
            $orderId   = $data['orderId'];
            $trackData = json_decode($data['orderTrack'], true);
            $track     = empty($trackData['order_route']) ? [] : $trackData['order_route'];

            $this->orderTrackService->setTrack($orderId, $track);

            applicationLog("[orderTrack] requestId={$requestId},orderId={$data['orderId']} Success order track saving");
        } catch (\Exception $ex) {
            applicationLog("[orderTrack] requestId={$requestId},orderId={$data['orderId']} Saving order track error: error={$ex->getMessage()}");

            \Yii::error('Saving order track error', 'order-track');
            \Yii::error($ex, 'order-track');
        }
    }
}