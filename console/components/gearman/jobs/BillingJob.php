<?php

namespace gearman\jobs;

use console\components\billing\Billing;
use gearman\jobs\behavior\DbReconnectBehavior;

/**
 * Class BillingJob
 * @package gearman\jobs
 */
class BillingJob extends BaseJob
{
    /**
     * @var Billing
     */
    private $billing;

    /**
     * BillingJob constructor.
     *
     * @param Billing $billing
     * @param array   $config
     */
    public function __construct(Billing $billing, array $config = [])
    {
        parent::__construct($config);

        $this->billing = $billing;
    }

    public function behaviors()
    {
        return [
            ['class' => DbReconnectBehavior::class, 'connection' => \Yii::$app->db]
        ];
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function execute(array $data)
    {
        return $this->billing->createTransaction($data);
    }

}