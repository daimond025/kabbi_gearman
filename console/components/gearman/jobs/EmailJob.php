<?php

namespace gearman\jobs;

use Ramsey\Uuid\Uuid;
use yii\helpers\ArrayHelper;
use yii\swiftmailer\Mailer;

class EmailJob extends BaseJob
{
    /**
     * @param array $data
     *
     * @return mixed|void
     * @throws \yii\base\InvalidConfigException
     */
    public function execute(array $data)
    {
        $mailer = \Yii::createObject(Mailer::className());
        $mailer->setTransport([
            'class'      => 'Swift_SmtpTransport',
            'host'       => ArrayHelper::getValue($data, 'transport.host'),
            'port'       => ArrayHelper::getValue($data, 'transport.port'),
            'username'   => ArrayHelper::getValue($data, 'transport.username'),
            'password'   => ArrayHelper::getValue($data, 'transport.password'),
            'encryption' => ArrayHelper::getValue($data, 'transport.encryption'),
        ]);

        $messages   = [];
        $addressees = (array)ArrayHelper::getValue($data, 'mail.to', []);

        foreach ($addressees as $addressee) {
            $messages[] = $mailer->compose()
                ->setFrom(ArrayHelper::getValue($data, 'mail.from'))
                ->setTo($addressee)
                ->setSubject(ArrayHelper::getValue($data, 'mail.subject'))
                ->setHtmlBody(ArrayHelper::getValue($data, 'mail.body'));
        }

        try {
            $successCount = $mailer->sendMultiple($messages);
        } catch (\Swift_TransportException $exception) {
            $errorData = $data;
            unset($errorData['mail']['body']);
            \Yii::error($exception->getMessage() . PHP_EOL
                . json_encode($errorData));
            return;
        }

        $mailer->getTransport()->stop();

        $requestId = ArrayHelper::getValue($data, 'requestId', Uuid::uuid4()->toString());
        applicationLog('[email] requestId={requestId} Successful {successCount} of {count} email sending: to={to}, subject={subject}',
            [
                'requestId'    => $requestId,
                'to'           => implode(', ', $addressees),
                'subject'      => ArrayHelper::getValue($data, 'mail.subject'),
                'successCount' => $successCount,
                'count'        => count($addressees),
            ]);

    }
}