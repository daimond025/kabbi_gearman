<?php

namespace gearman\jobs;

use yii\base\Component;

/**
 * Class BaseJob
 * @package gearman\jobs
 */
abstract class BaseJob extends Component
{
    const ACTION_PING = 'ping';
    const RESULT_PING = 'ok';
    const EVENT_BEFORE_RUN = 'eventBeforeRun';

    /**
     * @var \GearmanJob
     */
    private $job;

    /**
     * @param array $data
     *
     * @return mixed
     */
    abstract public function execute(array $data);

    /**
     * @return array
     */
    private function getJobData()
    {
        $data = json_decode($this->job->workload(), true);

        return empty($data) ? [] : $data;
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    private function isPingAction(array $data)
    {
        return isset($data['action']) && $data['action'] === self::ACTION_PING;
    }

    /**
     * @param \GearmanJob $job
     *
     * @return mixed
     */
    final public function run(\GearmanJob $job)
    {
        $this->trigger(self::EVENT_BEFORE_RUN);

        $this->job = $job;
        $data      = $this->getJobData();


        if ($this->isPingAction($data)) {
            return self::RESULT_PING;
        }
        return $this->execute($data);
    }

}