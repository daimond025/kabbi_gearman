<?php

namespace gearman\jobs;

use gearman\jobs\behavior\DbReconnectBehavior;
use Ramsey\Uuid\Uuid;
use yii\db\Connection;

/**
 * Class ParkingAddJob
 * @package gearman\jobs
 */
class ParkingAddJob extends BaseJob
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * Parking_AddJob constructor.
     *
     * @param Connection $connection
     * @param array      $config
     */
    public function __construct(Connection $connection, array $config = [])
    {
        parent::__construct($config);

        $this->connection = $connection;
    }

    public function behaviors()
    {
        return [
            ['class' => DbReconnectBehavior::class, 'connection' => $this->connection]
        ];
    }

    /**
     * @param array $data
     *
     * @return mixed|void
     * @throws \Exception
     */
    public function execute(array $data)
    {
        $requestId = array_key_exists('requestId', $data) ? $data['requestId'] : Uuid::uuid4()->toString();
        applicationLog("[parking] requestId={$requestId} Saving parking");

        try {
            foreach ($data['arrayOfParkings'] as $parkArr) {
                $insertValue[] = [$data['insertedParkingId'], $parkArr['parking_id']];
            }

            if (!empty($insertValue)) {
                $this->connection->createCommand()
                    ->batchInsert('tbl_fix_tariff', ['from', 'to'], $insertValue)
                    ->execute();
            }
            applicationLog("[parking] requestId={$requestId} Successful parking saving");
        } catch (\Exception $ex) {
            applicationLog("[parking] requestId={$requestId} Saving parking error: error={$ex->getMessage()}");
            throw $ex;
        }
    }
}
