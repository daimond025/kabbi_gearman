<?php

namespace gearman\jobs;

use console\modules\statistic\components\StatisticService;
use gearman\jobs\behavior\DbReconnectBehavior;
use Ramsey\Uuid\Uuid;

class OrderStatisticJob extends BaseJob
{
    const FEEDBACK_TYPE = 'feedback';

    public function behaviors()
    {
        return [
            ['class' => DbReconnectBehavior::class, 'connection' => \Yii::$app->db]
        ];
    }

    /**
     * @param array $data (order_id Order id, type: 'feedback' Type of statistic strategy)
     *
     * @return mixed
     */
    public function execute(array $data)
    {
        $requestId = array_key_exists('requestId', $data) ? $data['requestId'] : Uuid::uuid4()->toString();
        applicationLog("[orderStatistic] requestId={$requestId},orderId={$data['order_id']} Adding order to statistics");

        $statisticService = new StatisticService($data['order_id']);

        $error = null;
        if (isset($data['type']) && $data['type'] === self::FEEDBACK_TYPE) {
            if ($statisticService->addFeedback() === false) {
                $error = 'Error to add a feedback to statistic';
            }
        } else {
            if (!$statisticService->addOrder()) {
                $error = 'Error to add the order with id:' . $data['order_id'] . ' to statistic';
            }
        }

        if ($error) {
            applicationLog("[orderStatistic] requestId={$requestId},orderId={$data['order_id']} Error adding order to statistics: error={$error}");
            \Yii::error($error, 'statistic');
        } else {
            applicationLog("[orderStatistic] requestId={$requestId},orderId={$data['order_id']} Order successfully added to statistics");
        }
    }
}
