<?php

namespace gearman\jobs;

use console\modules\sms\components\SmsManager;
use console\modules\sms\components\SmsProviderFactory;
use gearman\jobs\behavior\DbReconnectBehavior;

/**
 * Class SmsJob
 * @package gearman\jobs
 */
class SmsJob extends BaseJob
{
    /**
     * @var SmsProviderFactory
     */
    public $smsProviderFactory;
    /**
     * @var SmsManager
     */
    private $smsManager;

    /**
     * SmsJob constructor.
     *
     * @param SmsManager $smsManager
     * @param array      $config
     */
    public function __construct(SmsManager $smsManager, array $config = [])
    {
        parent::__construct($config);
        $this->smsManager = $smsManager;
    }

    public function behaviors()
    {
        return [
            ['class' => DbReconnectBehavior::class, 'connection' => \Yii::$app->db]
        ];
    }

    /**
     * @param array $data
     *
     * @return mixed|void
     */
    public function execute(array $data)
    {
        if (!$this->smsManager->sendSms($data)) {
            \Yii::warning("Входные параметры:\n" . print_r($data, true) .
                "\nНе удалась отправка СМС: ни один из серверов не отвечает", 'SMS');
        }
    }
}
