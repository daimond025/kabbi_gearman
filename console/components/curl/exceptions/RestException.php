<?php

namespace console\components\curl\exceptions;

/**
 * REST exception
 */
class RestException extends \yii\base\Exception
{
    private $_response;

    /**
     * Constructor with response
     * @param type $message
     * @param type $code
     * @param type $response
     * @param \Exception $previous
     */
    public function __construct($message = "", $code = 0, $response = null,
        \Exception $previous = null)
    {
        $this->_response = $response;
        parent::__construct($message, $code, $previous);
    }

    /**
     * Getting the response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->_response;
    }

    /**
     * Get exception name
     * @return string
     */
    public function getName()
    {
        return 'REST exception';
    }

}