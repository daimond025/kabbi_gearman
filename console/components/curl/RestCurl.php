<?php

namespace console\components\curl;

use console\components\curl\Curl;
use console\components\curl\CurlResponse;
use console\components\curl\exceptions\RestException;

/**
 * Rest wrapper of Curl component
 */
class RestCurl extends \yii\base\Object
{
    /**
     * @var Curl
     */
    private $curl;

    public $connectTimeout;
    public $timeout;

    public function init()
    {
        $this->curl = new Curl();
        $this->connectTimeout = getenv('CURL_CONNECT_TIMEOUT');
        $this->timeout = getenv('CURL_TIMEOUT');
        $this->curl->options = [
            'connectTimeout' => $this->connectTimeout,
            'timeout'        => $this->timeout,
        ];

        parent::init();
    }

    /**
     * Execute get request
     *
     * @param string $url
     * @param array  $headers
     *
     * @return array
     * @throws RestException
     */
    public function get($url, $headers = [])
    {
        $this->curl->headers = $headers;

        /* @var $response CurlResponse|boolean */
        $response = $this->curl->get($url);

        if ($response === false) {
            throw new RestException($this->curl->error());
        } else {
            $result     = json_decode($response->body, true);
            $statusCode = $response->headers['Status-Code'];

            switch ($statusCode) {
                case 200:
                    return $result;
                case 404:
                    return null;
                default:
                    $message = empty($result['message'])
                        ? $this->curl->error() : $result['message'];
                    throw new RestException($message, $statusCode, $result);
            }
        }
    }

    /**
     * Execute create request
     *
     * @param string $url
     * @param array  $params
     * @param array  $headers
     *
     * @return array|null
     * @throws RestException
     */
    public function post($url, $params = null, $headers = [])
    {
        $this->curl->headers = $headers;

        /* @var $response CurlResponse|boolean */
        $response = $this->curl->post($url, $params);

        if ($response === false) {
            throw new RestException($this->curl->error());
        } else {
            $result     = json_decode($response->body, true);
            $statusCode = $response->headers['Status-Code'];

            switch ($statusCode) {
                case 200:
                    return $result;
                default:
                    $message = empty($result['message'])
                        ? $this->curl->error() : $result['message'];
                    throw new RestException($message, $statusCode, $result);
            }
        }
    }

    /**
     * Execute update request
     *
     * @param string $url
     * @param array  $params
     * @param array  $headers
     *
     * @return array|null
     * @throws RestException
     */
    public function put($url, $params = null, $headers = [])
    {
        $this->curl->headers = $headers;

        /* @var $response CurlResponse|boolean */
        $response = $this->curl->put($url, $params);

        if ($response === false) {
            throw new RestException($this->curl->error());
        } else {
            $result     = json_decode($response->body, true);
            $statusCode = $response->headers['Status-Code'];

            switch ($statusCode) {
                case 200:
                    return $result;
                default:
                    $message = empty($result['message'])
                        ? $this->curl->error() : $result['message'];
                    throw new RestException($message, $statusCode, $result);
            }
        }
    }

    /**
     * Execute delete request
     *
     * @param string $url
     * @param array  $params
     * @param array  $headers
     *
     * @return bool
     * @throws RestException
     */
    public function delete($url, $params = null, $headers = [])
    {
        $this->curl->headers = $headers;

        /* @var $response CurlResponse|boolean */
        $response = $this->curl->delete($url, $params);

        if ($response === false) {
            throw new RestException($this->curl->error());
        } else {
            $result     = json_decode($response->body, true);
            $statusCode = $response->headers['Status-Code'];

            switch ($statusCode) {
                case 200:
                    return true;
                default:
                    $message = empty($result['message'])
                        ? $this->curl->error() : $result['message'];
                    throw new RestException($message, $statusCode, $result);
            }
        }
    }

}