<?php

namespace console\components\log;

use Yii;
use yii\console\Application;
use yii\base\Component;

abstract class BaseLog extends Component
{
    public $enabled = true;
    public $separator = " | ";
    public $template = " >>> {messages} " . PHP_EOL . PHP_EOL;
    public $params = [];

    protected $messages;

    /**
     * Getting log params
     * @return string
     */
    protected function getParams()
    {
        $params = [];
        if (is_array($this->params)) {
            foreach ($this->params as $key => $value) {
                $params[] = "$key=$value";
            }
        }

        return implode(',', $params);
    }

    /**
     * Getting log messages
     * @return string
     */
    protected function getMessages()
    {
        return is_array($this->messages)
            ? implode($this->separator, $this->messages) : '';
    }

    /**
     * Add message to log
     *
     * @param $message
     */
    public function log($message)
    {
        $this->messages[] = $message;

        $this->export();
    }

    /**
     * Export messages
     */
    abstract function export();

}