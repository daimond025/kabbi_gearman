<?php

namespace console\components\log;

class SysLog extends BaseLog
{
    public $identity = false;
    public $priority = LOG_INFO;

    /**
     * @inheritdoc
     */
    public function export()
    {
        if (!$this->enabled) {
            return;
        }

        $message = $this->getMessages();
        $text     = str_replace(
            ['{params}', '{messages}'],
            [$this->getParams(), $message],
            $this->template
        );

        if (!empty($message)) {
            openlog($this->identity, LOG_ODELAY | LOG_PID, LOG_USER);
            syslog($this->priority, $text);
            closelog();
        }

        $this->messages = [];
    }
}
