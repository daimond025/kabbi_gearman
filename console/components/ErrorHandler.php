<?php

namespace console\components;

use Yii;
use \yii\base\Event;

/**
 * Custom error handler which trigger global event
 * @package console\components
 */
class ErrorHandler extends \yii\console\ErrorHandler
{
    const BEFORE_LOG_EXCEPTION_EVENT = 'beforeLogException';

    /**
     * @inheritdoc
     */
    public function logException($exception)
    {
//        Yii::error('Error handler!');
        Yii::$app->trigger(self::BEFORE_LOG_EXCEPTION_EVENT,
            new Event(['sender' => $exception]));

        parent::logException($exception);
    }

}