<?php


namespace console\modules\statistic\events;


use yii\base\Event;

class StatisticSaveEvent extends Event
{
    public $orderData;
}