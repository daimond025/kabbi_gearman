<?php

namespace console\modules\statistic\components\services\statistic\filters;


use console\modules\statistic\components\services\order\OrderService;
use console\modules\statistic\models\helpers\DeviceHelper;
use console\modules\statistic\models\helpers\OrderStatusGroup;
use console\modules\statistic\models\interfaces\IMobileAppRepository;
use console\modules\statistic\models\order\OrderStatistic;

class TenantCompanyFilter
{


    protected $orderData;
    protected $mobileAppRepository;
    protected $orderService;
    protected $isPreOrder = null;

    public function __construct($orderData, IMobileAppRepository $mobileAppRepository, OrderService $orderService)
    {
        $this->orderData = $orderData;
        $this->mobileAppRepository = $mobileAppRepository;
        $this->orderService = $orderService;
    }


    public function runFilter($statistics, OrderStatistic $document)
    {
        if (OrderStatusGroup::isCompleted($this->orderData['status_group']) && $this->isTenantCompanyId()) {

            $documentMainCompany = $this->getMainCompanyCollection();
            $documentMainCompany->statistics = $this->changeStatisticsMainCompany($documentMainCompany->statistics);
            $documentMainCompany->save();

            $statistics = $this->changeStatisticSubCompany($statistics);
            $document->tenant_company_id = (string)$this->orderData['tenant_company_id'];
        }

        return $statistics;
    }

    protected function changeStatisticSubCompany($statistics)
    {
        $statistics = $this->changeStatisticsReceived($statistics, true);

        if ($this->isPreOrder()) {
            $statistics = $this->changeStatisticsPreOrder($statistics, true);
        }

        return $statistics;
    }

    protected function changeStatisticsMainCompany($statistics)
    {
        $statistics = $this->changeStatisticsReceived($statistics, false);

        if ($this->isPreOrder()) {
            $statistics = $this->changeStatisticsPreOrder($statistics, false);
        }

        return $statistics;
    }

    protected function changeStatisticsPreOrder($statistics, $isForSubCompany)
    {
        if ($isForSubCompany) {
            $statistics['pre_order']['quantity']++;
        } else {
            $statistics['pre_order']['quantity']--;
        }

        //Устройство
        if (!empty($this->orderData['device'])) {
            $curDeviceValue = $statistics['pre_order']['device'][$this->orderData['device']];

            if ($isForSubCompany) {
                $statistics['pre_order']['device'][$this->orderData['device']] = $this->getDeviceCalculateValueForSubCompany($curDeviceValue);
            } else {
                $statistics['pre_order']['device'][$this->orderData['device']] = $this->getDeviceCalculateValueForMainCompany($curDeviceValue);
            }
        }

        if (!empty($this->orderData['parking_id'])) {
            //Детально - количество
            if ($isForSubCompany) {
                $this->prepareDetailPreOrderSubCompany($statistics);
                $statistics['pre_order']['detail'][$this->orderData['parking_id']]['quantity']++;
            } else {
                $statistics['pre_order']['detail'][$this->orderData['parking_id']]['quantity']--;
            }

            //Детально - устройство
            if (!empty($this->orderData['device'])) {
                $curDeviceValue = $statistics['pre_order']['detail'][$this->orderData['parking_id']][$this->orderData['device']];

                if ($isForSubCompany) {
                    $statistics['pre_order']['detail'][$this->orderData['parking_id']][$this->orderData['device']] = $this->getDeviceCalculateValueForSubCompany($curDeviceValue);
                } else {
                    $statistics['pre_order']['detail'][$this->orderData['parking_id']][$this->orderData['device']] = $this->getDeviceCalculateValueForMainCompany($curDeviceValue);
                }
            }
        }

        return $statistics;
    }

    protected function isPreOrder()
    {
        if (!is_bool($this->isPreOrder)) {
            $this->isPreOrder = $this->orderService->isPreOrder($this->orderData['order_id']);
        }

        return $this->isPreOrder;
    }


    protected function changeStatisticsReceived($statistics, $isForSubCompany)
    {
        if ($isForSubCompany) {
            $statistics['received']['quantity']++;
        } else {
            $statistics['received']['quantity']--;
        }

        //Устройство
        if (!empty($this->orderData['device'])) {
            $curDeviceValue = $statistics['received']['device'][$this->orderData['device']];

            if ($isForSubCompany) {
                $statistics['received']['device'][$this->orderData['device']] = $this->getDeviceCalculateValueForSubCompany($curDeviceValue);
            } else {
                $statistics['received']['device'][$this->orderData['device']] = $this->getDeviceCalculateValueForMainCompany($curDeviceValue);
            }

        }

        if (!empty($this->orderData['parking_id'])) {
            //Детально - количество

            if ($isForSubCompany) {
                $this->prepareDetailReceivedSubCompany($statistics);
                $statistics['received']['detail'][$this->orderData['parking_id']]['quantity']++;
            } else {
                $statistics['received']['detail'][$this->orderData['parking_id']]['quantity']--;
            }

            //Детально - устройство
            if (!empty($this->orderData['device'])) {
                $curDeviceValue = $statistics['received']['detail'][$this->orderData['parking_id']][$this->orderData['device']];

                if ($isForSubCompany) {
                    $statistics['received']['detail'][$this->orderData['parking_id']][$this->orderData['device']] = $this->getDeviceCalculateValueForSubCompany($curDeviceValue);
                } else {
                    $statistics['received']['detail'][$this->orderData['parking_id']][$this->orderData['device']] = $this->getDeviceCalculateValueForMainCompany($curDeviceValue);
                }

            }
        }

        return $statistics;
    }


    protected function prepareDetailReceivedSubCompany(&$statistics)
    {

        if (!array_key_exists($this->orderData['parking_id'], $statistics['received']['detail'])) {
            $statistics['received']['detail'][$this->orderData['parking_id']] = [
                'quantity' => 0,
                $this->orderData['device'] => 0,
            ];
        } elseif (!array_key_exists($this->orderData['device'], $statistics['received']['detail'][$this->orderData['parking_id']])) {
            $statistics['received']['detail'][$this->orderData['parking_id']] = [
                $this->orderData['device'] => 0,
            ];
        }
    }

    protected function prepareDetailPreOrderSubCompany(&$statistics)
    {

        if (!array_key_exists($this->orderData['parking_id'], $statistics['pre_order']['detail'])) {
            $statistics['pre_order']['detail'][$this->orderData['parking_id']] = [
                'quantity' => 0,
                $this->orderData['device'] => 0,
            ];
        } elseif (!array_key_exists($this->orderData['device'], $statistics['pre_order']['detail'][$this->orderData['parking_id']])) {
            $statistics['pre_order']['detail'][$this->orderData['parking_id']] = [
                $this->orderData['device'] => 0,
            ];
        }
    }


    protected function isTenantCompanyId()
    {
        return (bool)$this->orderData['tenant_company_id'];
    }


    /**
     * @return OrderStatistic
     */
    protected function getMainCompanyCollection()
    {
        applicationLog(
            "[orderStatistic] orderId={$this->orderData['order_id']} 'Поиск документа головной компании...'"
        );

        return OrderStatistic::find()
            ->where([
                'tenant_id'         => (string)$this->orderData['tenant_id'],
                'city_id'           => (string)$this->orderData['city_id'],
                'currency_id'       => (string)$this->orderData['currency_id'],
                'date'              => (string)date('d.m.Y', $this->orderData['create_time']),
                'position_id'       => (string)$this->orderData['position_id'],
            ])
            ->one();
    }


    private function getDeviceCalculateValueForSubCompany($deviceCurValue)
    {
        return $this->deviceCalculateValue($deviceCurValue, true);
    }


    private function getDeviceCalculateValueForMainCompany($deviceCurValue)
    {
        return $this->deviceCalculateValue($deviceCurValue, false);
    }


    private function deviceCalculateValue($deviceCurValue, $isAdding)
    {
        if (DeviceHelper::isMobile($this->orderData['device'])) {
            $appId = $this->getMobileAppId();

            if (!is_array($deviceCurValue)) {
                if (is_numeric($deviceCurValue)) {
                    if ($isAdding) {
                        $newDeviceValue = $deviceCurValue + 1;
                    } else {
                        $newDeviceValue = $deviceCurValue - 1;
                    }
                } else {
                    $newDeviceValue = 1;
                }

                $deviceCurValue = [$appId => $newDeviceValue];
            } else {
                if ($isAdding) {
                    $deviceCurValue[$appId]++;
                } else {
                    $deviceCurValue[$appId]--;
                }
            }
        } else {
            if ($isAdding) {
                $deviceCurValue++;
            } else {
                $deviceCurValue--;
            }
        }

        return $deviceCurValue;
    }


    /**
     * @return int
     */
    private function getMobileAppId()
    {
        return $this->orderData['app_id'] ?: $this->mobileAppRepository->getAppId($this->orderData['tenant_id']);
    }

}