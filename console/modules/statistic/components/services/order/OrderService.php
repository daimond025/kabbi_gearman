<?php

namespace console\modules\statistic\components\services\order;


use console\modules\statistic\models\order\OrderChangeData;

class OrderService
{

    const STATUSES_PRE_ORDER = [6, 16];

    public function isPreOrder($orderId)
    {
        /* @var $firstChangeOrder OrderChangeData */
        $firstChangeOrder = OrderChangeData::find()
            ->where(['order_id' => $orderId])
            ->orderBy(['change_time' => SORT_ASC])
            ->one();

        if (!$firstChangeOrder->change_field === 'status_id') {
            throw new OrderServiceException('Exception definitions pre order');
        }

        if (in_array($firstChangeOrder->change_val, self::STATUSES_PRE_ORDER)) {
            return true;
        }

        return false;
    }
}