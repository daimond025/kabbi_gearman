<?php


namespace console\modules\statistic\models\interfaces;


interface IStatisticService
{
    public function addOrder();

    public function addFeedback();
}