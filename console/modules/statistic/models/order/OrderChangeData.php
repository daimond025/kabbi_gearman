<?php

namespace console\modules\statistic\models\order;
use console\components\billing\models\Order;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_change_data}}".
 *
 * @property integer $change_id
 * @property integer $tenant_id
 * @property integer $order_id
 * @property string $change_field
 * @property string $change_object_id
 * @property string $change_object_type
 * @property string $change_subject
 * @property string $change_val
 * @property string $change_time
 */
class OrderChangeData extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_change_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'change_id' => 'Change ID',
            'tenant_id' => 'Tenant ID',
            'order_id' => 'Order ID',
            'change_field' => 'Change Field',
            'change_object_id' => 'Change Object ID',
            'change_object_type' => 'Change Object Type',
            'change_subject' => 'Change Subject',
            'change_val' => 'Change Val',
            'change_time' => 'Change Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

}