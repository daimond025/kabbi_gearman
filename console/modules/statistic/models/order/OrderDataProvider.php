<?php


namespace console\modules\statistic\models\order;


use console\modules\statistic\models\helpers\OrderStatusGroup;
use console\modules\statistic\models\helpers\OrderStatusHelper;
use console\modules\statistic\models\order\interfaces\IOrderRepository;

class OrderDataProvider
{
    /**
     * @var \console\modules\statistic\models\order\interfaces\IOrderRepository
     */
    private $repository;
    private $logStatuses;

    public function __construct(IOrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $orderId
     * @return array
     */
    public function getData($orderId)
    {
        $order = $this->repository->getById($orderId);

        if (!is_array($order)) {
            return [];
        }

        if (OrderStatusGroup::isFinished($order['status_group'])) {
            $statusLogData = $this->repository->getStatusLog($orderId);
            $this->logStatuses = array_column($statusLogData, 'change_val');

            if ($order['status_group'] == OrderStatusGroup::COMPLETED) {
                $order['price'] = $this->repository->getSummaryCost($orderId);
                $order['pick_up'] = $this->getPickupTime($order, $statusLogData);
            }

            $order['warning'] = $this->getWarningStatuses();
            $order['status_log'] = $this->logStatuses;
        }

        return $order;
    }

    /**
     * Определяем время подъезда авто
     * @param array $order
     * @param array $statusLogData
     * @return float
     */
    private function getPickupTime($order, $statusLogData)
    {
        $pickupTime = 0;
        $key = array_search(OrderStatusHelper::CAR_AT_PLACE, $this->logStatuses);

        if ($key !== false) {
            $orderTime = $this->getOrderTime($order);
            $pickupTime = ($statusLogData[$key]['change_time'] - $orderTime) / 60;
        }

        return $pickupTime;
    }

    /**
     * Определяем время заказа в зависимости от того предварительный заказ или нет.
     * У предварительного order_time переводим в utc
     * @param array $order
     * @return int
     */
    private function getOrderTime($order)
    {
        return array_search(OrderStatusHelper::PRE_ORDER, $this->logStatuses) ?
            $order['order_time'] - $order['time_offset'] : $order['create_time'];
    }

    /**
     * @return array
     */
    private function getWarningStatuses()
    {
        return array_intersect($this->logStatuses, OrderStatusHelper::getWarningStatuses());
    }
}