<?php


namespace console\modules\statistic\models\order;


use console\modules\statistic\components\services\order\OrderService;
use console\modules\statistic\components\services\statistic\filters\TenantCompanyFilter;
use console\modules\statistic\models\helpers\DeviceHelper;
use console\modules\statistic\models\helpers\OrderStatusGroup;
use console\modules\statistic\models\helpers\OrderStatusHelper;
use console\modules\statistic\models\interfaces\IMobileAppRepository;

class OrderDocumentCalculator
{
    /**
     * @var OrderStatistic
     */
    private $document;
    /**
     * @var array
     */
    private $orderData;
    /**
     * @var IMobileAppRepository
     */
    private $mobileAppRepository;

    public function __construct(OrderStatistic $document, array $orderData, IMobileAppRepository $mobileAppRepository)
    {
        $this->document = $document;
        $this->orderData = $orderData;
        $this->mobileAppRepository = $mobileAppRepository;
    }

    public function calculate()
    {
        $statistics = $this->document->statistics;

        if (OrderStatusGroup::isReceived($this->orderData['status_group'])) {
            //Количество
            $statistics['received']['quantity']++;
            //Устройство
            if (!empty($this->orderData['device'])) {
                $curDeviceValue = $statistics['received']['device'][$this->orderData['device']];
                $statistics['received']['device'][$this->orderData['device']] = $this->getDeviceCalculateValue($curDeviceValue);
            }

            if (!empty($this->orderData['parking_id'])) {
                //Детально - количество
                $statistics['received']['detail'][$this->orderData['parking_id']]['quantity']++;
                //Детально - устройство
                if (!empty($this->orderData['device'])) {
                    $curDeviceValue = $statistics['received']['detail'][$this->orderData['parking_id']][$this->orderData['device']];
                    $statistics['received']['detail'][$this->orderData['parking_id']][$this->orderData['device']] = $this->getDeviceCalculateValue($curDeviceValue);
                }
            }

            //Предварительный заказ
            if (OrderStatusGroup::isPreOrder($this->orderData['status_group'])) {
                //Количество
                $statistics['pre_order']['quantity']++;
                //Устройство
                if (!empty($this->orderData['device'])) {
                    $curDeviceValue = $statistics['pre_order']['device'][$this->orderData['device']];
                    $statistics['pre_order']['device'][$this->orderData['device']] = $this->getDeviceCalculateValue($curDeviceValue);
                }

                if (!empty($this->orderData['parking_id'])) {
                    //Детально - количество
                    $statistics['pre_order']['detail'][$this->orderData['parking_id']]['quantity']++;
                    //Детально - устройство
                    if (!empty($this->orderData['device'])) {
                        $curDeviceValue = $statistics['pre_order']['detail'][$this->orderData['parking_id']][$this->orderData['device']];
                        $statistics['pre_order']['detail'][$this->orderData['parking_id']][$this->orderData['device']] = $this->getDeviceCalculateValue($curDeviceValue);
                    }
                }
            }
        } elseif (OrderStatusGroup::isCompleted($this->orderData['status_group'])) {
            //Количество
            $statistics['completed']['quantity']++;
            //Сумма
            $statistics['completed']['sum'] += $this->orderData['price'];
            //Устройство
            if (!empty($this->orderData['device'])) {
                $curDeviceValue = $statistics['completed']['device'][$this->orderData['device']];
                $statistics['completed']['device'][$this->orderData['device']] = $this->getDeviceCalculateValue($curDeviceValue);
            }
            //Водители
            $statistics['completed']['workers']['worker_id'][] = $this->orderData['worker_id'];
            $statistics['completed']['workers']['worker_id'] = array_unique($statistics['completed']['workers']['worker_id']);
            $statistics['completed']['workers']['cnt'] = count($statistics['completed']['workers']['worker_id']);
            //-------------------------------------------------
            //Детально - Тарифы
            if (!empty($this->orderData['tariff_id'])) {
                //Количество
                $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['quantity']++;
                //Cумма
                $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['sum'] += $this->orderData['price'];
                //Устройство
                if (!empty($this->orderData['device'])) {
                    $curDeviceValue = $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']][$this->orderData['device']];
                    $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']][$this->orderData['device']] = $this->getDeviceCalculateValue($curDeviceValue);
                }
                //Водители
                $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['workers']['worker_id'][] = $this->orderData['worker_id'];
                $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['workers']['worker_id'] = array_unique($statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['workers']['worker_id']);
                $statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['workers']['cnt'] = count($statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['workers']['worker_id']);
            }
            //--------------------------------------------------
            //Детально - Виды оплат
            if (!empty($this->orderData['payment'])) {
                //Количество
                $statistics['completed']['detail']['payment'][$this->orderData['payment']]['quantity']++;
                //Cумма
                $statistics['completed']['detail']['payment'][$this->orderData['payment']]['sum'] += $this->orderData['price'];
                //Устройство
                if (!empty($this->orderData['device'])) {
                    $curDeviceValue = $statistics['completed']['detail']['payment'][$this->orderData['payment']][$this->orderData['device']];
                    $statistics['completed']['detail']['payment'][$this->orderData['payment']][$this->orderData['device']] = $this->getDeviceCalculateValue($curDeviceValue);
                }
                //Водители
                $statistics['completed']['detail']['payment'][$this->orderData['payment']]['workers']['worker_id'][] = $this->orderData['worker_id'];
                $statistics['completed']['detail']['payment'][$this->orderData['payment']]['workers']['worker_id'] = array_unique($statistics['completed']['detail']['payment'][$this->orderData['payment']]['workers']['worker_id']);
                if (!empty($this->orderData['tariff_id']) && isset($statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['workers']['worker_id'])) {
                    $statistics['completed']['detail']['payment'][$this->orderData['payment']]['workers']['cnt'] = count($statistics['completed']['detail']['tariffs'][$this->orderData['tariff_id']]['workers']['worker_id']);
                }
            }
            //---------------------------------------------------
            //Детально - Средние показатели
            $statistics['completed']['detail']['averages']['price'] = round($statistics['completed']['sum'] / $statistics['completed']['quantity'],
                2);
            $statistics['completed']['detail']['averages']['pick_up_sum_time'] += $this->orderData['pick_up'];
            $statistics['completed']['detail']['averages']['pick_up'] += round($statistics['completed']['detail']['averages']['pick_up_sum_time'] / $statistics['completed']['quantity']);
        } elseif (OrderStatusGroup::isRejected($this->orderData['status_group'])) {
            //Количество
            $statistics['rejected']['quantity']++;
            //Cумма
            $statistics['rejected']['sum'] += $this->orderData['predv_price'] != 0 ?
                $this->orderData['predv_price'] : $statistics['completed']['detail']['averages']['price'];
            //Устройство
            if (!empty($this->orderData['device'])) {
                $curDeviceValue = $statistics['rejected']['device'][$this->orderData['device']];
                $statistics['rejected']['device'][$this->orderData['device']] = $this->getDeviceCalculateValue($curDeviceValue);
            }
            //Водители
            if (!empty($this->orderData['worker_id'])) {
                $statistics['rejected']['workers']['worker_id'][] = $this->orderData['worker_id'];
                $statistics['rejected']['workers']['worker_id'] = array_unique($statistics['rejected']['workers']['worker_id']);
                $statistics['rejected']['workers']['cnt'] = count($statistics['rejected']['workers']['worker_id']);
            }
            //--------------------------------------------------------
            //Детально - Список причин отмены
            if (!empty($this->orderData['status_id'])) {
                $statistics['rejected']['detail']['reasons']['rejected'][$this->orderData['status_id']]++;
            }

            //считать сколько отмен заказов, после "водитель опаздывает"
            if (in_array(OrderStatusHelper::LATED_DRIVER, $this->orderData['status_log'])) {
                $statistics['rejected']['detail']['reasons']['rejected']['after'][OrderStatusHelper::LATED_DRIVER]++;
            }

            if (!empty($this->orderData['warning'])) {
                foreach ($this->orderData['warning'] as $warning) {
                    $statistics['rejected']['detail']['reasons']['warning'][$warning]['rejected']++;
                }
            }
        }

        if (!empty($this->orderData['warning'])) {
            foreach ($this->orderData['warning'] as $warning) {
                $statistics['rejected']['detail']['reasons']['warning'][$warning]['cnt']++;
            }
        }

        $tenantCompanyFilter = new TenantCompanyFilter(
            $this->orderData,
            $this->mobileAppRepository,
            new OrderService()
        );

        $statistics = $tenantCompanyFilter->runFilter($statistics, $this->document);


        $this->document->statistics = $statistics;
    }


    private function getDeviceCalculateValue($deviceCurValue)
    {
        if (DeviceHelper::isMobile($this->orderData['device'])) {
            $appId = $this->getMobileAppId();

            if (!is_array($deviceCurValue)) {
                if (is_numeric($deviceCurValue)) {
                    $newDeviceValue = $deviceCurValue + 1;
                } else {
                    $newDeviceValue = 1;
                }

                $deviceCurValue = [$appId => $newDeviceValue];
            } else {
                $deviceCurValue[$appId]++;
            }
        } else {
            $deviceCurValue++;
        }

        return $deviceCurValue;
    }

    /**
     * @return int
     */
    private function getMobileAppId()
    {
        return $this->orderData['app_id'] ?: $this->mobileAppRepository->getAppId($this->orderData['tenant_id']);
    }
}