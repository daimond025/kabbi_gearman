<?php


namespace console\modules\statistic\models\order;


use console\modules\statistic\models\DocumentCreator;

class OrderDocumentCreator extends DocumentCreator
{
    /**
     * @return OrderStatistic
     */
    public function create()
    {
        return new OrderStatistic([
            'tenant_id'         => $this->orderData['tenant_id'],
            'city_id'           => $this->orderData['city_id'],
            'currency_id'       => $this->orderData['currency_id'],
            'position_id'       => $this->orderData['position_id'],
            'statistics'        => $this->getStatistics(),
            'date'              => $this->getDate(),
            'timestamp'         => $this->getTimestamp(),
            'tenant_company_id' => '',
        ]);
    }

    /**
     * @return false|int
     */
    private function getTimestamp()
    {
        return mktime(23, 59, 59, date("n", $this->orderData['create_time']),
            date("j", $this->orderData['create_time']), date("Y", $this->orderData['create_time']));
    }

    /**
     * @return false|string
     */
    private function getDate()
    {
        return date('d.m.Y', $this->orderData['create_time']);
    }

    /**
     * @return array
     */
    private function getStatistics()
    {
        return [
            'received'     => [
                'quantity' => 0,
                'device'   => $this->getDevices(),
                'detail'   => [],
            ],
            'pre_order'    => [
                'quantity' => 0,
                'device'   => $this->getDevices(),
                'detail'   => [],
            ],
            'completed'    => [
                'quantity' => 0,
                'sum'      => 0,
                'device'   => $this->getDevices(),
                'workers'  => [
                    'cnt'       => 0,
                    'worker_id' => [],
                ],
                'detail'   => [
                    'tariffs'  => [],
                    'payment'  => [],
                    'averages' => [
                        'price'            => 0,
                        'pick_up'          => 0,
                        'pick_up_sum_time' => 0,
                    ],
                ],
            ],
            'rejected'     => [
                'quantity' => 0,
                'sum'      => 0,
                'device'   => $this->getDevices(),
                'workers'  => [
                    'cnt'       => 0,
                    'worker_id' => [],
                ],
                'detail'   => [
                    'reasons' => [
                        'rejected' => [],
                        'warning'  => [],
                    ],
                ],
            ],
            'bad_feedback' => [
                'one' => 0,
                'two' => 0,
            ],
        ];
    }
}