<?php

namespace console\modules\statistic\models\order;


use yii\mongodb\ActiveRecord;

/**
 * Class OrderStatistic
 * @package console\modules\statistic\
 *
 * @property array $statistics
 * @property string $city_id
 * @property string $timestamp
 * @property string $tenant_id
 * @property string $date
 * @property string $currency_id
 * @property string $position_id
 * @property string $tenant_company_id
 */
class OrderStatistic extends ActiveRecord
{
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'order_stat';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'statistics', 'city_id', 'timestamp', 'tenant_id', 'date', 'currency_id', 'position_id', 'tenant_company_id'];
    }
}
