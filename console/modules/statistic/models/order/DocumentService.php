<?php


namespace console\modules\statistic\models\order;


use console\modules\statistic\events\StatisticSaveEvent;
use console\modules\statistic\models\helpers\OrderStatusGroup;
use console\modules\statistic\models\order\interfaces\IOrderRepository;
use console\modules\statistic\repositories\MobileAppRepository;
use Yii;
use yii\base\Component;
use yii\helpers\Console;

class DocumentService extends Component
{
    const EVENT_AFTER_SAVE = 'afterSave';

    /** @var int */
    private $orderId;
    /** @var array */
    private $orderData;
    /** @var IOrderRepository */
    private $orderRepository;
    /** @var OrderStatistic */
    private $document;

    public function __construct($orderId, IOrderRepository $orderRepository, array $config = [])
    {
        parent::__construct($config);

        $this->orderRepository = $orderRepository;
        $this->orderId         = $orderId;
        $this->orderData       = $this->getOrderData($orderId);

        $this->log("Данные заказа: " . json_encode($this->orderData, JSON_UNESCAPED_UNICODE));

        $this->documentLoad();
    }

    /**
     * @return bool|null
     */
    public function addOrder()
    {
        if (empty($this->orderData)) {
            $this->log('Заказ не найден');

            return null;
        }

        $this->log('Добавляем заказ в документ...');

        /** @var MobileAppRepository $mobileAppRepository */
        $mobileAppRepository = Yii::createObject(MobileAppRepository::class);
        (new OrderDocumentCalculator($this->document, $this->orderData, $mobileAppRepository))->calculate();

        return $this->save();
    }

    /**
     * @return bool|null
     */
    public function addFeedback()
    {
        if (!OrderStatusGroup::isFinished($this->orderData['status_group'])) {
            return null;
        }

        $feedback = (int)$this->orderRepository->getBadFeedbackRating($this->orderData['order_id']);

        if (empty($feedback)) {
            return null;
        }

        $mark                        = $feedback === 1 ? 'one' : 'two';
        $this->orderData['feedback'] = $mark;
        $statistics                  = $this->document->statistics;
        $statistics['bad_feedback'][$mark]++;

        $this->document->statistics = $statistics;

        return $this->save();
    }

    /**
     * @return bool
     */
    private function save()
    {
        if (!$this->document->save(false)) {
            $this->log('Ошибка сохранения документа!');

            return false;
        }

        $this->log('Документ успешно сохранен.');

        $this->trigger(self::EVENT_AFTER_SAVE, (new StatisticSaveEvent(['orderData' => $this->orderData])));

        return true;
    }

    private function documentLoad()
    {
        $document = $this->getCollection();

        if (empty($document)) {
            $this->log('Документ не найден.');

            $document = $this->create();
        }

        $this->document = $document;
    }

    /**
     * @return null|OrderStatistic|\yii\mongodb\ActiveRecord
     */
    private function getCollection()
    {
        $this->log('Поиск документа...');

        return OrderStatistic::find()
            ->where([
                'tenant_id'         => (string)$this->orderData['tenant_id'],
                'city_id'           => (string)$this->orderData['city_id'],
                'currency_id'       => (string)$this->orderData['currency_id'],
                'date'              => (string)date('d.m.Y', $this->orderData['create_time']),
                'position_id'       => (string)$this->orderData['position_id'],
                'tenant_company_id' => (string)$this->orderData['tenant_company_id'],
            ])
            ->one();
    }

    /**
     * Формирование структуры нового документа
     * @return OrderStatistic
     */
    private function create()
    {
        $this->log('Создаем новый документ...');

        return (new OrderDocumentCreator($this->orderData))->create();
    }

    /**
     * @param $orderId
     *
     * @return array
     */
    private function getOrderData($orderId)
    {
        $this->log('Получаем заказ с id = ' . $orderId . '...');

        return (new OrderDataProvider($this->orderRepository))->getData($orderId);
    }

    /**
     * @param string $message
     */
    private function log($message)
    {
        applicationLog("[orderStatistic] orderId={$this->orderId} {$message}");
    }
}