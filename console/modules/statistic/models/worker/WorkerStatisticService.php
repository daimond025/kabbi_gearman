<?php


namespace console\modules\statistic\models\worker;


use console\modules\statistic\models\helpers\OrderStatusGroup;
use console\modules\statistic\models\interfaces\IStatisticService;
use Yii;
use yii\helpers\Console;

class WorkerStatisticService implements IStatisticService
{
    private $orderData;
    private $document;

    public function __construct(array $orderData)
    {
        $this->orderData = $orderData;
        $this->document  = $this->getDocument();
    }

    public function addOrder()
    {
        $this->log('Добавляем статистику по заказу водителю c id = ' . $this->orderData['worker_id']);

        $this->documentCalculate();

        $this->save();
    }

    public function addFeedback()
    {
        $feedback = isset($this->orderData['feedback']) ? $this->orderData['feedback'] : null;

        if (empty($feedback)) {
            return null;
        }

        $statistics = $this->document->statistics;

        $statistics['bad_feedback'][$feedback]++;

        $this->document->statistics = $statistics;

        return $this->save();
    }

    /**
     * @return bool
     */
    private function save()
    {
        if (!$this->document->save(false)) {
            Yii::error('Ошибка добавления статистики по исполнителю', 'statistic');
            $this->log('Ошибка добавления статистики по исполнителю!');
            $this->log(json_encode($this->document->getErrors(), JSON_UNESCAPED_UNICODE));

            return false;
        }

        $this->log('Исполнитель успешно добавлен в документ.');

        return true;
    }

    /**
     * @return WorkerStatistic
     */
    private function getDocument()
    {
        $this->log('Поиск документа статистики исполнителя...');

        $document = WorkerStatistic::find()
            ->where([
                'tenant_id'   => (string)$this->orderData['tenant_id'],
                'worker_id'   => (string)$this->orderData['worker_id'],
                'currency_id' => (string)$this->orderData['currency_id'],
                'date'        => (string)date('d.m.Y', $this->orderData['create_time']),
                'position_id' => (string)$this->orderData['position_id'],
            ])
            ->one();

        if (empty($document)) {
            $this->log('Документ не найден.');
            $this->log('Создаем новый...');
            $document = (new WorkerDocumentCreator($this->orderData))->create();
            $this->log('Структура документа сформирована.');
        } else {
            $this->log(json_encode($document->getAttributes(), JSON_UNESCAPED_UNICODE));
        }

        return $document;
    }

    private function documentCalculate()
    {
        $this->log('Добавляем заказ в документ...');

        (new WorkerDocumentCalculator($this->document, $this->orderData))->calculate();

        $this->log(json_encode($this->document->statistics, JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param string $message
     */
    private function log($message)
    {
        $orderId = isset($this->orderData['order_id']) ? $this->orderData['order_id'] : null;
        applicationLog("[orderStatistic] orderId={$orderId} {$message}");
    }
}