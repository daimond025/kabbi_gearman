<?php

namespace console\modules\statistic\models\worker;

use yii\mongodb\ActiveRecord;

/**
 * Class WorkerStatistic
 * @package console\modules\statistic\models\worker
 *
 * @property array $statistics
 * @property string $worker_id
 * @property string $timestamp
 * @property string $tenant_id
 * @property string $date
 * @property string $currency_id
 * @property string $position_id
 */
class WorkerStatistic extends ActiveRecord
{
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'worker_stat';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'statistics', 'worker_id', 'timestamp', 'tenant_id', 'date', 'currency_id', 'position_id'];
    }
}
