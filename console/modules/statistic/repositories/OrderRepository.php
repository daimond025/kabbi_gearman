<?php


namespace console\modules\statistic\repositories;


use console\modules\statistic\models\order\interfaces\IOrderRepository;
use Yii;

class OrderRepository implements IOrderRepository
{
    /**
     * @param int $orderId
     *
     * @return array|false
     */
    public function getById($orderId)
    {
        return Yii::$app->db->createCommand('SELECT * FROM {{%order}} as o
INNER JOIN {{%order_status}} as os ON o.status_id=os.status_id WHERE order_id=:order_id')
            ->bindValue(':order_id', $orderId)
            ->queryOne();
    }

    /**
     * @param int $orderId
     *
     * @return array
     */
    public function getStatusLog($orderId)
    {
        return Yii::$app->db->createCommand('SELECT `change_val`, `change_time` FROM {{%order_change_data}} 
WHERE order_id=:order_id AND change_field="status_id"')
            ->bindValue(':order_id', $orderId)
            ->queryAll();
    }

    /**
     * @param int $orderId
     *
     * @return false|null|string
     */
    public function getSummaryCost($orderId)
    {
        return (float)Yii::$app->db->createCommand('SELECT `summary_cost` FROM {{%order_detail_cost}} WHERE order_id=:order_id')
            ->bindValue(':order_id', $orderId)
            ->queryScalar();
    }

    /**
     * @param int $orderId
     *
     * @return false|null|string
     */
    public function getBadFeedbackRating($orderId)
    {
        return Yii::$app->db->createCommand('SELECT `rating` FROM {{%client_review}} WHERE order_id=:order_id AND rating IN(1, 2)')
            ->bindValue(':order_id', $orderId)
            ->queryScalar();
    }
}