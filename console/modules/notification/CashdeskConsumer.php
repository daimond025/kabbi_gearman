<?php

namespace notification;

use notification\cashdesk\CashdeskService;
use Psr\Log\LoggerInterface;

class CashdeskConsumer extends NotificationConsumer
{
    /**
     * @var CashdeskService
     */
    private $service;

    public function __construct(MQConnection $connection, CashdeskService $service, LoggerInterface $logger)
    {
        $this->service = $service;

        parent::__construct($connection, $logger, 'Cashdesk');
    }

    protected function processNotification($transactionId)
    {
        $this->service->process($transactionId);
    }
}
