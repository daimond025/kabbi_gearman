<?php

namespace notification;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use yii\db\Connection;

abstract class NotificationConsumer
{
    /**
     * @var MQConnection
     */
    private $connection;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $name;

    /**
     * WorkerBalanceConsumer constructor.
     *
     * @param MQConnection    $connection
     * @param LoggerInterface $logger
     * @param string          $name
     */
    public function __construct(MQConnection $connection, LoggerInterface $logger, string $name)
    {
        $this->connection = $connection;
        $this->logger = $logger;
        $this->name = $name;
    }

    public function start()
    {
        $connection = new AMQPStreamConnection($this->connection->getHost(), $this->connection->getPort(),
            $this->connection->getUser(), $this->connection->getPassword(), $this->connection->getVirtualHost());

        $channel = $connection->channel();
        try {
            $channel->exchange_declare(CompleteTransactionProducer::EXCHANGE_COMPLETE_TRANSACTION, 'fanout', false,
                false, false);

            list($queue, ,) = $channel->queue_declare('', false, false, true, false);
            $channel->queue_bind($queue, CompleteTransactionProducer::EXCHANGE_COMPLETE_TRANSACTION);

            $channel->basic_consume($queue, '', false, false, false, false, [$this, 'callback']);

            $this->logger->info('Consumer started:');
            while (count($channel->callbacks)) {
                $channel->wait();
            }
        } catch (\Exception $ex) {
            $this->logger->error("$this->name consumer error: error={$ex->getMessage()}");
        } finally {
            $this->logger->info('Consumer stopped.');

            $channel->close();
            $connection->close();
        }
    }

    /**
     * @param AMQPMessage $msg
     */
    public function callback($msg)
    {
        $transactionId = $msg->body;
        try {
            $this->processNotification($transactionId);
        } catch (\Exception $ex) {
            $this->logger->error("Process notification error: name={$this->name}, transactionId={$transactionId}, error=\"{$ex->getMessage()}\"");
        } finally {
            $this->closeConnection();
        }
    }

    abstract protected function processNotification($transactionId);

    protected function closeConnection()
    {
        $db = \Yii::$app->get('db', false);
        if ($db instanceof Connection) {
            $db->close();
        }
    }
}