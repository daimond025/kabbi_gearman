<?php

namespace notification\workerBalance;

use notification\workerBalance\ar\Account;
use notification\workerBalance\ar\Operation;
use notification\workerBalance\ar\WorkerBalanceNotification;
use notification\workerBalance\exceptions\NotFoundException;
use notification\workerBalance\exceptions\WorkerBalanceNotificationException;
use notification\workerBalance\models\Currency;
use notification\workerBalance\models\Money;
use notification\workerBalance\models\Operation as OperationModel;
use notification\workerBalance\models\Order;
use notification\workerBalance\models\Worker;
use Psr\Log\LoggerInterface;
use yii\db\ActiveQuery;

/**
 * Class WorkerBalanceService
 * @package notification\workerBalance
 */
class WorkerBalanceService
{
    /**
     * @var ApiClient
     */
    private $apiClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var int
     */
    private $transactionId;

    /**
     * WorkerBalanceService constructor.
     *
     * @param ApiClient       $apiClient
     * @param LoggerInterface $logger
     */
    public function __construct(ApiClient $apiClient, LoggerInterface $logger)
    {
        $this->apiClient = $apiClient;
        $this->logger = $logger;
    }

    /**
     * @param int $transactionId
     *
     * @throws WorkerBalanceNotificationException
     * @throws NotFoundException
     */
    public function process($transactionId)
    {
        $this->transactionId = $transactionId;

        $tenantId = $this->getTenantId();
        $notification = $this->getWorkerBalanceNotification($tenantId);

        if ($notification === null) {
            $this->logger->info("Worker balance notification not found: tenantId={$tenantId}, transactionId={$this->transactionId}");

            return;
        }

        $operations = $this->getOperations();
        if ($operations === []) {
            throw new NotFoundException('Transaction operations not found');
        }

        $this->sendWorkerBalanceNotifications($notification, $operations);
    }

    /**
     * @param WorkerBalanceNotification $notification
     * @param Operation[]               $operations
     *
     * @throws WorkerBalanceNotificationException
     */
    private function sendWorkerBalanceNotifications($notification, $operations)
    {
        foreach ($operations as $operation) {
            $order = empty($operation->transaction->order) ? null : new Order($operation->transaction->order->order_id,
                $operation->transaction->order->order_number, $operation->transaction->order->payment);

            if ((int)$notification->deposit_notify === 0
                && (int)$operation->type_id === Operation::TYPE_INCOME) {
                continue;
            }

            if ((int)$notification->writeoff_notify === 0
                && in_array((int)$operation->type_id, [Operation::TYPE_EXPENSES, Operation::TYPE_CASH_OUT], true)) {
                continue;
            }

            $this->apiClient->sendWorkerBalanceNotification(
                $notification->url,
                $notification->token,
                new OperationModel(
                    $operation->operation_id,
                    $this->getOperationType($operation->type_id),
                    (new \DateTime($operation->date))->format('Y-m-d H:i:s'),
                    $operation->getComment(),
                    new Worker($operation->account->worker->callsign),
                    new Money(round($operation->sum * (10 ** $operation->account->currency->minor_unit)),
                        new Currency($operation->account->currency->code, $operation->account->currency->minor_unit)),
                    $order));

            $this->logger->info("Worker balance notification was successful sent: transactionId={$this->transactionId}, operationId={$operation->operation_id}");
        }
    }

    /**
     * @param int $type
     *
     * @return string
     */
    private function getOperationType($type)
    {
        return (int)$type === Operation::TYPE_INCOME ? OperationModel::OPERATION_TYPE_RECHARGE : OperationModel::OPERATION_TYPE_CHARGE_OFF;
    }

    /**
     * @return int
     */
    private function getTenantId()
    {
        $tenantId = Account::find()
            ->alias('a')
            ->select('a.tenant_id')
            ->innerJoinWith(['operations as o'], false)
            ->where([
                'o.transaction_id' => $this->transactionId,
                'a.acc_kind_id'    => Account::WORKER_KIND,
                'a.acc_type_id'    => Account::PASSIVE_TYPE,
            ])
            ->scalar();

        return empty($tenantId) ? null : $tenantId;
    }

    /**
     * @param int $tenantId
     *
     * @return WorkerBalanceNotification
     */
    private function getWorkerBalanceNotification($tenantId)
    {
        $notification = WorkerBalanceNotification::find()
            ->where(['tenant_id' => $tenantId])
            ->one();

        return empty($notification) ? null : $notification;
    }

    /**
     * @return Operation[]
     */
    private function getOperations()
    {
        $operations = Operation::find()
            ->alias('op')
            ->select([
                'op.operation_id',
                'op.transaction_id',
                'op.account_id',
                'op.type_id',
                'op.sum',
                'op.date',
                'op.comment',
            ])
            ->innerJoinWith([
                'transaction as t'      => function ($query) {
                    /* @var ActiveQuery $query */
                    $query->select([
                        't.transaction_id',
                        't.order_id',
                        't.payment_method',
                        't.comment',
                        't.terminal_transact_number',
                    ]);
                },
                'account as a'          => function ($query) {
                    /* @var ActiveQuery $query */
                    $query->select([
                        'a.account_id',
                        'a.acc_kind_id',
                        'a.tenant_id',
                        'a.currency_id',
                        'a.owner_id',
                        'a.balance',
                    ]);
                },
                'account.currency as c' => function ($query) {
                    /* @var ActiveQuery $query */
                    $query->select(['c.currency_id', 'c.code', 'c.minor_unit']);
                },
                'account.worker as w'   => function ($query) {
                    /* @var ActiveQuery $query */
                    $query->select(['w.worker_id', 'w.callsign']);
                },
            ])
            ->joinWith([
                'transaction.order as o' => function ($query) {
                    /* @var ActiveQuery $query */
                    $query->select(['o.order_id', 'o.order_number', 'o.payment']);
                },
            ])
            ->where([
                't.transaction_id' => $this->transactionId,
                'a.acc_kind_id'    => Account::WORKER_KIND,
                'a.acc_type_id'    => Account::PASSIVE_TYPE,
            ])
            ->all();

        return empty($operations) ? [] : $operations;
    }


}