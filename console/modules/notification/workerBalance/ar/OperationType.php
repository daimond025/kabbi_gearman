<?php

namespace notification\workerBalance\ar;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%operation_type}}".
 *
 * @property integer     $type_id
 * @property string      $name
 *
 * @property Operation[] $operations
 */
class OperationType extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%operation_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'name'    => 'Name',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['type_id' => 'type_id']);
    }
}
