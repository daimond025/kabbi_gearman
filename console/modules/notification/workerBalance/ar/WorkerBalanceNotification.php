<?php

namespace notification\workerBalance\ar;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker_balance_notification}}".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property string  $name
 * @property string  $url
 * @property string  $token
 * @property integer $writeoff_notify
 * @property integer $deposit_notify
 * @property integer $created_at
 * @property integer $updated_at
 */
class WorkerBalanceNotification extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_balance_notification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name', 'url', 'token'], 'required'],
            [['tenant_id', 'writeoff_notify', 'deposit_notify', 'created_at', 'updated_at'], 'integer'],
            [['name', 'url', 'token'], 'string', 'max' => 255],
        ];
    }
}
