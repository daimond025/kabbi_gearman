<?php

namespace notification\workerBalance\ar;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $order_id
 * @property integer $tenant_id
 * @property integer $worker_id
 * @property integer $car_id
 * @property integer $city_id
 * @property integer $tariff_id
 * @property integer $user_create
 * @property integer $status_id
 * @property integer $user_modifed
 * @property integer $company_id
 * @property integer $parking_id
 * @property string  $address
 * @property string  $comment
 * @property string  $predv_price
 * @property string  $device
 * @property integer $order_number
 * @property string  $payment
 * @property integer $show_phone
 * @property integer $create_time
 * @property integer $status_time
 * @property integer $time_to_client
 * @property string  $client_device_token
 * @property integer $app_id
 * @property integer $order_time
 * @property string  $predv_distance
 * @property integer $predv_time
 * @property integer $call_warning_id
 * @property string  $phone
 * @property integer $client_id
 * @property integer $bonus_payment
 * @property integer $currency_id
 * @property integer $time_offset
 * @property integer $is_fix
 * @property integer $update_time
 * @property integer $deny_refuse_order
 * @property integer $position_id
 * @property integer $promo_code_id
 */
class Order extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'tenant_id',
                    'tariff_id',
                    'status_id',
                    'address',
                    'order_number',
                    'status_time',
                    'client_id',
                    'currency_id',
                    'position_id',
                ],
                'required',
            ],
            [
                [
                    'tenant_id',
                    'worker_id',
                    'car_id',
                    'city_id',
                    'tariff_id',
                    'user_create',
                    'status_id',
                    'user_modifed',
                    'company_id',
                    'parking_id',
                    'order_number',
                    'show_phone',
                    'create_time',
                    'status_time',
                    'time_to_client',
                    'app_id',
                    'order_time',
                    'predv_time',
                    'call_warning_id',
                    'client_id',
                    'bonus_payment',
                    'currency_id',
                    'time_offset',
                    'is_fix',
                    'update_time',
                    'deny_refuse_order',
                    'position_id',
                    'promo_code_id',
                ],
                'integer',
            ],
            [['address', 'comment', 'device', 'payment', 'client_device_token'], 'string'],
            [['predv_price', 'predv_distance'], 'number'],
            [['phone'], 'string', 'max' => 255],
            [
                ['tenant_id', 'order_number'],
                'unique',
                'targetAttribute' => ['tenant_id', 'order_number'],
                'message'         => 'The combination of Tenant ID and Order Number has already been taken.',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'            => 'Order ID',
            'tenant_id'           => 'Tenant ID',
            'worker_id'           => 'Worker ID',
            'car_id'              => 'Car ID',
            'city_id'             => 'City ID',
            'tariff_id'           => 'Tariff ID',
            'user_create'         => 'User Create',
            'status_id'           => 'Status ID',
            'user_modifed'        => 'User Modifed',
            'company_id'          => 'Company ID',
            'parking_id'          => 'Parking ID',
            'address'             => 'Address',
            'comment'             => 'Comment',
            'predv_price'         => 'Predv Price',
            'device'              => 'Device',
            'order_number'        => 'Order Number',
            'payment'             => 'Payment',
            'show_phone'          => 'Show Phone',
            'create_time'         => 'Create Time',
            'status_time'         => 'Status Time',
            'time_to_client'      => 'Time To Client',
            'client_device_token' => 'Client Device Token',
            'app_id'              => 'App ID',
            'order_time'          => 'Order Time',
            'predv_distance'      => 'Predv Distance',
            'predv_time'          => 'Predv Time',
            'call_warning_id'     => 'Call Warning ID',
            'phone'               => 'Phone',
            'client_id'           => 'Client ID',
            'bonus_payment'       => 'Bonus Payment',
            'currency_id'         => 'Currency ID',
            'time_offset'         => 'Time Offset',
            'is_fix'              => 'Is Fix',
            'update_time'         => 'Update Time',
            'deny_refuse_order'   => 'Deny Refuse Order',
            'position_id'         => 'Position ID',
            'promo_code_id'       => 'Promo Code ID',
        ];
    }
}
