<?php

namespace notification\workerBalance\ar;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%operation}}".
 *
 * @property integer       $operation_id
 * @property integer       $type_id
 * @property integer       $account_id
 * @property integer       $transaction_id
 * @property string        $comment
 * @property string        $date
 * @property string        $owner_name
 * @property string        $sum
 * @property string        $saldo
 *
 * @property Account       $account
 * @property Transaction   $transaction
 * @property OperationType $type
 */
class Operation extends ActiveRecord
{
    const TYPE_INCOME = 1;
    const TYPE_EXPENSES = 2;
    const TYPE_CASH_OUT = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%operation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'account_id', 'transaction_id'], 'required'],
            [['type_id', 'account_id', 'transaction_id'], 'integer'],
            [['date'], 'safe'],
            [['sum', 'saldo'], 'number'],
            [['comment', 'owner_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'operation_id'   => 'Operation ID',
            'type_id'        => 'Type ID',
            'account_id'     => 'Account ID',
            'transaction_id' => 'Transaction ID',
            'comment'        => 'Comment',
            'date'           => 'Date',
            'owner_name'     => 'Owner Name',
            'sum'            => 'Sum',
            'saldo'          => 'Saldo',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['transaction_id' => 'transaction_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OperationType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return string
     */
    public function getComment()
    {
        if ((int)$this->transaction->type_id === Transaction::GOOTAX_PAYMENT_TYPE) {
            if ((int)$this->type_id === self::TYPE_EXPENSES) {
                return t('payment', 'Payment for:', null, 'ru') . ' ' . $this->transaction->comment;
            }

            if ((string)$this->transaction->payment_method === Transaction::CASH_PAYMENT) {
                return t('balance', $this->type->name, null, 'ru');
            }

            if ((string)$this->transaction->payment_method === Transaction::PERSONAL_ACCOUNT_PAYMENT) {
                return t('payment', 'Refund money', null, 'ru');
            }
        }

        if ((int)$this->account->acc_kind_id === Account::TENANT_KIND
            && (string)$this->transaction->payment_method !== Transaction::TERMINAL_ELECSNET_PAYMENT
        ) {
            $description = !empty($this->transaction->comment)
                ? t('balance', 'Comment', null, 'ru') . ': ' . $this->transaction->comment : '';

            return t('balance', $this->type->name, null, 'ru') . $description;
        }

        $text = t('balance', $this->type->name, null, 'ru');
        if ((int)$this->type_id === self::TYPE_INCOME
            && (int)$this->account->acc_kind_id === Account::WORKER_KIND
            && (string)$this->transaction->payment_method === Transaction::CARD_PAYMENT
        ) {
            $text .= ' - ' . t('balance', Transaction::getPaymentMethodName(
                    isset($this->transaction->order_id) ? Transaction::PERSONAL_ACCOUNT_PAYMENT : $this->transaction->payment_method),
                    null, 'ru');
        } elseif ((int)$this->type_id === self::TYPE_INCOME
            && !in_array((int)$this->account->acc_kind_id,
                [Account::CLIENT_BONUS_KIND, Account::CLIENT_BONUS_UDS_GAME_KIND], true)
        ) {
            $text .= ' - ' . t('balance', Transaction::getPaymentMethodName(
                    isset($this->transaction->order_id) && (int)$this->account->acc_kind_id === Account::WORKER_KIND
                        ? Transaction::PERSONAL_ACCOUNT_PAYMENT : $this->transaction->payment_method), null, 'ru');
        }

        $description = $this->getDescription();

        return $text . ': ' . $description;
    }

    /**
     * @return string
     */
    private function getDescription()
    {
        $text = '';
        if (!empty($this->comment)) {
            if ((string)$this->transaction->payment_method === Transaction::WITHDRAWAL_TARIFF_PAYMENT) {
                $arComment = explode('||', $this->comment);
                $arComment[0] = t('balance', $arComment[0], null, 'ru');
                $arComment[1] = "\"$arComment[1]\"";
                $comment = implode(' ', $arComment);
            } else {
                $arComment = explode(' №', $this->comment);
                $arComment[0] = t('balance', $arComment[0], null, 'ru');
                $comment = count($arComment) === 1 ? $arComment[0] : implode(' №', $arComment);
            }

            $text .= $comment;
        }

        if (!empty($this->transaction->terminal_transact_number)
            && (string)$this->transaction->payment_method === Transaction::TERMINAL_ELECSNET_PAYMENT) {
            $text .= '. ' . t('balance',
                    'Terminal №{terminal} Check №{check}', [
                        'terminal' => substr($this->transaction->terminal_transact_number, 0, 8),
                        'check'    => substr($this->transaction->terminal_transact_number, 8, 4),
                    ], 'ru');
        }

        return $text;
    }
}
