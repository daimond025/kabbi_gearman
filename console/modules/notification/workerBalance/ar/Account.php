<?php

namespace notification\workerBalance\ar;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer     $account_id
 * @property integer     $acc_kind_id
 * @property integer     $acc_type_id
 * @property integer     $owner_id
 * @property integer     $currency_id
 * @property integer     $tenant_id
 * @property string      $balance
 *
 * @property Currency    $currency
 * @property Operation[] $operations
 * @property Worker      $worker
 */
class Account extends ActiveRecord
{
    const ACTIVE_TYPE = 1;
    const PASSIVE_TYPE = 2;

    const TENANT_KIND = 1;
    const WORKER_KIND = 2;
    const CLIENT_KIND = 3;
    const COMPANY_KIND = 4;
    const SYSTEM_KIND = 5;
    const CLIENT_BONUS_KIND = 7;
    const SYSTEM_BONUS_KIND = 8;
    const CLIENT_BONUS_UDS_GAME_KIND = 9;
    const SYSTEM_BONUS_UDS_GAME_KIND = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_kind_id', 'acc_type_id', 'currency_id'], 'required'],
            [['acc_kind_id', 'acc_type_id', 'owner_id', 'currency_id', 'tenant_id'], 'integer'],
            [['balance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id'  => 'Account ID',
            'acc_kind_id' => 'Acc Kind ID',
            'acc_type_id' => 'Acc Type ID',
            'owner_id'    => 'Owner ID',
            'currency_id' => 'Currency ID',
            'tenant_id'   => 'Tenant ID',
            'balance'     => 'Balance',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'owner_id']);
    }
}
