<?php

namespace notification\workerBalance\ar;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property integer $currency_id
 * @property integer $type_id
 * @property string  $name
 * @property string  $code
 * @property string  $symbol
 * @property integer $numeric_code
 * @property integer $minor_unit
 */
class Currency extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'name'], 'required'],
            [['type_id', 'numeric_code', 'minor_unit'], 'integer'],
            [['name', 'code', 'symbol'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_id'  => 'Currency ID',
            'type_id'      => 'Type ID',
            'name'         => 'Name',
            'code'         => 'Code',
            'symbol'       => 'Symbol',
            'numeric_code' => 'Numeric Code',
            'minor_unit'   => 'Minor Unit',
        ];
    }

}
