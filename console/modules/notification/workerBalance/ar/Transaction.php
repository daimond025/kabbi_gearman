<?php

namespace notification\workerBalance\ar;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "{{%transaction}}".
 *
 * @property integer     $transaction_id
 * @property integer     $type_id
 * @property integer     $sender_acc_id
 * @property integer     $receiver_acc_id
 * @property string      $payment_method
 * @property string      $date
 * @property string      $comment
 * @property integer     $user_created
 * @property integer     $city_id
 * @property string      $terminal_transact_number
 * @property string      $terminal_transact_date
 * @property integer     $order_id
 * @property string      $sum
 *
 * @property Operation[] $operations
 * @property Order       $order
 */
class Transaction extends ActiveRecord
{
    const DEPOSIT_TYPE = 1;
    const WITHDRAWAL_TYPE = 2;
    const ORDER_PAYMENT_TYPE = 3;
    const GOOTAX_PAYMENT_TYPE = 4;

    const BONUS_PAYMENT = 'BONUS';
    const CASH_PAYMENT = 'CASH';
    const CARD_PAYMENT = 'CARD';
    const PERSONAL_ACCOUNT_PAYMENT = 'PERSONAL_ACCOUNT';
    const CORP_BALANCE_PAYMENT = 'CORP_BALANCE';
    const TERMINAL_QIWI_PAYMENT = 'TERMINAL_QIWI';
    const WITHDRAWAL_PAYMENT = 'WITHDRAWAL';
    const WITHDRAWAL_TARIFF_PAYMENT = 'WITHDRAWAL_FOR_TARIFF';
    const WITHDRAWAL_CASH_OUT_PAYMENT = 'WITHDRAWAL_FOR_CASH_OUT';
    const TERMINAL_ELECSNET_PAYMENT = 'TERMINAL_ELECSNET';
    const TERMINAL_MILLION_PAYMENT = 'TERMINAL_MILLION';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'sender_acc_id', 'receiver_acc_id', 'user_created', 'city_id', 'order_id'], 'integer'],
            [['receiver_acc_id'], 'required'],
            [['sum'], 'number'],
            [['date'], 'safe'],
            [
                ['payment_method', 'comment', 'terminal_transact_number', 'terminal_transact_date'],
                'string',
                'max' => 255,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'transaction_id'           => 'Transaction ID',
            'type_id'                  => 'Type ID',
            'sender_acc_id'            => 'Sender Acc ID',
            'receiver_acc_id'          => 'Receiver Acc ID',
            'sum_old'                  => 'Sum Old',
            'payment_method'           => 'Payment Method',
            'date'                     => 'Date',
            'comment'                  => 'Comment',
            'user_created'             => 'User Created',
            'city_id'                  => 'City ID',
            'terminal_transact_number' => 'Terminal Transact Number',
            'terminal_transact_date'   => 'Terminal Transact Date',
            'order_id'                 => 'Order ID',
            'sum'                      => 'Sum',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['transaction_id' => 'transaction_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return static::getPaymentMethodName($this->payment_method);
    }

    /**
     * @param string $paymentMethod
     *
     * @return string
     */
    public static function getPaymentMethodName($paymentMethod)
    {
        return (new Query)
            ->select('name')
            ->from('{{%payment_method}}')
            ->where(['payment' => $paymentMethod])
            ->scalar();
    }
}
