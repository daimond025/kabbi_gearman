<?php

namespace notification\workerBalance\ar;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker}}".
 *
 * @property integer $worker_id
 * @property integer $tenant_id
 * @property integer $callsign
 * @property string  $password
 * @property string  $last_name
 * @property string  $name
 * @property string  $second_name
 * @property string  $phone
 * @property string  $photo
 * @property string  $device
 * @property string  $device_token
 * @property string  $lang
 * @property string  $description
 * @property string  $device_info
 * @property string  $email
 * @property string  $partnership
 * @property string  $birthday
 * @property integer $block
 * @property integer $activate
 * @property integer $create_time
 * @property string  $card_number
 * @property string  $created_via
 * @property integer $ya_password
 */
class Worker extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'callsign', 'phone'], 'required'],
            [['tenant_id', 'callsign', 'block', 'activate', 'create_time', 'ya_password'], 'integer'],
            [['device', 'device_token', 'partnership', 'created_via'], 'string'],
            [['birthday'], 'safe'],
            [['password', 'photo', 'description', 'device_info'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name', 'email', 'card_number'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 15],
            [['lang'], 'string', 'max' => 10],
            [['ya_password'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'worker_id'    => 'Worker ID',
            'tenant_id'    => 'Tenant ID',
            'callsign'     => 'Callsign',
            'password'     => 'Password',
            'last_name'    => 'Last Name',
            'name'         => 'Name',
            'second_name'  => 'Second Name',
            'phone'        => 'Phone',
            'photo'        => 'Photo',
            'device'       => 'Device',
            'device_token' => 'Device Token',
            'lang'         => 'Lang',
            'description'  => 'Description',
            'device_info'  => 'Device Info',
            'email'        => 'Email',
            'partnership'  => 'Partnership',
            'birthday'     => 'Birthday',
            'block'        => 'Block',
            'activate'     => 'Activate',
            'create_time'  => 'Create Time',
            'card_number'  => 'Card Number',
            'created_via'  => 'Created Via',
            'ya_password'  => 'Ya Password',
        ];
    }
}
