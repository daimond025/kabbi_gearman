<?php

namespace notification\workerBalance\models;

/**
 * Class Order
 * @package notification\workerBalance\models
 */
class Order
{
    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $orderNumber;

    /**
     * @var string
     */
    private $payment;

    /**
     * Order constructor.
     *
     * @param int    $orderId
     * @param int    $orderNumber
     * @param string $payment
     */
    public function __construct($orderId, $orderNumber, $payment)
    {
        $this->orderId = $orderId;
        $this->orderNumber = $orderNumber;
        $this->payment = $payment;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return int
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }
}