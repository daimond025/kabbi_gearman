<?php

namespace notification\workerBalance\models;

/**
 * Class Worker
 * @package notification\workerBalance\models
 */
class Worker
{
    /**
     * @var string
     */
    private $callsign;

    /**
     * Worker constructor.
     *
     * @param string $callsign
     */
    public function __construct($callsign)
    {
        $this->callsign = $callsign;
    }

    /**
     * @return string
     */
    public function getCallsign()
    {
        return $this->callsign;
    }
}