<?php

namespace notification\workerBalance\models;

/**
 * Class Currency
 * @package notification\workerBalance\models
 */
class Currency
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var int
     */
    private $minorUnit;

    /**
     * Currency constructor.
     *
     * @param string $code
     * @param int    $minorUnit
     */
    public function __construct($code, $minorUnit)
    {

        $this->code = $code;
        $this->minorUnit = $minorUnit;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return int
     */
    public function getMinorUnit()
    {
        return $this->minorUnit;
    }
}