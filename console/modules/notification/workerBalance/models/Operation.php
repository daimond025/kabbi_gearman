<?php

namespace notification\workerBalance\models;

/**
 * Class Operation
 * @package notification\workerBalance\models
 */
class Operation
{
    const OPERATION_TYPE_RECHARGE = 'RECHARGE';
    const OPERATION_TYPE_CHARGE_OFF = 'CHARGE-OFF';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $date;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var Worker
     */
    private $worker;

    /**
     * @var Money
     */
    private $amount;

    /**
     * @var Order
     */
    private $order;

    /**
     * OperationNotification constructor.
     *
     * @param int    $id
     * @param string $type
     * @param string $date
     * @param string $comment
     * @param Worker $worker
     * @param Money  $amount
     * @param Order  $order
     */
    public function __construct($id, $type, $date, $comment, $worker, $amount, $order)
    {
        $this->id = $id;
        $this->type = $type;
        $this->date = $date;
        $this->comment = $comment;
        $this->worker = $worker;
        $this->amount = $amount;
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return Worker
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * @return Money
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param $token
     *
     * @return string
     */
    public function generateSignature($token)
    {
        return md5(implode(';', [
            (string)$this->id,
            (string)$this->type,
            (string)$this->date,
            (string)$this->comment,
            (string)$this->worker->getCallsign(),
            (string)$this->amount->getAmount(),
            (string)$this->amount->getCurrency()->getCode(),
            (string)$this->amount->getCurrency()->getMinorUnit(),
            (string)($this->order === null ? '' : $this->order->getOrderId()),
            (string)($this->order === null ? '' : $this->order->getOrderNumber()),
            (string)($this->order === null ? '' : $this->order->getPayment()),
            (string)$token,
        ]));
    }

}