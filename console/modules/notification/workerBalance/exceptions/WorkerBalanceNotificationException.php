<?php

namespace notification\workerBalance\exceptions;

/**
 * Class WorkerBalanceNotificationException
 * @package notification\workerBalance\exceptions
 */
class WorkerBalanceNotificationException extends \DomainException
{

}