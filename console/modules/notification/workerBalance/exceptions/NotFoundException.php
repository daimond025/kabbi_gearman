<?php

namespace notification\workerBalance\exceptions;

/**
 * Class TransactionNotFoundException
 * @package notification\workerBalance\exceptions
 */
class NotFoundException extends \DomainException
{

}