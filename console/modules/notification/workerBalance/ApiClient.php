<?php

namespace notification\workerBalance;

use GuzzleHttp\Client;
use notification\workerBalance\exceptions\WorkerBalanceNotificationException;
use notification\workerBalance\models\Operation;

/**
 * Class ApiClient
 * @package notification\workerBalance
 */
class ApiClient
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * ApiClient constructor.
     *
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string    $url
     * @param string    $token
     * @param Operation $operation
     *
     * @throws WorkerBalanceNotificationException
     */
    public function sendWorkerBalanceNotification($url, $token, $operation)
    {
        try {
            $this->httpClient->post($url, [
                'headers'     => [
                    'signature' => $operation->generateSignature($token),
                ],
                'form_params' => [
                    'id'              => $operation->getId(),
                    'type'            => $operation->getType(),
                    'date'            => $operation->getDate(),
                    'comment'         => $operation->getComment(),
                    'worker_callsign' => $operation->getWorker()->getCallsign(),
                    'amount'          => $operation->getAmount()->getAmount(),
                    'currency'        => $operation->getAmount()->getCurrency()->getCode(),
                    'minor_unit'      => $operation->getAmount()->getCurrency()->getMinorUnit(),
                    'order_id'        => $operation->getOrder() === null ? '' : $operation->getOrder()->getOrderId(),
                    'order_number'    => $operation->getOrder() === null ? '' : $operation->getOrder()->getOrderNumber(),
                    'order_payment'   => $operation->getOrder() === null ? '' : $operation->getOrder()->getPayment(),
                ],
            ]);

        } catch (\Exception $ex) {
            throw new WorkerBalanceNotificationException("Send worker balance notification error: operationId={$operation->getId()}, error={$ex->getMessage()}",
                0, $ex);
        }
    }
}