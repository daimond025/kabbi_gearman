<?php

namespace notification\cashdesk;

use notification\cashdesk\models\SendingCondition;
use notification\cashdesk\repositories\ReceiptRepository;
use Psr\Log\LoggerInterface;

class CashdeskService
{
    private const DEVICE_WORKER = 'WORKER';

    private const PAYMENT_CASH = 'CASH';
    private const PAYMENT_CARD = 'CARD';
    private const PAYMENT_PERSONAL_ACCOUNT = 'PERSONAL_ACCOUNT';
    private const PAYMENT_CORP_ACCOUNT = 'CORP_BALANCE';

    /**
     * @var ReceiptRepository
     */
    private $repository;

    /**
     * @var ApiClient
     */
    private $apiClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(ReceiptRepository $repository, ApiClient $apiClient, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->apiClient = $apiClient;
        $this->logger = $logger;
    }

    public function process($transactionId)
    {
        $data = $this->repository->getReceiptData($transactionId);
        if (empty($data)) {
            $this->logger->info("Receipt data not found: transactionId=$transactionId");

            return;
        }

        $configurations = $this->apiClient->getConfiguration($data);
        $configuration = $configurations[0] ?? null;

        if ($configuration === null) {
            $this->logger->info("Cashdesk configuration not found: transactionId=$transactionId");

            return;
        }

        $sendingCondition = (int)($configuration['sending_condition'] ?? 0);
        $device = $data['device'] ?? '';
        $payment = $data['payment'] ?? '';
        if (!$this->isNeedSendReceipt($sendingCondition, $device, $payment)) {
            $this->logger->info("Order does not match to sending condition of cashdesk configuration: transactionId=$transactionId, sendingCondition=$sendingCondition, device=$device, payment=$payment");

            return;
        }

        $this->apiClient->sendReceipt($data);
    }

    private function isNeedSendReceipt($sendingCondition, $device, $payment)
    {
        $condition = new SendingCondition($sendingCondition);
        if ($device === self::DEVICE_WORKER) {
            return $condition->isBordur();
        }

        switch ($payment) {
            case self::PAYMENT_CASH:
                return $condition->isCash();
            case self::PAYMENT_PERSONAL_ACCOUNT:
                return $condition->isPersonal();
            case self::PAYMENT_CORP_ACCOUNT:
                return $condition->isCorp();
            case self::PAYMENT_CARD:
                return $condition->isCard();
        }

        return false;
    }
}
