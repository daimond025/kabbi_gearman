<?php

namespace notification\cashdesk;

use GuzzleHttp\Client;
use notification\cashdesk\exceptions\ApiClientException;

class ApiClient
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $cashdeskUrl;

    public function __construct(Client $client, $cashdeskUrl)
    {
        $this->client = $client;
        $this->cashdeskUrl = $cashdeskUrl;
    }

    public function getConfiguration($receiptData)
    {
        try {
            $tenantId = $receiptData['tenant_id'] ?? null;
            $cityId = $receiptData['city_id'] ?? null;
            $positionId = $receiptData['position_id'] ?? null;

            $response = $this->client->get("{$this->cashdeskUrl}api/v1/configurations", [
                'query' => [
                    'tenant_id'   => $tenantId,
                    'city_id'     => $cityId,
                    'position_id' => $positionId,
                ],
            ]);

            return json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $ex) {
            throw new ApiClientException("Cashdesk client error: tenantId=$tenantId, cityId=$cityId, positionId=$positionId, error=\"{$ex->getMessage()}\"");
        }
    }

    public function sendReceipt($receiptData)
    {
        try {
            $response = $this->client->post("{$this->cashdeskUrl}api/v1/receipts/send", [
                'json' => [
                    'tenant_id'    => isset($receiptData['tenant_id']) ? (int)$receiptData['tenant_id'] : null,
                    'city_id'      => isset($receiptData['city_id']) ? (int)$receiptData['city_id'] : null,
                    'position_id'  => isset($receiptData['position_id']) ? (int)$receiptData['position_id'] : null,
                    'order_id'     => isset($receiptData['order_id']) ? (int)$receiptData['order_id'] : null,
                    'order_number' => isset($receiptData['order_number']) ? (int)$receiptData['order_number'] : null,
                    'client_phone' => $receiptData['client_phone'] ?? null,
                    'client_email' => $receiptData['client_email'] ?? null,
                    'payment_type' => $receiptData['payment'] ?? null,
                    'payment_time' => time() + ($receiptData['timeoffset'] ?? 0),
                    'cost'         => isset($receiptData['cost']) ? (float)$receiptData['cost'] : null,
                    'car_number'   => $receiptData['gos_number'] ?? null,
                ],
            ]);

            return json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $ex) {
            throw new ApiClientException("Cashdesk client error: orderId={$receiptData['order_id']}, error=\"{$ex->getMessage()}\"");
        }
    }
}