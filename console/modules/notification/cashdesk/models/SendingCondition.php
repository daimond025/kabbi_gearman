<?php

namespace notification\cashdesk\models;

class SendingCondition
{
    private const CASH = 1;
    private const PERSONAL = 2;
    private const CORP = 4;
    private const CARD = 8;
    private const BORDUR = 16;

    private $flags;

    public function __construct($flags)
    {
        $this->flags = $flags;
    }

    public function isCash()
    {
        return $this->getFlag(self::CASH);
    }

    public function isPersonal()
    {
        return $this->getFlag(self::PERSONAL);
    }

    public function isCorp()
    {
        return $this->getFlag(self::CORP);
    }

    public function isCard()
    {
        return $this->getFlag(self::CARD);
    }

    public function isBordur()
    {
        return $this->getFlag(self::BORDUR);
    }

    private function getFlag($flag)
    {
        return (bool)($this->flags & $flag);
    }
}