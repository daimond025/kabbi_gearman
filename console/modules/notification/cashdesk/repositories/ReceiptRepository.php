<?php

namespace notification\cashdesk\repositories;

use yii\db\Connection;

class ReceiptRepository
{
    private const SQL = <<<SQL
SELECT DISTINCT o.tenant_id, o.city_id, o.position_id,
  o.order_id, o.order_number, o.device, o.payment, d.summary_cost cost,
  c.client_id, c.last_name client_last_name, c.name client_name, c.second_name client_second_name, c.email client_email, o.phone client_phone,
  w.worker_id, w.callsign, w.last_name worker_last_name, w.name worker_name, w.second_name worker_second_name, w.phone worker_phone,
  r.car_id, r.gos_number,
  IFNULL((SELECT r.timezone * 3600
          FROM tbl_city AS c
            JOIN tbl_republic AS r ON c.republic_id = r.republic_id
          WHERE c.city_id = o.city_id), 0) timeoffset
FROM tbl_order AS o
  INNER JOIN tbl_order_detail_cost AS d on o.order_id = d.order_id
  INNER JOIN tbl_transaction AS t ON o.order_id = t.order_id
  INNER JOIN tbl_client AS c ON o.client_id = c.client_id
  INNER JOIN tbl_worker AS w ON o.worker_id = w.worker_id
  LEFT JOIN tbl_car AS r ON o.car_id = r.car_id
WHERE t.transaction_id = :transactionId
SQL;

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getReceiptData($transactionId)
    {
        return $this->connection->createCommand(self::SQL, ['transactionId' => $transactionId])->queryOne();
    }
}