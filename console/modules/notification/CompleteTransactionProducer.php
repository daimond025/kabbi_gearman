<?php

namespace notification;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

/**
 * Class CompleteTransactionProducer
 * @package notification
 */
class CompleteTransactionProducer
{
    const EXCHANGE_COMPLETE_TRANSACTION = 'COMPLETE_TRANSACTION';

    /**
     * @var MQConnection
     */
    private $connection;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(MQConnection $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    /**
     * @param int $transactionId
     */
    public function notify($transactionId)
    {
        $connection = new AMQPStreamConnection($this->connection->getHost(), $this->connection->getPort(),
            $this->connection->getUser(), $this->connection->getPassword(), $this->connection->getVirtualHost());

        $channel = $connection->channel();
        try {
            $msg = new AMQPMessage($transactionId);

            $channel->exchange_declare(self::EXCHANGE_COMPLETE_TRANSACTION, 'fanout', false, false, false);
            $channel->basic_publish($msg, self::EXCHANGE_COMPLETE_TRANSACTION);

            $this->logger->info("Complete transaction notification was sent: transactionId={$transactionId}");
        } catch (\Exception $ex) {
            $this->logger->error("Complete transaction notify error: transactionId={$transactionId}, error={$ex->getMessage()}");
        } finally {
            $channel->close();
            $connection->close();
        }
    }
}