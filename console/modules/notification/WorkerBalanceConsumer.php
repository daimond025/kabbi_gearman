<?php

namespace notification;

use notification\workerBalance\WorkerBalanceService;
use Psr\Log\LoggerInterface;

/**
 * Class WorkerBalanceConsumer
 * @package notification
 */
class WorkerBalanceConsumer extends NotificationConsumer
{
    /**
     * @var WorkerBalanceService
     */
    private $service;

    /**
     * WorkerBalanceConsumer constructor.
     *
     * @param MQConnection         $connection
     * @param WorkerBalanceService $service
     * @param LoggerInterface      $logger
     */
    public function __construct(MQConnection $connection, WorkerBalanceService $service, LoggerInterface $logger)
    {
        $this->service = $service;

        parent::__construct($connection, $logger, 'WorkerBalance');

    }

    protected function processNotification($transactionId)
    {
        $this->service->process($transactionId);
    }
}