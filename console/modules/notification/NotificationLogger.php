<?php

namespace notification;

use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;
use yii\log\Logger;

/**
 * Class Logger
 * @package notification
 */
class NotificationLogger extends AbstractLogger
{
    /**
     * @var string
     */
    private $class;

    /**
     * NotificationLogger constructor.
     *
     * @param string $class
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return void
     */
    public function log($level, $message, array $context = [])
    {
        $replace = [];
        foreach ($context as $key => $val) {
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        \Yii::getLogger()->log("[{$this->class}] $message", $this->getLogLevel($level), 'notification');
    }

    /**
     * @param string $level
     *
     * @return int
     */
    private function getLogLevel($level)
    {
        switch ($level) {
            case LogLevel::INFO:
                return Logger::LEVEL_INFO;
            case LogLevel::DEBUG:
                return Logger::LEVEL_TRACE;
            case LogLevel::NOTICE:
            case LogLevel::WARNING:
                return Logger::LEVEL_WARNING;
            case LogLevel::ALERT:
            case LogLevel::ERROR:
            case LogLevel::CRITICAL:
            case LogLevel::EMERGENCY:
                return Logger::LEVEL_ERROR;
            default:
                return Logger::LEVEL_ERROR;
        }
    }
}