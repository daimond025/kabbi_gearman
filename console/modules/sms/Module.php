<?php

namespace console\modules\sms;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'console\modules\sms\controllers';
}
