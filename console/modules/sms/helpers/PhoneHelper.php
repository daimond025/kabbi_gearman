<?php

namespace console\modules\sms\helpers;

class PhoneHelper
{
    public static function format($phone)
    {
        if (strlen($phone) === 11 && substr($phone, 0, 1) === '7') {
            $phone = "+$phone";
        }

        return $phone;
    }
}