<?php

namespace console\modules\sms\exceptions;

class RequestBalanceException extends \yii\base\Exception
{

    public function getName()
    {
        return 'Error via balance request';
    }

}
