<?php

namespace console\modules\sms\exceptions;

class InvalidServerIdException extends \yii\base\Exception
{

    public function getName()
    {
        return 'Invalid server id';
    }

}
