<?php

namespace console\modules\sms\components;

use console\modules\sms\exceptions\InvalidServerIdException;
use console\modules\sms\models\SmsForbid;
use Ramsey\Uuid\Uuid;
use console\modules\sms\models\SmsLog;

/**
 * Class SmsManager
 * @package console\modules\sms\components
 */
class SmsManager
{
    /**
     * @var SmsProviderFactory
     */
    private $smsProviderFactory;

    /**
     * SmsManager constructor.
     *
     * @param SmsProviderFactory $smsProviderFactory
     */
    public function __construct(SmsProviderFactory $smsProviderFactory)
    {
        $this->smsProviderFactory = $smsProviderFactory;
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function sendSms($data)
    {
        $requestId = array_key_exists('requestId', $data) ? $data['requestId'] : Uuid::uuid4()->toString();

        if (!empty($data['server'])) {
            try {
                $server   = current($data['server']);
                $serverId = isset($server['server_id']) ? $server['server_id'] : null;
                $login    = isset($server['login']) ? $server['login'] : null;
                $sign     = isset($server['sign']) ? $server['sign'] : null;
                $tenantId = isset($data['tenant_id']) ? $data['tenant_id'] : null;
                $message  = isset($data['text']) ? $data['text'] : null;
                $phone    = isset($data['phone']) ? $data['phone'] : null;
                $expires  = isset($data['expires']) ? $data['expires'] : null;

                applicationLog("[sms] requestId={$requestId} Sending sms: server={$login}, sign={$sign}, phone={$phone}, message={$message}, expires={$expires}");

                $currentTime = time();
                if ($expires !== null && $currentTime > $expires) {
                    SmsLog::saveLog($message, 0, $login, $phone, $serverId, $tenantId, $sign,
                        "СМС просрочена (time={$currentTime}, expires={$expires})");

                    applicationLog("[sms] requestId={$requestId} SMS was expired (time={$currentTime}, expires={$expires})");

                    return true;
                }

                $smsProvider = $this->smsProviderFactory->getProvider($serverId);

                $phoneForbid = SmsForbid::find()->where(['phone' => $phone])->one();
                $result = false;
                if(!$phoneForbid){
                    $result      = $smsProvider->sendSms($login, $server['password'], $message, $phone, $sign);
                }
                if(isset($phoneForbid->phone)){
                    echo ($phoneForbid->phone);
                }

                if ($result &&  !$phoneForbid) {
                    SmsLog::saveLog($message, 1, $login, $phone, $serverId, $tenantId, $sign, null);
                    applicationLog("[sms] requestId={$requestId} Successful sms sending");
                    return true;
                }elseif (!$result &&  $phoneForbid){
                    SmsLog::saveLog($message, 10, $login, $phone, $serverId, $tenantId, $sign, null);
                    applicationLog("[sms] requestId={$requestId} Successful sms sending");
                    return true;
                }
                else {
                    $error = $smsProvider->getError();
                    SmsLog::saveLog($message, 0, $login, $phone, $serverId, $tenantId, $sign, $error);
                    applicationLog("[sms] requestId={$requestId} Sms sending was failed: error={$error}");

                    return $error;
                }
            } catch (InvalidServerIdException $ex) {
                $error = $ex->getMessage();
                applicationLog("[sms] requestId={$requestId} Sms sending was failed: error={$error}");
                \Yii::error($error);
            } catch (\Exception $ex) {
                $error = $ex->getMessage();
                applicationLog("[sms] requestId={$requestId} Sms sending was failed: error={$error}");
                \Yii::error($error);
            }
            //}
        } else {
            applicationLog("[sms] requestId={$requestId} Sms sending was failed: error=Sms provider not specified");
        }

        return false;
    }
}
