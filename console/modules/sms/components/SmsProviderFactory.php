<?php

namespace console\modules\sms\components;

use console\modules\sms\models\alphasms\AlphasmsProvider;
use console\modules\sms\models\bsg\BsgSmsProvider;
use console\modules\sms\models\comtass\ComtassProvider;
use console\modules\sms\models\gosms\GosmsProvider;
use console\modules\sms\models\magfa\MagfaProvider;
use console\modules\sms\models\mediasend\MediasendProvider;
use console\modules\sms\models\megafonTjSmpp\MegafonTjSmppProvider;
use console\modules\sms\models\messangebir\MessagebirdSmsProvider;
use console\modules\sms\models\nikita\NikitaProvider;
use console\modules\sms\models\nikitaKg\NikitaKgProvider;
use console\modules\sms\models\promoSms\PromoSmsProvider;
use console\modules\sms\models\QtSmsProviderKuban;
use console\modules\sms\models\semysms\SemysmsProvider;
use console\modules\sms\models\smsbroker\SmsBrokerProvider;
use console\modules\sms\models\smsc\SmscProvider;
use console\modules\sms\models\smsLine\SmsLineProvider;
use console\modules\sms\models\smsOnline\SmsOnlineProvider;
use console\modules\sms\models\SmsProviderInterface;
use Yii;
use console\modules\sms\exceptions\InvalidServerIdException;
use console\modules\sms\models\IqSmsProvider;
use console\modules\sms\models\IbateleProvider;
use console\modules\sms\models\SapsanProvider;
use console\modules\sms\models\EyelineSmsProvider;
use console\modules\sms\models\QtSmsProvider;
use console\modules\sms\models\MsmSmsProvider;
use console\modules\sms\models\bulkSms\BulkSmsProvider;
use console\modules\sms\models\gateway\GatewaySmsProvider;
use console\modules\sms\models\maraditSms\MaraditProvider;
use console\modules\sms\models\ibateleSmpp\IbateleSmppProvider;
use console\modules\sms\models\clickatell\ClickatellProvider;

class SmsProviderFactory
{

    /**
     * @param $server_id
     *
     * @return object|SmsProviderInterface
     * @throws InvalidServerIdException
     * @throws \yii\base\InvalidConfigException
     */
    public function getProvider($server_id)
    {
        switch ($server_id) {
            case 1:
                return Yii::createObject(IqSmsProvider::class);

            case 2:
                return Yii::createObject(IbateleProvider::class);

            case 3:
                return Yii::createObject(SapsanProvider::class);

            case 4:
                return Yii::createObject(EyelineSmsProvider::class);

            case 5:
                return Yii::createObject(QtSmsProvider::class);

            case 6:
                return Yii::createObject(MsmSmsProvider::class);

            case 7:
                return Yii::createObject(BulkSmsProvider::class);

            case 8:
                return Yii::createObject(GatewaySmsProvider::class);

            case 9:
                return Yii::createObject(MaraditProvider::class);

            case 10:
                return Yii::createObject(IbateleSmppProvider::class);

            case 11:
                return Yii::createObject(ClickatellProvider::class);

            case 12:
                return Yii::createObject(SmscProvider::class);

            case 13:
                return Yii::createObject(GosmsProvider::class);

            case 14:
                return Yii::createObject(BsgSmsProvider::class);

            case 15:
                return Yii::createObject(MagfaProvider::class);

            case 16:
                return Yii::createObject(AlphasmsProvider::class);

            case 17:
                return Yii::createObject(QtSmsProviderKuban::class);

            case 18:
                return Yii::createObject(PromoSmsProvider::class);

            case 19:
                return Yii::createObject(MegafonTjSmppProvider::class);

            case 20:
                return Yii::createObject(SmsBrokerProvider::class);

            case 21:
                return Yii::createObject(ComtassProvider::class);

            case 22:
                return Yii::createObject(MediasendProvider::class);

            case 23:
                return Yii::createObject(NikitaProvider::class);

            case 24:
                return Yii::createObject(SmsLineProvider::class);

            case 25:
                return Yii::createObject(NikitaKgProvider::class);

            case 26:
                return Yii::createObject(SmsOnlineProvider::class);

            case 27:
                return Yii::createObject(SemysmsProvider::class);

            case 28:
                return Yii::createObject(\console\modules\sms\models\messagebird\MessagebirdSmsProvider::class);

            default:
                throw new InvalidServerIdException('Invalid server id: ' . $server_id);
        }
    }
}
