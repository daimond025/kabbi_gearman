<?php

namespace console\modules\sms\models;

class QtSmsProviderKuban extends QtSmsProvider
{
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->gate = \Yii::createObject(QtSmsKuban::class);
    }
}
