<?php
namespace console\modules\sms\models\messagebird;


use console\modules\sms\models\SmsProviderInterface;
use MessageBird\Objects\Recipient;
use yii\base\Component;

class MessagebirdSmsProvider extends Component implements SmsProviderInterface
{
    private $error;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $MessageBird = new \MessageBird\Client($password);
        $Message             = new \MessageBird\Objects\Message();
        $Message->originator = $login;
        $Message->recipients = array($target);
        $Message->body       = $message;
        $Message->datacoding = 'unicode';


        try {
            $MessageResult = $MessageBird->messages->create($Message);
            $date_recipients = (array)$MessageResult->recipients;
            if($date_recipients['totalDeliveryFailedCount'] === 0){
                return true;
            }
            return false;
        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            // That means that your accessKey is unknown
            echo 'wrong login';
            return false;
        } catch (\MessageBird\Exceptions\BalanceException $e) {
            // That means that you are out of credits, so do something about it.
            echo 'no balance';
            return false;
        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }

     /*  $MessageBird = new \MessageBird\Client($password); // Set your own API access key here.
        $VoiceMessage             = new \MessageBird\Objects\VoiceMessage();
        $VoiceMessage->recipients = array ($target);
        $VoiceMessage->body = 'Der Auftrag Nummer 4567. Nehmen Sie den Kunden und liefern zur Zieladresse.';
        $VoiceMessage->originator = '79772602817';
        $VoiceMessage->language = 'de-de';
        $VoiceMessage->voice = 'male';
        $VoiceMessage->repeat = 2;
        $VoiceMessage->ifMachine = 'continue'; // We don't care if it is a machine.
        try {
            $VoiceMessageResult = $MessageBird->voicemessages->create($VoiceMessage);
            var_dump($VoiceMessageResult);
        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            // That means that your accessKey is unknown
            echo 'wrong login';
        } catch (\MessageBird\Exceptions\BalanceException $e) {
            // That means that you are out of credits, so do something about it.
            echo 'no balance';
        } catch (\Exception $e) {
            echo $e->getMessage();
        }*/






    }

    public function requestBalance($login, $password)
    {

        // баланс
        $MessageBird = new \MessageBird\Client($password);
        try {
            $Balance = $MessageBird->balance->read();
            return $Balance->amount;
        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            // That means that your accessKey is unknown
            echo 'wrong login';
            return '';
        } catch (\Exception $e) {
            echo($e->getMessage());
            return '';
        }
    }

    public function getError()
    {
        return $this->error;
    }

}
