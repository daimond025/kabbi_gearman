<?php

namespace console\modules\sms\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_sms_log".
 *
 * @property integer $id
 * @property string $text
 * @property integer $status
 * @property string $login
 * @property string $phone
 * @property string $create_time
 * @property string $signature
 * @property integer $server_id
 * @property integer $tenant_id
 * @property string $response
 */
class SmsLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'status', 'phone', 'create_time', 'server_id'], 'required'],
            [['status', 'server_id', 'tenant_id'], 'integer'],
            [['create_time'], 'safe'],
            [['text', 'response'], 'string', 'max' => 255],
            [['login', 'phone', 'signature'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'status' => 'Статус',
            'login' => 'Логин',
            'phone' => 'Телефон',
            'create_time' => 'Дата отправки',
            'signature' => 'Подпись',
            'server_id' => 'ID сервера',
            'tenant_id' => 'ID арендатора',
            'response' => 'Ответ',
        ];
    }

    /**
     *
     * @param string $text
     * @param integer $status
     * @param string $login
     * @param string $phone
     * @param integer $server_id
     * @param integer $tenant_id
     * @param string $signature
     * @param string $result
     * @return boolean
     */
    public static function saveLog($text, $status, $login, $phone, $server_id, $tenant_id = null, $signature = null, $result = null) {
        $model = new self();
        $model->text        = (string) $text;
        $model->login       = $login;
        $model->server_id   = $server_id;
        $model->status      = $status;
        $model->signature   = $signature;
        $model->phone       = $phone;
        $model->tenant_id   = $tenant_id;
        $model->response    = (string) $result;
        $model->create_time = new Expression('NOW()');

        if (!$model->save()) {
            Yii::error($model->getErrors());
            return false;
        }

        return true;
    }
}
