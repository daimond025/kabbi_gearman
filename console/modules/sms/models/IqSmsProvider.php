<?php

namespace console\modules\sms\models;

use console\modules\sms\exceptions\RequestBalanceException;

class IqSmsProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error;
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(iqsms_JsonGate::class);
    }

    /**
     * @inheritdoc
     */
    public function sendSms($login, $password, $message, $target, $sender)
    {

        $this->gate->setApiLogin($login);
        $this->gate->setApiPassword($password);
        $messages = array(
            array(
                'phone'  => $target,
                'text'   => $message,
                'sender' => $sender
            ),
        );
        $request = $this->gate->send_message($messages);
        if (!empty($request['messages']) && $request['messages']['0']['status'] == 'accepted') {
            return true;
        } else {
            $this->error = !empty($request['description']) ? serialize($request['description']) : serialize($request['messages']['0']['status']);
            return false;
        }
    }

    public function requestBalance($login, $password)
    {
        $this->gate->setApiLogin($login);
        $this->gate->setApiPassword($password);
        $result = $this->gate->credits();
        if ($result['status'] == 'error') {
            throw new RequestBalanceException($result['description']);
        } else {
            return $result['balance'][0]['balance'];
        }
    }

    public function getError()
    {
        return $this->error;
    }

}
