<?php

namespace console\modules\sms\models;

use console\modules\sms\exceptions\RequestBalanceException;

class IbateleProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error;
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(IbateleSms::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $xml = '<?xml version="1.0" encoding="utf-8" ?>
        <request>
        <message type="sms">
        <sender>' . $sender . '</sender>
        <text>' . htmlspecialchars($message) . '</text>
        <abonent phone="' . $target . '" number_sms="1" />
        </message>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        </request>';
        $result = $this->gate->send_message($xml);

        $xmlObj = simplexml_load_string($result);
        $xml = (array) $xmlObj;
        if (!empty($xml['error']) || (!empty($xml['information']) && $xml['information'] != 'send')) {
            $this->error = !empty($xml['error']) ? $xml['error'] : $xml['information'];
            return false;
        } else {
            return true;
        }
    }

    public function requestBalance($login, $password)
    {
        $request = $this->gate->request_balance($login, $password);
        if ($request == IbateleSms::EMPTY_BALANCE_RESPONSE) {
            throw new RequestBalanceException('Invalid login/password');
        } else {
            $balance = trim(explode(':', $request)[2], ' ');
            $balance = trim($balance, '"}');
            return + $balance;
        }
    }

    public static function getUrl($login, $password, $null = null)
    {
        return 'https://lk.ibatele.com/sendsms.php?user=' . $login . '&pwd=' . $password . '&balance=1';
    }

    public function getError()
    {
        return $this->error;
    }

}
