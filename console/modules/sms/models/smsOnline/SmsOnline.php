<?php

namespace console\modules\sms\models\smsOnline;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class SmsOnline extends Object
{
    const ACTION_SEND = '';

    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function sendSms($login, $password, $from, $to, $message)
    {

        $params = [
            'user'  => $login,
            'from'  => $from,
            'phone' => $to,
            'txt'   => $message,
            'sign'  => $this->getSignature($login, $password, $from, $to, $message),
        ];

        $response = $this->post(SmsOnline::ACTION_SEND, $params);

        if (ArrayHelper::getValue($response, 'status') === 'ok') {

            $message = ArrayHelper::getValue($response, 'message', '');

            $data    = simplexml_load_string($message);
            $code    = (string)$data->code;
            $message = $this->getMessageByCode($code);
        } else {
            $message = ArrayHelper::getValue($response, 'message');
        }

        return [
            'status'  => 'error',
            'message' => $message,
        ];
    }

    public function post($action, $params)
    {
        $options = [
            'form_params'       => $params,
        ];

        return $this->send('POST', $action, $options);
    }

    public function send($method, $action, $options)
    {
        $client = new Client([
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);

        try {
            $url      = $this->getUrl($action);
            $response = $client->request($method, $url, $options);

            return [
                'status'  => 'ok',
                'message' => (string)$response->getBody(),
            ];
        } catch (ClientException $exception) {
            return [
                'status'  => 'error',
                'message' => 'Status code: ' . $exception->getResponse()->getStatusCode(),
            ];
        }
    }

    protected function getSignature($login, $password, $from, $to, $message)
    {
        return md5($login . $from . $to . $message . $password);
    }

    protected function getUrl($action)
    {
        return rtrim($this->baseUrl, '/') . '/' . trim($action, '/');
    }


    protected function getMessageByCode($code)
    {
        return ArrayHelper::getValue(SmsOnline::getMessageMap(), $code, $code);
    }

    public static function getMessageMap()
    {
        return [
            0  => 'Сообщение обработано',
            -1 => 'Неверные входные данные',
            -2 => 'Ошибка аутентификации',
            -3 => 'Отказ в обработке',
            -4 => 'Временная техническая ошибка',
            -5 => 'Исчерпан баланс SMS-сообщений',
        ];
    }
}
