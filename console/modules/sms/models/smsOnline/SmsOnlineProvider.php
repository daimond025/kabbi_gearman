<?php

namespace console\modules\sms\models\smsOnline;

use console\modules\sms\models\SmsProviderInterface;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class SmsOnlineProvider extends Component implements SmsProviderInterface
{

    private $error;

    /** @var SmsOnline */
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(SmsOnline::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $sender, $target, $message);
        if (ArrayHelper::getValue($response, 'status') === 'ok') {
            return true;
        } else {
            $this->error = ArrayHelper::getValue($response, 'message');

            return false;
        }
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

}
