<?php

namespace console\modules\sms\models;

interface SmsProviderInterface
{
    /**
     * 
     * @param type $login
     * @param type $password
     * @param type $message
     * @param type $target
     * @param type $sender
     * provides SMS sending
     */
    public function sendSms($login, $password, $message, $target, $sender);
/**
 * 
 * @param type $login
 * @param type $password
 */
    public function requestBalance($login, $password);
    
    /**
     * 
     */
    public function getError();
}
