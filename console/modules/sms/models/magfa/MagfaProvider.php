<?php

namespace console\modules\sms\models\magfa;

use Yii;

class MagfaProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error = 'error';
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = Yii::createObject(Magfa::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $message, $target, $sender);
        if (!$response) {
            $this->error = 'Code 0';
            return false;
        }

        if (strlen($response) < 3) {
            $this->error = ' Code ' . $response;
            return false;
        }

        return true;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

}
