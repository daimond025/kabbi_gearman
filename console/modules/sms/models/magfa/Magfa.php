<?php

namespace console\modules\sms\models\magfa;

use yii\base\Object;

class Magfa extends Object
{
    private $baseUrl;
    private $domain;

    private function sendResponse($params)
    {
        $curl = app()->curl;

        return $curl->get($this->baseUrl, $params);
    }

    public function sendSms($login, $password, $text, $phone, $sender)
    {
        $params = [
            'service'  => 'Enqueue',
            'domain'   => $this->domain,
            'username' => $login,
            'password' => $password,
            'from'     => $sender,
            'to'       => $phone,
            'message'  => $text,
        ];

        return $this->sendResponse($params);
    }


}
