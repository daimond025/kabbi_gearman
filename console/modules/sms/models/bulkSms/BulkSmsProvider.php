<?php

namespace console\modules\sms\models\bulkSms;

use Yii;
use yii\helpers\Console;
use yii\base\Exception;

class BulkSmsProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $response;
    private $error;

    /* @var $gate BulkSms */
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = Yii::createObject(BulkSms::class);
    }

    /**
     * Sending sms
     * @param string $login
     * @param string $password
     * @param string $message
     * @param string $target
     * @param string $sender
     * @return boolean
     */
    public function sendSms($login, $password, $message, $target, $sender)
    {
        try {
            $this->response = $this->gate->sendSms($login, $password, $sender,
                $target, $message);
            return true;
        } catch (exceptions\AuthorizationErrorException $ex) {
            $this->error = $ex->getMessage();
        } catch (exceptions\DenySendingSmsException $ex) {
            $this->error = $ex->getMessage();
        } catch (exceptions\InsufficientFundsException $ex) {
            $this->error = $ex->getMessage();
        } catch (exceptions\InternalErrorException $ex) {
            $this->error = $ex->getMessage();
        } catch (exceptions\WrongSenderException $ex) {
            $this->error = $ex->getMessage();
        } catch (\Exception $ex) {
            $this->error = $ex->getMessage();
        }
        return false;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getError()
    {
        return $this->error;
    }
}
