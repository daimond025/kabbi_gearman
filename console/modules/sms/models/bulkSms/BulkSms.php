<?php

namespace console\modules\sms\models\bulkSms;

use console\modules\sms\models\bulkSms\exceptions\AuthorizationErrorException;
use console\modules\sms\models\bulkSms\exceptions\InsufficientFundsException;
use console\modules\sms\models\bulkSms\exceptions\WrongSenderException;
use console\modules\sms\models\bulkSms\exceptions\DenySendingSmsException;
use console\modules\sms\models\bulkSms\exceptions\InternalErrorException;
use yii\base\Exception;
use yii\base\Object;

/**
 * Model of Bulk sms provider
 */
class BulkSms extends Object
{
    const SUCCESSFUL_STATUS_CODE = [200, 202];
    const SUCCESSFUL_STATUS = 'Accepted for delivery (200)';

    public $baseUrl;

    /**
     * Sending sms
     *
     * @param type $username
     * @param type $password
     * @param type $sign
     * @param type $phone
     *
     * @return boolean
     * @throw AuthorizationErrorException
     * @throw InsufficientFundsException
     * @throw WrongSenderException
     * @throw DenySendingSmsException
     * @throw InternalErrorException
     * @throw \yii\base\Exception
     */
    public function sendSms($username, $password, $sign, $phone, $text)
    {
        /* @var $curl \console\components\curl\Curl */
        $curl = app()->curl;

        $params = [
            'username' => $username,
            'password' => $password,
            'from'     => $sign,
            'to'       => $phone,
            'text'     => $text,
            'dlrmask'  => 0,
            'dlrurl'   => '',
            'charset'  => 'utf8',
            'coding'   => 2,
        ];

        /* @var $response \console\components\curl\CurlResponse */
        $response = $curl->get($this->baseUrl, $params);
        if (!empty($response)) {
            $status       = $response->headers['Status'];
            $statusCode   = $response->headers['Status-Code'];
            $body         = isset($response->body) ? $response->body : null;
            $errorMessage = empty($body) ? $status : $body;

            if (in_array($statusCode, self::SUCCESSFUL_STATUS_CODE)
                && $body != self::SUCCESSFUL_STATUS) {
                throw new Exception($body);
            }
        } else {
            $errorMessage = $curl->error();
        }

        switch ($statusCode) {
            case 200:
            case 202:
                return true;
            case 401:
                throw new AuthorizationErrorException($errorMessage);
            case 402:
                throw new InsufficientFundsException($errorMessage);
            case 406:
                throw new WrongSenderException($errorMessage);
            case 430:
                throw new DenySendingSmsException($errorMessage);
            case 501:
                throw new InternalErrorException($errorMessage);
            default:
                throw new Exception($errorMessage);
        }
    }
}
