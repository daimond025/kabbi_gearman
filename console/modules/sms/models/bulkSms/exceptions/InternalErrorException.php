<?php

namespace console\modules\sms\models\bulkSms\exceptions;

class InternalErrorException extends \yii\base\Exception
{

    function getName()
    {
        return "Internal error";
    }
}
