<?php

namespace console\modules\sms\models\bulkSms\exceptions;

class DenySendingSmsException extends \yii\base\Exception
{

    function getName()
    {
        return "SMS Send is prohibited at this time";
    }
}
