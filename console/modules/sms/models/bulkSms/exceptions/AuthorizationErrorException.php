<?php

namespace console\modules\sms\models\bulkSms\exceptions;

class AuthorizationErrorException extends \yii\base\Exception
{

    function getName()
    {
        return "Authrization error";
    }
}
