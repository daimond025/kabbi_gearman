<?php

namespace console\modules\sms\models\bulkSms\exceptions;

class InsufficientFundsException extends \yii\base\Exception
{

    function getName()
    {
        return "Insufficient funds";
    }
}
