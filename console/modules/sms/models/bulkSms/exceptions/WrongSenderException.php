<?php

namespace console\modules\sms\models\bulkSms\exceptions;

class WrongSenderException extends \yii\base\Exception
{

    function getName()
    {
        return "Wrong sender";
    }
}
