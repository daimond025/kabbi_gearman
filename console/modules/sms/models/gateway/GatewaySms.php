<?php

namespace console\modules\sms\models\gateway;

use yii\base\Object;

class GatewaySms extends Object
{
    public $baseUrl;

    public function sendSms($login,$passw, $text, $phone)
    {
        /* @var $curl \console\components\curl\Curl */
        $curl = app()->curl;

        $params = [
            'login' => $login,
            'passw' => $passw,
            'phone' => $phone,
            'text' => $text
        ];
        /* @var $response \console\components\curl\CurlResponse */
        $response = $curl->post($this->baseUrl, $params);

        if (!empty($response)) {
            $result = simplexml_load_string($response->body);

            $r['error'] = $result->result['status'];
            if($result->result['status'] == '1')
                $r['msg'] = $result->result;
            return $r;
        }
        return [
            'error' => '1',
            'msg' => 'No connect',
        ];
    }

}
