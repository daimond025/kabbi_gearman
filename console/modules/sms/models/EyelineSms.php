<?php

namespace console\modules\sms\models;


use yii\base\Object;

class EyelineSms extends Object
{
    const PROTOCOL = 'sms';

    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function send_message($login, $message, $target)
    {
        $params = [
            'service'    => $login,
            'subscriber' => $target,
            'message'    => $message,
            'protocol'   => self::PROTOCOL,
        ];

        $url = $this->getUrl($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectionTimeout);
        curl_setopt($ch, CURLOPT_TIMEOUT,  $this->timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    public function getUrl($params)
    {
        $query = http_build_query($params, '', '&', PHP_QUERY_RFC3986);

        return $this->baseUrl . '?' . $query;
    }

}
