<?php

namespace console\modules\sms\models\megafonTjSmpp;

use console\modules\sms\models\SmsProviderInterface;
use yii\base\Component;

/**
 * Class MegafonTjSmppProvider
 * @package console\modules\sms\megafonTjSmpp
 *
 * @property MegafonTjSmpp $gate
 */
class MegafonTjSmppProvider extends Component implements SmsProviderInterface
{
    private $error = 'error';

    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(MegafonTjSmpp::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $sender, $target, $message);

        if($response['status'] === MegafonTjSmpp::RESPONSE_ERROR) {
            $this->error = $response['message'];
            return false;
        }

        return true;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }
}