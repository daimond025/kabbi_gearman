<?php

namespace console\modules\sms\models\megafonTjSmpp;

use yii\base\Object;

class MegafonTjSmpp extends Object
{
    const ADDR = '10.241.201.184';
    const PORT = 2775;
    const SEND_TIMEOUT = 5000;
    const RECEIVE_TIMEOUT = 7500;

    const RESPONSE_OK = 'ok';
    const RESPONSE_ERROR = 'error';
    const COUNT_QUERY = 3;
    const DELAY_QUERY = 5;

    public $hosts;
    public $port;

    public function sendSms($login, $password, $from, $to, $message)
    {
        $result = [];

        for ($i = 1; $i <= self::COUNT_QUERY; $i++) {

            if ($i !== 1) {
                sleep(self::DELAY_QUERY);
            }

            self::log('Attempt {current} of {total}', ['current' => $i, 'total' => self::COUNT_QUERY]);

            try {
                $debugHandler = [self::class, 'log'];
                $transport    = new \SocketTransport($this->hosts, $this->port, false, $debugHandler);

                $transport->setSendTimeout(self::SEND_TIMEOUT);
                $transport->setRecvTimeout(self::RECEIVE_TIMEOUT);

                $smpp = new \SmppClient($transport);

                // Activate binary hex-output of server interaction
                //            $smpp->debug      = true;
                $transport->debug = true;

                // Open the connection
                $transport->open();
                $smpp->bindTransmitter($login, $password);

                // Optional connection specific overrides
                \SmppClient::$sms_null_terminate_octetstrings = false;
                \SmppClient::$csms_method                     = \SmppClient::CSMS_PAYLOAD;
                \SmppClient::$sms_registered_delivery_flag    = \SMPP::REG_DELIVERY_SMSC_BOTH;


                $encodedMessage = iconv('UTF-8', "UCS-2BE//IGNORE", $message);
                //            $encodedMessage = \GsmEncoder::utf8_to_gsm0338($message);

                $from = new \SmppAddress($from, \SMPP::TON_ALPHANUMERIC);
                $to   = new \SmppAddress($to, \SMPP::TON_INTERNATIONAL, \SMPP::NPI_E164);

                // Send
                $response = $smpp->sendSMS($from, $to, $encodedMessage, null, \SMPP::DATA_CODING_UCS2);

                // Close connection
                $smpp->close();

                return [
                    'status'  => self::RESPONSE_OK,
                    'message' => $response,
                ];

            } catch (\SocketTransportException $exception) {
                $result = [
                    'status'  => self::RESPONSE_ERROR,
                    'message' => $exception->getMessage(),
                ];
            } catch (\SmppException $exception) {
                return [
                    'status'  => self::RESPONSE_ERROR,
                    'message' => $exception->getMessage(),
                ];
            }
        }

        return $result;
    }

    public static function log($message, $context = [])
    {
        $message = '[sms] ' . $message;
        applicationLog($message, $context);
    }
}