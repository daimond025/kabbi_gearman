<?php

namespace console\modules\sms\models;

use console\modules\sms\models\EyelineSms;
use Yii;
use yii\helpers\Console;

class EyelineSmsProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error;
    public $gate;

    const SUCCESS = 'Queued';

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = Yii::createObject(EyelineSms::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->send_message($login, $message, $target);
        if ($response == self::SUCCESS) {
            return true;
        } else {
            $this->error = $response;
            return false;
        }
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

}
