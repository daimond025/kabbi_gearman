<?php

namespace console\modules\sms\models\nikita;

use console\modules\sms\models\smsbroker\SmsBrokerProvider;

class NikitaProvider extends SmsBrokerProvider
{
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(Nikita::class);
    }
}
