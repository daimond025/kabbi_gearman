<?php

namespace console\modules\sms\models\maraditSms;

use console\modules\sms\models\SmsProviderInterface;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class MaraditProvider
 * @package console\modules\sms\models\maraditSms
 * @property Maradit $gate
 */
class MaraditProvider extends Component implements SmsProviderInterface
{

    public $gate;
    private $error;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(Maradit::class);
    }


    /**
     * Sending sms
     *
     * @param string $login
     * @param string $password
     * @param string $message
     * @param string $target
     * @param string $sender
     *
     * @return boolean
     */
    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($sender, $target, $message, $login, $password);

        if ($response['status'] === Maradit::STATUS_ERROR) {
            $this->error = $response['message'];

            return false;
        }

        if (!empty($response['message']['errorMessage'])) {
            $this->error = $response['message']['errorMessage'];

            return false;
        }

        return true;
    }

    public function requestBalance($login, $password)
    {
        $response = $this->gate->requestBalance($login, $password);

        if ($response['status'] === Maradit::STATUS_ERROR) {
            return '';
        }

        if (!empty($response['message']['errorMessage'])) {
            return '';
        }

        return (string)$response['message']['obj'];
    }

    public function getError()
    {
        return $this->parseError();
    }

    protected function parseError()
    {
        $errors = [
            '-100' => 'Неправильный ключ',
            '-101' => 'Сообщение больше допустимой длины',
            '-102' => 'Неправильный формат номера',
            '-103' => 'Неправильная подпись',
            '-104' => 'Недостаточный баланс',
            '-105' => 'Номер в черном списке',
            '-106' => 'Неверный идентификатор транзакции',
            '-107' => 'IP-фдрес не разрешен',
            '-108' => 'Неправильный хеш',
            '-109' => 'Нет хоста',
            '-500' => 'Внутренняя ошибка',
        ];

        return ArrayHelper::getValue($errors, $this->error, $this->error);
    }
}
