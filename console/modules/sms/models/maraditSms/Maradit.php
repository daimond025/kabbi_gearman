<?php

namespace console\modules\sms\models\maraditSms;


use GuzzleHttp\Client;
use yii\base\Object;

class Maradit extends Object
{
    /* Constants */
    const SEND_SMS = 'send';
    const REQUEST_BALANCE = 'balance';

    const STATUS_OK = 'ok';
    const STATUS_ERROR = 'error';

    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function sendSms($from, $to, $message, $login, $password)
    {
        $string = $message . $to . $from;

        $params = [
            'login'  => $login,
            'msisdn' => $to,
            'text'   => $message,
            'sender' => $from,
            'key'    => $this->generateKey($login, $password, $string),
        ];

        return $this->send(self::SEND_SMS, $params);
    }

    public function requestBalance($login, $password)
    {
        $params = [
            //            'app' => 'quick_sms',
            'login' => $login,
            'key'   => $this->generateKey($login, $password),
        ];

        return $this->send(self::REQUEST_BALANCE, $params);
    }

    protected function send($action, $params)
    {

        try {
            $client = new Client([
                'connectionTimeout' => $this->connectionTimeout,
                'timeout'           => $this->timeout,
            ]);

            $response = $client->request('GET', $this->getUrl($action), [
                'query'  => $params,
                'verify' => false,
            ]);

            return [
                'status'  => self::STATUS_OK,
                'message' => json_decode($response->getBody(), true),
            ];

        } catch (\Exception $ex) {
            return [
                'status'  => self::STATUS_ERROR,
                'message' => 'Connect Error',
            ];
        }
    }

    protected function getUrl($action)
    {
        return $this->baseUrl . $action;
    }

    protected function generateKey($login, $password, $string = '')
    {
        $md5password = md5($password);

        return md5($md5password . $login . $string);
    }

}