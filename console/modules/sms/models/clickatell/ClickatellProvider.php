<?php

namespace console\modules\sms\models\clickatell;

use Yii;

class ClickatellProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error;

    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = Yii::createObject(Clickatell::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($password, $message, $target);
        if($response['error'] == 1) {
            $this->error = $response['msg'];
            return false;
        }

        return true;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

}
