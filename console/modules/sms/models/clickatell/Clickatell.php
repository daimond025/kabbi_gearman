<?php

namespace console\modules\sms\models\clickatell;

use GuzzleHttp\Client;
use yii\base\Object;

class Clickatell extends Object
{
    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function sendSms($password, $text, $phone)
    {

        try {
            $url     = $this->baseUrl . 'messages/http/send';
            $options = [
                'query' => [
                    'apiKey'  => $password,
                    'to'      => $phone,
                    'content' => $text,
                ],
            ];
            $client  = new Client([
                'connectionTimeout' => $this->connectionTimeout,
                'timeout'           => $this->timeout,
            ]);

            $response = $client->request('GET', $url, $options);

            $resBody = json_decode($response->getBody(), true);

            if (!empty($resBody['error'])) {
                return [
                    'error' => '1',
                    'msg'   => $resBody['error'],
                ];
            }

            if (!empty($resBody['messages'][0]['error'])) {
                return [
                    'error' => '1',
                    'msg'   => $resBody['messages'][0]['error'],
                ];
            }

            return [
                'error' => '0',
            ];


        } catch (\Exception $ignore) {
            return [
                'error' => '1',
                'msg'   => 'No connect',
            ];
        }


    }


}
