<?php

namespace console\modules\sms\models\gosms;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use yii\base\Object;

class Gosms extends Object
{
    private $_token = [];
    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    // Получение токена
    private function getToken($login, $password)
    {
        if(empty( $this->_token[$login][$password] )) {
            $curl = app()->curl;
            $params = [
                'client_id' => $login,
                'client_secret' => $password,
                'grant_type' => 'client_credentials',
            ];
            /* @var $response \console\components\curl\CurlResponse */
            $response = $curl->get($this->baseUrl . 'oauth/v2/token', $params);

            if (!empty($response)) {
                $response = json_decode($response, true);

                if(isset($response['error'])) {
                    return [
                        'error' => 1,
                        'msg' => $response['error']
                    ];
                }

                $this->_token[$login][$password] = $response['access_token'];
                return [
                    'error' => 0,
                    'token' => $this->_token[$login][$password],
                ];
            }
        }
    }

    public function sendSms($login, $password, $text, $phone) {

        $login = explode('-', $login,2);
        list($chanel, $username) = $login;
        $result = $this->getToken($username, $password);

        if($result['error'] == 1) {
            return $result;
        }
        $params = [
            'message' => urldecode($text),
            'recipients' => $phone,
            'channel' => (int)$chanel,
        ];
        $body = json_encode($params);
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $result['token'],
        ];

        try {
            $client = new Client([
                'connectionTimeout' => $this->connectionTimeout,
                'timeout'           => $this->timeout,
            ]);
            $response = $client->request('POST', $this->baseUrl . 'api/v1/messages/', [
                'headers' => $headers,
                'body'  => $body,
            ]);
            return [
                'error' => 0,
                'msg' => '',
            ];
        } catch (ClientException $ex) {
            $resBody  = \GuzzleHttp\json_decode($ex->getResponse()->getBody(), true);
            return [
                'error' => 1,
                'msg' => $resBody['detail'],
            ];
        } catch (\Exception $ex) {
            return [
                'error' => 1,
                'msg' => 'Other'
            ];
        }
    }


}
