<?php

namespace console\modules\sms\models\gosms;

use Yii;

class GosmsProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error = 'error';

    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = Yii::createObject(Gosms::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $message, $target);
        if($response['error'] == 1) {
            $this->error = $response['msg'];
            return false;
        }

        return true;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

}
