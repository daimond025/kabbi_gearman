<?php

namespace console\modules\sms\models\semysms;

use console\modules\sms\models\SmsProviderInterface;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class SemysmsProvider
 * @package console\modules\sms\models\semysms
 *
 * @property Semysms $gate
 */
class SemysmsProvider extends Component implements SmsProviderInterface
{
    private $error;

    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(Semysms::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $target, $message);

        if (ArrayHelper::getValue($response, 'status') === Semysms::STATUS_OK) {
            return true;
        }

        $this->error = ArrayHelper::getValue($response, 'message');

        return false;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }
}