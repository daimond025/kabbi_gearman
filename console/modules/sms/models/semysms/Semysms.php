<?php

namespace console\modules\sms\models\semysms;

use console\modules\sms\helpers\PhoneHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class Semysms extends Object
{
    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    const SEND_SMS = 'sms.php';
    const STATUS_OK = 0;
    const STATUS_ERROR = 1;

    public function sendSms($device, $token, $phone, $message)
    {
        $params = [
            'token'  => $token,
            'device' => $device,
            'phone'  => PhoneHelper::format($phone),
            'msg'    => $message,
        ];

        try {
            $response = $this->get(self::SEND_SMS, $params);

            if (ArrayHelper::getValue($response, 'code') == 0) {
                return [
                    'status'  => self::STATUS_ERROR,
                    'message' => 'Сообщение отправлено',
                ];
            }

            return [
                'status'  => self::STATUS_ERROR,
                'message' => ArrayHelper::getValue($response, 'error'),
            ];

        } catch (ClientException $exception) {
            return [
                'status'  => self::STATUS_ERROR,
                'message' => $exception->getMessage(),
            ];
        }
    }

    protected function get($action, $params)
    {
        $options = [
            'query' => $params,
        ];

        return $this->send('GET', $action, $options);
    }


    protected function send($method, $action, $options)
    {
        $url = $this->getUrl($action);

        $client = new Client([
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);

        $response = $client->request($method, $url, $options);

        return json_decode($response->getBody(), true);
    }

    protected function getUrl($action)
    {
        return "{$this->baseUrl}api/3/$action";
    }
}