<?php

namespace console\modules\sms\models;

use console\modules\sms\exceptions\RequestBalanceException;

class QtSmsProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error;
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(QtSms::class);
    }

    /**
     * @inheritdoc
     */
    public function sendSms($login, $password, $message, $target, $sender)
    {
        $this->error = null;
        $this->gate->user = $login;
        $this->gate->pass = $password;

        $xmlObj = simplexml_load_string($this->gate->post_message($message, $target, $sender));
        $result = (array) $xmlObj;

        if (!empty($result['errors'])) {
            $this->error = $result['errors'];
        }

        return empty($result['errors']);
    }

    public function requestBalance($login, $password)
    {
        $this->gate->user = $login;
        $this->gate->pass = $password;

        $xmlObj = simplexml_load_string($this->gate->get_balance());
        $xml = (array) $xmlObj;
        if (empty($xml['errors'])) {
            $balance = $xml['balance']->OVERDRAFT + $xml['balance']->AGT_BALANCE;
            return $balance;
        } else {
            throw new RequestBalanceException((string) $xml['errors']->error[0]);
        }
    }

    /**
     * 
     * @return type
     */
    public function getError()
    {
        return $this->error;
    }

}
