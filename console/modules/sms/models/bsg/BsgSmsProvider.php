<?php


namespace console\modules\sms\models\bsg;


use console\modules\sms\models\SmsProviderInterface;
use yii\base\Component;

class BsgSmsProvider extends Component implements SmsProviderInterface
{

    private $error;
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(BsgSms::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        /*$MessageBird = new \MessageBird\Client("nMVTZi8iDOQBeNdUaYzADTbDC");
        $Message             = new \MessageBird\Objects\Message();
        $Message->originator = 'MessageBird';
        $Message->recipients = array($target);
        $Message->body       = 'Это кириллл!!11';
        $Message->datacoding = 'unicode';

        var_dump($Message);

        try {
            $MessageResult = $MessageBird->messages->create($Message);
            var_dump($MessageResult);
        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            // That means that your accessKey is unknown
            echo 'wrong login';
        } catch (\MessageBird\Exceptions\BalanceException $e) {
            // That means that you are out of credits, so do something about it.
            echo 'no balance';
        } catch (\Exception $e) {
            echo $e->getMessage();
        }*/

        try {
            $response = $this->gate->sendSms($login, $password, $message, $target, $sender);
            $xmlObj = simplexml_load_string($response);
            $xml = (array) $xmlObj;

            if (!empty($xml['error'])) {
                $this->error = $xml['error'];
                return false;
            }
            return !empty($xml['information']) && ($xml['information'] == 'send');
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function requestBalance($login, $password)
    {
        try {
            $response = $this->gate->getBalance($login, $password);
            $xmlObj = simplexml_load_string($response);
            $xml = (array) $xmlObj;
            if (!empty($xml['error'])) {
                $this->error = $xml['error'];
//                return '';
            }
            return $xml['money'];
        } catch (\Exception $ex) {
            return '';
        }
    }

    public function getError()
    {
        return $this->error;
    }

}
