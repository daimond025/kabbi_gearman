<?php

namespace console\modules\sms\models\bsg;

use GuzzleHttp\Client;
use yii\base\Object;

class BsgSms extends Object
{
    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function sendSms($login, $password, $text, $phone, $sender)
    {
        $body = '<?xml version = "1.0" encoding = "utf-8"?>
            <request>
            <message type = "sms">
            <sender>' . $sender . '</sender>
            <text>' . $text . '</text>
            <abonent phone = "' . $phone . '" number_sms="1"/>
            </message>
            <security>
            <login value="' . $login . '"/>
            <password value="' . $password . '"/>
            </security>
            </request>';
        return $this->send('', $body);
    }

    public function getBalance($login, $password)
    {
        $body = '<?xml version="1.0" encoding="utf-8"?>
            <request>
            <security>
            <login value="' . $login . '"/>
            <password value="' . $password . '"/>
            </security>
            </request>';
        return $this->send('balance', $body);
    }

    private function send($method, $body)
    {
        $url = $this->baseUrl . '/' . $method;
        $headers = [
            'content-type' => 'text/xml; charset=utf-8',
        ];

        $client = new Client([
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);

        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'body'    => $body,
        ]);

        var_dump($body);
        var_dump($response->getBody()->getContents());

        return $response->getBody();
    }

}
