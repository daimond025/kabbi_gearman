<?php

namespace console\modules\sms\models\smsc;

use yii\base\Object;

class Smsc extends Object
{
    const OK = 'OK';

    public $baseUrl;

    public function sendSms($login, $password, $text, $phone)
    {
        $params = [
            'login' => $login,
            'psw' => $password,
            'phones' => $phone,
            'mes' => $text,
            'charset' => 'utf-8'
        ];

        /* @var $curl \console\components\curl\Curl */
        $curl = app()->curl;
        /* @var $response \console\components\curl\CurlResponse */
        $response = $curl->get($this->baseUrl . 'send.php', $params);

        if (!empty($response)) {
//            $text = 'OK - 1 SMS, ID - 17940';
//            $text = 'ERROR = 9 (duplicate request, wait a minute)';
            if (preg_match('/^(\bERROR\b|\bOK\b)\D+(\d+)( \((.+)\))?/i', $response, $arr)) {
                $status = $arr[1];
                if ($status != self::OK) {
                    return [
                        'error' => $arr[2],
                        'msg' => $arr[4]
                    ];
                }
                return ['error' => 0, 'msg' => ''];
            }
        }
        return [
            'error' => '-1',
            'msg' => 'No connect',
        ];
    }


}
