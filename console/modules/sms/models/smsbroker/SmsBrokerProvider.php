<?php

namespace console\modules\sms\models\smsbroker;

use console\modules\sms\models\SmsProviderInterface;
use yii\base\Component;

/**
 * Class SmsBrokerProvider
 * @package console\modules\sms\models\smsbroker
 *
 * @property SmsBroker $gate
 */
class SmsBrokerProvider extends Component implements SmsProviderInterface
{

    private $error = 'error';
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(SmsBroker::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $message, $target, $sender);

        if ($response['status'] === SmsBroker::STATUS_OK && $response['message'] === 'Delivered') {
            return true;
        }

        $this->error = empty($response['message']) ? 'Истекло время ожидания статуса успешного сообщения' : $response['message'];

        return false;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

}
