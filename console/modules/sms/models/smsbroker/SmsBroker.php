<?php

namespace console\modules\sms\models\smsbroker;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException;
use yii\base\Object;

class SmsBroker extends Object
{
    const STATUS_OK = 'ok';
    const STATUS_ERROR = 'error';
    const DELIVERY_NOTIFICATION_REQUESTED = 'true';
    const VERSION = '1.0';
    const ORDINAL = '1';

    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function sendSms($login, $password, $text, $phone, $sender)
    {
        $client = new Client([
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);

        try {
            $reqId = $this->generateRefId($login);
            $client->request('POST', $this->baseUrl, [
                'headers' => [
                    'Content-Type' => 'text/xml',
                ],
                'body'    => $this->getContentBySendSms($login, $password, $phone, $text, $sender, $reqId),
            ]);

            return $this->getStatus($login, $password, $reqId);
        } catch (ConnectException $exception) {
            return [
                'status'  => self::STATUS_ERROR,
                'message' => $exception->getMessage(),
            ];
        } catch (BadResponseException $exception) {
            return [
                'status'  => self::STATUS_ERROR,
                'message' => 'Bad response: ' . $exception->getResponse()->getStatusCode(),
            ];
        }
    }

    public function getContentBySendSms($login, $password, $to, $text, $sender, $reqId)
    {
        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'UTF-8');

        $xml->startElement('bulk-request');

        $xml->startAttribute('login');
        $xml->text($login);
        $xml->endAttribute();

        $xml->startAttribute('password');
        $xml->text($password);
        $xml->endAttribute();

        $xml->startAttribute('ref-id');
        $xml->text($reqId);
        $xml->endAttribute();

        $xml->startAttribute('delivery-notification-requested');
        $xml->text(self::DELIVERY_NOTIFICATION_REQUESTED);
        $xml->endAttribute();

        $xml->startAttribute('version');
        $xml->text(self::VERSION);
        $xml->endAttribute();

        $xml->startElement('message');

        $xml->startAttribute('id');
        $xml->text(1);
        $xml->endAttribute();

        $xml->startAttribute('msisdn');
        $xml->text($to);
        $xml->endAttribute();

        if (!empty($sender)) {
            $xml->startAttribute('service-number');
            $xml->text($sender);
            $xml->endAttribute();
        }

        $xml->startAttribute('validity-period');
        $xml->text(3);
        $xml->endAttribute();

        $xml->startAttribute('priority');
        $xml->text(1);
        $xml->endAttribute();

        $xml->startElement('content');

        $xml->startAttribute('type');
        $xml->text('text/plain');
        $xml->endAttribute();

        $xml->text(iconv('utf-8', 'windows-1251', $text));

        $xml->endElement();

        $xml->endElement();

        $xml->endElement();

        $xml->endElement();
        $xml->endDocument();

        return $xml->outputMemory();
    }

    public function getStatus($login, $password, $reqId)
    {
        $response = [];
        for ($i = 1; $i <= 6; $i++) {

            sleep(4);

            $response = $this->sendGetStatus($login, $password, $reqId);

            if ($response['status'] === self::STATUS_OK && !empty($response['message'])) {
                return $response;
            }
        }

        return $response;
    }

    protected function sendGetStatus($login, $password, $reqId)
    {
        $client = new Client([
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout
        ]);

        try {
            $resp = $client->request('POST', $this->baseUrl, [
                'headers' => [
                    'Content-Type' => 'text/xml',
                ],
                'body'    => $this->getContentByGetStatus($login, $password, $reqId),
            ]);

            $response = (string)$resp->getBody();

            libxml_use_internal_errors(true);
            $xml = simplexml_load_string($response);

            return [
                'status'  => self::STATUS_OK,
                'message' => $xml->status ? (string)$xml->status->attributes()->state : '',
            ];
        } catch (ConnectException $exception) {
            return [
                'status'  => self::STATUS_ERROR,
                'message' => $exception->getMessage(),
            ];
        } catch (BadResponseException $exception) {
            return [
                'status'  => self::STATUS_ERROR,
                'message' => 'Bad response: ' . $exception->getResponse()->getStatusCode(),
            ];
        }
    }


    protected function getContentByGetStatus($login, $password, $reqId)
    {
        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'UTF-8');

        $xml->startElement('delivery-notification');

        $xml->startAttribute('login');
        $xml->text($login);
        $xml->endAttribute();

        $xml->startAttribute('password');
        $xml->text($password);
        $xml->endAttribute();

        $xml->startAttribute('ref-id');
        $xml->text($reqId);
        $xml->endAttribute();

        $xml->startAttribute('ordinal');
        $xml->text(self::ORDINAL);
        $xml->endAttribute();

        $xml->startAttribute('version');
        $xml->text(self::VERSION);
        $xml->endAttribute();

        $xml->endElement();
        $xml->endDocument();

        return $xml->outputMemory();
    }


    protected function generateRefId($login)
    {
        return $login . '-' . microtime(true);
    }


}
