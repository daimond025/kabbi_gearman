<?php

namespace console\modules\sms\models;

use yii\base\Object;

class MsmSms extends Object
{
    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public $errors = [
        0   => 'Missing parameters',
        10  => 'Configuration error',
        20  => 'Invalid msisdn',
        25  => 'Blacklisted msisdn',
        30  => 'Unauthorized destination network',
        40  => 'Invalid username/password',
        50  => 'Unauthorized sender name',
        60  => 'Insufficient balance',
        80  => 'Invalid validity period',
        85  => 'Invalid delivery datetime',
        90  => 'Exceeded message size limit / Too many requests',
        100 => 'OK',
        200 => 'Server error',
    ];

    public function execute($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectionTimeout);
        curl_setopt($ch, CURLOPT_TIMEOUT,  $this->timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function send_message($login, $password, $message, $target, $sender)
    {
        $login    = urlencode($login);
        $password = urlencode($password);
        $message  = urlencode($message);
        $target   = urlencode($target);
        $sender   = urlencode($sender);

        $url = $this->baseUrl . 'sendsms?user=' . $login . '&password=' . $password . '&gsm=' . $target . '&from=' . $sender . '&text=' . $message;

        return $this->execute($url);
    }

    public function request_balance($login, $password)
    {
        $url = $this->baseUrl . 'query/balance?username=' . $login . '&apikey=' . $password;
        return $this->execute($url);
    }

}
