<?php

namespace console\modules\sms\models;

use Yii;
use frontend\modules\tenant\models\Tenant;

/**
 * This is the model class for table "{{%tbl_tenant_has_sms}}".
 *
 * @property string $id
 * @property integer $server_id
 * @property string $tenant_id
 * @property string $login
 * @property string $password
 * @property integer $active
 * @property string $sign
 *
 * @property TblSmsServer $server
 * @property TblTenant $tenant
 */
class TenantHasSms extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_sms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'active'], 'required'],
            [['server_id', 'tenant_id', 'active'], 'integer'],
            [['login', 'password', 'sign'], 'string', 'max' => 45]
        ];
    }
    
    public function subRules()
    {
        $rules = [
            [['login', 'password', 'server_id', 'tenant_id', 'sign'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
        return array_merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('app', 'ID'),
            'server_id' => Yii::t('setting', 'Server'),
            'tenant_id' => Yii::t('app', 'Tenant ID'),
            'login'     => Yii::t('setting', 'Login'),
            'password'  => Yii::t('app', 'Password'),
            'active'    => Yii::t('setting', 'Active'),
            'sign'      => Yii::t('setting', 'Sign'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServer()
    {
        return $this->hasOne(SmsServer::className(), ['server_id' => 'server_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

}
