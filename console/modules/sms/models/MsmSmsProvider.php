<?php

namespace console\modules\sms\models;

use console\modules\sms\exceptions\RequestBalanceException;

class MsmSmsProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error;
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(MsmSms::class);
    }

    /**
     * @inheritdoc
     */
    public function sendSms($login, $password, $message, $target, $sender)
    {
        $data = $this->gate->send_message($login, $password, $message, $target, $sender);
        $data = explode('&', $data);
        $response = str_replace('errno=', '', $data[0]);
        if ($response == 100) {
            return true;
        } else {
            $this->error = $this->gate->errors[$response];
            return false;
        }
    }

    public function requestBalance($login, $password)
    {
        $data = $this->gate->request_balance($login, $password);
        $data = explode('&', $data);
        
        if ($data[0][0] == 'b') {
            return str_replace('balance=', '', $data[0]);
        } else {
            throw new
            RequestBalanceException($this->gate->errors[str_replace('errno=', '', $data[0])]);
        }
    }

    /**
     * 
     * @return type
     */
    public function getError()
    {
        return $this->error;
    }

}
