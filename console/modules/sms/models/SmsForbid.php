<?php

namespace console\modules\sms\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_sms_forbid".
 *
 * @property integer $id
 * @property string $phone
 */
class SmsForbid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_forbid}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['phone'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'PHONE',
        ];
    }
}


