<?php

namespace console\modules\sms\models\alphasms;

class AlphasmsProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error = 'error';
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(Alphasms::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $message, $target, $sender);
        if (!$response) {
            $this->error = 'No connect';
            return false;
        }

        $response = explode(':', $response,2);

        if (count($response) != 2) {
            $this->error = 'Unknown error';
            return false;
        }

        if (in_array($response[0], ['error', 'errors'])) {
            $this->error = $response[1];
            return false;
        }

        if ($response[0] == 'id') {
            return true;
        }

        $this->error = 'Unknown error';
        return false;
    }

    public function requestBalance($login, $password)
    {
        $response = $this->gate->getBalance($login, $password);

        if (!$response) {
            $this->error = 'No connect';
            return '';
        }

        $response = explode(':', $response,2);

        if (count($response) != 2) {
            $this->error = 'Unknown error';
            return '';
        }

        if (in_array($response[0], ['error', 'errors'])) {
            $this->error = $response[1];
            return '';
        }

        if ($response[0] == 'balance') {
            return $response[1];
        }

        $this->error = 'Unknown error';
        return '';
    }

    public function getError()
    {
        return $this->error;
    }
}