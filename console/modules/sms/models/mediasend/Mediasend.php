<?php

namespace console\modules\sms\models\mediasend;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class Mediasend extends Object
{
    const ACTION_SEND = 'add';
    const ACTION_CHECK = 'check';
    const VERIFY = true;
    const OK = 'ok';
    const ERROR = 'error';
    const CHECK_COUNT = 6;
    const CHECK_DELAY = 10;

    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function sendSms($login, $password, $from, $to, $message)
    {
        $options = [
            'form_params' => [
                'text'   => $message,
                'phones' => [
                    [
                        'name'    => '',
                        'surname' => '',
                        'phone'   => $to,
                    ],
                ],
                'appid'  => $login,
            ],
            'query'       => [
                'apiKey' => $password,
            ],
            'verify'      => self::VERIFY,
        ];

        $responseSend = $this->post(self::ACTION_SEND, $options);

        if ($responseSend['success']) {

            $messageId = $responseSend['results'][0]['actionId'];

            $responseCheck = $this->checkSms($messageId, $password);

            if ($responseCheck['success']) {
                switch ($responseCheck[$messageId]['status']) {
                    case 1:
                        return [
                            'status'  => self::OK,
                            'message' => null,
                        ];

                    case 2:
                        return [
                            'status'  => self::ERROR,
                            'message' => 'Ошибка при отправке сообщения',
                        ];

                    case 1:
                        return [
                            'status'  => self::ERROR,
                            'message' => 'Истекло время ожидания проверки статуса сообщения',
                        ];
                }
            }

            return [
                'status'  => self::ERROR,
                'message' => 'Внутренняя ошибка',
            ];

        } else {
            return [
                'status'  => self::ERROR,
                'message' => $this->getErrorString($responseSend['error']),
            ];
        }
    }


    public function checkSms($messageIds, $password)
    {
        $options = [
            'query'   => [
                'apiKey'   => $password,
                'messages' => (array)$messageIds,
            ],
            'verify'  => self::VERIFY,
        ];

        $checkCount = self::CHECK_COUNT > 1 ? (int)self::CHECK_COUNT : 1;
        $checkDelay = self::CHECK_DELAY > 0 ? self::CHECK_DELAY : 0;

        $response = null;

        for ($i = 1; $i < $checkCount; $i++) {

            if ($i !== 1) {
                sleep($checkDelay);
            }

            $response = $this->get(self::ACTION_CHECK, $options);

            if (!$response['success'] || $response[$messageIds]['status'] != 0) {
                return $response;
            }

        }

        return $response;
    }


    protected function post($action, $options)
    {
        return $this->send('POST', $action, $options);
    }


    protected function get($action, $options)
    {
        return $this->send('GET', $action, $options);
    }


    protected function send($type = 'GET', $action, $options)
    {
        $client = new Client([
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);
        $url    = $this->getUrl($action);

        try {
            $response = $client->request($type, $url, $options);
            $result   = json_decode($response->getBody(), true);

            return $result;
        } catch (ClientException $ex) {

            return [
                'success' => false,
                'message' => 'Connect error. Status code ' . $ex->getResponse()->getStatusCode(),
            ];
        }
    }

    protected function getUrl($action)
    {
        return $this->baseUrl . $action;
    }

    protected function getErrorString($code)
    {
        $data = [
            400 => 'Некорректный массив телефонов (400)',
            401 => 'Текст сообщения не должен быть пустым (401)',
            402 => 'Не найдено приложение для интерграции (402)',
            403 => 'Не правильный пароль (403)',
            406 => 'Приложение отклонило сообщение. Проверьте время интеграции (406)',
        ];

        return ArrayHelper::getValue($data, $code, $code);
    }
}