<?php

namespace console\modules\sms\models\smsLine;

use console\modules\sms\models\SmsProviderInterface;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class SmsLineProvider
 * @package console\modules\sms\models\smsLine
 *
 * @property SmsLine $gate
 * @property string $error
 */
class SmsLineProvider extends Component implements SmsProviderInterface
{

    private $error;
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(SmsLine::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $target, $sender, $message);

        if (ArrayHelper::getValue($response, 'status', false)) {
            return true;
        }

        $this->error = ArrayHelper::getValue($response, 'message');

        return false;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

}
