<?php

namespace console\modules\sms\models\smsLine;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use yii\base\Object;
use yii\helpers\ArrayHelper;

/**
 * Class SmsLine
 *
 * Документация https://api.smsline.by/v3/
 *
 * @package console\modules\sms\models\smsLine
 */
class SmsLine extends Object
{
    const VERSION = 'v3';

    const ACTION_SEND_SMS = 'messages/single/sms';
    const ACTION_CHECK_SMS = 'messages';

    const CHECK_COUNT = 5;
    const CHECK_DELAY = 5;

    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function getUrl($action)
    {
        return rtrim($this->baseUrl, '/') . '/' . trim(SmsLine::VERSION, '/') . '/' . $action;
    }

    public function sendSms($login, $password, $to, $from, $message)
    {
        $params = [
            'target' => $from,
            'msisdn' => $to,
            'text'   => $message,
        ];

        $paramsToString = json_encode($params);

        $options = [
            'headers'     => [
                'Authorization-User' => $login,
                'Authorization'      => 'Bearer ' . $this->getHash($password,
                        str_replace('/', '', SmsLine::ACTION_SEND_SMS) . $paramsToString),
                'Content-Type'       => 'application/json',
            ],
            'body'        => $paramsToString,
            'http_errors' => false,
        ];


        $response = $this->post(SmsLine::ACTION_SEND_SMS, $options);

        if (!$response) {
            return [
                'status'  => false,
                'message' => 'Сервер не отвечает',
            ];
        }

        if (ArrayHelper::keyExists('error', $response)) {
            return [
                'status'  => false,
                'message' => ArrayHelper::getValue($response, 'error.message'),
            ];
        }

        $messageId = ArrayHelper::getValue($response, 'message.id_message');

        $messageError = '';

        for ($i = 1; $i <= SmsLine::CHECK_COUNT; $i++) {

            sleep(SmsLine::CHECK_DELAY);

            $responseCheck = $this->checkSms($login, $password, $messageId);

            if (ArrayHelper::getValue($responseCheck, 'status', false)) {

                return [
                    'status'  => true,
                    'message' => '',
                ];
            }

            $messageError = ArrayHelper::getValue($responseCheck, 'message');
        }

        return [
            'status'  => false,
            'message' => $messageError,
        ];
    }


    public function checkSms($login, $password, $messageId)
    {
        $action = SmsLine::ACTION_CHECK_SMS . '/' . $messageId;
        $params = [
            'id_message' => $messageId,
        ];

        $paramsToString = $this->paramsToStringByHash($params);

        $options = [
            'headers'     => [
                'Authorization-User' => $login,
                'Authorization'      => 'Bearer ' . $this->getHash($password,
                        str_replace('/', '', $action) . $paramsToString),
            ],
            'query'       => $params,
            'http_errors' => false,
        ];

        $response = $this->get($action, $options);

        if ($response) {
            $state = ArrayHelper::getValue($response, 'message.state.state');

            if ($state === 'delivered') {
                return [
                    'status' => true,
                ];
            } else {
                return [
                    'status'  => false,
                    'message' => ArrayHelper::getValue($response, 'message.state.description'),
                ];
            }
        }

        return null;

    }


    protected function post($action, $options)
    {
        return $this->send('POST', $action, $options);
    }


    protected function get($action, $options)
    {
        return $this->send('GET', $action, $options);
    }


    protected function send($type = 'GET', $action, $options)
    {
        $client = new Client([
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);
        $url    = $this->getUrl($action);

        try {
            $response = $client->request($type, $url, $options);
            $result   = json_decode($response->getBody(), true);

            return $result;
        } catch (ClientException $ex) {

            return null;
        }
    }


    public function paramsToStringByHash($params)
    {
        ksort($params);

        $paramsToString = '';

        foreach ($params as $key => $value) {
            $paramsToString .= $key . $value;
        }

        return $paramsToString;
    }


    public function getHash($password, $string)
    {
        $hash = hash_init('sha256', HASH_HMAC, $password);
        hash_update($hash, $string);

        return hash_final($hash);
    }
}
