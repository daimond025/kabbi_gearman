<?php

namespace console\modules\sms\models;

use yii\base\Object;

class IbateleSms extends Object
{
    //Тексты сообщений об ошибках
    const POST_DATA_MISSING_ERROR_MESSAGE = 'POST данные отсутствуют';
    const EMPTY_BALANCE_RESPONSE = '{"money":{"@attributes":{"currency":""}}}';
    const INCORRECT_LOGIN_DATA = 'Неправильный логин или пароль';
    const ACCOUNT_IS_BANNED = 'Ваш аккаунт заблокирован';
    const INCORRECT_XML_FORMAT = 'Неправильный формат XML документа';

    //Адреса отправки запросов
    public $sendServerAddress;
    public $requestSmsStateServerAddress;
    public $requestBalanceServerAddress;
    public $requestSenderListServerAddress;
    public $requestIncomingMessagesServerAddress;
    public $requestInfoAboutPhone;
    public $requestListOfBases;
    public $requestBasesEditUpdate;
    public $requestGetClientBase;
    public $connectionTimeout;
    public $timeout;

    /**
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER
     */
    public function actionRequestSenderList($login, $password)
    {
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        </request>';
        return $this->send_request($xml, $this->requestSenderListServerAddress);
    }

    public function send_message($xml)
    {
        return $this->send_request($xml, $this->sendServerAddress, false);
    }

    public function request_balance($login, $password)
    {
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        </request>';
        $result = $this->send_request($xml, $this->requestBalanceServerAddress);

        $xmlObj = simplexml_load_string($result);
        $xml = (array) $xmlObj;

        return json_encode($xml);
    }

    /**
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER
     *
     * PS: time format: YYYY-MM-DD HH:MM on null receive all messages
     */
    public function actionRequestIncomingSms($login, $password, $period_of_time_start = NULL, $period_of_time_end = NULL)
    {
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        <timestart="' . $period_of_time_start . '" ' . $period_of_time_end . '" />
        </request>';
        return $this->send_request($xml, $this->requestIncomingMessagesServerAddress);
    }

    /**
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER
     */
    public function actionRequestInformationAboutPhones($login, $password, $phones)
    {
        foreach ($phones as $rows) {
            $phoneList .= '<phone>' . $rows['phone'] . '</phone><br/>';
        }
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        <phones>' .
                $phoneList .
                '</phones>
        </request>';
        return $this->send_request($xml, $this->requestInfoAboutPhone);
    }

    /**
     * $local_time_birth, $doShowMessage bool
     * $base_name array()
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER
     *
     * time format is : HH:MM
     */
    public function actionBaseParameters($login, $password, $base_name, $birth_time, $doShowMessage, $local_time_birth, $day_before, $success_message_author, $successMessageSendTime, $base_id, $message_on_success)
    {
        if ($doShowMessage == false) {
            $doShowMessage = 'no';
        } else if ($doShowMessage == true) {
            $doShowMessage = 'yes';
        } else {
            return 'doShowMessage must be bool';
        } foreach ($bases as $rows) {
            $baseList .= $rows;
        }
        $xml = '<?xml version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="логин" />
        <password value="пароль" />
        </security>
        <bases>
        <base id_base="' . $base_id . '" name_base="' . $base_name . '" time_birth="' .
                $birth_time . '" local_time_birth="yes" day_before="' . $day_before . '"originator_birth="' .
                $success_message_author . '" on_birth="yes">"' .
                $message_on_success . '"!</base>
        </bases>
        <delete_bases>
        <base id_base="1235" />
        <base id_base="1236" />
        …
        </delete_bases>
        </request>';
        return $this->send_request($xml, $this->requestBasesEditUpdate);
    }

    /**
     *
     * @param type $xml
     * @param type $server_address remote IP address of server
     * depends on what action you gonna do
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER if incoming data available
     */
    public function send_request($xml, $server_address, $balance = false)
    {
        $res = '';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/xml; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CRLF, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectionTimeout);
        curl_setopt($ch, CURLOPT_TIMEOUT,  $this->timeout);
        if ($balance = true) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        }
        curl_setopt($ch, CURLOPT_URL, $server_address);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result; // отправить на функцию - парсер
    }

    /**
     * Checking sms status.
     * @param mixed json encoded server response
     */
    public function isResultSuccess($result)
    {
        if (!empty($result['error']))
            return false;

        return true;
    }

}
