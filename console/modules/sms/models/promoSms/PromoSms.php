<?php

namespace console\modules\sms\models\promoSms;

use GuzzleHttp\Client;
use yii\base\Object;

class PromoSms extends Object
{
    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function sendSms($login, $password, $text, $phone, $sender)
    {
        try {
            $httpClient = new Client([
                'connectionTimeout' => $this->connectionTimeout,
                'timeout'           => $this->timeout,
            ]);

            $resp = $httpClient->request('GET', $this->baseUrl, [
                'query' => [
                    'action' => 'post_sms',
                    'user' => $login,
                    'pass' => $password,
                    'target' => $phone,
                    'message' => $text,
                    'sender' => $sender
                ],
            ]);

            return (string)$resp->getBody();
        } catch (\Exception $ex) {
            return null;
        }
    }


}