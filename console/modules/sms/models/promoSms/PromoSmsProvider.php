<?php

namespace console\modules\sms\models\promoSms;

class PromoSmsProvider extends \yii\base\Component implements \console\modules\sms\models\SmsProviderInterface
{

    private $error;

    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(PromoSms::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $message, $target, $sender);

        $xml = $this->parseResponse($response);

        if (!empty($xml->errors[0])) {
            $errors = (array)$xml->errors[0];
            $errors = (array)$errors['error'];
            $this->error = reset($errors);

            return false;
        }

        return true;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

    protected function parseResponse($string)
    {
        return simplexml_load_string($string);
    }

}