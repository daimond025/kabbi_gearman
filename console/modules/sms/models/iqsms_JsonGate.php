<?php

namespace console\modules\sms\models;

use yii\base\Exception;
use yii\base\Object;

class iqsms_JsonGate extends Object
{

    const ERROR_EMPTY_API_LOGIN = 'Empty api login not allowed';
    const ERROR_EMPTY_API_PASSWORD = 'Empty api password not allowed';
    const ERROR_EMPTY_RESPONSE = 'errorEmptyResponse';

    protected $apiLogin = null;
    protected $apiPassword = null;
    protected $packetSize = 200;
    protected $results = [];

    public $host;
    public $host_balance;
    public $connectionTimeout;
    public $timeout;

    public function setApiLogin($apiLogin)
    {
        $this->apiLogin = $apiLogin;
    }

    public function setApiPassword($apiPassword)
    {
        $this->apiPassword = $apiPassword;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function getHost($uri = '')
    {
        if($uri == 'messages/v2/balance.json')
        {
            return $this->host_balance;
        }
        return $this->host;
    }

    private function _sendRequest($uri, $params = null)
    {
        $url = $this->_getUrl($uri);
        $data = $this->_formPacket($params);

        $client = curl_init($url);
        curl_setopt_array($client, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => true,
            CURLOPT_HEADER         => false,
            CURLOPT_HTTPHEADER     => array('Host: ' . $this->getHost()),
            CURLOPT_POSTFIELDS     => $data,
            CURLOPT_CONNECTTIMEOUT => $this->connectionTimeout,
            CURLOPT_TIMEOUT        => $this->timeout,
        ));

        $body = curl_exec($client);
        curl_close($client);
        if (empty($body)) {
            throw new Exception(self::ERROR_EMPTY_RESPONSE);
        }

        $decodedBody = json_decode($body, true);

        if (is_null($decodedBody)) {
            throw new Exception($body);
        }
        return $decodedBody;
    }

    private function _getUrl($uri)
    {
        if ($uri == 'messages/v2/balance.json') {
            return 'http://' . $this->getHost($uri) . '/' . $uri . '/';
        }
        return 'http://' . $this->getHost($uri) . '/' . $uri . '/';
    }

    private function _formPacket($params = null)
    {
        $params['login'] = $this->apiLogin;
        $params['password'] = $this->apiPassword;
        foreach ($params as $key => $value) {
            if (empty($value)) {
                unset($params[$key]);
            }
        }
        $packet = json_encode($params);
        return $packet;
    }

    public function getPacketSize()
    {
        return $this->_packetSize;
    }

    public function send_message($messages, $statusQueueName = null, $scheduleTime = null)
    {
        $params = array(
            'messages' => $messages,
            'statusQueueName' => $statusQueueName,
            'scheduleTime' => $scheduleTime,
        );
        return $this->_sendRequest('send', $params);
    }

    public function status($messages)
    {
        return $this->_sendRequest('status', array('messages' => $messages));
    }

    public function statusQueue($name, $limit)
    {
        return $this->_sendRequest('statusQueue', array(
                    'statusQueueName' => $name,
                    'statusQueueLimit' => $limit,
        ));
    }

    public function credits()
    {
        return $this->_sendRequest('messages/v2/balance.json');
    }

    public function senders()
    {
        return $this->_sendRequest('senders');
    }

}
