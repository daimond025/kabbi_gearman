<?php

namespace console\modules\sms\models\nikitaKg;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Ramsey\Uuid\Uuid;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class NikitaKg extends Object
{
    const ACTION_SEND = 'api/message';
    const ACTION_CHECK = 'api/dr';

    const CHECK_TIME_DELAY = 5;
    const CHECK_COUNT_DELAY = 5;

    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    /**
     * Отправка сообщения
     *
     * @param $login
     * @param $password
     * @param $from
     * @param $to
     * @param $message
     *
     * @return array
     */
    public function sendSms($login, $password, $from, $to, $message)
    {
        $reqId        = $login . '-' . Uuid::uuid4()->toString();
        $content      = $this->getContentBySendSms($login, $password, $from, $to, $message, $reqId);
        $sendResponse = $this->post(NikitaKg::ACTION_SEND, $content);

        $sendResponse['status'] = 'ok';

        if (ArrayHelper::getValue($sendResponse, 'status') === 'ok') {

            $sendResponse = simplexml_load_string(ArrayHelper::getValue($sendResponse, 'message', ''));
            $status       = (string)$sendResponse->status;
            $message      = $this->getSendMessageByStatus($status);

            if ($status === '0') {

                for ($i = 1; $i <= NikitaKg::CHECK_COUNT_DELAY; $i++) {

                    sleep(NikitaKg::CHECK_TIME_DELAY);

                    $checkResponse = $this->checkSms($login, $password, $reqId, $to);

                    if (ArrayHelper::getValue($checkResponse, 'status') === 'ok') {
                        return $checkResponse;
                    }

                    $message = ArrayHelper::getValue($checkResponse, 'message', '');
                }
            }

            return [
                'status'  => 'error',
                'message' => $message,
            ];

        }

        return $sendResponse;
    }


    public function checkSms($login, $password, $reqId, $phone)
    {
        $content       = $this->getContentByCheckSms($login, $password, $reqId, $phone);
        $checkResponse = $this->post(NikitaKg::ACTION_CHECK, $content);

        if (ArrayHelper::getValue($checkResponse, 'status') === 'ok') {
            $checkResponse = simplexml_load_string(ArrayHelper::getValue($checkResponse, 'message', ''));
            $status        = (string)$checkResponse->status;

            if ($status === '0') {
                $report = (string)$checkResponse->phone->report;
                if ($report === '3') {
                    return [
                        'status'  => 'ok',
                        'message' => '',
                    ];
                } else {
                    return [
                        'status'  => 'error',
                        'message' => $this->getMessageByReport($report),
                    ];
                }
            } else {
                return [
                    'status'  => 'error',
                    'message' => $this->getCheckMessageByStatus($status),
                ];
            }
        }

        return $checkResponse;
    }


    public function post($action, $content)
    {

        $options = [
            'headers' => [
                'Content-Type' => 'text/xml',
            ],
            'body'    => $content,
        ];

        return $this->send('POST', $action, $options);
    }


    public function send($type, $action, $options)
    {
        $client = new Client([
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);
        $url    = $this->getUrl($action);
        try {
            $response = $client->request($type, $url, $options);

            return ['status' => 'ok', 'message' => (string)$response->getBody()];
        } catch (ClientException $ex) {
            return ['status' => 'error', 'message' => 'Status code: ' . $ex->getCode()];
        }
    }

    public function getUrl($action)
    {
        return rtrim($this->baseUrl, '/') . '/' . trim($action, '/');
    }


    public function getContentBySendSms($login, $password, $from, $to, $text, $reqId, $isTesting = false)
    {
        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'UTF-8');

        $xml->startElement('message');

        $xml->startElement('login');
        $xml->text($login);
        $xml->endElement();

        $xml->startElement('pwd');
        $xml->text($password);
        $xml->endElement();

        $xml->startElement('id');
        $xml->text($reqId);
        $xml->endElement();

        $xml->startElement('sender');
        $xml->text($from);
        $xml->endElement();

        $xml->startElement('text');
        $xml->text($text);
        $xml->endElement();

        $xml->startElement('phones');
        $xml->startElement('phone');
        $xml->text($to);
        $xml->endElement();
        $xml->endElement();

        if ($isTesting) {
            $xml->startElement('test');
            $xml->text(1);
            $xml->endElement();
        }

        $xml->endElement();

        $xml->endDocument();

        return $xml->outputMemory();
    }


    public function getContentByCheckSms($login, $password, $reqId, $phone)
    {
        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'UTF-8');

        $xml->startElement('dr');

        $xml->startElement('login');
        $xml->text($login);
        $xml->endElement();

        $xml->startElement('pwd');
        $xml->text($password);
        $xml->endElement();

        $xml->startElement('id');
        $xml->text($reqId);
        $xml->endElement();

        $xml->startElement('phone');
        $xml->text($phone);
        $xml->endElement();

        $xml->endElement();

        $xml->endDocument();

        return $xml->outputMemory();
    }


    public function getSendMessageByStatus($code)
    {
        $array = [
            0  => 'Успешно принято к отправке',
            1  => 'Ошибка в формате запроса',
            2  => 'Неверная авторизация',
            3  => 'Недопустимый IP-адрес отправителя',
            4  => 'Недостаточно средств',
            5  => 'Недопустимая подпись',
            6  => 'Сообщение заблокировано по стоп-словам',
            7  => 'Некорректный номер телефона',
            8  => 'Неверный формат времени отправки',
            9  => 'Отправка заблокирована из-за срабатывания SPAM фильтра',
            10 => 'Ошибочная переотправка',
            11 => 'Успешно, но не отправлено(для тестов)',
        ];

        return ArrayHelper::getValue($array, $code, $code);
    }


    public function getCheckMessageByStatus($code)
    {
        $array = [
            0 => 'Успешно принято к отправке',
            1 => 'Ошибка в формате запроса',
            2 => 'Неверная авторизация',
            3 => 'Недопустимый IP-адрес отправителя',
            4 => 'Отчет не найден',
        ];

        return ArrayHelper::getValue($array, $code, $code);
    }


    public function getMessageByReport($code)
    {
        $array = [
            0 => 'В очереди на отправку',
            1 => 'Передано оператору',
            2 => 'Отклонено',
            3 => 'Доставлено',
            4 => 'Не доставлено',
            5 => 'Недостаточно средств',
            6 => 'Неизвестный статус отправки',
        ];

        return ArrayHelper::getValue($array, $code, $code);
    }
}
