<?php

namespace console\modules\sms\models\nikitaKg;

use console\modules\sms\models\SmsProviderInterface;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class NikitaKgProvider
 * @package console\modules\sms\models\nikitaKg
 *
 */
class NikitaKgProvider extends Component implements SmsProviderInterface
{

    private $error;

    /* @var $gate NikitaKg */
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(NikitaKg::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $sender, $target, $message);
        if (ArrayHelper::getValue($response, 'status') === 'error') {
            $this->error = ArrayHelper::getValue($response, 'message');

            return false;
        }

        return true;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

}
