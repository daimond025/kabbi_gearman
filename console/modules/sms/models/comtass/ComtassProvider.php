<?php

namespace console\modules\sms\models\comtass;

use console\modules\sms\models\SmsProviderInterface;
use yii\base\Component;

/**
 * Class ComtassProvider
 * @property Comtass $gate
 * @package console\modules\sms\models\gosms
 */
class ComtassProvider extends Component implements SmsProviderInterface
{

    private $error = 'error';

    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(Comtass::class);
    }

    public function sendSms($login, $password, $message, $target, $sender)
    {
        $response = $this->gate->sendSms($login, $password, $message, $target, $sender);

        if ($response === Comtass::STATUS_OK) {
            return true;
        }

        $this->error = $response;

        return false;
    }

    public function requestBalance($login, $password)
    {
        return json_encode('Method does not work');
    }

    public function getError()
    {
        return $this->error;
    }

}
