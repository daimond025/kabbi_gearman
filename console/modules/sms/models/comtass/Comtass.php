<?php

namespace console\modules\sms\models\comtass;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use yii\base\Object;

class Comtass extends Object
{
    const STATUS_OK = 'OK';
    const STATUS_REJECTED = 'Rejected';
    const STATUS_INVALID = 'Invalid';

    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function sendSms($login, $password, $text, $phone, $sender)
    {
        try {
            $httpClient = new Client([
                'connectionTimeout' => $this->connectionTimeout,
                'timeout'           => $this->timeout,
            ]);

            $resp = $httpClient->request('GET', $this->baseUrl, [
                'query' => [
                    'user' => $login,
                    'pwd' => $password,
                    'Nums' => $phone,
                    'smstext' => $text,
                    'Sender' => $sender
                ],
            ]);

            return (string)$resp->getBody();
        } catch (ClientException $ex) {
            return 'Undefined';
        }
    }


}