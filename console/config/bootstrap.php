<?php

\Yii::setAlias('api', dirname(dirname(__DIR__)) . '/api');
\Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
\Yii::setAlias('notification', '@console/modules/notification');
\Yii::setAlias('gearman', '@console/components/gearman');
\Yii::setAlias('bonusSystem', '@console/components/bonusSystem');
\Yii::setAlias('referralSystem', '@console/components/referralSystem');
\Yii::setAlias('promoCode', '@console/components/promoCode');
\Yii::setAlias('orderTrackService', '@console/components/orderTrackService');
\Yii::setAlias('credit', '@console/components/billing/models/credit');
\Yii::setAlias('healthCheck', '@api/components/healthCheck');
\Yii::setAlias('paymentGate', '@console/components/paymentGate');
