<?php

$db     = require __DIR__ . '/db.php';
$params = require __DIR__ . '/params.php';

return [
    'id'                  => 'tests',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components'          => [
        // Yii2 components
        'cache' => [
            'class' => 'yii\caching\DummyCache',
        ],

        'i18n' => [
            'translations' => [
                '*'    => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
                'app*' => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
            ],
        ],

        'log' => [
            'traceLevel'    => 3,
            'flushInterval' => 1,
            'targets'       => [
                'app'    => [
                    'class'          => 'yii\log\FileTarget',
                    'levels'         => ['error', 'warning', 'info'],
                    'except'         => ['appLog'],
                    'exportInterval' => 1,
                ],
                'errors' => [
                    'class'          => 'yii\log\FileTarget',
                    'logFile'        => '@runtime/logs/errors.log',
                    'levels'         => ['error', 'warning'],
                    'except'         => ['appLog'],
                    'exportInterval' => 1,
                ],
            ],
        ],

        'mailer_log'  => [
            'class'            => 'yii\swiftmailer\Mailer',
            'viewPath'         => '@console/mail',
            'useFileTransport' => true,
        ],
        'mailer'      => [
            'class'            => 'yii\swiftmailer\Mailer',
            'viewPath'         => '@console/mail',
            'useFileTransport' => true,
        ],

        // Databases
        'db'          => $db['dbMainTest'],
        'db_log'      => $db['dbMainTest'],

        // Other components and services
        'curl'        => [
            'class'   => 'console\components\curl\Curl',
            'options' => [
                'connectTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
                'timeout'        => getenv('CURL_TIMEOUT'),
            ],
        ],
        'paygateCurl' => [
            'class'          => 'console\components\curl\RestCurl',
            'connectTimeout' => getenv('PAYGATE_CURL_CONNECT_TIMEOUT'),
            'timeout'        => getenv('PAYGATE_CURL_TIMEOUT'),
        ],

        'logger' => [
            'class'   => 'console\components\log\SysLog',
            'enabled' => false,
        ],
    ],

    'modules' => [
        'sms' => 'console\modules\sms\Module',
    ],

    'params' => $params,
];