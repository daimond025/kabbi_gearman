<?php

return [
    'my.networkTimeout'        => getenv('MY_NETWORK_TIMEOUT'),

    'siteDomain' => getenv('SITE_DOMAIN'),

    'mail.adminEmail'           => getenv('MAIL_DISPATCH_USERNAME'),
    'mail.infoEmail'            => getenv('MAIL_DISPATCH_USERNAME'),
    'mail.attachmentPath'       => '@console/mail/attachment/',
    'mail.gootaxInstructionUrl' => 'https://docs.google.com/document/d/1zNoZGow2OjjBvZCo7og2xkgJk-fzyvLHCJiYQ5jssUs/',

    'rabbitMQ.host'        => getenv('RABBITMQ_MAIN_HOST'),
    'rabbitMQ.virtualHost' => getenv('RABBITMQ_MAIN_VIRTUAL_HOST'),
    'rabbitMQ.port'        => getenv('RABBITMQ_MAIN_PORT'),
    'rabbitMQ.user'        => getenv('RABBITMQ_MAIN_USER'),
    'rabbitMQ.password'    => getenv('RABBITMQ_MAIN_PASSWORD'),

    'pushApi.url'        => getenv('SERVICE_PUSH_URL'),
    'paymentGateApi.url' => getenv('API_PAYGATE_URL'),
    'cashDeskApi.url'    => getenv('API_CASHDESK_URL'),

    'bonusSystem.UDSGame.baseUrl'           => 'https://udsgame.com/v1/partner/',
    'bonusSystem.UDSGame.connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
    'bonusSystem.UDSGame.timeout'           => getenv('CURL_TIMEOUT'),

    'version' => require __DIR__ . '/version.php',
];
