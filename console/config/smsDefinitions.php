<?php

return [
    \console\modules\sms\models\maraditSms\Maradit::class => [
        'baseUrl'           => getenv('SMS_MARADIT_URL') . 'sendsmsjm/restful/quicksms/',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\alphasms\Alphasms::class => [
        'baseUrl' => getenv('SMS_ALPHASMS_URL') . 'api/http.php',
        'version' => 'http',
    ],

    \console\modules\sms\models\bsg\BsgSms::class => [
        'baseUrl'           => getenv('SMS_BSGSMS_URL') . 'xml',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\bulkSms\BulkSms::class => [
        'baseUrl' => getenv('SMS_BULKSMS_URL') . 'BulkSMSAPI/UnifunBulkSMSAPI.asmx/SendSMSNoneDigitsEncoded',
    ],

    \console\modules\sms\models\clickatell\Clickatell::class => [
        'baseUrl'           => getenv('SMS_CLICKATELL_URL'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\comtass\Comtass::class => [
        'baseUrl'           => getenv('SMS_COMTASS_URL') . 'dsms/webacc.aspx',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\gateway\GatewaySms::class => [
        'baseUrl' => getenv('SMS_GATEWAYSMS_URL') . 'mobile.php',
    ],

    \console\modules\sms\models\gosms\Gosms::class => [
        'baseUrl'           => getenv('SMS_GOSMS_URL'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\ibateleSmpp\IbateleSmppSms::class => [
        'sendServerAddress'                    => getenv('SMS_IBATELE_SMPP_URL') . 'xml',
        'requestSmsStateServerAddress'         => getenv('SMS_IBATELE_LK_URL') . 'xml/state.php',
        'requestBalanceServerAddress'          => getenv('SMS_IBATELE_SMPP_URL') . 'xml/balance.php',
        'requestSenderListServerAddress'       => getenv('SMS_IBATELE_LK_URL') . 'xml/originator.php',
        'requestIncomingMessagesServerAddress' => getenv('SMS_IBATELE_LK_URL') . 'xml/incoming.php',
        'requestInfoAboutPhone'                => getenv('SMS_IBATELE_LK_URL') . 'xml/def.php',
        'requestListOfBases'                   => getenv('SMS_IBATELE_LK_URL') . 'xml/list_bases.php',
        'requestBasesEditUpdate'               => getenv('SMS_IBATELE_LK_URL') . 'xml/bases.php',
        'requestGetClientBase'                 => getenv('SMS_IBATELE_LK_URL') . 'xml/list_phones.php',
        'connectionTimeout'                    => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'                              => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\magfa\Magfa::class => [
        'baseUrl' => getenv('SMS_MAGFA_URL') . 'magfaHttpService',
        'domain'  => 'magfa',
    ],

    \console\modules\sms\models\mediasend\Mediasend::class => [
        'baseUrl'           => getenv('SMS_MEDIASEND_URL') . 'api/sms/',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\megafonTjSmpp\MegafonTjSmpp::class => [
        'hosts' => [getenv('SMS_MEGAFON_TJ_SMPP_HOST')],
        'port'  => getenv('SMS_MEGAFON_TJ_SMPP_PORT'),
    ],

    \console\modules\sms\models\nikita\Nikita::class => [
        'baseUrl'           => getenv('SMS_NIKITA_URL') . 'broker',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\nikitaKg\NikitaKg::class => [
        'baseUrl'           => getenv('SMS_NIKITA_KG_URL'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\promoSms\PromoSms::class => [
        'baseUrl'           => getenv('SMS_PROMOSMS_URL') . 'smw/aisms',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\smsbroker\SmsBroker::class => [
        'baseUrl'           => getenv('SMS_SMSBROKER_URL') . 're-smsbroker',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\smsc\Smsc::class => [
        'baseUrl' => getenv('SMS_SMSC_URL') . 'sys/',
    ],

    \console\modules\sms\models\smsLine\SmsLine::class => [
        'baseUrl'           => getenv('SMS_SMSLINE_URL'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\smsOnline\SmsOnline::class => [
        'baseUrl'           => getenv('SMS_SMSONLINE_URL'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\EyelineSms::class => [
        'baseUrl'           => getenv('SMS_EYELINESMS_URL') . 'sads/push',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\IbateleSms::class => [
        'sendServerAddress'                    => getenv('SMS_IBATELE_LK_URL') . 'xml',
        'requestSmsStateServerAddress'         => getenv('SMS_IBATELE_LK_URL') . 'xml/state.php',
        'requestBalanceServerAddress'          => getenv('SMS_IBATELE_LK_URL') . 'xml/balance.php',
        'requestSenderListServerAddress'       => getenv('SMS_IBATELE_LK_URL') . 'xml/originator.php',
        'requestIncomingMessagesServerAddress' => getenv('SMS_IBATELE_LK_URL') . 'xml/incoming.php',
        'requestInfoAboutPhone'                => getenv('SMS_IBATELE_LK_URL') . 'xml/def.php',
        'requestListOfBases'                   => getenv('SMS_IBATELE_LK_URL') . 'xml/list_bases.php',
        'requestBasesEditUpdate'               => getenv('SMS_IBATELE_LK_URL') . 'xml/bases.php',
        'requestGetClientBase'                 => getenv('SMS_IBATELE_LK_URL') . 'xml/list_phones.php',
        'connectionTimeout'                    => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'                              => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\iqsms_JsonGate::class => [
        'host'              => getenv('SMS_IQSMS_HTTP_HOST'),
        'host_balance'      => getenv('SMS_IQSMS_BALANCE_HTTP_HOST'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\MsmSms::class => [
        'baseUrl'           => getenv('SMS_MSMSMS_URL'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\QtSms::class => [
        'hostname' => getenv('SMS_QTSMS_SSL_HOST'),
        'timeout'  => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\QtSmsKuban::class => [
        'hostname' => getenv('SMS_QTSMS_KUBAN_SSL_HOST'),
        'timeout'  => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\SapsanSms::class => [
        'sendServerAddress'           => getenv('SMS_SAPSANSMS_URL') . 'xml',
        'requestBalanceServerAddress' => getenv('SMS_SAPSANSMS_URL') . 'xml/balance.php',
        'connectionTimeout'           => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'                     => getenv('CURL_TIMEOUT'),
    ],

    \console\modules\sms\models\semysms\Semysms::class => [
        'baseUrl'           => getenv('SMS_SEMYSMS_NET_URL'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],
];