<?php

$db = require __DIR__ . '/db.php';
$params = require __DIR__ . '/params.php';

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/logVarsFilter.php'
);

$applicationId = 'gearman_workers';

$config = [
    'id'                  => 'gearman',
    'name'                => getenv('APPLICATION_NAME'),
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases'    => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'          => [
        // Yii2 components
        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],

        'i18n' => [
            'translations' => [
                '*'    => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
                'app*' => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                ],
            ],
        ],

        'log' => [
            'traceLevel'    => 3,
            'flushInterval' => 1,
            'targets'       => [
                'appLog'   => [
                    'class'          => 'console\components\log\CustomSyslogTarget',
                    'identity'       => "{$applicationId}-{$params['version']}",
                    'categories'     => ['appLog', 'notification'],
                    'logVars'        => [],
                    'exportInterval' => 1,
                ],
                'app'      => [
                    'class'          => 'yii\log\FileTarget',
                    'levels'         => ['error', 'warning', 'info'],
                    'except'         => ['appLog'],
                    'logVars'        => $logVars,
                    'exportInterval' => 1,
                ],
                'errors'   => [
                    'class'          => 'yii\log\FileTarget',
                    'logFile'        => '@runtime/logs/errors.log',
                    'levels'         => ['error', 'warning'],
                    'except'         => ['appLog'],
                    'logVars'        => $logVars,
                    'exportInterval' => 1,
                ],
                'email'    => [
                    'class'          => 'yii\log\EmailTarget',
                    'mailer'         => 'mailer_log',
                    'levels'         => ['error', 'warning'],
                    'except'         => ['appLog'],
                    'logVars'        => $logVars,
                    'message'        => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT'),
                    ],
                    'exportInterval' => 1,
                ],
                'bankCard' => [
                    'class'          => 'yii\log\FileTarget',
                    'categories'     => ['bank-card'],
                    'logFile'        => '@runtime/logs/bankCard.log',
                    'levels'         => ['error', 'warning'],
                    'except'         => ['appLog'],
                    'logVars'        => $logVars,
                    'exportInterval' => 1,
                ],
                'sentry' => [
                    'class'   => 'notamedia\sentry\SentryTarget',
                    'dsn'     => getenv('SENTRY_DSN'),
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'clientOptions' => [
                        'environment' => getenv('ENVIRONMENT_NAME'),
                    ],
                ],
            ],
        ],

        'mailer_log'               => [
            'class'     => 'yii\swiftmailer\Mailer',
            'viewPath'  => '@console/mail',
            'transport' => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],

        // Databases
        'db'                       => $db['dbMain'],
        'db_log'                   => $db['dbMain'],
        'mongodb'                  => $db['mongodbMain'],
        'redis_cache_check_status' => $db['redisCacheCheckStatus'],

        // Other components and services
        'curl'                     => [
            'class'   => 'console\components\curl\Curl',
            'options' => [
                'connectTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
                'timeout'        => getenv('CURL_TIMEOUT'),
            ],
        ],
        'paygateCurl'              => [
            'class'          => 'console\components\curl\RestCurl',
            'connectTimeout' => getenv('PAYGATE_CURL_CONNECT_TIMEOUT'),
            'timeout'        => getenv('PAYGATE_CURL_TIMEOUT'),
        ],

        'logger' => [
            'class'    => 'console\components\log\SysLog',
            'identity' => "{$applicationId}-{$params['version']}",
            'template' => "[billing] {params} {messages}\n",
        ],
    ],

    'modules' => [
        'sms' => 'console\modules\sms\Module',
    ],

    'container' => [
        'definitions' => require __DIR__ . '/smsDefinitions.php',
    ],

    'params' => $params,
];

if (true ) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['192.168.88.*' ],
    ];
}


return $config;