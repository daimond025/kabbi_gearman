<?php

use yii\helpers\ArrayHelper;

return ArrayHelper::merge(require __DIR__ . '/base.php', [
    'id'                  => 'gearman',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'components'          => [
        'errorHandler' => [
            'class' => 'console\components\ErrorHandler',
        ],

        'gearman' => [
            'class' => 'gearman\Worker',
            'host'  => getenv('GEARMAN_MAIN_HOST'),
            'port'  => getenv('GEARMAN_MAIN_PORT'),
        ],
    ],
]);