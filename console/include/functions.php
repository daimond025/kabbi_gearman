<?php
/**
 * functions.php file.
 * Global shorthand functions for commonly used Yii methods.
 */

/**
 * Returns the application instance.
 */
function app()
{
    return Yii::$app;
}

/**
 * Dumps the given variable using CVarDumper::dumpAsString().
 *
 * @param mixed $var
 * @param int   $depth
 * @param bool  $highlight
 */
function dump($var, $depth = 10, $highlight = true)
{
    return \yii\helpers\VarDumper::dump($var, $depth, $highlight);
}

/**
 * Debug function with die() after
 * dd($var);
 */
function dd($var)
{
    dump($var);
    die();
}

/**
 * Returns user component.
 */
function user()
{
    return app()->getUser()->identity;
}

function getUserName()
{
    $user = user();

    return is_object($user) ? $user->name : null;
}

function getUserLastName()
{
    $user = user();

    return is_object($user) ? $user->last_name : null;
}

/**
 * Returns post data
 */
function post($name = null, $defaultValue = null)
{
    return app()->request->post($name, $defaultValue);
}

/**
 * Returns get data
 */
function get($name = null, $defaultValue = null)
{
    return app()->request->get($name, $defaultValue);
}

/**
 * Yii translate
 */
function t($category, $message, $params = [], $language = null)
{
    return Yii::t($category, $message, $params, $language);
}


/**
 * @param string $message
 * @param array  $context
 *
 * @return string
 */
function applicationLog($message, array $context = [])
{
    $replace = [];
    foreach ($context as $key => $val) {
        if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
            $replace['{' . $key . '}'] = $val;
        }
    }

    \Yii::info(strtr($message, $replace), 'appLog');
}

function session()
{
    return app()->getSession();
}

function getMonth($monthNumber)
{
    $monthList = getMonthList();

    return getValue($monthList[$monthNumber]);
}

function getMonthList()
{
    $lang      = substr(app()->language, 0, 2);
    $monthList = [
        'ru' => [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ],
        'en' => [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'Decemer',
        ],
    ];

    return getValue($monthList[$lang], []);
}

/**
 * Преобразует строку вида "01.01" в строку "1 Января"
 *
 * @param string $data Формат "01.01"
 *
 * @return string
 */
function getFormatData($data)
{
    $arData = explode('.', $data);

    switch ($arData[1]) {
        case '01':
            $month = 'Января';
            break;
        case '02':
            $month = 'Февраля';
            break;
        case '03':
            $month = 'Марта';
            break;
        case '04':
            $month = 'Апреля';
            break;
        case '05':
            $month = 'Мая';
            break;
        case '06':
            $month = 'Июня';
            break;
        case '07':
            $month = 'Июля';
            break;
        case '08':
            $month = 'Августа';
            break;
        case '09':
            $month = 'Сентября';
            break;
        case '10':
            $month = 'Октября';
            break;
        case '11':
            $month = 'Ноября';
            break;
        case '12':
            $month = 'Декабря';
            break;
        default :
            $month = 'Января';
    }

    return intval($arData[0]) . ' ' . $month;
}

function getDayList($lang = 'ru')
{
    $monthList = [
        'ru' => [
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
            'Воскресенье',
        ],
        'en' => [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',
        ],
    ];

    return $monthList[$lang];
}

function getBirthDay()
{
    $arBirthDay = [];

    for ($i = 1; $i <= 31; $i++) {
        $day              = $i < 10 ? '0' . $i : $i;
        $arBirthDay[$day] = $day;
    }

    return $arBirthDay;
}

function getBirthYear($start = 15, $end = 80)
{
    $start_day = date("Y") - $start;
    $end_date  = date("Y") - $end;
    $arYear    = [];

    for ($i = $start_day; $i >= $end_date; $i--) {
        $arYear[$i] = $i;
    }

    return $arYear;
}

/**
 * Return flash with models error.
 *
 * @param array $arModels
 */
function displayErrors(array $arModels)
{
    $error_mes = '';

    foreach ($arModels as $model) {
        $errors = $model->getErrors();

        foreach ($errors as $error) {
            foreach ($error as $mes) {
                if (!empty($mes)) {
                    $error_mes .= "$mes<br/>";
                }
            }
        }
    }

    session()->setFlash('error', $error_mes);
}

function getFormatedDate($date)
{
    $date = new DateTime($date);

    return $date->format('d.m.Y');
}

function getFormatedTime($date)
{
    $date = new DateTime($date);

    return $date->format('H.i');
}

//Функция перевода секунд во время(по разрядам)
function parse_seconds($duration)
{
    $hours   = floor($duration / 3600);
    $minutes = ($duration / 60) % 60;
    $seconds = $duration % 60;

    if (intval($hours) < 10) {
        $hours = '0' . $hours;
    }
    if (intval($minutes) < 10) {
        $minutes = '0' . $minutes;
    }
    if (intval($seconds) < 10) {
        $seconds = '0' . $seconds;
    }

    return ['HOURS' => $hours, 'MINUTES' => $minutes, 'SECONDS' => $seconds];
}

function getValue($var, $return = null)
{
    return isset($var) ? $var : $return;
}

function mb_ucfirst($str)
{
    $fc = mb_strtoupper(mb_substr($str, 0, 1));

    return $fc . mb_substr($str, 1);
}

function getFullName($last_name, $name = '', $second_name = '')
{
    return trim($last_name . ' ' . $name . ' ' . $second_name);
}

function secondsToStr($seconds)
{
    $seconds = $seconds < 0 ? 0 : $seconds;
    $str     = '';

    $hours     = (int)($seconds / 3600);
    $hours_str = $hours < 10 ? '0' . $hours : $hours;
    if ($hours > 0) {
        $str .= $hours_str . ' ' . t('app', 'h.') . ' ';
    }

    $min     = $seconds / 60 % 60;
    $min_str = $min < 10 ? '0' . $min : $min;
    if ($min > 0 || !empty($str)) {
        $str .= $min_str . ' ' . t('app', 'min.') . ' ';
    }

    $sec     = $seconds % 60;
    $sec_str = $sec < 10 ? '0' . $sec : $sec;
    $str     .= $sec_str . ' ' . t('app', 'sec.');

    return $str;
}

/**
 * Склонение слов
 *
 * @param integer $num
 * @param array   $arWords
 *
 * @return string
 */
function declension_words($num, $arWords)
{
    $number = $num < 21 ? $num : (int)substr($num, -1);

    if ($number == 1) {
        $w = $arWords[0];
    } elseif ($number > 1 && $number < 5) {
        $w = $arWords[1];
    } else {
        $w = $arWords[2];
    }

    return $w;
}

// Generate a random character string
function rand_str($length = 25, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')
{
    // Length of character list
    $chars_length = (strlen($chars) - 1);

    // Start our string
    $string = $chars{rand(0, $chars_length)};

    // Generate random string
    for ($i = 1; $i < $length; $i = strlen($string)) {
        // Grab a random character from our list
        $r = $chars{rand(0, $chars_length)};

        // Make sure the same two characters don't appear next to each other
        if ($r != $string{$i - 1}) {
            $string .= $r;
        }
    }

    // Return the string
    return $string;
}

/**
 * Получение значения из кеша по ключу, либо создание кеша.
 *
 * @param string   $key
 * @param callback $callback
 *
 * @return mixed|false
 */
function getFromCache($key, $callback, $duration = 0, $dependency = null)
{
    // try retrieving $data from cache
    $cache = app()->cache;
    $data  = $cache->get($key);

    if ($data === false && is_callable($callback)) {
        // $data is not found in cache, calculate it from scratch
        $data = call_user_func($callback);
        // store $data in cache so that it can be retrieved next time
        $cache->set($key, $data, $duration, $dependency);
    }

    return $data;
}

function phoneFilter($phone)
{
    return preg_replace("/[^0-9]/", '', $phone);
}

function getCurrencyFormat($value, $symbol = null)
{
    $currency = Yii::$app->formatter->asCurrency($value, $symbol);

    return app()->language == 'ru-RU' ? str_replace('руб.', '₽', $currency) : $currency;
}

function getCurrencySymbol()
{
    $currency = explode('-', app()->language);
    $currency = end($currency);

    return getValue(app()->params['symbol'][$currency]);
}

function console_debug($data)
{
    print_r($data);
    die();
}