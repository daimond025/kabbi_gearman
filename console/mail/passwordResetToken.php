<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$template = 'Dear {name},<br>'
    . 'You recently have requested a password reset. Please use the link below to change your password.<br>'
    . '<a href="{url}">{url}</a><br>'
    . '{footer}';

$name   = ArrayHelper::getValue($data, 'NAME');
$url    = ArrayHelper::getValue($data, 'URL');
$lang   = ArrayHelper::getValue($data, 'LANGUAGE');
$footer = ArrayHelper::getValue($data, 'footer', '');

$text = t('email', $template, [
    'name'   => Html::encode($name),
    'url'    => $url,
    'footer' => $footer,
], $lang);

echo Html::tag('p', $text);