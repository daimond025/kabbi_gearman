<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$template = 'Dear {name},<br>'
    . 'Thank you for registering.<br>'
    . 'Below you will find your activation link that you can use to activate your account. Please click on the Activation Link {link}<br>'
    . 'Then, you will be able to log in and begin using your account.<br>'
    . 'Your password: {password}<br>';

$name     = ArrayHelper::getValue($data, 'NAME');
$url      = ArrayHelper::getValue($data, 'URL');
$lang     = ArrayHelper::getValue($data, 'LANGUAGE');
$password = ArrayHelper::getValue($data, 'PASSWORD', '');

$text = t('email', $template, [
    'name'     => Html::encode($name),
    'link'     => Html::a($url, Html::encode($url)),
    'password' => $password,
], $lang);

echo Html::tag('p', $text);