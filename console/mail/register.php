<?php

use yii\helpers\Html;

$this->params['contact-phone'] = $data['PHONE'];
$this->params['contact-email'] = $data['EMAIL'];
$this->params['language']      = $data['LANGUAGE'];

$email    = $data['EMAIL'];
$phone    = $data['PHONE'];
$user     = $data['USER'];
$url      = $data['URL'];
$login    = $data['LOGIN'];
$password = $data['PASSWORD'];

?>

<!-- START TITLE + TEXT -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="title+text">
    <tr>
        <td width="100%" valign="top" align="center">
            <!-- Start Wrapper -->
            <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper"
                   bgcolor="#daf6ef">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#daf6ef">
                        <!-- Start Container -->
                        <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="center" colspan="3" class="mobile"
                                    style="font-family:arial, sans-serif; font-size:20px; line-height:26px; font-weight:bold;"
                                    st-title="title+text">
                                    Здравствуйте, <?= Html::encode($user) ?>!
                                </td>
                            </tr>
                            <tr>
                                <td height="20" colspan="3" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left" colspan="3"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 15px; color: #4d4d4d; line-height:18px; padding:0 20px;"
                                    st-content="title+text">
                                    Спасибо за регистрацию в Гутаксе! Сохраните ваши данные для входа.
                                </td>
                            </tr>
                            <tr>
                                <td height="25" colspan="3" style="line-height:25px; font-size:25px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 20px;"
                                    st-content="title+text">
                                    <b>Ссылка на вашу систему:</b><br/><br/>
                                    <a href="<?= $url ?>" target="_blank" alias=""
                                       style="font-family:arial, sans-serif; color: #1867a3;">
                                        <?= Html::encode($url); ?>
                                    </a>
                                </td>
                                <td align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 20px;"
                                    st-content="title+text">
                                    <b>Логин:</b><br/><br/>
                                    <?= Html::encode($login) ?>
                                </td>
                                <td align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px; padding:0 20px;"
                                    st-content="title+text">
                                    <b>Пароль:</b><br/><br/>
                                    <?= Html::encode($password) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="30" colspan="3" style="line-height:30px; font-size:30px;"></td>
                                <!-- Spacer -->
                            </tr>
                        </table>
                        <!-- End Container -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End Wrapper -->
        </td>
    </tr>
</table>
<!-- END TITLE + TEXT -->
<!-- START 1 IMAGE + TEXT COLUMN -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="1-image+text-column">
    <tr>
        <td width="100%" valign="top" align="center">
            <!-- Start Wrapper -->
            <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper"
                   bgcolor="#ffffff">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#ffffff">
                        <!-- Start Container -->
                        <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td width="100%" colspan="2" align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px;padding:0 20px;">
                                    <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0"
                                           bgcolor="#ffffff">
                                        <tr>
                                            <td width="100%" valign="top" style="border: 1px dashed #dddddd;">
                                                <table width="100%" cellpadding="0" cellspacing="0" align="left"
                                                       border="0" class="container">
                                                    <tr>
                                                        <td colspan="2" align="left"
                                                            style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:16px;">
                                                            <table width="500" cellpadding="0" cellspacing="0" align="left"
                                                                   border="0" class="container">
                                                                <tr>
                                                                    <td colspan="2" height="20"
                                                                        style="line-height:20px; font-size:20px;"></td>
                                                                    <!-- Spacer -->
                                                                </tr>
                                                                <tr>
                                                                    <td width="65" valign="middle" align="center">
                                                                        <a href="https://www.youtube.com/watch?v=glpYw0pXIa8"
                                                                           target="_blank" style="text-decoration: none;"><img
                                                                                src="http://foto.gootax.ru/img/youtube-icon.jpg" width="40"
                                                                                height="28"
                                                                                style="margin:0; padding:0; border:none; display:block;"
                                                                                border="0" class="centerClass" alt=""
                                                                                st-image="image"/></a>
                                                                    </td>
                                                                    <td width="400"
                                                                        style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:16px;"
                                                                        st-content="title+text">
                                                                        <a href="https://www.youtube.com/watch?v=glpYw0pXIa8"
                                                                           target="_blank" alias=""
                                                                           style="font-family:Verdana, arial, sans-serif; color: #1867a3;">Видео с первоначальными настройками</a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="20"
                                                            style="line-height:20px; font-size:20px;"></td>
                                                        <!-- Spacer -->
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td width="100%" colspan="2" align="center"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px;padding:0 20px;">
                                    <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0"
                                           bgcolor="#ffffff">
                                        <tr>
                                            <td width="100%" valign="top" style="border: 1px dashed #dddddd;">
                                                <table width="100%" cellpadding="0" cellspacing="0" align="center"
                                                       border="0" class="container">
                                                    <tr>
                                                        <td colspan="2" height="20"
                                                            style="line-height:20px; font-size:20px;"></td>
                                                        <!-- Spacer -->
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="center"
                                                            style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:16px;">
                                                            <b>Нужна помошь с настройкой? </b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="20"
                                                            style="line-height:20px; font-size:20px;"></td>
                                                        <!-- Spacer -->
                                                    </tr>
                                                    <tr>
                                                        <td width="50%" align="center"
                                                            style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:16px;">
                                                            <?= Html::encode($phone) ?>
                                                        </td>
                                                        <td width="50%" align="center"
                                                            style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:16px;">
                                                            <a href="mailto:<?= Html::encode($email) ?> "
                                                               target="_blank" alias=""
                                                               style="font-family:Verdana, arial, sans-serif; color: #1867a3;"><?= Html::encode($email) ?></a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="20"
                                                            style="line-height:20px; font-size:20px;"></td>
                                                        <!-- Spacer -->
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img src="http://foto.gootax.ru/img/splash.jpg" width="640" height="260"
                                         style="margin:0; padding:0; border:none; display:block;" border="0"
                                         class="centerClass" alt="" st-image="image"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td colspan="2" align="center" class="mobile"
                                    style="font-family:Verdana, arial, sans-serif; font-size:16px; line-height:20px; color: #000000; padding: 0 20px;"
                                    st-title="title+text">
                                    Каждый руководитель бизнеса такси замечает сильные
                                    изменения, происходящие в этой сфере. Мировые сервисы заказа такси Убер, Гетт,
                                    Яндекс.Такси вывели рынок пассажирских перевозок на новый качественный уровень. Для
                                    клиента это преимущества: стоимость стала ниже, а удобств больше. Службам такси,
                                    наоборот, стало тяжелей бороться за клиентов.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td width="327" align="center"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px;">
                                    <table width="290" cellpadding="0" cellspacing="0" align="center" border="0"
                                           bgcolor="#ffffff">
                                        <tr>
                                            <td width="100%" valign="top" style="border: 2px solid #2cbc92;">
                                                <table width="288" height="220" valign="top" cellpadding="0"
                                                       cellspacing="0" align="center" border="0" class="container">
                                                    <tr>
                                                        <td valign="top" height="25"
                                                            style="line-height:25px; font-size:25px;"></td>
                                                        <!-- Spacer -->
                                                    </tr>
                                                    <tr>
                                                        <td height="60" valign="top" width="100%"
                                                            style="font-family:Verdana, Arial, sans serif; font-size: 18px; color: #000000; line-height:24px; padding:0 20px;"
                                                            st-content="title+text">
                                                            <b>Руководителю службы такси важно</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="100%"
                                                            style="font-family:Verdana, Arial, sans serif; font-size: 14px; color: #000000; line-height:16px; padding:0 20px;"
                                                            st-content="title+text">
                                                            чтобы бизнес развивался, компания была конкурентной
                                                            и прибыльной, а процессы автоматизированы и прозрачны.
                                                            Стоимость и сроки внедрения не должны быть большими.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" height="25"
                                                            style="line-height:25px; font-size:25px;"></td>
                                                        <!-- Spacer -->
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="308" align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #000000; line-height:14px;">
                                    <table width="290" cellpadding="0" cellspacing="0" align="left" border="0"
                                           bgcolor="#ffffff">
                                        <tr>
                                            <td width="100%" valign="top" style="border: 2px solid #2cbc92;">
                                                <table width="288" height="220" valign="top" cellpadding="0"
                                                       cellspacing="0" align="center" border="0" class="container">
                                                    <tr>
                                                        <td valign="top" height="25"
                                                            style="line-height:25px; font-size:25px;"></td>
                                                        <!-- Spacer -->
                                                    </tr>
                                                    <tr>
                                                        <td height="35" valign="top" width="100%"
                                                            style="font-family:Verdana, Arial, sans serif; font-size: 18px; color: #000000; line-height:24px; padding:0 20px;"
                                                            st-content="title+text">
                                                            <b>Диспетчеру важно</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="100%"
                                                            style="font-family:Verdana, Arial, sans serif; font-size: 14px; color: #000000; line-height:16px; padding:0 20px;"
                                                            st-content="title+text">
                                                            чтобы программа была удобна
                                                            в использовании, не зависела
                                                            от внешних факторов (интернет, сервер, системный
                                                            администратор), рутинные процессы автоматизированы.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" height="25"
                                                            style="line-height:25px; font-size:25px;"></td>
                                                        <!-- Spacer -->
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="50" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                        </table>
                        <!-- End Container -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End Wrapper -->
        </td>
    </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="1-image+text-column">
    <tr>
        <td width="100%" valign="top" align="center">
            <!-- Start Wrapper -->
            <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper"
                   bgcolor="#ffffff">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#ffffff">
                        <!-- Start Container -->
                        <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                            <tr>
                                <td colspan="2" height="15" style="line-height:15px; font-size:15px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" colspan="2" width="100%" align="center"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 24px; color: #000000; line-height:24px; padding:0 20px;"
                                    st-content="title+text">
                                    <b>Преимущества Гутакса </b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="10" style="line-height:10px; font-size:10px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td colspan="2" height="30" style="line-height:30px; font-size:30px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>1.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    Современное и удобное мобильное приложение для заказа такси всего за 5 000 ₽ в месяц
                                    (iOS и Android). Выпускается индивидуально для вашей службы. Оно оформляется в
                                    соответствии с вашим фирменным стилем. Ваши клиенты оценят удобство заказа в 1 клик.
                                    А благодаря обновлениям, приложение будет оставаться в тренде и работатьна разных
                                    версиях мобильных систем;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>2.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    Оплата поездки <b>банковской картой</b> через мобильное приложение. Это самый
                                    удобный и быстрый способ оплаты на сегодняшний день;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>3.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    Возможности для быстрого масштабирования компании.<br/><b>Открывайте филиалы</b> в
                                    разных городах и странах, а управляйте
                                    из браузера своего компьютера через один аккаунт системы;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>4.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    Возможность оказывать одновременно <b>несколько услуг</b>: такси, курьеры,
                                    грузоперевозки, сантехник, ремонт окон и другие; <sup style="color: #959595;"><font
                                            color="#959595">Скоро</font></sup>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>5.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    <b>Быстрый старт</b>: посмотреть программу или начать работу можно сразу после
                                    регистрации;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>6.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    <b>Бесплатная установка</b> сервиса в течение нескольких рабочих дней;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>7.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    <b>Полная автоматизация</b> вашей службы такси: принятие заказов, распределение
                                    между водителями, отчёты о работе компании
                                    и многое другое теперь в одном месте;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>8.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    Внутренний и внешний <b>обмен заказами</b> с другими службами. Продавайте заказы,
                                    если не можете их выполнить или покупайте при небольшом загрузке; <sup
                                        style="color: #959595;"><font color="#959595">Скоро</font></sup>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>9.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    <b>Гарантия безопасности</b> и надёжности облачного хранения информации;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td valign="top" width="60" align="center"
                                    style="font-family:arial, sans-serif; text-align: center; font-size:24px; color: #2cbc92; line-height:24px; padding:0; width: 60px;">
                                    <font color="#2cbc92"><b>10.</b></font>
                                </td>
                                <td valign="top" width="580" align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:22px; padding:0 20px 0 0; width: 580px;">
                                    <b>Экономия средств</b> за счёт облачной организации серверов: нет вложений в
                                    программу, не нужно покупать собственный сервер,
                                    не нужно держать системного администратора в штате.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="45" style="line-height:45px; font-size:45px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td colspan="2" width="100%">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center"
                                           bgcolor="#ededed" st-sortable="content">
                                        <tr>
                                            <td colspan="3" height="40" style="line-height:40px; font-size:40px;"></td>
                                            <!-- Spacer -->
                                        </tr>
                                        <tr>
                                            <td valign="top" width="33%" align="center"
                                                style="font-family:Verdana, arial, sans-serif; font-size:16px; line-height:20px; padding:0 20px;">
                                                <img src="http://foto.gootax.ru/img/reg_icon1.png" width="48"
                                                     height="48" style="margin:0; padding:0; border:none;" border="0"
                                                     class="centerClass" alt="" st-image="image"/><br/><br/>Получайте
                                                больше заказов!
                                            </td>
                                            <td valign="top" width="33%" align="center"
                                                style="font-family:Verdana, arial, sans-serif; font-size:16px; line-height:20px; padding:0 20px;">
                                                <img src="http://foto.gootax.ru/img/reg_icon2.png" width="48"
                                                     height="48" style="margin:0; padding:0; border:none;" border="0"
                                                     class="centerClass" alt="" st-image="image"/><br/><br/>Оптимизируйте
                                                расходы!
                                            </td>
                                            <td valign="top" width="33%" align="center"
                                                style="font-family:Verdana, arial, sans-serif; font-size:16px; line-height:20px; padding:0 20px;">
                                                <img src="http://foto.gootax.ru/img/reg_icon3.png" width="48"
                                                     height="48" style="margin:0; padding:0; border:none;" border="0"
                                                     class="centerClass" alt="" st-image="image"/><br/><br/>Увеличивайте
                                                прибыль!
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="40" style="line-height:40px; font-size:40px;"></td>
                                            <!-- Spacer -->
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="55" style="line-height:55px; font-size:55px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <td colspan="2" width="100%" align="center"
                                style="font-family:Verdana, arial, sans-serif; font-size:20px; line-height:22px; padding:0 20px; color: #000000;">
                                <b>Создайте успешный бизнес с помощью Гутакса!</b>
                            </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="40" style="line-height:40px; font-size:40px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;" colspan="2">
                                    <hr style="border-style: solid; border-width: 1px 0 0 0; border-color: #cccccc;"/>
                                </td>
                                <!-- Spacer -->
                            </tr>
                        </table>
                        <!-- End Container -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End Wrapper -->
        </td>
    </tr>
</table>
<!-- END 1 IMAGE + TEXT COLUMN -->