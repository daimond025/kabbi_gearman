<?php

use yii\helpers\Html;

$this->params['contact-phone'] = $data['PHONE'];
$this->params['contact-email'] = $data['EMAIL'];
$this->params['language']      = $data['LANGUAGE'];

$name    = empty($data['NAME']) || $data['NAME'] === '-' ? null : $data['NAME'];
$url     = $data['URL'];
$message = isset($data['MESSAGE']) ? $data['MESSAGE'] : '';

?>

<!-- START TITLE + TEXT -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="title+text">
   <tr>
      <td width="100%" valign="top" align="center">
         <!-- Start Wrapper -->
         <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper" bgcolor="#daf6ef">
            <tbody>
               <tr>
                  <td align="center" bgcolor="#daf6ef">
                     <!-- Start Container -->
                     <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                        <tr>
                           <td height="20" style="line-height:20px; font-size:20px;"> </td>
                           <!-- Spacer -->
                        </tr>
                        <tr>
                           <td align="center" class="mobile" style="font-family:arial, sans-serif; font-size:20px; line-height:26px; font-weight:bold;" st-title="title+text">
                              Здравствуйте<?= !empty($name) ? ", $name!" : '!' ?>
                           </td>
                        </tr>
                        <tr>
                           <td height="20" style="line-height:20px; font-size:20px;"> </td>
                           <!-- Spacer -->
                        </tr>
                        <tr>
                           <td align="center" style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #4d4d4d; line-height:24px; padding:0 20px;" st-content="title+text">
                               <?= $message ?>
                           </td>
                        </tr>
                        <tr>
                           <td height="20" style="line-height:20px; font-size:20px;"> </td>
                           <!-- Spacer -->
                        </tr>
                     </table>
                     <!-- End Container -->
                  </td>
               </tr>
            </tbody>
         </table>
         <!-- End Wrapper -->
      </td>
   </tr>
</table>
<!-- END TITLE + TEXT -->
<!-- START 1 IMAGE + TEXT COLUMN -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="1-image+text-column">
   <tr>
      <td width="100%" valign="top" align="center">
         <!-- Start Wrapper -->
         <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper" bgcolor="#ffffff">
            <tbody>
               <tr>
                  <td align="center" bgcolor="#ffffff">
                     <!-- Start Container -->
                     <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                        <tr>
                           <td height="30" style="line-height:30px; font-size:30px;"> </td>
                           <!-- Spacer -->
                        </tr>



                         <tr>
                          <td align="left" class="mobile" style="font-family:Verdana, arial, sans-serif; font-size:16px; line-height:30px; color: #000000; padding: 0 30px;" st-title="title+text">
                                Не забудьте зарегистрироваться в Гутаксе! Вы познакомитесь с интерфейсом системы, а мы вышлем инструкции для старта работы. Так вы быстрее разберетесь с бизнесом такси и принципами работы программы для такси.
                          </td>
                       </tr>
                         <tr>
                           <td height="25" style="line-height:25px; font-size:25px;"> </td>
                           <!-- Spacer -->
                        </tr>
                         <tr>
                         <td class="mobile" style="font-size:14px; line-height:20px;" align="center">
                              <!-- Start Button -->
                              <table width="300" cellpadding="0" cellspacing="0" align="center" border="0" bgcolor="#2cbc92" st-button="2-images+text-columns">
                                 <tr>
                                    <td width="300" height="50" align="center" valign="middle" style="font-family:Verdana, arial, sans-serif; font-size: 18px; color: #ffffff; line-height:22px; border-radius:3px;" st-content="2-images+text-columns">
                                       <a href="<?= $url ?>" target="_blank" alias="" style="font-family:arial, sans-serif; text-decoration: none; color: #ffffff;"><b>Зарегистрироваться сейчас</b></a>
                                    </td>
                                 </tr>
                              </table>
                              <!-- End Button -->
                           </td>

                             </tr>
                         <tr>
                           <td height="30" style="line-height:30px; font-size:30px;"> </td>
                           <!-- Spacer -->
                        </tr>
                         <tr>
                           <td height="20" style="line-height:20px; font-size:20px;" colspan="2">
                              <hr style="border-style: solid; border-width: 1px 0 0 0; border-color: #cccccc;"/>
                           </td>
                           <!-- Spacer -->
                        </tr>
                     </table>
                     <!-- End Container -->
                  </td>
               </tr>
            </tbody>
         </table>
         <!-- End Wrapper -->
      </td>
   </tr>
</table>
 <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="1-image+text-column">
   <tr>
      <td width="100%" valign="top" align="center">
         <!-- Start Wrapper -->
         <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper" bgcolor="#ffffff">
            <tbody>
               <tr>
                  <td align="center" bgcolor="#ffffff">
                     <!-- Start Container -->
                     <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                        <tr>
                           <td colspan="2" height="15" style="line-height:15px; font-size:15px;"> </td>
                           <!-- Spacer -->
                        </tr>
                         <tr>
                           <td align="center" style="font-family:Verdana, Arial, sans serif; font-size: 20px; color: #000; line-height:24px; padding:0 20px; font-weight: bold;" st-content="title+text">
                              <b>Успехов в создании бизнеса такси!</b>
                           </td>
                        </tr>

                        <tr>
                           <td height="20" style="line-height:20px; font-size:20px;"> </td>
                           <!-- Spacer -->
                        </tr>
                        <tr>
                           <td height="20" style="line-height:20px; font-size:20px;" colspan="2">
                              <hr style="border-style: solid; border-width: 1px 0 0 0; border-color: #cccccc;"/>
                           </td>
                           <!-- Spacer -->
                        </tr>
                     </table>
                     <!-- End Container -->
                  </td>
               </tr>
            </tbody>
         </table>
         <!-- End Wrapper -->
      </td>
   </tr>
</table>
<!-- END 1 IMAGE + TEXT COLUMN -->