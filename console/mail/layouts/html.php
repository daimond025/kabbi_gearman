<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td>
                <div style="padding: 20px 10px 20px 6%">
                <font face="tahoma,sans-serif" color="#000000" size="2">
                    <?= $content ?>
                </font>
                    <div style="border-top: 1px #e7e7e7 solid; padding: 20px 0; margin-top: 20px;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td><font face="tahoma,sans-serif" color="#a8a4a3" size="2"><?=t('email', 'Our site is ')?> <?= \Yii::$app->params['siteDomain'] ?> <br/><?=t('email', 'You have questions? Please contact us:')?> <?= \Yii::$app->params['mail.infoEmail'] ?></font></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
