<?php

use yii\helpers\Html;
?>

<p><?= t('email', 'Hello', NULL, $data['LANGUAGE']) ?>!</p>

<p><?= t('email', 'You have a new unread ticket!', NULL, $data['LANGUAGE']) ?></p>

<p><?= t('email', 'From user', NULL, $data['LANGUAGE']) ?>: <?= $data['USER_INFO']['name'] ?>, <?= t('email', 'domain', NULL, $data['LANGUAGE']) ?>: <?= $data['USER_INFO']['domain'] ?></p>

<p style="white-space: pre-wrap;"><?= $data['TEXT'] ?></p>

<p><?= t('email', 'Please, ', NULL, $data['LANGUAGE']) ?> <a href="http://<?= $data['LINK'] ?>"><?= t('email', 'click here ', NULL, $data['LANGUAGE']) ?></a>  <?= t('email', 'to read and give an answer', NULL, $data['LANGUAGE']) ?></p>
