<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
?>

<p><?=t('email', 'Hello', NULL, $data['LANGUAGE'])?>, <?= Html::encode($data['USERNAME']) ?>!</p>

<p><?=t('email', 'Your ticket', NULL, $data['LANGUAGE'])?><?= Html::encode($data['TICKETID']) ?> <?=t('email', 'received a new answer', NULL, $data['LANGUAGE'])?></p>

<p><?=$data['ANSWER']?></p>
