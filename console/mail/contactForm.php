<?php
/* @var $this yii\web\View */
?>
 
<p><?=t('email', 'Hello', NULL, $data['LANGUAGE'])?>!</p>
 
<p><?=t('email', 'Data from feedback form', NULL, $data['LANGUAGE'])?>:</p>
 
<p><?=t('email', 'Name', NULL, $data['LANGUAGE'])?>: <?=$data['NAME']?></p>
<p><?=t('email', 'Phone number', NULL, $data['LANGUAGE'])?>: <?=$data['PHONE']?></p>
<p>Email: <?=$data['EMAIL']?></p>
<p><?=t('email', 'Message', NULL, $data['LANGUAGE'])?>: <?=$data['MESSAGE']?></p>