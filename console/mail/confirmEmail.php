<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
?>
<p><?= t('email', 'Hello', null, $data['LANGUAGE']) ?>, <?= Html::encode($data['NAME']) ?>!</p>
<p><?= t('email', 'Thank you for your registration in Gootax', null, $data['LANGUAGE']) ?>.</p>
<p><?= t('email', 'Your gootax', null, $data['LANGUAGE']) ?> - <?= Html::a($data['URL'], $data['URL']) ?></p>
<p><?= t('email', 'Login', null, $data['LANGUAGE']) ?>: <?= $data['LOGIN'] ?></p>
<p><?= t('email', 'Password', null, $data['LANGUAGE']) ?>: <?= $data['PASSWORD'] ?></p>
<p><?= t('email', 'How to work with Gootax', null, $data['LANGUAGE']) ?>
    - <?= app()->params['mail.gootaxInstructionUrl'] ?></p>
<p><?= t('email', 'You have questions? Please contact us:', null,
        $data['LANGUAGE']) ?> <?= app()->params['mail.adminEmail'] ?></p>
<p><?= t('email', 'Enjoy!', null, $data['LANGUAGE']) ?></p>