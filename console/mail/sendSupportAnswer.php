<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;

/* @var $this \yii\web\View */

$this->params['contact-phone'] = $data['PHONE'];
$this->params['contact-email'] = $data['EMAIL'];
$this->params['language']      = $data['LANGUAGE'];

$user        = $data['USER'];
$ticketUrl   = $data['TICKET_URL'];
$ticketId    = $data['TICKET_ID'];
$title       = $data['TITLE'];
$answer      = $data['ANSWER'];
$language    = $data['LANGUAGE'];
$supportUser = $data['SUPPORT_USER'];
$answerTime  = $data['ANSWER_TIME'];

?>

<!-- START TITLE + TEXT -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center"
       st-sortable="title+text">
    <tr>
        <td width="100%" valign="top" align="center">
            <!-- Start Wrapper -->
            <table width="640" cellpadding="0" cellspacing="0" align="center" border="0"
                   class="wrapper" bgcolor="#daf6ef">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#daf6ef">
                        <!-- Start Container -->
                        <table width="640" cellpadding="0" cellspacing="0" align="center"
                               border="0" class="container">
                            <tr>
                                <td height="20"
                                    style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="center" class="mobile"
                                    style="font-family:arial, sans-serif; font-size:20px; line-height:26px; font-weight:bold;"
                                    st-title="title+text">
                                    <?= Html::encode(Yii::t('email', 'Hello {user}!', ['user' => $user], $language)) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="20"
                                    style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #4d4d4d; line-height:18px; padding:0 20px;"
                                    st-content="title+text">
                                    <?= Html::encode(Yii::t('email',
                                        'Reply for ticket #{ticketId} was received from Gootax technical support', [
                                            'ticketId' => $ticketId,
                                        ], $language)) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="20"
                                    style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                        </table>
                        <!-- End Container -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End Wrapper -->
        </td>
    </tr>
</table>
<!-- END TITLE + TEXT -->

<!-- START 1 IMAGE + TEXT COLUMN -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="1-image+text-column">
    <tr>
        <td width="100%" valign="top" align="center">
            <!-- Start Wrapper -->
            <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="wrapper"
                   bgcolor="#ffffff">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#ffffff">
                        <!-- Start Container -->
                        <table width="640" cellpadding="0" cellspacing="0" align="center" border="0" class="container">
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left"
                                    style="font-family:arial, sans-serif; font-size:16px; line-height:20px; padding:0 25px;"
                                    st-title="1-image+text-column">
                                    <a href="<?= $ticketUrl ?>" target="_blank" alias=""
                                       style="font-family:arial, sans-serif; color: #1867a3;"><b><?= Html::encode($title) ?></b></a>
                                </td>
                            </tr>
                            <tr>
                                <td height="15" style="line-height:15px; font-size:15px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left"
                                    style="font-family:Verdana, Arial, sans serif; font-size: 16px; color: #4d4d4d; line-height:18px; padding:0 20px;"
                                    st-content="1-image+text-column">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center"
                                           st-sortable="footer">
                                        <tr>
                                            <td width="100%" valign="top" cellpadding="0" cellspacing="0" border="0"
                                                bgcolor="#f9f7f5">
                                                <!-- Start Wrapper  -->
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left"
                                                            style="font-family:arial, sans-serif; font-size:16px; line-height:20px; padding: 5px; background: #f9f7f5;"
                                                            bgcolor="#f9f7f5"><?= HTMLPurifier::process($answer) ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- End Wrapper  -->
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="10" style="line-height:10px; font-size:10px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td align="left"
                                    style="font-family:arial, sans-serif; font-size:12px; line-height:14px; padding:0 25px;"
                                    st-title="1-image+text-column">
                                    <?= Html::encode($supportUser) ?>, <?= Html::encode($answerTime) ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td class="mobile" style="font-size:14px; line-height:20px;" align="center">
                                    <!-- Start Button -->
                                    <table width="190" cellpadding="0" cellspacing="0" align="center" border="0"
                                           bgcolor="#2cbc92" st-button="2-images+text-columns">
                                        <tr>
                                            <td width="190" height="40" align="center" valign="middle"
                                                style="font-family:arial, sans-serif; font-size: 20px; color: #ffffff; line-height:22px; border-radius:3px;"
                                                st-content="2-images+text-columns">
                                                <a href="<?= $ticketUrl ?>" target="_blank" alias=""
                                                   style="font-family:arial, sans-serif; text-decoration: none; color: #ffffff;">
                                                    <?= Html::encode(Yii::t('email', 'Go to the answer', [],
                                                        $language)) ?>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- End Button -->
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;"></td>
                                <!-- Spacer -->
                            </tr>
                            <tr>
                                <td height="20" style="line-height:20px; font-size:20px;">
                                    <hr style="border-style: solid; border-width: 1px 0 0 0; border-color: #cccccc;"/>
                                </td>
                                <!-- Spacer -->
                            </tr>
                        </table>
                        <!-- End Container -->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- End Wrapper -->
        </td>
    </tr>
</table>
<!-- END 1 IMAGE + TEXT COLUMN -->